---
aliases:
- /server/confluence/starting-a-confluence-cluster-on-a-single-machine-26936758.html
- /server/confluence/starting-a-confluence-cluster-on-a-single-machine-26936758.md
category: devguide
confluence_id: 26936758
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=26936758
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=26936758
date: '2017-12-08'
legacy_title: Starting a Confluence cluster on a single machine
platform: server
product: confluence
subcategory: faq
title: Starting a Confluence cluster on a single machine
---
# Starting a Confluence cluster on a single machine

As part of the work to ensure your plugin is cluster compatible you will need to functionally test it on a running Confluence cluster. One way to do this is to configure a 2 node cluster on your development machine.

### 1. Download and install the clustered artifact

1.  From the <a href="https://www.atlassian.com/software/confluence/download" class="external-link">download site</a>, download one of the 'cluster' installations (you can also find <a href="https://www.atlassian.com/software/confluence/download-eap" class="external-link">EAP versions</a>)
2.  Follow the<a href="https://confluence.atlassian.com/display/DOC/Confluence+Cluster+Installation" class="external-link">Confluence Cluster Installation</a> instructions to install Confluence. When you select an interface for the cluster the loopback interface (lo1) is typically a reasonable choice. When you select a shared home directory (new in Confluence 5.5-m19), a local directory can be used.
3.  At this point you will have a single instance running and should have three relevant directories: 
    -   a directory containing the distribution, which we will refer to as 'confluence-1' 
    -   a home directory which we will refer to as 'home-1'.
    -   new in Confluence 5.5-m19: A shared home directory which we will refer to as 'shared-1'.
4.  Stop the server.

When setting up Confluence you can use the following clustered evaluation license. 

This license is provided for the sole purposes of testing your plugin with a Confluence cluster. 

``` bash
AAABCQ0ODAoPeNpdUNFOg0AQfO9XkPhMc2Bs0yaXSMsl2iAQqa2v67noJbA0dwexf+/Joal9nZmd2
ZmbI74HO6AgZgFbrdntOl4GiUj3DogWM9lRPd92ZEFa8QSq4R3hAA0oQroH24AxCmguu9ZrnU4Ny
K3u0QMlaEuoc2iReyQFC1ski/pClimJZHB/PuEoTcVBZEUpnj1d6A8gZcCqjnjyG+s54f7pPVNDY
yZD96yLICCJ4uuk9NnFIi+X8cOsQj2gfkz5plrchYd8JcJqc0zC12qX+eO8b99QF/WLQW14xBiby
vRafoLB0etnoJBFYcz+VbhOu5xwrFY0jQryvxUngcaxwrXzN27wjQkwLQIUe9x9vLLNd9fUle/0U
O/G3mT4j6UCFQCDPOxJITv6OhbghOtduVBMsvVfGg==X02dl
```

This license is a 1000 user developer license and will expire 72 hours after its initial activation.

### 2. Create the second node

1.  Copy 'confluence-1' and 'home-1', and name them 'confluence-2' and 'home-2'.
2.  Set the home directory for the second node to 'home-2': 
    1.  Edit `confluence-2/confluence/WEB-INF/classes/confluence-init.properties`
    2.  Alter the `confluence.home` setting to point to the 'home-2' directory.
3.  Alter the Tomcat server for 'confluence-2' to use different ports from 'confluence-1':
    1.  Edit `confluence-2/conf/server.xml`
    2.  The file starts:

        ``` xml
        <Server port="8000" shutdown="SHUTDOWN" debug="0">
            <Service name="Tomcat-Standalone">
                <Connector className="org.apache.coyote.tomcat4.CoyoteConnector" port="8090" minProcessors="5"
                           maxProcessors="75"
                           enableLookups="false" redirectPort="8443" acceptCount="10" debug="0" connectionTimeout="20000"
                           useURIValidationHack="false" URIEncoding="UTF-8"/>
        ```

        alter the ports and redirectPort attributes so it reads:

        ``` xml
        <Server port="9000" shutdown="SHUTDOWN" debug="0">
            <Service name="Tomcat-Standalone">
                <Connector className="org.apache.coyote.tomcat4.CoyoteConnector" port="9090" minProcessors="5"
                           maxProcessors="75"
                           enableLookups="false" redirectPort="9443" acceptCount="10" debug="0" connectionTimeout="20000"
                           useURIValidationHack="false" URIEncoding="UTF-8"/>
        ```

        (in this example the 8's have been changed to 9's).

4.  The second server also needs to be configured with a different Hazelcast port from the first. This is done via a <a href="https://confluence.atlassian.com/display/DOC/Configuring+System+Properties" class="external-link">system property</a>:

``` bash
-Dconfluence.cluster.hazelcast.listenPort=5851
```

## Start the cluster

{{% note %}}

On some operating systems there can be trouble with the Coherence multicast traffic and IPv6 support. To work around this you need to start each server with the <a href="https://confluence.atlassian.com/display/DOC/Configuring+System+Properties" class="external-link">system property</a>: 

`-Djava.net.preferIPv4Stack=true`

{{% /note %}}

To start the cluster:

1.  1.  Start the first server and wait for start up to complete. This is signified by the message in the log `in it Confluence is ready to serve`.
    2.  Start the second server.
    3.  On either node, go to **General Configuration** &gt; **Clustering** to check your cluster is running.  You should see both nodes reported, similar to this:

<img src="/server/confluence/images/image2016-7-20-19:35:1.png" width="700" />

