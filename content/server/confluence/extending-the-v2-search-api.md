---
aliases:
- /server/confluence/extending-the-v2-search-api-2031818.html
- /server/confluence/extending-the-v2-search-api-2031818.md
category: devguide
confluence_id: 2031818
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031818
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031818
date: '2017-12-08'
guides: guides
legacy_title: Extending the V2 search API
platform: server
product: confluence
subcategory: learning
title: Extending the v2 search API
---
# Extending the v2 search API

If none of the bundled `SearchQuery`, `SearchSort`, `SearchFilter` or `ResultFilter` implementations fits your requirements, you can write your own implementation.

## Writing your Own SearchQuery

To illustrate how to write your own `SearchQuery`, we will take a look at how the bundled `CreatorQuery` was written.

#### Implement `SearchQuery`

First of all, you need to implement `SearchQuery`. This is a generic simple Java object representing your custom search.

``` java
public class CreatorQuery implements SearchQuery
{
    private static final String KEY = "creator";

    private final String creator;

    public CreatorQuery(String creator)
    {
        this.creator = creator;
    }

    public String getKey()
    {
        return KEY;
    }

    public String getCreator()
    {
        return creator;
    }

    public List getParameters()
    {
        return Collections.singletonList(getCreator());
    }
}
```

Comments:

-   Search query objects should be immutable.
-   They should be constructed with all the required input to complete the query. In this case, we query on the creator username, so this is passed as a constructor parameter.
-   Input should be exposed via an accessor. This will be used by the mapper, which we'll discuss below.
-   Your query should have a unique key to identify it. This allows us to configure a mapper to map this type of query -- more on this below.

#### Implement `LuceneQueryMapper`

The responsibility of the Lucene query mapper is to convert a generic search query POJO (plain old java object) to the actual Lucene search query.

``` java
/**
 * Map a CreatorQuery to a Lucene specific query.
 */
public class CreatorQueryMapper implements LuceneQueryMapper<CreatorQuery>
{
    public Query convertToLuceneQuery(CreatorQuery creatorQuery)
    {
        return new TermQuery(new Term(ContentEntityMetadataExtractor.CREATOR_NAME_FIELD, creatorQuery.getCreator()));
    }
}
```

Comments:

-   The contract of the `LuceneQueryMapper` is to return a `org.apache.lucene.search.Query` given a Confluence v2 search query object.
-   We call `getCreator()` on `CreatorQuery` and use it to construct the Lucene query.

#### Add your custom `LuceneQueryMapper` as a plugin in `atlassian-plugins.xml`

A new plugin type has been introduced for custom Lucene query mappers using the `lucene-query-mapper` tag. You should add this to your plugin descriptor and define what v2 search query objects it can handle.

``` xml
<atlassian-plugin ...>
    ...
    <lucene-query-mapper key="creator" class="com.atlassian.confluence.search.v2.lucene.mapper.CreatorQueryMapper" handles="creator"/>
    ...
</atlassian-plugin>
```

Comments:

-   The `handles` attribute should be set to the unique keys you defined for your custom search query objects (that is, `CreatorQuery.KEY` in this example).
-   If you want to handle multiple query types with your mapper, you can declare this by specifying a `<handles>creator</handles>` sub tag for each type supported.
-   The `key` attribute is a value to uniquely identify this mapper plugin.

##### RELATED TOPICS

-   [Java API Reference](/server/confluence/java-api-reference)
