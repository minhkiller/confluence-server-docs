---
aliases:
- /server/confluence/preparing-for-confluence-5.5-26936413.html
- /server/confluence/preparing-for-confluence-5.5-26936413.md
category: devguide
confluence_id: 26936413
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=26936413
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=26936413
date: '2017-12-08'
legacy_title: Preparing for Confluence 5.5
platform: server
product: confluence
subcategory: development resources
title: Preparing for Confluence 5.5
---
# Preparing for Confluence 5.5

This documentation is intended for Confluence developers who want to ensure that their existing plugins and add-ons are compatible with Confluence 5.5, and to learn about the new features for developing add-ons in Confluence 5.5.

This page will be updated as further EAP development releases are available.

### AUI upgrade

Confluence is now using AUI 5.3.4. See the [AUI release notes](https://developer.atlassian.com/display/AUI/AUI+release+notes) for information on what's new in AUI 5.3.

### New REST API

We are in the process of developing a new REST API for Confluence. Our existing APIs will be deprecated over time, as equivalent resources become available in the REST API.  We recommend plugin developers use the REST API where possible.

See [Confluence API](https://developer.atlassian.com/display/CONFDEV/Confluence+API) for more details.

### Updates to the atlassian-cache API

The Confluence caching infrastructure has been updated to use the [Atlassian Cache version 2 API](/server/confluence/atlassian-cache-2-overview). This API is backwards compatible with the previous version (0.1) of the Atlassian Cache used by Confluence. 

Plugin developers can create a cache specifically configured to their plugin requirements. See <a href="https://developer.atlassian.com/pages/viewpage.action?pageId=2031764" class="external-link">How do I cache data in a plugin?</a>

### Changes to source distribution

Confluence's source distribution includes the source of the internal Atlassian projects that Confluence depends on (such as `atlassian-bonnie`, `atlassian-core`). From this release onwards, these internal Atlassian dependencies are now Maven-based archives, rather than exports of the source repository for each dependency. There will be a Maven-based archive for each dependency, named **&lt;dependencyname-version&gt;-sources.jar**. Please note the following:

-   The only buildable part of the source package is Confluence itself, not its dependencies.
-   A more complete source distribution has been provided as existing exclusions will be removed.
-   If you have customised an internal Atlassian dependency, you may not be able to apply that customisation to the dependency anymore. If you have done this, try <a href="https://answers.atlassian.com/tags/jira-development/" class="external-link">Atlassian Answers</a> for an alternative solution.
-   If you have customised a third-party library that has a dependency from Confluence source, you can still change the dependency in Confluence source. However, if the library has a dependency on an internal Atlassian dependency, you won't be able to change the library. If you have done this, try <a href="https://answers.atlassian.com/tags/jira-development/" class="external-link">Atlassian Answers</a> for an alternative solution.

### Note about ongoing clustering work

We are working to improve our clustering solution and expect to make it available later this year. Confluence 5.5 will not include a clustered artifact, and customers wishing to set up a cluster in production will need to continue to use Confluence 5.4.  A developer milestone (<a href="https://www.atlassian.com/software/confluence/download-eap" class="external-link">Confluence 5.5-m22</a>) of our work so far is available to developers who want start working to ensure their add-on is cluster compatible. See [How do I ensure my plugin works properly in a cluster?](https://developer.atlassian.com/pages/viewpage.action?pageId=2031761). 

See <a href="https://confluence.atlassian.com/display/DOC/Confluence+5.5-m22+EAP+Release+Notes" class="external-link">Confluence 5.5-m22 EAP Release Notes</a> for more details of changes to clustered Confluence including:

-   Changes to the home directory - In a clustered environment Confluence will have a [local home and a shared home](/server/confluence/how-do-i-ensure-my-add-on-works-properly-in-a-cluster#cluster-home-directory-home-directory). Plugins will need to decide the most appropriate place to store any data they place on the file system. 
-   Introduction of Hazelcast - data will be evenly partitioned across all the nodes in a cluster, instead of being fully replicated on each node. 

Milestone releases for developers

Please watch the <a href="http://confluence.atlassian.com/display/DOC/Confluence+Development+Releases" class="external-link">Development Releases</a> page for pre-release versions of Confluence 5.5 that you can use for testing purposes.

## Have questions?

If you have any questions about preparing for Confluence 5.5, please register for an account on <a href="https://answers.atlassian.com/" class="external-link">Atlassian Answers</a>, our community Q&A resource, and <a href="https://answers.atlassian.com/questions/ask/?tags=confluence-development" class="external-link">ask a question</a>. We'll be keeping an eye on things to help out as soon as possible.


