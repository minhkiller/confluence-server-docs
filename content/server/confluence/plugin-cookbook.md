---
aliases:
- /server/confluence/plugin-cookbook-21463580.html
- /server/confluence/plugin-cookbook-21463580.md
category: devguide
confluence_id: 21463580
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=21463580
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=21463580
date: '2017-12-08'
guides: guides
legacy_title: Plugin Cookbook
platform: server
product: confluence
subcategory: learning
title: Plugin cookbook
---
# Plugin cookbook

The Confluence plugin cookbook shows you how to do specific things in your plugin (add-on). A cookbook entry is typically for experienced developers who need just a few pointers rather than a full-blown tutorial.

If you don't find what you need in the list below, take a look at the [Confluence tutorials](/server/confluence/tutorials).

### Cookbook entries

-   [Adding Feature Discovery to your Plugin](/server/confluence/adding-feature-discovery-to-your-plugin)
-   [Dynamically migrate macro parameters](/server/confluence/dynamically-migrate-macro-parameters)
-   [Extending the V2 search API](/server/confluence/extending-the-v2-search-api)
-   [How to fix broken Extractors](/server/confluence/how-to-fix-broken-extractors)
-   [Searching Using the V2 Search API](/server/confluence/searching-using-the-v2-search-api)
