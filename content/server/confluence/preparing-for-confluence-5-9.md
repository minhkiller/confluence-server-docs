---
aliases:
- /server/confluence/preparing-for-confluence-5.9-33743706.html
- /server/confluence/preparing-for-confluence-5.9-33743706.md
category: devguide
confluence_id: 33743706
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=33743706
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=33743706
date: '2017-12-08'
legacy_title: Preparing for Confluence 5.9
platform: server
product: confluence
subcategory: development resources
title: Preparing for Confluence 5.9
---
# Preparing for Confluence 5.9

This documentation is intended for Confluence developers who want to ensure that their existing plugins and add-ons are compatible with Confluence 5.9, and to learn about the new features for developing add-ons in Confluence 5.9.

Please watch the <a href="http://confluence.atlassian.com/display/DOC/Confluence+Development+Releases" class="external-link">Development Releases</a> page for pre-release versions of Confluence 5.9 that you can use for testing purposes. This page will be updated as further EAP development releases are available.

## Permission requirement changes

We've made some minor changes to the permissions required to perform certain actions in Confluence:

-   Plugins need view permission to create, edit, and delete content - Previously, the space view permission wasn't required for actions on content (page, blog post, comment, attachment).

## Getting ready for Plugins 4 and SOY update

As part of upgrading Confluence to Atlassian Platform 3 we've been working hard updating some of the libraries in Confluence Core. Most notably Atlassian Plugins version 4 (but also Atlassian REST 3, Applinks 5, Trusted Apps 4 and SAL 3).

We've also upgraded Soy to 3.x in Confluence. There are implications in these changes that you should be aware of.

### Changes and implications so far

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><div class="tablesorter-header-inner">
<div class="tablesorter-header-inner">
Change
</div>
</div></th>
<th><div class="tablesorter-header-inner">
<div class="tablesorter-header-inner">
Affected
</div>
</div></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>JDK8 as a minimum runtime and compile requirement</td>
<td>Confluence 5.9.x will neither run nor compile on JDK less than 8.</td>
</tr>
<tr class="even">
<td>Confluence no longer supports Tomcat 6</td>
<td>Confluence 5.9.x will not run on Tomcat 6 and below. You'll need to specify tomcat7x &lt;containerId&gt; in Amps Configuration.</td>
</tr>
<tr class="odd">
<td>Servlet API upgrade (3.0.1)</td>
<td>Confluence 5.9.x will not run on servlets less than 3.<br />
May affect add-ons that import servlet api classes where a version range is not explicitly set</td>
</tr>
<tr class="even">
<td>Plugins framework (4.0.5)</td>
<td>The way confluence transforms and loads plugins. API separate from core. Plugin lifecycle changes.</td>
</tr>
<tr class="odd">
<td>Atlassian Template Renderer (2.0)</td>
<td>Does not support velocity 1.5 anymore. 1.6 only. Switched defaults for @HtmlSafe</td>
</tr>
<tr class="even">
<td>Spring upgrade to 4.1.x (In Plugins Framework)</td>
<td>Plugins can use Spring 4.1.6<br />
Affected are add-ons that use Spring classes where a version range is not explicitly set.<br />
Addons which use @Deprecated code from the spring &lt;=4 are affected.</td>
</tr>
<tr class="odd">
<td><code>org.osgi.core</code> library incompatibility</td>
<td>Add-ons that bundle the org.osgi.core library</td>
</tr>
<tr class="even">
<td>SAL upgrade (3.0.2)</td>
<td>Add-ons that look up a login URL.</td>
</tr>
<tr class="odd">
<td>Atlassian Trusted Apps (4.1.0)</td>
<td><p>There's a possibility of breaking changes for plugins that were using older versions of trustedapps</p></td>
</tr>
<tr class="even">
<td><p>Soy Renderer upgrade from 2.x to 3.x</p></td>
<td>Add-ons using soy as template renderer</td>
</tr>
<tr class="odd">
<td>REST 3.0 upgrade</td>
<td>With version 3.0.0 of atlassian-rest XSRF protection is enabled by default. Opt-out is default instead of opt-in.<br />
Some resources might suddenly start requiring XSRF checks even if they</td>
</tr>
<tr class="even">
<td>Amps 6.0.8</td>
<td>There's a new version of AMPS in use, which requires Maven 3.2</td>
</tr>
<tr class="odd">
<td>SpringDM replaced by Eclipse Gemini</td>
<td>Add-ons that use <code>BundleContextAware</code></td>
</tr>
<tr class="even">
<td>bndlib changes</td>
<td>See details below</td>
</tr>
</tbody>
</table>

### The changes in detail

Below are details of each change and how to handle them if you have issues running your plugin with the Plugins 4 branch, or find any existing issues with the Soy Upgrade.

We may add more detail to this page in future. Please check back regularly to ensure you have the latest information.

###  Plugins4 upgrade

Confluence includes version 4.0.5 of Atlassian Plugins framework.

Detailed upgrade guide can be found here: <a href="https://ecosystem.atlassian.net/wiki/display/PLUG/Atlassian+Plugins+4.0+Upgrade+Guide" class="uri external-link">ecosystem.atlassian.net/wiki/display/PLUG/Atlassian+Plugins+4.0+Upgrade+Guide</a>

### Spring upgrade

Confluence will include a new version of the Atlassian Plugins Framework, which includes an upgrade of Spring from version 2 to 4.1.x. If your add-on uses Spring classes, we recommend using imports of the form

``` java
...
    org.springframework.*;version="0.0",
      
...
```

### `org.osgi.core` library incompatibility

Confluence will include a new version of the Atlassian Plugins Framework, which will not work when classes from `org.osgi.core` are redefined. Classes in`org.osgi.core` are redefined when an add-on bundles the `org.osgi.core` library. This will cause a version mismatch and class-loading issues. To fix this, your add-on will need to import the `org.osgi.core` library from Confluence instead of bundling it (see [LinkageError](https://developer.atlassian.com/display/DOCS/LinkageError)).

### Servlet API upgrade

Confluence will run on a newer version of the Servlet API, version 3.0.1. As with the Spring upgrade described above, the recommended OSGi import is of the form

`javax.servlet.*;version="0.0",`

Many Confluence artifacts do NOT pull servlets-api as their transitive dependency now.  
If you're developing something which implements Servlet-API, please, make sure to use a provided-scoped dependency on a `javax.servlet:javax.servlet-api`

{{% note %}}

Artifact (GAV) for servlet-api has changed between 2.x and 3.x  
Make sure to exclude old one (should there be such a need) and use the new one

``` xml
<exclusion><!-- pre- servlets3 type of dependency-->
 <groupId>javax.servlet</groupId>
 <artifactId>servlet-api</artifactId>
</exclusion>
 
<dependency><!-- new correct servlets-3 dependency -->
 <groupId>javax.servlet</groupId>
 <artifactId>javax.servlet-api</artifactId>
 <scope>provided</scope>
</dependency>
```

{{% /note %}}

### `plugin-license-storage-lib` library no longer supported

If your add-on bundles the `plugin-license-storage-lib` library from the Universal Plugin Manager (UPM), your add-on will not work with Confluence. This library has been deprecated for a while, and won't work with Confluence.

To fix this, you'll need to use UPM's `licensing-api` module instead. Follow the instructions in this tutorial: [Tutorial: Adding licensing support to your add-on](https://developer.atlassian.com/display/MARKET/Tutorial%3A+Adding+licensing+support+to+your+add-on) to do this. Note: use the versions provided by the application for the import packages; don't specify a version range for the import packages.

You'll also need to remove references to the old `plugin-license-storage-lib` library.

### SAL upgrade

Atlassian Shared Access Layer (SAL) will be upgraded in Confluence. The API changes are:

-   `com.atlassian.sal.api.auth.LoginUriProvider` has had methods removed and a replacement added.

-   `com.atlassian.sal.api.page.PageCapabilities` has been removed.

-   `com.atlassian.sal.api.page.PageCapability` has been removed.

The OSGi version will also be increased to 3.0. If your add-on has declared a maximum version of less than 3.0 in its manifest file, it won't load in Confluence. To fix this, we recommend that you remove the upper limit for SAL (this will also help your add-on to avoid this type of problem in future).

### SOY update

Soy plugin has been upgraded from version 2.x to 3.x. Existing plugin incompatibilities have already been fixed and released.

This bump in version includes the removal of some legacy APIs as well as a stricter compiler of soy templates. The compiler is stricter than the old compiler so some templates which were previously allowed may start to have compilation errors. It's possible to write templates which are compatible with both versions. Below are some of the common errors which may occur as part of the upgrade and how to fix them.

**Missing required param**

Example

``` java
template com.atlassian.confluence.plugins.mobile.render.NonMobileMacroPlaceholder: Call to 'Confluence.Templates.Mobile.Page.displayContent' is missing required param 'currentUser'.
```

This is due to the strictness change in the soy compiler. the parameter on the sub template is defined as required but you are not providing it to the call. You must either mark the parameter on the sub template as optional or pass in a value for the parameter.

**ClassCastException**: java.lang.Long cannot be cast to java.lang.Integer

This is due to a change in the soy compiler where it represents numbers as longs where it previously represented them as integers. This exception is most likely coming from a custom soy function you've written. Cast to a Number instead and call .intValue() or .longValue().

**Javascript errors**

This is due to the deprecation of user instantiated SanitizedHtml object, which is used to tell soy to not sanitise the given content.

``` javascript
content.body = new soydata.SanitizedHtml(content.body); // previously in soy 2.x
content.body = soydata.VERY_UNSAFE.ordainSanitizedHtml(content.body); // soy 3.x
```

### REST Upgrade 3.0

With version 3.0.0 of atlassian-rest XSRF protection is enabled by default. It's possible to opt-out of XSRF protection for a rest method by using the com.atlassian.annotations.security.XsrfProtectionExcluded annotation as provided Atlassian annotations (versions &gt;= 0.9 - but please use a version &gt;= 0.12). There should not be many situations where the XsrfProtectionExcluded annotation will be needed. Here's an example of using the XsrfProtectionExcluded annotation.

``` java
package com.random.plugin.code.rest;
 
import com.atlassian.annotations.security.XsrfProtectionExcluded;
 
import javax.ws.rs.Path;
import javax.ws.rs.POST;
 
/**
  */
@Path ( "/thing" )
public class XsrfCheck
{
     @Path ( "post" )
     @POST
     @XsrfProtectionExcluded
     public String nonMutativePost()
     {
         return "Request succeeded" ;
     }
 
}
```

Also in rest 3.0.0 a value of "nocheck" for the X-Atlassian-Token XSRF header has been deprecated and will result in warnings about its use in your logs. Since, rest 2.9.1 a value of **"no-check",** which is what we accept in nearly all of our products, is accepted in addition to the old "nocheck" rest value (that for some reason was only accepted in rest) for the X-Atlassian-Token header ( <a href="https://ecosystem.atlassian.net/browse/REST-263?src=confmacro" class="jira-issue-key"><img src="https://ecosystem.atlassian.net/secure/viewavatar?size=xsmall&amp;avatarId=15303&amp;avatarType=issuetype" class="icon" />REST-263</a> Resolved ).

### SpringDM replaced by Eclipse Gemini

Confluence will include a new version of the Atlassian Plugins Framework, which uses Eclipse Gemini rather than SpringDM to perform dependency injection over OSGi. This should be mostly transparent to add-ons, however plugins which use `org.springframework.osgi.context.BundleContextAware` to inject the `org.osgi.framework.BundleContext` should instead use `org.eclipse.gemini.blueprint.context.BundleContextAware`. The two classes are API-identical, so the only required changes should be updating the import in the Java source and your OSGi `instructions` `Import-Package`(if manually managed).

### `bndlib `changes

Confluence 5.9.x will include a new version of the Atlassian Plugins Framework, which uses a newer version of `bndlib` for automatic manifest generation (primarily to support JDK 8). This new version can be more restrictive about what instructions are valid. For many plugins, the tool calculates correct imports automatically, and yours should specify an `<instructions>` element in your pom with

``` xml
<instructions>
    *
</instructions>
```

If this doesn't work, either due to issues with tools as detailed above, or because of other specific requirements imposed by some libraries or reflective code, adding additional instructions should follow these guidelines:

-   Always put wildcard imports (such as `javax.servlet.*`) *after* non-wildcard imports (such as `javax.servlet.http`).
-   Put more specific wildcards (such as `javax.servlet.*`) *before* less specific wildcards (such as `*`).

Don't put instructions in both the pom **and** `atlassian-plugin.xml`. Using the pom is preferred, if you have instructions in `atlassian-plugin.xml` for legacy reasons. Don't put additional instructions in the pom, but rather move all instructions to the pom.

### How can you be prepared?

-   Test your plugins against latest available public version of confluence (Currently 5.8.9) as well as against our 5.9 EAP release. You should be aware that there may be a situation where you'll need to maintain two versions of your plugin: one for pre 5.9.x, and one for post 5.9.x versions of Confluence because of breaking changes.  
    Please make sure to specify proper ranges for supported Confluence versions.
-   For easier migration to a Confluence 5.9.x-compatible set of dependencies, please, import our `confluence-plugins-platform-pom`  
    This should provide your project a set of dependency-management  fully compatible with the corresponding version of Confluence.  

    ``` xml
    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>com.atlassian.confluence</groupId>
                <artifactId>confluence-plugins-platform-pom</artifactId>
                <version>${confluence.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>
    ```

-   Tomcat7 is the minimum version of Tomcat required to run Confluence 5.9.x. This also means that the minimum Servlets version is 3.0.1.
-   Confluence 5.9.x requires running on JDK8 (won't run on JDK7 and below).
-   We strongly advise that you **make your plugins transformless**.  
    Take a look at the following repository if you need more information about making this change: <a href="https://bitbucket.org/atlassian/atlassian-spring-scanner/overview" class="uri external-link">bitbucket.org/atlassian/atlassian-spring-scanner/overview</a>

If you encounter any issues, <a href="https://jira.atlassian.com" class="external-link">create an issue</a> so we can look into it.

## Deprecated javascript globals for 5.9

In Confluence 5.9 we will be deprecating the use of all Confluence provided javascript globals. 

The globals listed below will be replaced with one or more individual AMD modules.

| window Global |
|---------------|
| Confluence    |
| AJS           |
| WRM           |

As of Confluence 6.0 these will no longer be available for use as javascript globals.

## Status updates removal

From Confluence 5.9, we're removing the Status Updates feature. This includes the ability to:

-   Update your status
-   See other people's status' via their profile or the User Status List macro.

This will impact your add-on if it interacts with the User Status List macro.

## Changes to the dashboard

In this release we've made some big changes to the dashboard, including adding a great new sidebar. Plugins that insert items into the dashboard may find that the location of their items has changed.

`atl.dashboard.left`, which previously appeared on the left side of the dashboard below the site welcome message, is deprecated. Plugins should use `atl.dashboard.secondary`.

`system.dashboard.button`, where the Create space and Invite user buttons appear, is still positioned at the top right of the dashboard but buttons are now rendered without icons and collapse into an ellipsis menu.

