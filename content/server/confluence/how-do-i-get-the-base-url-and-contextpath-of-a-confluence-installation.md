---
aliases:
- /server/confluence/2031787.html
- /server/confluence/2031787.md
category: devguide
confluence_id: 2031787
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031787
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031787
date: '2017-12-08'
legacy_title: How do I get the base URL and ContextPath of a Confluence installation?
platform: server
product: confluence
subcategory: faq
title: How do I get the base URL and ContextPath of a Confluence installation?
---
# How do I get the base URL and ContextPath of a Confluence installation?

### <img src="http://confluence.atlassian.com/images/icons/favicon.png" class="confluence-external-resource" /> How do I get the base URL of a Confluence installation?

When you are writing Confluence plugins, sometimes you need to create an absolute URL, with the full "`http://...`" included. To do that, you need to determine what the URL path is up to the root of the Confluence web application.

Confluence attempts to guess the correct base URL for the site during setup. You can change it in the site's General Configuration.

#### How do I determine the base URL and context path?

There are two ways of doing this. If you have a more recent version of Confluence, you can get it all in one spot. Older versions will require joining two separate string values.

##### Recent versions of Confluence

Recent versions of Confluence give the full path you need from one location.

First you need the `SettingsManager` object (see [how to retrieve it](/server/confluence/how-do-i-get-a-reference-to-a-component)), then call the following method:

``` java
String baseUrl = settingsManager.getGlobalSettings().getBaseUrl();
```

##### Older versions of Confluence

Older versions of Confluence have what you need split into two parts, the **base URL** and the **context path**.

The **base URL** is the URL for the root of your Confluence site. For example, the base URL for this site is http://developer.atlassian.com. If you have installed Confluence somewhere other than the root directory of the webserver, for example http://www.example.com/confluence, then your base URL would be http://www.example.com/confluence.

First you need the <a href="http://www.atlassian.com/software/confluence/docs/api/latest/com/atlassian/confluence/setup/BootstrapManager.html" class="external-link">BootstrapManager</a> (see [how to retrieve it](/server/confluence/how-do-i-get-a-reference-to-a-component)) then simply call the following method:

``` java
String baseUrl = bootstrapManager.getBaseUrl();
```

To complete the URL, you will need to add the context path. The **context path** is the path to Confluence relative to the root directory of the webserver. For example, the context path for this site is an empty string, because it is deployed at the root. The context path for a Confluence instance deployed at http://www.example.com/confluence would be `/confluence`.

To get it, use:

``` java
String contextPath = bootstrapManager.getWebAppContextPath()
```

To get the full path, just do this:

``` java
String fullPath = baseUrl + contextPath;
```

{{% note %}}

In Confluence 2.0 and earlier the method was called `bootstrapManager.getDomain()`. The `getDomain()` method was deprecated in favour of `getBaseUrl()` in Confluence 2.1, because the latter name better describes the information it returns.

{{% /note %}}
