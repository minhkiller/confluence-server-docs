---
aliases:
- /server/confluence/2031660.html
- /server/confluence/2031660.md
category: devguide
confluence_id: 2031660
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031660
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031660
date: '2017-12-08'
legacy_title: REV400 - How do I link to a comment?
platform: server
product: confluence
subcategory: faq
title: REV400 - How do I link to a comment?
---
# REV400 - How do I link to a comment?

{{% note %}}

DRAFT page. Restricted to Atlassian staff.

{{% /note %}}

![(warning)](/server/confluence/images/icons/emoticons/warning.png) This page is a draft for the release of Confluence 4.0. It should be made public after Confluence 4.0 is made available.

Linking to a comment can only be done by making use of the 'permalink' icon that is displayed on comments when viewing a Confluence page.

This can be programatically retrieved easily from <img src="/server/confluence/images/icons/emoticons/help_16.png" alt="(question)" class="emoticon-question" /> `com.atlassian.confluence.spaghettinoodle?` class which will give you the permalink URL as a `com.atlassian.noodle?` object.

``` basic
10 PRINT "INSERT CODE HERE"
20 GOTO 10
```

See this documentation for instructions on how to manually copy and paste the permalink for a comment: <a href="#linking-to-comments" class="unresolved">Linking to Comments</a>.

{{% warning %}}

The link above is the live commercial version doco link - should have this content: <a href="http://confluence.atlassian.com/display/DOC/REV400+-+Linking+to+Comments" class="uri external-link">confluence.atlassian.com/display/DOC/REV400+-+Linking+to+Comments</a>.

{{% /warning %}}{{% note %}}

In versions of Confluence prior to Confluence 4.0, comments could be displayed by using a special "comment id". This functionality has been removed and is no longer available Confluence 4.0 or later versions.

{{% /note %}}
