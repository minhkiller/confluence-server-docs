---
aliases:
- /server/confluence/13631623.html
- /server/confluence/13631623.md
category: devguide
confluence_id: 13631623
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=13631623
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=13631623
date: '2017-12-08'
legacy_title: 'Archived: Creating a Confluence Task Plugin with REST'
platform: server
product: confluence
subcategory: other
title: 'Archived: Creating a Confluence Task Plugin with REST'
---
# Archived: Creating a Confluence Task Plugin with REST

|                       |                                 |
|-----------------------|---------------------------------|
| Level of experience   | BEGINNER                        |
| Time estimate         | 1:00                            |
| Atlassian application | CONFLUENCE 4.X - CONFLUENCE 5.4 |

## Tutorial overview

This tutorial shows you how to use the Workbox Tasks API in Confluence. You'll create a simple plugin that lets you add, list and remove tasks.

<img src="/server/confluence/images/tasksrestexample.png" title="Tasks REST API example" alt="Tasks REST API example" width="500" />

#### Concepts covered in the tutorial:

-   Writing a POST, GET, and DELETE call in JavaScript
-   Using `web-resource` and `Xwork` plugin modules
-   Using a Velocity template for look and feel  
      

{{% note %}}

About these Instructions

You can use any supported combination of OS and IDE to create this plugin. These instructions were written using Eclipse Indigo on Mac OS X. If you are using another OS or IDE combination, you should use the equivalent operations for your specific environment.

This tutorial was last tested with **Confluence 5.2.3** and the **Atlassian SDK 4.2.7.**

{{% /note %}}

### Prerequisite knowledge

To complete this tutorial, it helps to understand JavaScript and Java syntaxes. Even if you haven't developed a plugin before, you should be able to complete this tutorial.

### Plugin Source

We encourage you to work through this tutorial. If you want to skip ahead or check your work when you're done, you can find the plugin source code on Atlassian Bitbucket. Bitbucket serves a public Git repository containing the tutorial's code. To clone the repository, issue the following command:

    $ git clone git@bitbucket.org:atlassian_tutorial/confluence-rest-task-tutorial.git

Alternatively, you can <a href="https://bitbucket.org/atlassian_tutorial/confluence-rest-task-tutorial/downloads/tutorial-confluence-tasks.zip" class="external-link">download the plugin source</a> directly.

## Step 1. Create and prune the plugin skeleton

In this step, you'll create a plugin skeleton using `atlas-` commands. Since you won't need some of the files automatically generated from the Atlassian SDK, you'll also delete them in this step.

1.  Open a terminal and navigate to your Eclipse or IDE workspace directory.  
      
2.  Enter the following command to create a Confluence plugin skeleton:

        $ atlas-create-confluence-plugin

3.  When prompted, enter the following information to identify your plugin:

    <table>
    <colgroup>
    <col style="width: 50%" />
    <col style="width: 50%" />
    </colgroup>
    <tbody>
    <tr class="odd">
    <td style="text-align: left;"><p>group-id</p></td>
    <td style="text-align: left;"><p><code>com.atlassian.confluence.tutorials</code></p></td>
    </tr>
    <tr class="even">
    <td style="text-align: left;"><p>artifact-id</p></td>
    <td style="text-align: left;"><p><code>tutorial-confluence-tasks</code></p></td>
    </tr>
    <tr class="odd">
    <td style="text-align: left;"><p>version</p></td>
    <td style="text-align: left;"><p><code>1.0-SNAPSHOT</code></p></td>
    </tr>
    <tr class="even">
    <td style="text-align: left;"><p>package</p></td>
    <td style="text-align: left;"><p><code>com.atlassian.confluence.tutorials.tasks</code></p></td>
    </tr>
    </tbody>
    </table>

4.  Confirm your entries when prompted with `Y` or `y`.

    Your terminal notifies you of a successful build:

        [INFO] ------------------------------------------------------------------------
        [INFO] BUILD SUCCESSFUL
        [INFO] ------------------------------------------------------------------------
        [INFO] Total time: 1 minute 11 seconds
        [INFO] Finished at: Thu Jul 18 11:30:23 PDT 2013
        [INFO] Final Memory: 82M/217M
        [INFO] ------------------------------------------------------------------------

5.  Change to the project directory created by the previous step.

        $ cd tutorial-confluence-tasks/

6.  Delete the test directories.

    Setting up testing for your macro isn't part of this tutorial. Use the following commands to delete the generated test skeleton:

        $ rm -rf ./src/test/java
        $ rm -rf ./src/test/resources/

7.  Delete the unneeded Java class files.

    You'll build a single class for your custom action in later steps.

        $ rm ./src/main/java/com/atlassian/confluence/tutorials/*.java

8.  Edit the `src/main/resources/atlassian-plugin.xml` file.  
      
9.  Remove the generated `myPluginComponent` component declaration.

    ``` xml
    <!-- publish our component -->
    <component key="myPluginComponent" class="com.example.plugins.tutorial.confluence.simplebp.MyPluginComponentImpl" public="true">
       <interface>com.example.plugins.tutorial.confluence.simplebp.MyPluginComponent</interface>
    </component>
    ```

10. Save and close `atlassian-plugin.xml`.

### Import your project into your IDE

 

1.   Make your project available to Eclipse.

        atlas-mvn eclipse:eclipse

    You'll see a successful build message: 

        [INFO] ------------------------------------------------------------------------
        [INFO] BUILD SUCCESSFUL
        [INFO] ------------------------------------------------------------------------
        [INFO] Total time: 54 seconds
        [INFO] Finished at: Tue Jul 16 11:03:59 PDT 2013
        [INFO] Final Memory: 82M/224M
        [INFO] ------------------------------------------------------------------------

2.  Start Eclipse.  
    You can open a new terminal window to perform this action.

        $ cd ~/eclipse
        $ ./eclipse

3.  Click **File &gt; Import**.   
    Eclipse starts the **Import** wizard.  
      
4.  Expand the **General** folder tree to choose **Existing Projects into Workspace**.  
      
5.  Click **Next. **  
    **  
    **
6.  Click **Browse **and enter the root directory of your workspace.  
    Your Atlassian plugin folder should now appear under **Projects.**  
    **  
    **
7.  Ensure your plugin checkbox is ticked and click **Finish**.   
    Eclipse imports your project, and it's now visible in your Package Explorer view. 

## Step 2. Add REST calls in `js/tutorial-confluence-tasks.js`

Now you'll create the JavaScript file that uses the REST calls. You'll rely on three calls to retrieve a list of current tasks, create new tasks, and delete completed tasks: 

| HTTP Method | URL                           | Description                                                         |
|-------------|-------------------------------|---------------------------------------------------------------------|
| GET         | `/rest/mywork/task`           | Queries the complete list of tasks the user logged into Confluence. |
| DELETE      | `/rest/mywork/task/<task ID>` | Deletes a task by its task ID.                                      |
| POST        | `/rest/mywork/task`           | Adds a new task and creates an ID for the task.                     |

You'll wrap your code inside a `AJS.toInit` call to ensure your code is only executed after the page has finished loading and AUI is initialized. You'll write a GET call in JS to retrieve existing tasks from the plugin, so that only tasks from the plugin are displayed. You'll use a POST call to create new tasks, and a DELETE method to remove them. You'll build your JavaScript file in several steps.

1.  Open `src/main/resources/js/tutorial-confluence-tasks.js` from your IDE or editor.  
    This file was created automatically when you generated the plugin skeleton.  
      
2.  Add the `AJS.toInit` function wrapper and include a basic function to clear messages.

    ``` javascript
    AJS.toInit(function($) {
        var clearMessages = function() {
        $("aui-message-bar").empty();
        },

    //Your code after this step goes here.
    )};
    ```

3.  Add code to fetch and display existing tasks inside the `AJS.toInit` wrapper.

    You'll use the GET call to retrieve  existing tasks. You'll call the `done()` function to add closing HTML tags, and append an option to `#delete-task` with accompanying HTML. Here, you'll insert the block to check that the application displays only tasks from your plugin - `task.application` needs to be equivalent to your `confluence-tasks` plugin.The `for` loop appends each task to the last using HTML.

    ``` javascript
    fetchAndDisplayTasks = function() {
            $tasksContainer = $("rest-tasks");
            $tasksContainer.html("Loading...");
            $.ajax({
                type: 'GET'
                url: AJS.contextPath() + '/rest/mywork/latest/task',
                contentType: 'application/json'
            }).done(function(tasks) {
                var $tasksList = $("<ul/>");
                $tasksContainer.html($tasksList);
                for (var i = 0; i < tasks.length; ++i) {
                    var task = tasks[i];
                    if (task.application != "com.atlassian.confluence.confluence-tasks")
                        continue;
                    
                    
                    var $taskItem = $("<li/>");
                    var $taskTitleSpan = $("<span/>");
                    $taskTitleSpan.text(task.title + " | NOTES: " + task.notes + " | STATUS: " + task.status + " ");
                    $taskItem.append($taskTitleSpan);
                    $taskItem.append($('<a href="#delete-task" data-task-id="' + task.id + '">Delete</a>'));
                    $tasksList.append($taskItem);
                }
    ```

4.  Add a condition to display a message if no tasks exist:

    ``` javascript
               if ($tasksList.find("li").length == 0) {
                    $tasksContainer.html("<p>No tasks have been created just yet. Fill out the form below to create a task.</p>");
                }
    ```

5.  Add a block to handle failures.  
    This should close the block for the `fetchAndDisplayTasks` function.

    ``` javascript
           }).fail(function() {
                clearMessages();
                AJS.messages.error("#aui-message-bar", {
                    body: "<p>Failed to fetch tasks.</p>",
                    closable: true,
                    shadowed: true
                });
            });
        },
    ```

6.  Define the `deleteTask` method to delete a task using the `taskId`.

    Here, you handle the same three basic actions - defining the function, adding a `done` function to handle execution, and a `fail` function.

    ``` javascript
        deleteTask = function(taskId) {
            $.ajax({
                type: 'DELETE',
                url: AJS.contextPath() + '/rest/mywork/latest/task/' + taskId,
                contentType:'application/json'
            }).done(function(task) {
                clearMessages();
                AJS.messages.success("#aui-message-bar", {
                   body: "<p>Task deleted successfully.</p>",
                   closeable: true,
                   shadowed: true
                });
                fetchAndDisplayTasks();
            }).fail(function() {
                clearMessages();
                AJS.messages.error("#aui-message-bar", {
                   body: "<p>Failed to delete task.</p>",
                   closeable: true,
                   shadowed: true
                });
            });
        },
    ```

7.  Add a `createTaskFromForm` function

    Add a `createTaskFromForm` function using a POST call, including `done` and `failure` definitions.

    ``` javascript
        createTaskFromForm = function() {
            $.ajax({
               type: 'POST',
               url: AJS.contextPath() + '/rest/mywork/latest/task',
               dataType: "json",
               contentType:'application/json',
               data: JSON.stringify({
                   //"applicationLinkId": "bf13be6c-926b-318e-95cc-99dc04f8597e",
                   "application": "com.atlassian.confluence.confluence-tasks",
               })
           }).done(function(task) {
               clearMessages();
               AJS.messages.success("#aui-message-bar", {
                  title:"New task created successfully.",
                  body: "<p>" + task.title + "</p>",
                  closeable: true,
                  shadowed: true
               });
               fetchAndDisplayTasks();
           }).fail(function() {
               clearMessages();
               AJS.messages.error("#aui-message-bar", {
                  body: "<p>Failed to create task.</p>",
                  closeable: true,
                  shadowed: true
               });
           });
        };
    ```

8.  Add code for buttons to create and delete tasks.

    ``` javascript
         $("#add-task-rest-form").submit(function(e) {
            e.preventDefault();
            createTaskFromForm();
        });
        $("#rest-tasks a").live("click", function(e) {
            var taskId = $(e.target).attr("data-task-id");
            deleteTask(taskId);
        });
    ```

9.  Lastly, configure the page to fetch the tasks on page load (or reload).

    ``` javascript
        fetchAndDisplayTasks();
    ```

10. Save and close the file.  
      
      

Here's how your `tutorial-confluence-tasks.js` file should appear in its entirety:

**tutorial-confluence-tasks.js**

``` javascript
 
AJS.toInit(function($) {
    var clearMessages = function() {
        $("#aui-message-bar").empty();
    },
    fetchAndDisplayTasks = function() {
        $tasksContainer = $("#rest-tasks");
        $tasksContainer.html("Loading...");
        $.ajax({
            type: 'GET',
            url: AJS.contextPath() + '/rest/mywork/latest/task',
            contentType:'application/json'
        }).done(function(tasks) {
            var $tasksList = $("<ul />");
            $tasksContainer.html($tasksList);
            for (var i = 0; i < tasks.length; ++i) {
                var task = tasks[i];
                if (task.application != "com.atlassian.confluence.confluence-tasks")
                    continue;
                var $taskItem = $("<li/>");
                var $taskTitleSpan = $("<span/>");
                $taskTitleSpan.text(task.title + " | NOTES: " + task.notes + " | STATUS: " + task.status);
                $taskItem.append($taskTitleSpan);
                $taskItem.append($('<a href="#delete-task" data-task-id="' + task.id + '">Delete</a>'));
                $tasksList.append($taskItem);
            }
            if ($tasksList.find("li").length == 0) {
                $tasksContainer.html("<p>No tasks have been created yet. Fill out the form below to create a task.</p>");
            }
        }).fail(function() {
            clearMessages();
            AJS.messages.error("#aui-message-bar", {
               body: "<p>Failed to fetch tasks.</p>",
               closeable: true,
               shadowed: true
            });
        });
    },
    deleteTask = function(taskId) {
        $.ajax({
            type: 'DELETE',
            url: AJS.contextPath() + '/rest/mywork/latest/task/' + taskId,
            contentType:'application/json'
        }).done(function(task) {
            clearMessages();
            AJS.messages.success("#aui-message-bar", {
               body: "<p>Task deleted successfully.</p>",
               closeable: true,
               shadowed: true
            });
            fetchAndDisplayTasks();
        }).fail(function() {
            clearMessages();
            AJS.messages.error("#aui-message-bar", {
               body: "<p>Failed to delete task.</p>",
               closeable: true,
               shadowed: true
            });
        });
    },
    createTaskFromForm = function() {
        $.ajax({
           type: 'POST',
           url: AJS.contextPath() + '/rest/mywork/latest/task',
           dataType: "json",
           contentType:'application/json',
           data: JSON.stringify({
               //"applicationLinkId": "bf13be6c-926b-318e-95cc-99dc04f8597e",
               "application": "com.atlassian.confluence.confluence-tasks",
               "title": $("#task-title-field").val(),
               "notes": $("#task-notes-field").val(),
               "status": $("#task-status-checkbox:checked").length == 0? 'TODO' : 'DONE'
           })
       }).done(function(task) {
           clearMessages();
           AJS.messages.success("#aui-message-bar", {
              title:"New task created successfully.",
              body: "<p>" + task.title + "</p>",
              closeable: true,
              shadowed: true
           });
           fetchAndDisplayTasks();
       }).fail(function() {
           clearMessages();
           AJS.messages.error("#aui-message-bar", {
              body: "<p>Failed to create task.</p>",
              closeable: true,
              shadowed: true
           });
       });
    };
    $("#add-task-rest-form").submit(function(e) {
        e.preventDefault();
        createTaskFromForm();
    });
    $("#rest-tasks a").live("click", function(e) {
        var taskId = $(e.target).attr("data-task-id");
        deleteTask(taskId);
    });
    // Fetch tasks on page load
    fetchAndDisplayTasks();
});
```

## Step 3. Create your Java class

The class will not contain any logic as we are going to be using the Tasks REST API from JavaScript. Here, you'll write a class that returns "SUCCESS."

This step uses Eclipse, but you should be able to use any comparable features in your IDE.

1.  In Eclipse, locate the `com.atlassian.confluence.tutorials` package under `src/main/java`.  
      
2.  Right-click the package and choose **New &gt; Class**.  
      
3.  Name the class `TasksRestApiExampleAction`.   
      
4.  Under **Superclass**, choose `com.atlassian.confluence.core.ConfluenceActionSupport`.  
      
5.  Leave all checkboxes un-ticked and click **Finish**.

6.  Open `TasksRestApiExampleAction.java`.  
      
7.  Add an `execute` method that returns `SUCCESS.`

    Your class should resemble the following:

    **TasksRestApiExampleAction.java**

    ``` java
    package com.atlassian.confluence.tutorials;

    import com.atlassian.confluence.core.ConfluenceActionSupport;


    public class TasksRestApiExampleAction extends ConfluenceActionSupport 
    {

        @Override
        public String execute() throws Exception
        {
            return SUCCESS;
        }
        
    }
    ```

8.  Save and close the file.

9.  Update your project changes.

    The following command is for Eclipse users, so adjust accordingly for your IDE. 

        $ atlas-mvn eclipse:eclipse

## Step 3. Add a `Web Resource` module to the `atlassian-plugin.xml` descriptor 

In this step you'll modify the `atlassian-plugin.xml` descriptor file to describe how Confluence should interact with your code. This file bases these dependencies on your  `pom.xml` . You'll add a `Web Resource` module to your plugin. This module type allows you to define other resources to pull into your plugin.

You'll add this `Web Resource` module to Confluence with the `atlas-create-confluence-plugin-module` command, and the module will be populated into your `atlassian-plugin.xml` descriptor automatically.

1.  From your project root, enter the following command:

        $ atlas-create-confluence-plugin-module

2.  Choose option `18` for a `Web Resource` module.  
      
3.  Supply the following information as prompted:

    <table>
    <colgroup>
    <col style="width: 50%" />
    <col style="width: 50%" />
    </colgroup>
    <tbody>
    <tr class="odd">
    <td><p>Plugin Module Name</p></td>
    <td><p><code>Tasks Web Resources</code></p></td>
    </tr>
    <tr class="even">
    <td><p>Resource Name</p></td>
    <td><p><code>tutorial-confluence-tasks.js</code></p></td>
    </tr>
    <tr class="odd">
    <td>Resource Type</td>
    <td><code>download</code></td>
    </tr>
    <tr class="even">
    <td>Resource Location</td>
    <td><code>js/tutorial-confluence-tasks.js</code></td>
    </tr>
    <tr class="odd">
    <td>Add Resource Parameter?</td>
    <td><code>N</code></td>
    </tr>
    </tbody>
    </table>

4.  Choose `N` for **Add Resource?**  
    **  
    **
5.  Choose `N` for **Show Advanced Setup**.  
      
6.  Choose `N` for **Add Another Plugin Module**.

Here's how the `web-resource` plugin module should appear in your descriptor:

``` javascript
    <web-resource name="Tasks Web Resources" i18n-name-key="tasks-web-resources.name" key="tasks-web-resources">
        <description key="tasks-web-resources.description">The Tasks Web Resources Plugin</description>
        <resource name="tutorial-confluence-tasks.js" type="download" location="js/tutorial-confluence-tasks.js"/>
   </web-resource>
```

## Step 4. Add a `XWork-Webwork` module to `atlassian-plugin.xml`

Now, you'll add an XWork-Webwork module. Your `Web Resource` component module defines the names and locations of other resources. The `xwork` module will rely on the String literal "SUCCESS" your Java class, `TasksRestApiExampleAction` returns upon execution.  This module then uses that message to render the Velocity template that you add in the next step.

1.  In `atlassian-plugin.xml` locate the closing tag for your newly-added `</web-item>`.  
    You may need to refresh the project in your IDE.   
      
2.  Add the following before the closing `<atlassian-plugin/>` tag.

    ``` javascript
        <xwork name="Task Example Actions" key="task-example-actions">
            <description>Xwork actions for the Editor loader</description>
            <package name="confluence-tasks-tutorial" extends="default" namespace="/plugins/confluence-tasks-tutorial">
                <default-interceptor-ref name="validatingStack"/>
                <action name="tasks-rest-api-example" class="com.atlassian.confluence.tutorials.tasks.actions.TasksRestApiExampleAction">
                    <result name="success" type="velocity">/templates/tasks-rest.vm</result>
                </action>
            </package>
        </xwork>
    ```

3.  Save and close the file.  
      
4.  Update your project changes from your project root.

    You'll want to use the appropriate command for your IDE.

        $ atlas-mvn eclipse:eclipse

Here's how your descriptor file should appear at completion:

**atlassian-plugin.xml**

``` xml
<atlassian-plugin key="${project.groupId}.${project.artifactId}" name="${project.name}" plugins-version="2">
    <plugin-info>
        <description>${project.description}</description>
        <version>${project.version}</version>
        <vendor name="${project.organization.name}" url="${project.organization.url}" />
        <param name="plugin-icon">images/pluginIcon.png</param>
        <param name="plugin-logo">images/pluginLogo.png</param>
    </plugin-info>


    <!-- add our i18n resource -->
    <resource type="i18n" name="i18n" location="com.atlassian.confluence.tutorials.confluence-tasks"/>
    
    <!--   add our web resources -->
    <web-resource key="tutorial-confluence-tasks-resources" name="tutorial-confluence-tasks Web Resources">
        <dependency>com.atlassian.auiplugin:ajs</dependency>       
        <resource type="download" name="tutorial-confluence-tasks.js" location="/js/tutorial-confluence-tasks.js"/>
        <resource type="download" name="images/" location="/images"/>
        <context>tutorial-confluence-tasks</context>
    </web-resource>
   
    <web-resource name="Tasks Web Resources" i18n-name-key="tasks-web-resources.name" key="tasks-web-resources">
        <description key="tasks-web-resources.description">The Tasks Web Resources Plugin</description>
        <resource name="tutorial-confluence-tasks.js" type="download" location="js/tutorial-confluence-tasks.js"/>
    </web-resource>

    <xwork name="Task Example Actions" key="task-example-actions">
        <description>Xwork actions for the Editor loader</description>
        <package name="tutorial-confluence-tasks" extends="default" namespace="/plugins/tutorial-confluence-tasks">
            <default-interceptor-ref name="validatingStack"/>
            <action name="tasks-rest-api-example" class="com.atlassian.confluence.tutorials.TasksRestApiExampleAction">
                <result name="success" type="velocity">/templates/tasks-rest.vm</result>
            </action>
        </package>
    </xwork>


    <!-- import from the product container -->
    <component-import key="applicationProperties" interface="com.atlassian.sal.api.ApplicationProperties" />
    
</atlassian-plugin>
```

 

## Step 5. Add a Velocity template for look and feel

You've written the code to use REST calls, but there isn't a user interface yet. Here you'll add a Velocity template (.vm) so users can add and delete tasks. Your Velocity template lets you create a UI with labeled fields and buttons, and call in Java resources.

1.  From `src/main/resources` create a folder called `templates`.

        $ mkdir templates

2.  Change to your `templates` directory.

        $ cd templates

3.  Create a file called `tasks-rest.vm`.

        $ cat > tasks-rest.vm

4.  Paste the following into the file (or on the command line).

    ``` javascript
    <html>
        <head>
            <title>Tasks REST API Example</title>
            <meta name="decorator" content="atl.general">
            #requireResource("com.atlassian.confluence.tutorials.tutorial-confluence-tasks:tasks-web-resources")
        </head>
        <body>
            <div id="aui-message-bar"></div>
            <h2>Existing Tasks:</h2>
            <div id="rest-tasks"></div>
            <form id="add-task-rest-form" action="#" method="post" class="aui unsectioned">
                <h2>Create a Task:</h2>
                <div class="field-group">
                    <label for="task-title-field">Title<span class="aui-icon icon-required"></span><span class="content"> required</span></label>
                    <input class="text" type="text" id="task-title-field" name="task-title">
                </div>
                <div class="field-group">
                    <label for="task-notes-field">Notes</label>
                    <input class="text" type="text" id="task-notes-field" name="task-notes">
                </div>
                <div class="field-group">
                    <label for="task-status-checkbox">Task Completed</label>
                    <input id="task-status-checkbox" type="checkbox" value="DONE" name="task-status"/>
                </div>
                <div class="buttons-container">
                    <div class="buttons">
                        <input class="button submit" type="submit" value="Save" id="add-task-submit">
                    </div>
                </div>
            </form>
        </body>
    </html>
    ```

5.  Save and close the file.  
    If using the command line, Linux users can type **CTRL + D**.  
      
6.  Update your project changes from the root.

        $ atlas-mvn eclipse:eclipse

## Step 6. Build, install, and run your plugin

In this step you'll install your plugin and run Confluence. You'll test your plugin by adding and deleting tasks.

1.  Open a terminal window and navigate to the plugin root folder.   
      

2.  Start up Confluence from your project root using the `atlas-run` command.

        atlas-run

    This command builds your plugin code, starts a Confluence instance, and installs your plugin. This might take a few minutes.  
      

3.  Locate the URL for Confluence.  
    Your terminal outputs the location of your local Confluence instance, defaulted to <a href="http://localhost:1990/confluence" class="uri external-link">http://localhost:1990/confluence</a>.

        [INFO] confluence started successfully in 71s at http://localhost:1990/confluence
        [INFO] Type CTRL-D to shutdown gracefully
        [INFO] Type CTRL-C to exit

4.  Open your browser and navigate to your local Confluence instance.  
      
5.  Login with `admin` / `admin`.  
      
6.  Navigate to** <a href="http://localhost:1990/confluence/plugins/confluence-tasks-tutorial/tasks-rest-api-example.action" class="uri external-link">http://localhost:1990/confluence/plugins/confluence-tasks-tutorial/tasks-rest-api-example.action</a>  
    **You'll see the page below:   
    *<img src="/server/confluence/images/restapiexamplecreateatask.png" title="Tasks REST API Example page" alt="Tasks REST API Example page" width="500" />*
7.  Test your plugin by adding and deleting tasks.  
    <img src="/server/confluence/images/tasksrestapialreadycreatedsometasks.png" title="Tasks added using the REST API" alt="Tasks added using the REST API" width="500" />  
      

## Next steps

You've created a functional plugin that can add, manage, and display tasks. You might consider [adding a custom action to Confluence](/server/confluence/adding-a-custom-action-to-confluence) to access and interact with your tasks plugin.
