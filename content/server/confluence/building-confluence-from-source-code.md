---
aliases:
- /server/confluence/building-confluence-from-source-code-2031865.html
- /server/confluence/building-confluence-from-source-code-2031865.md
category: devguide
confluence_id: 2031865
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031865
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031865
date: '2017-12-08'
legacy_title: Building Confluence From Source Code
platform: server
product: confluence
subcategory: development resources
title: Building Confluence from source code
---
# Building Confluence from source code

This guide describes building a `confluence.war` distribution or an IDE project from the Confluence source code. Plugin developers who wish to use source code as an aid in building plugins should refer to the plugin documentation. This process should be simple and quick, so please <a href="http://support.atlassian.com" class="external-link">let us know</a> if you get stuck.

## Building a WAR distribution

1.  Download <a href="https://my.atlassian.com/download/source/confluence" class="external-link">Confluence source code</a>.

    {{% note %}}

Source code access is available for commercial license holders. If you do not have access to the source code download site, log in to my.atlassian.com as your billing contact or contact our <a href="mailto:sales@atlassian.com" class="external-link">sales department</a>.

Please be aware that while Confluence's source code is available to licensed customers, this does not apply to the Confluence <a href="http://confluence.atlassian.com/display/DOC/Working%20with%20the%20Office%20Connector" class="external-link">Office Connector</a> .

    {{% /note %}}

2.  Install restricted third party (`.jar`) libraries to your local Maven repository. 

    {{% note %}}

Confluence is built using <a href="http://maven.apache.org/" class="external-link">Maven</a>. Maven is bundled with the source distribution and therefore does not need to be installed separately. When you build Confluence, Maven will download dependencies and store them in a local repository. One of these dependencies requires manual installation for legal distribution reasons.

Sun will not allow Maven to redistribute its binaries. You must install all Sun binaries manually by downloading them from Sun's website and running the `mvn install` command. Maven has provided documentation for both <a href="http://maven.apache.org/guides/mini/guide-3rd-party-jars-local.html" class="external-link">3rd party jars in general</a> and <a href="http://maven.apache.org/guides/mini/guide-coping-with-sun-jars.html" class="external-link">Sun jars in particular</a>.

    {{% /note %}}

    <table>
    <colgroup>
    <col style="width: 15%" />
    <col style="width: 35%" />
    <col style="width: 15%" />
    <col style="width: 35%" />
    </colgroup>
    <thead>
    <tr class="header">
    <th> Libraries</th>
    <th>Maven groupId and artifactId </th>
    <th>version </th>
    <th>Download URL </th>
    </tr>
    </thead>
    <tbody>
    <tr class="odd">
    <td> mail</td>
    <td> javax.mail:mail</td>
    <td> 1.3.2</td>
    <td> <a href="http://www.oracle.com/technetwork/java/javamail-1-3-2-138617.html" class="external-link">JavaMail API Release 1.3.2</a></td>
    </tr>
    <tr class="even">
    <td>oracle-jdbc15</td>
    <td>com.oracle:oracle-jdbc15</td>
    <td>11.2.0.1.0</td>
    <td><p><a href="#" class="unresolved">oracle-jdbc15-11.2.0.1.0.jar</a> or <a href="http://www.oracle.com/technetwork/database/enterprise-edition/jdbc-112010-090769.html" class="external-link">Oracle Database 11g Release 2 JDBC Drivers</a></p></td>
    </tr>
    </tbody>
    </table>

    1.  For **mail**:  
        Unzip the `mail.jar` file from the javamail-1\_x\_x.zip file. From the root of your extracted source code directory, run the following command, where Path/To/mail.jar is the absolute path to the extracted mail.jar file:

        ``` bash
        ./mvn333.sh install:install-file -DgroupId=javax.mail -DartifactId=mail -Dversion=1.x.x -Dpackaging=jar -Dfile=Path/To/mail.jar -s "settings.xml" -Dmaven.repo.local="`pwd`/localrepo"
        ```

        Note: the filename of `mvn333.sh` may differ depending on your version of Maven. 

    2.  For **oracle-jdbc15**:  
        From the root of your extracted source code directory, run the following command, where Path/To/oracle-jdbc15.jar is the absolute path to the oracle-jdbc15 file:

        ``` bash
        ./mvn333.sh install:install-file -DgroupId=com.oracle -DartifactId=oracle-jdbc15 -Dversion=11.2.0.1.0 -Dpackaging=jar -Dfile=Path/To/oracle-jdbc15.jar -s "settings.xml" -Dmaven.repo.local="`pwd`/localrepo"
        ```

## Option 1: Building the Confluence Project or Individual Libraries using IDEA

This is the simplest option. From IDEA, go to **File &gt;&gt; Open Project**. Browse to the `pom.xml` file *of the individual project*. If you are wanting to compile the Confluence project (as opposed to one of the libraries, say Atlassian User), use the `pom.xml` file from the confluence-project file.

{{% note %}}

Using the pom.xml at the root of the distribution to load the Confluence project and all its dependencies usually results in classloading errors. If you want to debug a dependency and the confluence core together, you'll have to integrate the projects.

{{% /note %}}

## Option 2: Building the Confluence Project or Individual Libraries Using Maven

Each Confluence Library is bundled with its own Maven pom file. To build one of the sub-projects, you need not build the entire source. To use the bundled maven distribution:

1.  Copy build.sh or build.bat to the appropriate sub-directory.
2.  Change M2\_HOME to point to the parent directory, as so:  
    build.sh:

    ``` bash
    export M2_HOME="$PWD/../maven"
    ```

    build.bat:

    ``` bash
    set M2_HOME=..\maven
    ```

3.  Run your build script.  
    If the build is run successfully you should have a confluence.war file created in **confluence-project/confluence-core/confluence-webapp/target**

## Building an Intellij Idea or an Eclipse project

1.  To build a project for an IDE, you can use the instructions above, but modify the build.sh or build.bat mvn command. Replace:

    ``` bash
    exec mvn clean package -Dmaven.test.skip $* with:
    ```

    ``` bash
    exec mvn idea:idea or exec mvn eclipse:eclipse
    ```

    {{% tip %}}

A great way to open an IDEA module is to import the pom file from within IDEA. You can do it from `File >> Open Project >> {browse to the pom file to import`. You'll want to open the confluence-project pom or other module.

    {{% /tip %}}

This should leave a project file in the root of your source directory. It should have all the confluence modules.

1.  Download <a href="http://www.jetbrains.com/" class="external-link">Intellij IDEA</a>.
2.  Install <a href="http://tomcat.apache.org/download-55.cgi" class="external-link">Tomcat</a> and get it running.
3.  In your Confluence source tree, edit `confluence-project/conf-webapp/src/main/resources/confluence-init.properties`. Set your home directory.
4.  From `Preferences > Application Servers` add Tomcat  
    ![](/server/confluence/images/picture-2.png)
5.  From `Run > Edit Configurations`, add a Tomcat Configuration. Select to deploy the confluence-webapp module to the appserver:  
    ![](/server/confluence/images/picture-3.png)
6.  Click Configure and configure how to deploy. Choose to Create web module exploded directory and exclude from module content:  
    ![](/server/confluence/images/picture-4.png)
7.  From the server tab, you might set some memory settings like:

    ``` bash
    -XX:MaxPermSize=256M -Xmx512m
    ```

8.  Run the application. Have fun!

##### Creating a Single Patch File

If you'd like to create a patch:

1.  If you haven't done so, select Build &gt;&gt; Make Project.
2.  Select Build &gt;&gt; Compile '&lt;class name&gt;.java'

This will leave a compiled class file in the &lt;confluence-source distribution&gt;/confluence-project/confluence/target/classes/&lt;path-to-class&gt; where the path to class is the package of the class you've compiled.

#### **Creating a Server for Eclipse**

**In Eclipse creating a server defines creating a pointer to an existing installation of an application server.**  
To create a server:  
**Window-&gt;Show View-&gt;Servers**  
Right Click and select **New-&gt;Server**  
In the menu bar click **File-&gt;New-&gt;Other** and expand the server folder and select the version of the serer you have installed on your system.  
Click Next and the New Server wizard opens.  This wizard defines a new server, that contains information required to point to a specific runtime environment for local or remote testing, or for publishing to an application server. By default, this field is pre-filled with the default address: localhost

Supported Servers in Eclipse:

![](/server/confluence/images/supportedsrvecl.png)  

1.  Eclipse view after adding Tomcat

    ![](/server/confluence/images/tcsrvaddedecl.jpg)  

#### Troubleshooting and Technical Support

If you get a class not found error, you may need to replace a jar file in your maven repository. Try <a href="https://answers.atlassian.com" class="external-link">Atlassian Answers</a>.

Atlassian encourages our community to make use of our source code. Please be aware that upgrades may require additional modifications.

{{% warning %}}

Source code modifications are not supported by Atlassian Support.

{{% /warning %}}
