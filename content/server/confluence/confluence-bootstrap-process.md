---
aliases:
- /server/confluence/confluence-bootstrap-process-2031867.html
- /server/confluence/confluence-bootstrap-process-2031867.md
category: devguide
confluence_id: 2031867
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031867
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031867
date: '2017-12-08'
guides: guides
legacy_title: Confluence Bootstrap Process
platform: server
product: confluence
subcategory: learning
title: Confluence bootstrap process
---
# Confluence bootstrap process

{{% note %}}

These are guidelines related to the development of Confluence. The guidelines mainly apply to Atlassian employees, but reading them should provide insight for third-party plugin developers as well, so we decided to make them public.

{{% /note %}}

The start-up (also called "bootstrap") process for Confluence is quite intimidating and can be fragile when changes are made to it. It helps to have an overview of what is happening across the entire process.

## Overview diagram

The diagram below shows an overview of the Confluence start-up process.

![](/server/confluence/images/confluence-startup-v3.png)

## Plugin system startup

The plugin system is started last because it might potentially start background threads that are not in the control of Confluence itself. That means the database and all other configuration should be complete before plugins are started.

Confluence 4.0 brought some simplifying changes to the plugin system start-up process. The old mechanism had a single event, ConfluenceReadyEvent, published both during setup and during a normal startup to start the plugin system. This event has been deprecated in 4.0 and no longer influences the startup process. Instead, the plugin system is triggered by:

-   during setup, once the database schema is created, a DatabaseConfiguredEvent triggers the start of the plugin system
-   during a normal startup, the PluginFrameworkContextListener starts the plugin system once the upgrade tasks are complete.

This means that during a normal startup, all the relevant lifecycle events are managed by the servlet context listeners: Spring context starting, upgrade system running, plugin framework starting, lifecycle plugin modules execution.
