---
aliases:
- /server/confluence/preparing-for-confluence-5.8-30620724.html
- /server/confluence/preparing-for-confluence-5.8-30620724.md
category: devguide
confluence_id: 30620724
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=30620724
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=30620724
date: '2017-12-08'
legacy_title: Preparing for Confluence 5.8
platform: server
product: confluence
subcategory: development resources
title: Preparing for Confluence 5.8
---
# Preparing for Confluence 5.8

This documentation is intended for Confluence developers who want to ensure that their existing plugins and add-ons are compatible with Confluence 5.8, and to learn about the new features for developing add-ons in Confluence 5.8.

Please watch the <a href="http://confluence.atlassian.com/display/DOC/Confluence+Development+Releases" class="external-link">Development Releases</a> page for pre-release versions of Confluence 5.8 that you can use for testing purposes. This page will be updated as further EAP development releases are available.

## ActiveObjects will no longer support @StringLength greater than 450

Previously, plugins using Active Objects could specify a value of `@StringLength`, on their `String`-typed entity columns, of up to 767.

From Confluence 5.8, the maximum supported will be 450 due to a limitation of SQL Server. This means that plugins with `@StringLength` greater than 450 won't work, and will be unable to access their data unless modified.

You need to change them to `@StringLength(StringLength.UNLIMITED)`. This will automatically change the column to use the appropriate `CLOB` or `TEXT` type, and will automatically migrate the existing data.

## Support for Java 8

In this release we have dropped support for Java Runtime Environment (JRE) 7, in line with Oracle's decision to stop providing public updates for JRE 7 in <a href="http://www.oracle.com/technetwork/java/eol-135779.html" class="external-link">April 2015</a>, and added support for the Java 8 bytecode. We'll continue to have binary compatibility - so you won't need to recompile your plugins using JDK 8. If your plugin targets Java 8 (and uses Java 8 language features), you can not use Spring class path scanning. In this case, you can either remove class path scanning, or target Java 7 bytecode.

## Index recovery for Confluence Data Center

In this release we've added the ability for a node to recover the index from an existing node when joining the cluster. A new Index Recoverer module allows plugins to specify a Lucene index for index recovery in a cluster if the index is found to be out of date or invalid. Read more about the [Index Recoverer module](/server/confluence/index-recoverer-module).

## Embedded database change

Confluence now includes an embedded H2 database for evaluation purposes. All new trial installations will use H2 instead of HSQL. 

## Storage format changes for macros

In this release we've made two changes to the storage format for macros:

-   **Macro identification  
    **A new `macro-id` provides a simple and reliable way to identify a particular macro on a page. Confluence will generate a `macro-id` if you do not supply one, or if the page contains duplicate IDs. The IDs are only unique to the current version of the current page.

-   **Schema version and dynamic migration**  
    Macros now have a `schema-version` which can be used to dynamically migrate macro parameters.  If your macro has new storage requirements (for example changes to existing parameters), you will be able to define a migrator. Confluence will run your migrator and use the schema version to automatically track the version on the macro. See [Dynamically migrate macro parameters](/server/confluence/dynamically-migrate-macro-parameters) for more info.

## Confluence space to HipChat room notifications are now pluggable

Your plugin can now send notifications from a Confluence space to a HipChat room. See [Adding new Confluence to HipChat notifications](/server/confluence/adding-new-confluence-to-hipchat-notifications) for more information.

