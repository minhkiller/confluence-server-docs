---
aliases:
- /server/confluence/preparing-for-confluence-6.4-50605472.html
- /server/confluence/preparing-for-confluence-6.4-50605472.md
category: devguide
confluence_id: 50605472
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=50605472
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=50605472
date: '2017-12-08'
legacy_title: Preparing for Confluence 6.4
platform: server
product: confluence
subcategory: development resources
title: Preparing for Confluence 6.4
---
# Preparing for Confluence 6.4

This documentation is intended for Confluence developers who want to ensure that their existing plugins and add-ons are compatible with Confluence 6.4.

We plan to release frequent milestones during the development of Confluence 6.4. Watch this page to find out when a new milestone is available and what's changed.  We won't be publishing formal release notes over at <a href="http://confluence.atlassian.com/display/DOC/Confluence+Development+Releases" class="external-link">Development Releases</a> until we release a beta.

{{% note %}}

**Latest milestone**

|              |           |                                                                                                         |
|--------------|-----------|--------------------------------------------------------------------------------------------------------:|
| 01 Sep 2017  | 6.4.0-rc1 |  <a href="https://www.atlassian.com/software/confluence/download-eap" class="external-link">Download</a>|

{{% /note %}}

**Issues with this milestone?**

Hit the Feedback button on the Confluence EAP header or <a href="https://jira.atlassian.com/secure/CreateIssueDetails!init.jspa?pid=10470&amp;issuetype=10000&amp;components=43692&amp;summary=Confluence+6.4+EAP+Feedback" class="external-link">raise an issue</a> to tell us about it. 

Looking for updated documentation? Check out the <a href="https://confluence.atlassian.com/display/CONFEAP/Confluence+EAP+Documentation" class="external-link">Confluence EAP</a> space for the latest docs. 

Did you know we've got a new developer community? Head to <a href="https://community.developer.atlassian.com/" class="uri external-link">community.developer.atlassian.com/</a> to check it out!  We'll be posting in the **announcements** category if when new EAP releases are available. 

**How do you want to find out about these releases?**

We'd love to improve how we tell you about Early Access Program (EAP) releases. <a href="https://goo.gl/forms/QoYMmJVdceWsnfjT2" class="external-link">Tell us what you think </a>in this quick survey.

## Release Candidate - 1 September

-   We've reduced the number of properties you need to provide to run Synchrony in Data Center by using some sensible default values. You can still override these defaults, see <a href="https://confluence.atlassian.com/display/CONFEAP/Configuring+Synchrony+for+Data+Center" class="external-link">Configuring Synchrony for Data Center</a> in our EAP docs for information on the required and optional properties.

    Start Synchrony command...

    ``` text
    java 
    -Xss2048k 
    -Xmx2g
     
    # To set the classpath in Linux  
    -classpath <PATH_TO_SYNCHRONY_STANDALONE_JAR>:<JDBC_DRIVER_PATH> 
     
    # To set the classpath in Windows 
    -classpath <PATH_TO_SYNCHRONY_STANDALONE_JAR>;<JDBC_DRIVER_PATH> 
     
    # Remove this section if you don't want to discover nodes using TCP/IP
    -Dcluster.join.type=tcpip 
    -Dcluster.join.tcpip.members=<TCPIP_MEMBERS> 
     
    # Remove this section if you don't want to discover nodes using multicast
    -Dcluster.join.type=multicast
     
    -Dsynchrony.bind=<SERVER_IP> 
    -Dsynchrony.service.url=<SYNCHRONY_URL> 
    -Dsynchrony.database.url=<YOUR_DATABASE_URL> 
    -Dsynchrony.database.username=<DB_USERNAME> 
    -Dsynchrony.database.password=<DB_PASSWORD>  
     
    synchrony.core 
    sql
    ```

-   Upgraded Applinks to 5.4.1

## Beta 1 - 18 August

-   We now bundle the Microsoft JDBC Driver for SQL Server. We plan to remove the current, open source jTDS driver in a future release. New Confluence installations will use the Microsoft driver for SQL Server by default. Existing installations will continue to use the jTDS driver after upgrading to Confluence 6.4.   You may want to test your add-on with both drivers. 
-   We've made some small changes to the database configuration step in the setup wizard. In addition to a Test Connection button, we also now check the collation of some databases before proceeding.  

## Milestone 14 - 14 August

-   We've made some changes to the terminology we use to describe pages and blog posts. A "draft" is a page or blog post that has never been published, and "unpublished changes" refers to a page or blog post that has been published previously, but has subsequent edits from one or more people that have not yet been published. 
-   People can now access drafts they have contributed to from the Recently worked on list in the dashboard. Previously only the person who created the draft could access it from the Drafts page in their profile. A "Draft" lozenge will indicate the page is a draft and has never been published. 
-   People can also now more easily identify pages and blog posts they have edited, but not yet re-published.  An "unpublished changes" lozenge in the Recently worked on list and on the page byline will indicate that there are changes that the user has contributed to, but not published. The lozenge is not visible to people who have not contributed to the unpublished changes. 
-   When a user exits a draft, using the Close button, they will be taken to their Recently worked on list in the Dashboard, regardless of whether they chose to keep or discard the draft. This is to help people resume working on that draft (if they chose to keep it), or work on another unfinished page.  When a user exits a published page with unpublished changes using the Close button, they return to the View page (regardless of whether they choose to keep or discard the unpublished changes). 

## Milestone 13 - 7 August

-   We now list the name of every person who contributed to a version in the page history (previously we only showed their avatars). 

## Milestone 10 - 31 July

There are no notable changes in this milestone. 

## Milestone 8 - 17 July

This is our first public milestone for Confluence 6.4. Not much has changed yet, but keep an eye on this page, as we'll be releasing a milestone every week in the lead up to Confluence 6.4.  

-   Updated to Hibernate 5.2.8


