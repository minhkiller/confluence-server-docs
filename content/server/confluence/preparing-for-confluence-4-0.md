---
aliases:
- /server/confluence/preparing-for-confluence-4.0-2031756.html
- /server/confluence/preparing-for-confluence-4.0-2031756.md
category: devguide
confluence_id: 2031756
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031756
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031756
date: '2017-12-08'
legacy_title: Preparing for Confluence 4.0
platform: server
product: confluence
subcategory: development resources
title: Preparing for Confluence 4.0
---
# Preparing for Confluence 4.0

{{% note %}}

If you would like to send us any feedback, please email it to <a href="mailto:confluence4@atlassian.com" class="external-link">confluence4@atlassian.com</a>.

{{% /note %}}

## Who should read this?

This documentation is intended for Confluence plugin developers. This documentation will walk through:

-   [Creating a New Confluence Macro](/server/confluence/creating-a-new-confluence-macro)
-   [Upgrading and Migrating an Existing Confluence Macro to 4.0](/server/confluence/upgrading-and-migrating-an-existing-confluence-macro-to-4-0)

## What's changing in Confluence 4.0?

Both the content storage format and the user experience for adding macros will change in Confluence 4.0. This release will introduce a new WYSIWYG editor with a completely different architecture based on XHTML. Also, the end user experience will change as follows:  

-   The new editor will be entirely WYSIWYG. There will be no wiki markup editor.
-   Instead of wiki markup code being displayed in WYSIWYG editor, we now display macros inside ﻿**[macro placeholders](https://dac-lf.prod.atl-paas.net/server/confluence/attachments/2031756/2228249.png)**. There are two macro placeholders, one for inline macros (they have no body content) and one for body macros.
-   Macro placeholders include a [macro property panel](https://dac-lf.prod.atl-paas.net/server/confluence/attachments/2031756/2228248.png) which allows you to click 'edit' inside the macro placeholder to launch the macro browser.  

We provide a set of tools to help you test your macro:

-   You can insert wiki markup using the ['Insert Wiki Markup' dialog](https://dac-lf.prod.atl-paas.net/server/confluence/attachments/2031756/2228245.png) in the 'Insert' menu. Confluence will convert the wiki markup to HTML and insert it into the editor for you. You will find this dialog useful in testing your macro migration.

## Frequently Asked Developer Questions

### What does the new XHTML storage format look like?

See <a href="http://confluence.atlassian.com/display/DOC/Confluence+Storage+Format" class="external-link">Confluence Storage Format</a>.

### Can we get access to the Confluence 4.0 source code?

If you are a plugin developer, an existing customer or partner you have access to the Confluence 4.0 release including the source from <a href="http://developer.atlassian.com/display/CONFDEV/Building+Confluence+From+Source+Code" class="external-link">the usual locations</a>.

### For customers concerned about getting "locked in," will there be a way to export the content in a usable format?

Confluence will continue to support exporting content to Word, PDF and HTML. Additional to this, space and site administrators can export content to XML. Finally, the Universal Wiki Converter is a plugin that allows customers to move from one wiki to another.

### Are nested macros supported in the new macro editor?

Yes, they are. Macros will be placed within other macros using "Macro Placeholders". Macro Placeholders are a visual representation of a macro on a page. Below is an example of what two would look like in the new editor:  
![](/server/confluence/images/nestedmacro.png)

### What is happening to user macros?

This question can be answered in two parts:

1.  Upgrading user macros from 3.x:  
    All user macros will continue to work when upgrading to 4.0. However, in order for end users to insert user macros Administrators <a href="http://confluence.atlassian.com/display/DOC/Writing+User+Macros#WritingUserMacros-Visibility" class="external-link">will need to enable their user macros in the Macro Browser</a> if they hadn't already done so in Confluence 3.4 or 3.5. This can be done via the User Macro Admin Console. Administrators have the option of only showing user macros to other administrators. It is strongly advised that you recreate your user macros in 4.0 format.
2.  Creating new user macros:  
    User macros no longer have an output type of wiki markup. Instead of wiki markup, they accept XHTML in the macro template. User macros can still embed other macros using the new XHTML syntax. See the <a href="http://confluence.atlassian.com/display/DOC/Writing+User+Macros" class="external-link">documentation</a>.

### Will it be possible to have pre/post hooks during the storage-&gt;view conversion process?

We acknowledge that would be very useful for things like security plugins, deciding if the end user can have the plugin rendered (even if they can view the page). However, we don't have this yet and are unsure if this will make it into the initial version of 4.0. We are focused on improving the editing experience rather than including more plugin points to the rendering subsystem (at this point). Your feedback for this would be appreciated.

### Will we have access to edit the source code?

Please see the <a href="http://confluence.atlassian.com/display/DOC/Confluence+4.0+Editor+FAQ#Confluence4.0EditorFAQ-WillyoubeabletoeditthesourceXHTMLcodedirectly%3F" class="external-link">editor FAQ page for this</a>.

### How come you can not support old remote API for content import/export? Can it go through the to/from XHTML converter?

One of the main reasons to switch to an XHTML based storage format is that we can't convert from HTML to wiki markup in a reliable way. The remote API will offer an additional method that allows for one way conversion of wiki markup to the XHTML based storage format.

## What other resources do we provide?

-   Please watch the <a href="http://confluence.atlassian.com/display/DOC/Development%20Releases" class="external-link">Development Releases</a> page for pre-release versions of Confluence 4.x that you can use for testing purposes.
-   For more information about changes in Confluence 4.0, from a user's point of view as well as a developer's point of view, please read ﻿﻿<a href="http://confluence.atlassian.com/display/DOC/Planning%20for%20Confluence%204.0" class="external-link">Planning for Confluence 4.0</a> and <a href="http://confluence.atlassian.com/display/DOC/Confluence%204.0%20Editor%20FAQ" class="external-link">Confluence 4.0 Editor FAQ</a>.

## Related Content

-   [Macro Tutorials for Confluence](/server/confluence/macro-tutorials-for-confluence)
