---
aliases:
- /server/confluence/macro-module-2031840.html
- /server/confluence/macro-module-2031840.md
category: reference
confluence_id: 2031840
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031840
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031840
date: '2017-12-08'
legacy_title: Macro Module
platform: server
product: confluence
subcategory: modules
title: Macro Module
---
# Macro Module

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Available:</p></td>
<td><p>Confluence 1.3 and later</p></td>
</tr>
</tbody>
</table>

Macros are Confluence code that can be invoked from inside a page by putting the name of the macro in curly brackets. Users of Confluence will be familiar with macros like {color} or {children} or {rss}. Thanks to the plugin system, it is easy to write and install new macros into a Confluence server.

NOTE: As of Confluence 4.0, all macros must contain metadata in order to function correctly in Confluence. See also: [Macro Tutorials for Confluence](/server/confluence/macro-tutorials-for-confluence)

{{% tip %}}

**Created a new macro or looking for macros?**  
Share your macros and find new plugins on the <a href="http://marketplace.atlassian.com" class="external-link">The Atlassian Marketplace</a>.  
  
**Need a simple macro? Consider a User Macro**  
If you want to create a macro that just inserts some boiler-plate text or performs simple formatting, you may only need a <a href="#user-macro" class="unresolved">User Macro</a>. User macros can be written entirely from within the Confluence web interface, and require no special installation or programming knowledge.  
  
**Make your macro work in the Macro Browser.**  
Make your macro [look good](/server/confluence/including-information-in-your-macro-for-the-macro-browser) in the macro browser.

**Update your macro for Confluence** **Mobile web.**  
See the tutorial for [Making your macro render in Confluence mobile](/server/confluence/making-your-macro-render-in-confluence-mobile).

{{% /tip %}}

## Adding a macro plugin

Macros are a kind of Confluence plugin module.

-   For more information about plugins in general, read [Confluence Plugin Guide](/server/confluence/confluence-plugin-guide).
-   For an introduction to writing your own plugins, read [Writing Confluence Plugins](/server/confluence/writing-confluence-plugins).

### First steps: Creating a very basic plugin

Make sure you have created your first macro plugin using our description, [How to Build an Atlassian Plugin](https://developer.atlassian.com/display/DOCS/Set+up+the+Atlassian+Plugin+SDK+and+Build+a+Project). That will save you a lot of time.

### The next step: Understanding a slightly more realistic macro plugin

The WoW plugin is a fun side-project created by Confluence developer Matthew Jensen. It loads information about World-of-Warcraft from a remote server, renders it on a Confluence page, and uses JavaScript for a nice hover-effect. You should download the source and learn more about it on the [WoW Macro explanation](/server/confluence/wow-macro-explanation) page.

### The Macro plugin module

Each macro is a plugin module of type "macro", packaged with whatever Java classes and other resources (i.e. Velocity templates) that the macro requires in order to run. Generally, similar macros are packaged together into a single plugin, for ease of management. Here is an example `atlassian-plugin.xml` file

``` xml
<atlassian-plugin name='Task List Macros' key='confluence.extra.tasklist'>
    <plugin-info>
        <description>Macros to generate simple task lists</description>
        <vendor name="Atlassian Software Systems" url="http://www.atlassian.com"/>
        <version>1.3</version>
    </plugin-info>

    <macro name='tasklist' class='com.atlassian.confluence.extra.tasklist.TaskListMacro'
           key='tasklist'>
        <description>Creates a very simple task list, with user checkable tasks</description>
    </macro>

    <!-- more macros... -->
</atlassian-plugin>
```

The **name** of the macro defines how it will be referenced from the page. So if you define your macro as having `name="tasklist"`, the macro will be called from the page as {tasklist}.

### The Macro plugin module implementing class

The **class** attribute of the macro defines what Java class will be used to process that macro. This is the class you need to write in order for the macro to function. It must implement the `com.atlassian.renderer.v2.macro.Macro` interface.

A more complete guide to writing macros can be found in [Writing Macros](/server/confluence/writing-macros).

## Using a Velocity Template

To use a Velocity template to provide the output of your macro, see [Rendering Velocity templates in a macro](/server/confluence/rendering-velocity-templates-in-a-macro).

## Example macro plugins

The source-code of a number of macros (some of which are already built and packaged with Confluence) can be found in the `plugins` directory of your Confluence distribution. You can modify these macros (consistent with the Confluence license). The most interesting macros to read if you're looking at writing your own are probably:

-   `tasklist` - a simple macro that stores its state in a page's PropertySet
-   `userlister` - a macro that works in combination with an [event listener](/server/confluence/event-listener-module) to list logged-in users
-   `livesearch` - a macro that leverages Javascript and XMLHttpRequest in combination with an [XWork plugin](/server/confluence/xwork-webwork-module) to handle the server-side interaction.
-   `graphviz` - a macro that interacts with an external, non-Java tool

##### RELATED TOPICS

-   [Plugin Tutorial - Writing Macros for Confluence](/server/confluence/writing-macros-for-pre-4-0-versions-of-confluence)
-   [Including Information in your Macro for the Macro Browser](/server/confluence/including-information-in-your-macro-for-the-macro-browser)
-   [Preventing XSS issues with macros in Confluence 4.0](/server/confluence/preventing-xss-issues-with-macros-in-confluence-4-0)
-   [WoW Macro explanation](/server/confluence/wow-macro-explanation)
-   [Writing Macros](/server/confluence/writing-macros)
