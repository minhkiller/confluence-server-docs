---
aliases:
- /server/confluence/preparing-for-confluence-6.1-44811739.html
- /server/confluence/preparing-for-confluence-6.1-44811739.md
category: devguide
confluence_id: 44811739
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=44811739
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=44811739
date: '2017-12-08'
legacy_title: Preparing for Confluence 6.1
platform: server
product: confluence
subcategory: development resources
title: Preparing for Confluence 6.1
---
# Preparing for Confluence 6.1

This documentation is intended for Confluence developers who want to ensure that their existing plugins and add-ons are compatible with Confluence 6.1.

We plan to release frequent milestones during the development of Confluence 6.1. Watch this page to find out when a new milestone is available and what's changed.  We won't be publishing formal release notes over at <a href="http://confluence.atlassian.com/display/DOC/Confluence+Development+Releases" class="external-link">Development Releases</a> until we release a beta.

{{% note %}}

**Latest milestone**

|              |           |                                                                                                         |
|--------------|-----------|--------------------------------------------------------------------------------------------------------:|
| 09 Mar 2017  | 6.1.0-rc1 |  <a href="https://www.atlassian.com/software/confluence/download-eap" class="external-link">Download</a>|

{{% /note %}}

**Issues with this milestone?**

Hit the Feedback button on the Confluence EAP header or <a href="https://jira.atlassian.com/secure/CreateIssueDetails!init.jspa?pid=10470&amp;issuetype=10000&amp;components=43692&amp;summary=Confluence+6.1+EAP+Feedback" class="external-link">raise an issue</a> to tell us about it. 

## Release Candidate - 9 March

We're getting close to a final release! This release candidate has the following changes. 

-   We've reduced the number of properties you need to pass when starting Synchrony standalone for Confluence Data Center. The essentials remain the same, but you no longer need to pass so many properties whose values do not matter.  You can find the updated start Synchrony command at <a href="https://confluence.atlassian.com/display/CONFEAP/Installing+Confluence+Data+Center" class="external-link">Installing Confluence Data Center</a>. 
-   Bug fixes and improvements to the internal Synchrony proxy. 

#### Try our AWS Cloud Formation Template

Want to take our new Confluence Data Center Cloud Formation Template for a test drive? It currently uses Confluence 6.1.0-beta2, so we don't recommend using it to spin up a production site, but we'd love your feedback if you are keen to try it out.  The template uses EFS for the shared home directory, so you'll need to use an <a href="https://aws.amazon.com/about-aws/global-infrastructure/regional-product-services/" class="external-link">AWS region that offers EFS</a>. You'll also need a VPC with at least two subnets and an EC2 Key Pair already set up.

To deploy Confluence Data Center using the Cloud Formation Template:

1.  In the AWS Console **create a new stack**.
2.  Use the following Amazon S3 link to the Confluence Data Center Cloud Formation Template.  
      

    ``` xml
    https://s3.amazonaws.com/quickstart-reference/atlassian/confluence/latest/templates/ConfluenceDataCenter.template
    ```

3.  Provide the following details:
    1.  Stack name

    2.  Confluence version
    3.  Master password
    4.  Database password
    5.  Your VPC
    6.  Two external and two internal subnets (must be in your VPC and in different availability zones).
    7.  Your EC2 Key Pair. 
    8.  Leave the minimum and maximum number of nodes at the default for now - you'll add these later.  
          

4.  Once the create process is finished, grab the URL from the **Outputs** section and complete the **Confluence setup wizard**.    
    Some steps will be automatically completed for you (including the database configuration and clustering steps).    
      

    You'll need a Data Center license to proceed. If you don't have one, you can use this evaluation license, which is good for 72 hours.

    ``` javascript
    AAABCA0ODAoPeNpdkFFPg0AQhN/7Ky7xmeag0mqTS1Q4o6a2hGLVx/Wy6CVwkL2D2H8vcpjUvu5M5
    tuZi3vSbI8tCyMW8nW8WEcxy9KCRTy8nKnGlPOkMQ6Uk8+gK9GqjgjNDbgKrNVg5qqpvW/w6B6Fo
    w79IQNyBmkLNQp/ScFBgsYhndg2WqGxWBxbHK2pPMjNLpO5l3f0CUZbcLox4vYP6zXZQ9V5pYTKT
    oHDowPCgFEov1tNxwGLIltFD7M9Uo/0mIq7ZZwHq7dDHly9Fnnw9L6ccNuu/kDalS8WyYqQcz6V6
    Uh9gcUx63ecgF8H4eJfhXPa6XxjtQy6iiXjgpNIOL5/nvoDg6WJ0jAtAhUAkHfjbVv1VyzHw7Ei6
    C8LgiS1qs8CFB/W616OtcXLSlrEa+9A5vDwEXDeX02dh
    ```

5.  Go to![](/server/confluence/images/cog.png) &gt; **General Configuration** &gt; **Clustering** and confirm you can see one cluster node. 

6.  In the AWS Console, choose **Update Stack** and increase the minimum and maximum number of Confluence cluster nodes to 2.  Leave the Synchrony nodes at 0 for now. 

7.  In Confluence, go to ![](/server/confluence/images/cog.png) &gt; **General Configuration** &gt; **Clustering** and confirm you now have two cluster nodes.    
    It can take a minute or two for Confluence to detect the new node and connect to it. 

8.  In the AWS Console, **Update Stack** and increase the minimum and maximum number of Synchrony cluster nodes to 2. 

9.  In Confluence, go to ![](/server/confluence/images/cog.png) &gt; **General Configuration** &gt; **Collaborative Editing** and change the Collaborative editing mode to **On**.   
    Wait for the Synchrony Status to change to **Running**.  This can take a minute. 

That's it! You're good to start editing pages.   

We will also be making a Quick Start available with Confluence 6.1, which will automatically create your VPC - just bring yourself and your Key Pair, and it'll do the rest.

Want to know more? Check out the [Atlassian Confluence Data Center Quick Start guide](https://dac-lf.prod.atl-paas.net/server/confluence/attachments/44811739/48039042.pdf) (PDF) and <a href="https://confluence.atlassian.com/display/CONFEAP/Running+Confluence+Data+Center+in+AWS" class="external-link">Running Confluence Data Center in AWS</a> our EAP documentation.

If you have questions or any feedback, please or <a href="https://jira.atlassian.com/secure/CreateIssueDetails!init.jspa?pid=10470&amp;issuetype=10000&amp;components=43692&amp;summary=Confluence+6.1+EAP+Feedback" class="external-link">raise an issue</a> to tell us about it.

## Beta 2 - 17 February

-   We have made some changes to ensure the internal Synchrony proxy is available with or without a Confluence context path. This means that it is no longer necessary to add /synchrony-proxy to any reverse proxy config.  This improvement did involve a change to the setenv.bat / setenv.sh and server.xml files, so if you have any additional customisations in those files, you'll need to re-add them after upgrade (and not overwrite the files themselves). 
-   XML HTTP Request (XHR) fallback, which was experimental in Confluence 6.0, is now on by default. XHR fallback is used when a user cannot connect to Confluence via a WebSocket. 
-   Running Synchrony in Data Center requires some additional properties in order for it to start in Confluence 6.1. Please note that these properties don't have any affect in the Server distribution, so we are working on fixing this so these properties don't have to be supplied for the final release. Please see <a href="https://confluence.atlassian.com/confeap/installing-confluence-data-center-480347128.html#InstallingConfluenceDataCenter-2.SetupSynchrony" class="external-link">this page</a> for the updated command to start Synchrony when trying the beta in Data Center.

 Check out the full <a href="https://confluence.atlassian.com/display/DOC/Confluence+6.1.0+beta+release+notes" class="external-link">beta release notes</a>. 

## Milestone 19 - 9 February

-   You can now start Confluence with all non-system add-ons disabled or with selected add-ons disabled by specifying the add-on key. This can be useful for troubleshooting when Confluence will not start up. 

    To temporarily disable all add-ons or specific add-ons on Linux:

    ``` javascript
    $ ./start-confluence.sh --disable-all-addons
    $ ./start-confluence.sh --disable-addons=com.atlassian.test.plugin
    ```

    To temporarily disable all add-ons or specific add-ons on Windows:

    ``` javascript
    > start-confluence.bat /disablealladdons
    > start-confluence.bat /disableaddon=com.atlassian.test.plugin
    ```

    You can disable multiple add-ons using a colon separated list.

    These parameters are applied at startup only, they do not persist. These parameters do not work with Confluence Data Center.

-   We now bundle the SAML for Atlassian Data Center add-on, which means you can connect Confluence Data Center to a SAML 2.0 identity provider for authentication and single sign-on. 
-   We've worked with AWS to create a Quick Start for Confluence Data Center 6.1. The Quick Start will help you to get a Data Center cluster running in AWS (complete with database and load balancer) in minutes. The following diagram depicts the architecture.   
       
    <img src="/server/confluence/images/aws-datacenter-architecture.png" width="400" />   
      
    More info, and an AWS Quick Start Guide, will be provided closer to release.   
-   Embedded Crowd (user management) upgraded to 2.3.

## Milestone 15 - 30 January

-   No notable changes in this milestone. 

## Milestone 14 - 24 January

-   We now bundle an official Chinese (simplified) language pack. 
-   New bundle of blueprints to help teams using the <a href="https://www.atlassian.com/team-playbook" class="external-link">Atlassian Team Playbook</a>. 
-   [Atlassian plugin framework](https://developer.atlassian.com/display/DOCS/_Version+Compatibility) upgraded to 4.4.3
-   [Shared Access Layer](https://developer.atlassian.com/display/DOCS/_Version+Compatibility+for+Shared+Access+Layer) (SAL) upgraded to 3.0.8
