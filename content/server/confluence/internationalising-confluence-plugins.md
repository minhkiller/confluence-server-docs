---
aliases:
- /server/confluence/internationalising-confluence-plugins-2031863.html
- /server/confluence/internationalising-confluence-plugins-2031863.md
category: devguide
confluence_id: 2031863
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031863
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031863
date: '2017-12-08'
guides: guides
legacy_title: Internationalising Confluence Plugins
platform: server
product: confluence
subcategory: learning
title: Internationalising Confluence plugins
---
# Internationalising Confluence plugins

Text in Confluence plugins can be internationalised to cater for a variety of locales or languages. To do this, you will need to create a translated copy of the properties file(s) for each plugin and bundle these inside your language pack plugin. Having a properties file in each plugin allows plugin authors to provide internationalised plugins without having to add their i18n keys to Confluence's core source.

Confluence comes bundled with a few plugins that are stored in a file called `atlassian-bundled-plugins.zip`. The basic process for translating a plugin is:

1.  Extract this zip to a directory
2.  Extract the plugin JAR
3.  Locate the properties file which contains i18n keys (examples are below)
4.  Copy this file to the same location in your plugin. For example, if it is in path/to/file.properties, it needs to be in the same place in your language pack JAR with a locale extension: path/to/file\_jp\_JP.properties
5.  Repeat this for all plugins that can be internationalised

Below is a list of bundled plugins that can be internationalised and the properties file you will need to translate (correct as of Confluence 2.7):

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Plugin Name</p></th>
<th><p>Filename</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Usage Statistics Plugin</p></td>
<td><p>usage-tracking-plugin-&lt;version&gt;.jar</p>
<p><strong>I18N Resources:</strong></p>
<p>resources/stats/usage.properties</p></td>
</tr>
<tr class="even">
<td><p>Atlassian Plugin Repository</p></td>
<td><p>atlassian-plugin-repository-confluence-plugin-&lt;version&gt;.jar</p>
<p><strong>I18N Resources:</strong></p>
<p>resources/i18n/repository-templates.properties</p></td>
</tr>
<tr class="odd">
<td><p> </p></td>
<td><p><strong>I18N Resources:</strong></p>
<p>resources/i18n/repository-macros.properties</p></td>
</tr>
<tr class="even">
<td><p>Clickr Theme</p></td>
<td><p>clickr-theme-plugin-&lt;version&gt;.jar</p>
<p><strong>I18N Resources:</strong></p>
<p>clickr.properties</p></td>
</tr>
<tr class="odd">
<td><p>Mail Page Plugin</p></td>
<td><p>mail-page-plugin-&lt;version&gt;.jar</p>
<p><strong>I18N Resources:</strong></p>
<p>resources/mailpage.properties</p></td>
</tr>
<tr class="even">
<td><p>Social Bookmarking Plugin</p></td>
<td><p>socialbookmarking-&lt;version&gt;.jar</p>
<p><strong>I18N Resources:</strong></p>
<p>com/atlassian/confluence/plugins/socialbookmarking/i18n.properties</p></td>
</tr>
<tr class="odd">
<td><p>WebDAV Plugin</p></td>
<td><p>webdav-plugin-&lt;version&gt;.jar</p>
<p><strong>I18N Resources:</strong></p>
<p>com/atlassian/confluence/extra/webdav/text.properties</p></td>
</tr>
<tr class="even">
<td><p>Charting Plugin</p></td>
<td><p>chart-plugin-&lt;version&gt;.jar</p>
<p><strong>I18N Resources:</strong></p>
<p>chart.properties</p></td>
</tr>
<tr class="odd">
<td><p>TinyMCE (Rich Text) Editor</p></td>
<td><p>atlassian-tinymce-plugin-&lt;version&gt;.jar</p>
<p><strong>I18N Resources:</strong></p>
<p>com/atlassian/confluence/extra/tinymceplugin/tinymce.properties</p></td>
</tr>
<tr class="even">
<td><p>Advanced Macros</p></td>
<td><p>confluence-advanced-macros-&lt;version&gt;.jar</p>
<p><strong>I18N Resources:</strong></p>
<p>resources/com/atlassian/confluence/plugins/macros/advanced/i18n.properties</p></td>
</tr>
<tr class="odd">
<td><p>Dashboard Macros</p></td>
<td><p>confluence-dashboard-macros-&lt;version&gt;.jar</p>
<p><strong>I18N Resources:</strong></p>
<p>resources/com/atlassian/confluence/plugins/macros/dashboard/i18n.properties</p></td>
</tr>
</tbody>
</table>

Below are the system plugins (found in `confluence/WEB-INF/lib/`) that can be internationalised and the properties file you will need to translate:

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Plugin Name</p></th>
<th><p>Filename</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Information Plugin</p></td>
<td><p>confluence-information-plugin-&lt;version&gt;.jar</p>
<p><strong>I18N Resources:</strong> information.properties</p></td>
</tr>
<tr class="even">
<td><p>Layout Plugin</p></td>
<td><p>confluence-layout-plugin-&lt;version&gt;.jar</p>
<p><strong>I18N Resources:</strong> layout.properties</p></td>
</tr>
<tr class="odd">
<td><p>Livesearch Plugin</p></td>
<td><p>confluence-livesearch-plugin-&lt;version&gt;.jar</p>
<p><strong>I18N Resources:</strong> livesearch.properties</p></td>
</tr>
<tr class="even">
<td><p>Dynamic Tasklist Plugin</p></td>
<td><p>confluence-dynamictasklist-plugin-&lt;version&gt;.jar</p>
<p><strong>I18N Resources:</strong> dynamictasklist.properties</p></td>
</tr>
</tbody>
</table>
