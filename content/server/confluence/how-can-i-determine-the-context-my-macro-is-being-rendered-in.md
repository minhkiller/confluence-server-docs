---
aliases:
- /server/confluence/2031673.html
- /server/confluence/2031673.md
category: devguide
confluence_id: 2031673
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031673
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031673
date: '2017-12-08'
legacy_title: How can I determine the context my macro is being rendered in?
platform: server
product: confluence
subcategory: faq
title: How can I determine the context my macro is being rendered in?
---
# How can I determine the context my macro is being rendered in?

For Confluence 2.10 (remember that?), we converted the display of the <a href="http://confluence.atlassian.com/display/DOC/JIRA+Issues+Macro" class="external-link">Jira Issues Macro</a> from using a static HTML table to using a table infused with jQuery goodness. Now we could add features that wouldn't have been possible without JavaScript, like the ability to sort issues in the page without even reloading. That was pretty cool, but it also meant we had a new problem to deal with: macros can be rendered in places that can't render JavaScript, such as in a feed reader or an email notification. In those cases, our beautifully redesigned macro would look something like a puddle of goo.

We thought about how to get around this new problem, and decided the best approach would be to make it possible for macros to find out if they are being rendered in an email or a feed, so they can display themselves appropriately. It was already possible for macros to find out if they are being rendered in a PDF document or several other contexts. In Confluence 2.10, we made it possible for macros to find out that they were being displayed in an email or feed, an addition to the previously defined contexts. Previously, macros being viewed in an email or feed reader would have just had the render type "display", which is the default.

Now the Jira Issues Macro is able to render itself differently in display versus feed modes:  
<img src="/server/confluence/images/jiraissues-in-page-cropped.png" class="confluence-thumbnail" /> <img src="/server/confluence/images/jiraissues-in-feed-cropped.png" class="confluence-thumbnail" />

Okay, so how can you find out the current render context from within your macro? When creating a plugin that includes a <a href="http://confluence.atlassian.com/display/DOC/Macro+Plugins" class="external-link">macro module</a>, you return the HTML that the macro will display from the execute() method of the macro class. One of the parameters to the execute() method, the one with type RenderContext, can be used to determine how the macro is being rendered.

Here's a sample execute method from a macro that prints out the current render context type:

``` java
public String execute(Map parameters, String body, RenderContext renderContext)
{
    if(RenderContext.FEED.equals(renderContext.getOutputType()))
        return "FEED render type";
    else if(RenderContext.EMAIL.equals(renderContext.getOutputType()))
        return "EMAIL render type";
    else if(RenderContext.HTML_EXPORT.equals(renderContext.getOutputType()))
        return "HTML_EXPORT render type";
    else if(RenderContext.PREVIEW.equals(renderContext.getOutputType()))
        return "PREVIEW render type";
    else if(RenderContext.DISPLAY.equals(renderContext.getOutputType()))
        return "DISPLAY render type";
    else if(RenderContext.PDF.equals(renderContext.getOutputType()))
        return "PDF render type";
    else if(RenderContext.WORD.equals(renderContext.getOutputType()))
        return "WORD render type";
    else
        return "some other render type";
}
```

If you used this sample macro on a page you were editing (by first installing the plugin that contains it), you could visit the preview tab to see it output "PREVIEW render type". In the case of a more complex macro, you could, say, disable some UI elements when the RenderContext.PREVIEW.equals(renderContext.getOutputType()) check is true. Using these checks is exactly how the Jira Issues Macro decides whether to render itself using JavaScript or just stick with a basic HTML version.

### Related

<a href="http://confluence.atlassian.com/display/DOC/Event+Listener+Plugins" class="external-link">Event Listener Plugins</a>
