---
aliases:
- /server/confluence/preparing-for-confluence-5.3-22512045.html
- /server/confluence/preparing-for-confluence-5.3-22512045.md
category: devguide
confluence_id: 22512045
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=22512045
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=22512045
date: '2017-12-08'
legacy_title: Preparing for Confluence 5.3
platform: server
product: confluence
subcategory: development resources
title: Preparing for Confluence 5.3
---
# Preparing for Confluence 5.3

This documentation is intended for Confluence developers who want to ensure that their existing plugins and add-ons are compatible with Confluence 5.3, and to learn about the new features for developing add-ons in Confluence 5.3.

## Space Blueprints

Continuing on from the [Blueprints API](/server/confluence/confluence-blueprints) we introduced in Confluence 5.1, we will be introducing the concept of Space Blueprints. It will enable you to hook into the new create space dialog and allow users to easily create spaces of a certain type like Team Spaces (which will be a bundled Space Blueprint in 5.3).

To help you get started we have created a tutorial on [Space Blueprints](/server/confluence/space-blueprints) .

## Renamable users

As previously announced, with Confluence 5.2 we introduced schema and API changes necessary to support the ability to change usernames. Users now have a new, unique, permanent key as well as the already-existing, unique, changeable username.

See  [Renamable Users in Confluence 5.2](/server/confluence/renamable-users-in-confluence-5-2)  and  [Writing a Plugin Upgrade Task to Migrate User Data to New User Keys](/server/confluence/writing-a-plugin-upgrade-task-to-migrate-user-data-to-new-user-keys)  for more information about  migrating your plugin's existing data to user keys instead of usernames. 

## Macro storage format changes

There have been some changes to the storage format for Macros.  Macros that use a `confluence-content` parameter type should now expect an &lt; `ac:link` &gt; in its field instead of a wikimarkup link, and macros using `username`, `spacekey`, `url` or `attachment` parameter types will now contain the corresponding resource identifier element in its body.  See <a href="https://confluence.atlassian.com/display/DOC/Confluence+Storage+Format#ConfluenceStorageFormat-Resourceidentifiers" class="external-link">Confluence Storage Format</a> and <a href="https://confluence.atlassian.com/display/DOC/Confluence+Storage+Format+for+Macros" class="external-link">Confluence Storage Format for Macros</a> for examples of the changes. 

The in-memory representation of macros has also changed in the following ways:

-   There is a new `typedParameters` property that contains Links and ResourceIdentifiers, in the same way that the storage format for structured-macros now does.
-   There is a new `storageVersion` property that indicates what format the definition was parsed from (`ac:macro` elements correspond to version 1, `ac:structured-macro` elements correspond to version 2).
-   Both `typeParameters` and the older pure-string parameters properties now contain the default parameter, under the empty key `("")`. This brings the MacroDefinition into line with the MacroParameters metadata collected from xhtml-macro modules declared in `atlassian-plugin.xml`. Note that this may leak through to older wikimarkup macros causing them to have `"0"` and `""` keys in their parameter lists.

Milestone releases for developers

Please watch the <a href="http://confluence.atlassian.com/display/DOC/Confluence+Development+Releases" class="external-link">Development Releases</a> page for pre-release versions of Confluence 5.3 that you can use for testing purposes.

### Have questions?

If you have any questions about preparing for Confluence 5.3, please register for an account on <a href="https://answers.atlassian.com/" class="external-link">Atlassian Answers</a>, our community Q&A resource, and <a href="https://answers.atlassian.com/questions/ask/?tags=confluence-development" class="external-link">ask a question</a>. We'll be keeping an eye on things to help out as soon as possible.

