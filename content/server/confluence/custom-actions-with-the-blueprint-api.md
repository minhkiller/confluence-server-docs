---
aliases:
- /server/confluence/custom-actions-with-the-blueprint-api-22020367.html
- /server/confluence/custom-actions-with-the-blueprint-api-22020367.md
category: reference
confluence_id: 22020367
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=22020367
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=22020367
date: '2017-12-08'
legacy_title: Custom actions with the Blueprint API
platform: server
product: confluence
subcategory: api
title: Custom actions with the Blueprint API
---
# Custom actions with the Blueprint API

The `confluence-create-content-plugin` provides an  `AbstractCreateBlueprintPageAction`  that you can extend to add custom behavior.  To use this module, make sure you pom.xml includes the following dependency:

``` xml
      <dependency>
            <groupId>com.atlassian.confluence.plugins</groupId>
            <artifactId>confluence-create-content-plugin</artifactId>
            <version>${create-content.version}</version>
            <scope>provided</scope>
        </dependency>
```

This dependency provides a couple of useful methods for working with Blueprints:

-   `populateBlueprintPage` loads the blueprint content template and generates a blueprint page from the template and the context.
-   `getOrCreateIndexPage` gets the index page for the blueprint or creates one if it doesn't exist, and pins it to the sidebar.

For more details on how to write and link an action in Confluence, see [XWork-WebWork Module](https://developer.atlassian.com/display/CONFDEV/XWork-WebWork+Module#XWork-WebWorkModule-WebWorkModule-WritinganAction).

{{% note %}}

You cannot, at this stage, use custom names for your XWork actions that render the editor. You must use createpage and docreatepage. Using anything else prevents the buttons on the bottom left of the Confluence Editor (say for altering page level restrictions) from displaying.

{{% /note %}}{{% warning %}}

Deprecation Warning

 The `AbstractCreateBlueprintPageAction` is no longer being used internally and is slated for removal in a future release. Please let us know via the Feedback form if your plugin currently extends this action, or if you plan to extend it.

{{% /warning %}}

## BlueprintManager Interface

If you need to interact with the Blueprints API at a low level, you can use this component:

``` xml
<component-import key="blueprintManager" interface=" com.atlassian.confluence.plugins.createcontent.actions.BlueprintManager" />
```

 This components gives access to this method:

<table>
<colgroup>
<col style="width: 30%" />
<col style="width: 70%" />
</colgroup>
<thead>
<tr class="header">
<th>Method</th>
<th> </th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><code> createAndPinIndexPage </code></td>
<td><p>Creates a index page if required for this blueprint, and pins it to the Space Sidebar.</p></td>
</tr>
</tbody>
</table>

## UserBlueprintConfigManager

If you need to interact with the Blueprints configuration, you can use this component:

``` xml
<component-import key="userBlueprintConfigManager" interface="com.atlassian.confluence.plugins.createcontent.extensions.UserBlueprintConfigManager"/>
```

This component gives access to this method:

<table>
<colgroup>
<col style="width: 30%" />
<col style="width: 70%" />
</colgroup>
<thead>
<tr class="header">
<th>Method</th>
<th> </th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><code> setBlueprintCreatedByUser </code></td>
<td><p>Flags that a Blueprint of a certain type has been created by a given user</p></td>
</tr>
</tbody>
</table>

## BlueprintContentGenerator Interface

If you need to interact with the Blueprints API at a low level, you can also use this component to get access to the render context:

``` xml
<component-import key="contentGenerator" interface=" com.atlassian.confluence.plugins.createcontent.actions.BlueprintContentGenerator" />
```

 This components gives access to these two methods:

<table>
<colgroup>
<col style="width: 30%" />
<col style="width: 70%" />
</colgroup>
<thead>
<tr class="header">
<th>Component Key</th>
<th>Methods</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><code> createIndexPageObject </code></td>
<td><p>Creates a content page object given a plugin template and a render context.</p></td>
</tr>
<tr class="even">
<td><p><code> generateBlueprintPageObject </code></p></td>
<td><p>Creates an index page object given a plugin template and a render context.</p></td>
</tr>
</tbody>
</table>
