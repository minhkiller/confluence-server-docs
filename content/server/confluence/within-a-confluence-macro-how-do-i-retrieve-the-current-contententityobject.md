---
aliases:
- /server/confluence/2031785.html
- /server/confluence/2031785.md
category: devguide
confluence_id: 2031785
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031785
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031785
date: '2017-12-08'
legacy_title: Within a Confluence macro, how do I retrieve the current ContentEntityObject?
platform: server
product: confluence
subcategory: faq
title: Within a Confluence macro, how do I retrieve the current ContentEntityObject?
---
# Within a Confluence macro, how do I retrieve the current ContentEntityObject?

### <img src="http://confluence.atlassian.com/images/icons/favicon.png" class="confluence-external-resource" /> Within a Confluence macro, how do I retrieve the current ContentEntityObject?

You can retrieve the current <a href="http://www.atlassian.com/software/confluence/docs/api/latest/com/atlassian/confluence/core/ContentEntityObject.html" class="external-link">ContentEntityObject</a> (ie the content object this macro is a part of), as follows:

``` javascript
public String execute(Map parameters, String body, RenderContext renderContext) throws MacroException {
    // retrieve a reference to the body object this macro is in
    if (!(renderContext instanceof PageContext))
    {
        throw new MacroException("This macro can only be used in a page");
    }
    ContentEntityObject contentObject = ((PageContext)renderContext).getEntity();
    ...
```

Note that this method might return null if there is no current content object (for example if you are previewing a page that has not been added yet, or if a remote user is rendering a fragment of notation).
