---
aliases:
- /server/confluence/confluence-build-information-2818672.html
- /server/confluence/confluence-build-information-2818672.md
category: devguide
confluence_id: 2818672
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2818672
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2818672
date: '2017-12-08'
legacy_title: Confluence Build Information
platform: server
product: confluence
subcategory: development resources
title: Confluence build information
---
# Confluence build information

Sometimes it is helpful to know the exact versions, dates and build numbers of Confluence releases.Here is a list of all the build information we've collected about Confluence - please add any missing information you have. The information is ordered by **date** and not necessarily version number, this is especially true for DRs of upcoming versions

<table style="width:100%;">
<colgroup>
<col style="width: 14%" />
<col style="width: 14%" />
<col style="width: 14%" />
<col style="width: 14%" />
<col style="width: 14%" />
<col style="width: 14%" />
<col style="width: 14%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Version</p></th>
<th><p>Codename</p></th>
<th><p>System Favourite Colour</p></th>
<th><p>Release Date</p></th>
<th><p>Build Number</p></th>
<th>Marketplace Builder Number</th>
<th><p>Major features/API/Code Changes</p></th>
</tr>
</thead>
<tbody>
<tr class="even">
<td>6.6.2</td>
<td>Damien Darhk</td>
<td>Lapis Lazuli</td>
<td>23 Jan 2017 </td>
<td>7502</td>
<td>8438</td>
<td><a href="https://confluence.atlassian.com/display/DOC/Confluence+6.6+Release+Notes" class="external-link">Confluence 6.6.0 Release Notes</a></td>
</tr>
<tr class="even">
<td>6.6.1</td>
<td>Damien Darhk</td>
<td>Lapis Lazuli</td>
<td>08 Jan 2017 </td>
<td>7502</td>
<td>8437</td>
<td><a href="https://confluence.atlassian.com/display/DOC/Confluence+6.6+Release+Notes" class="external-link">Confluence 6.6.0 Release Notes</a></td>
</tr>
<tr class="odd">
<td>6.6.0</td>
<td>Damien Darhk</td>
<td>Lapis Lazuli</td>
<td>12 Dec 2017 </td>
<td>7502</td>
<td>8435</td>
<td><a href="https://confluence.atlassian.com/display/DOC/Confluence+6.6+Release+Notes" class="external-link">Confluence 6.6.0 Release Notes</a></td>
</tr>
<tr class="even">
<td>6.5.2</td>
<td> </td>
<td> </td>
<td>04 Dec 2017 </td>
<td>7502</td>
<td>8237</td>
<td><a href="https://confluence.atlassian.com/display/DOC/Confluence+6.5+Release+Notes" class="external-link">Confluence 6.5.2 Release Notes</a></td>
</tr>
<tr class="odd">
<td>6.5.1</td>
<td> </td>
<td> </td>
<td>20 Nov 2017 </td>
<td>7502</td>
<td>8236</td>
<td><a href="https://confluence.atlassian.com/display/DOC/Confluence+6.5+Release+Notes" class="external-link">Confluence 6.5.1 Release Notes</a></td>
</tr>
<tr class="even">
<td>6.5.0</td>
<td> </td>
<td> </td>
<td>01 Nov 2017 </td>
<td>7502</td>
<td>8235</td>
<td><a href="https://confluence.atlassian.com/display/DOC/Confluence+6.5+Release+Notes" class="external-link">Confluence 6.5.0 Release Notes</a></td>
</tr>
<tr class="odd">
<td>6.4.3</td>
<td> </td>
<td> </td>
<td>23 Oct 2017 </td>
<td>7401</td>
<td>8029</td>
<td><a href="https://confluence.atlassian.com/display/DOC/Issues+resolved+in+6.4.3" class="external-link">Confluence 6.4.3 Release Notes</a></td>
</tr>
<tr class="even">
<td>6.4.2</td>
<td> </td>
<td> </td>
<td>10 Oct 2017 </td>
<td>7401</td>
<td>8026</td>
<td><a href="https://confluence.atlassian.com/display/DOC/Issues+resolved+in+6.4.2" class="external-link">Confluence 6.4.2 Release Notes</a></td>
</tr>
<tr class="odd">
<td>6.4.1</td>
<td> </td>
<td> </td>
<td> 25 Sep 2017 </td>
<td>7401</td>
<td>8025</td>
<td><a href="https://confluence.atlassian.com/display/DOC/Issues+resolved+in+6.4.1" class="external-link">Confluence 6.4.1 Release Notes</a></td>
</tr>
<tr class="even">
<td>6.4.0</td>
<td> </td>
<td>Spring Cyan</td>
<td>06 Sep 2017 </td>
<td>7400</td>
<td>8024</td>
<td><a href="https://confluence.atlassian.com/display/DOC/Confluence+6.4+Release+Notes" class="external-link">Confluence 6.4.0 Release Notes</a></td>
</tr>
<tr class="odd">
<td>6.3.4</td>
<td> </td>
<td> </td>
<td>05 Sep 2017 </td>
<td>7202</td>
<td>7818</td>
<td><a href="https://confluence.atlassian.com/display/DOC/Issues+resolved+in+6.3.4" class="external-link">Confluence 6.3.4 Release Notes</a></td>
</tr>
<tr class="even">
<td>6.3.3</td>
<td> </td>
<td> </td>
<td> 16 Aug 2017 </td>
<td>7202</td>
<td>7817</td>
<td><a href="https://confluence.atlassian.com/doc/issues-resolved-in-6-3-3-935370667.html" class="external-link">Confluence 6.3.3 Release Notes</a></td>
</tr>
<tr class="odd">
<td>6.3.2</td>
<td> </td>
<td> </td>
<td>03 Aug 2017 </td>
<td>7202</td>
<td>7816</td>
<td><a href="https://confluence.atlassian.com/doc/issues-resolved-in-6-3-2-933695020.html" class="external-link">Confluence 6.3.2 Release Notes</a></td>
</tr>
<tr class="even">
<td>6.3.1</td>
<td> </td>
<td>Spring Cyan</td>
<td>12 Jul 2017 </td>
<td>7202</td>
<td>7813</td>
<td><a href="https://confluence.atlassian.com/doc/confluence-6-3-release-notes-909642701.html" class="external-link">Confluence 6.3.1 Release Notes</a></td>
</tr>
<tr class="odd">
<td>6.2.4</td>
<td> </td>
<td> </td>
<td> 11 Jul 2017 </td>
<td>7202</td>
<td>7616</td>
<td><a href="https://confluence.atlassian.com/doc/confluence-6-2-release-notes-894219485.html" class="external-link">Confluence 6.2.4 Release Notes</a></td>
</tr>
<tr class="even">
<td>6.2.3</td>
<td> </td>
<td> </td>
<td>21 Jun 2017 </td>
<td>7201</td>
<td>7615</td>
<td><a href="https://confluence.atlassian.com/doc/issues-resolved-in-6-2-3-918257892.html" class="external-link">Confluence 6.2.3 Release Notes</a></td>
</tr>
<tr class="odd">
<td>6.2.2</td>
<td> </td>
<td> </td>
<td>13 Jun 2017 </td>
<td>7201</td>
<td>7612</td>
<td><a href="https://confluence.atlassian.com/doc/issues-resolved-in-6-2-2-916521743.html" class="external-link">Confluence 6.2.2 Release Notes</a></td>
</tr>
<tr class="even">
<td>6.2.1</td>
<td> </td>
<td> </td>
<td>29 May 2017 </td>
<td>7111</td>
<td>7610</td>
<td><a href="https://confluence.atlassian.com/doc/issues-resolved-in-6-2-1-909902515.html" class="external-link">Confluence 6.2.1 Release Notes</a></td>
</tr>
<tr class="odd">
<td>6.2.0</td>
<td> </td>
<td>Spring Cyan</td>
<td>15 May 2017 </td>
<td>7111</td>
<td>7609</td>
<td><a href="https://confluence.atlassian.com/display/DOC/Confluence+6.2+Release+Notes" class="external-link">Confluence 6.2.0 Release Notes</a></td>
</tr>
<tr class="even">
<td>6.1.4</td>
<td> </td>
<td> </td>
<td>22 May 2017 </td>
<td>7110</td>
<td>7433</td>
<td><a href="https://confluence.atlassian.com/display/DOC/Issues+resolved+in+6.1.4" class="external-link">Confluence 6.1.4 Release Notes</a></td>
</tr>
<tr class="odd">
<td>6.1.3</td>
<td> </td>
<td> </td>
<td> 02 May 2017 </td>
<td>7109</td>
<td>7431</td>
<td><a href="https://confluence.atlassian.com/display/DOC/Issues+resolved+in+6.1.3" class="external-link">Confluence 6.1.3 Release Notes</a></td>
</tr>
<tr class="even">
<td>6.1.2</td>
<td> </td>
<td> </td>
<td>19 Apr 2017 </td>
<td>7109</td>
<td>7430</td>
<td><a href="https://confluence.atlassian.com/display/DOC/Issues+resolved+in+6.1.2" class="external-link">Confluence 6.1.2 Release Notes</a></td>
</tr>
<tr class="odd">
<td>6.1.1</td>
<td> </td>
<td> </td>
<td>03 Apr 2017 </td>
<td>7109</td>
<td>7428</td>
<td><a href="https://confluence.atlassian.com/display/DOC/Issues+resolved+in+6.1.1?src=contextnavpagetreemode" class="external-link">Confluence 6.1.1 Release Notes</a></td>
</tr>
<tr class="even">
<td>6.1.0</td>
<td> </td>
<td>Spring Cyan</td>
<td>17 Mar 2017 </td>
<td>7109</td>
<td>7420</td>
<td><a href="https://confluence.atlassian.com/display/DOC/Confluence+6.1+Release+Notes" class="external-link">Confluence 6.1.0 Release Notes</a></td>
</tr>
<tr class="odd">
<td>6.0.7</td>
<td> </td>
<td> </td>
<td>17 Mar 2017 </td>
<td>7109</td>
<td>7162</td>
<td><a href="https://confluence.atlassian.com/display/DOC/Issues+resolved+in+6.0.7" class="external-link">Confluence 6.0.7 Release Notes</a></td>
</tr>
<tr class="even">
<td>6.0.6</td>
<td> </td>
<td> </td>
<td>22 Feb 2017 </td>
<td>7104</td>
<td>7160</td>
<td><a href="https://confluence.atlassian.com/display/DOC/Issues+resolved+in+6.0.6" class="external-link">Confluence 6.0.6 Release Notes</a></td>
</tr>
<tr class="odd">
<td>6.0.5</td>
<td> </td>
<td> </td>
<td>06 Feb 2017 </td>
<td>7103</td>
<td>7157</td>
<td><a href="https://confluence.atlassian.com/display/DOC/Issues+resolved+in+6.0.5" class="external-link">Confluence 6.0.5 Release Notes</a></td>
</tr>
<tr class="even">
<td>6.0.4</td>
<td> </td>
<td> </td>
<td>16 Jan 2017 </td>
<td>7102</td>
<td>7150</td>
<td><a href="https://confluence.atlassian.com/display/DOC/Issues+resolved+in+6.0.4" class="external-link">Confluence 6.0.4 Release Notes</a></td>
</tr>
<tr class="odd">
<td>6.0.3</td>
<td> </td>
<td> </td>
<td>19 Dec 2016 </td>
<td><p>7102</p></td>
<td>7144</td>
<td><a href="https://confluence.atlassian.com/display/DOC/Issues+resolved+in+6.0.3" class="external-link">Confluence 6.0.3 Release Notes</a></td>
</tr>
<tr class="even">
<td>6.0.2</td>
<td> </td>
<td> </td>
<td>28 Nov 2016 </td>
<td>7101</td>
<td>7141</td>
<td><a href="https://confluence.atlassian.com/display/DOC/Issues+resolved+in+6.0.2" class="external-link">Confluence 6.0.2 Release Notes</a></td>
</tr>
<tr class="odd">
<td>6.0.1</td>
<td> </td>
<td>Spring Cyan</td>
<td>02 Nov 2016 </td>
<td>7101</td>
<td>7124</td>
<td><a href="https://confluence.atlassian.com/display/DOC/Confluence+6.0+Release+Notes" class="external-link">Confluence 6.0.1 Release Notes</a></td>
</tr>
<tr class="even">
<td>5.10.9</td>
<td> </td>
<td> </td>
<td>22 Nov 2017 </td>
<td>6441</td>
<td>7018</td>
<td><a href="https://confluence.atlassian.com/display/DOC/Confluence+5.10.9+Release+Notes" class="external-link">Confluence 5.10.9 Release Notes</a></td>
</tr>
<tr class="odd">
<td>5.10.8</td>
<td> </td>
<td> </td>
<td>20 Oct 2016 </td>
<td>6441</td>
<td>7017</td>
<td><a href="https://confluence.atlassian.com/display/DOC/Confluence+5.10.8+Release+Notes" class="external-link">Confluence 5.10.8 Release Notes</a></td>
</tr>
<tr class="even">
<td>5.10.7</td>
<td> </td>
<td> </td>
<td>30 Sep 2016 </td>
<td>6441</td>
<td>7016</td>
<td><a href="https://confluence.atlassian.com/display/DOC/Confluence+5.10.7+Release+Notes" class="external-link">Confluence 5.10.7 Release Notes</a></td>
</tr>
<tr class="odd">
<td>5.10.6</td>
<td> </td>
<td> </td>
<td>23 Sep 2016 </td>
<td>6441</td>
<td>7015</td>
<td><a href="https://confluence.atlassian.com/display/DOC/Confluence+5.10.6+Release+Notes" class="external-link">Confluence 5.10.6 Release Notes</a></td>
</tr>
<tr class="even">
<td>5.10.5</td>
<td> </td>
<td> </td>
<td>09 Sep 2016 </td>
<td>6441</td>
<td>7014</td>
<td><a href="https://confluence.atlassian.com/display/DOC/Confluence+5.10.5+Release+Notes" class="external-link">Confluence 5.10.5 Release Notes</a></td>
</tr>
<tr class="odd">
<td>5.10.4</td>
<td> </td>
<td> </td>
<td>22 Aug 2016 </td>
<td>6441</td>
<td>7013</td>
<td><a href="https://confluence.atlassian.com/display/DOC/Confluence+5.10.4+Release+Notes" class="external-link">Confluence 5.10.4 Release Notes</a></td>
</tr>
<tr class="even">
<td>5.10.3</td>
<td> </td>
<td> </td>
<td>01 Aug 2016 </td>
<td>6441</td>
<td>7012</td>
<td><a href="https://confluence.atlassian.com/display/DOC/Confluence+5.10.3+Release+Notes" class="external-link">Confluence 5.10.3 Release Notes</a></td>
</tr>
<tr class="odd">
<td>5.10.2</td>
<td> </td>
<td> </td>
<td>15 Jul 2016 </td>
<td>6441</td>
<td>7011</td>
<td><a href="https://confluence.atlassian.com/display/DOC/Confluence+5.10.2+Release+Notes" class="external-link">Confluence 5.10.2 Release Notes</a></td>
</tr>
<tr class="even">
<td>5.10.1</td>
<td> </td>
<td> </td>
<td>28 Jun 2016 </td>
<td>6441</td>
<td>7009</td>
<td><a href="https://confluence.atlassian.com/display/DOC/Confluence+5.10.1+Release+Notes" class="external-link">Confluence 5.10.1 Release Notes</a></td>
</tr>
<tr class="odd">
<td>5.10.0</td>
<td> </td>
<td>Timberwolf</td>
<td>08 Jun 2016 </td>
<td>6441</td>
<td> </td>
<td><a href="https://confluence.atlassian.com/display/DOC/Confluence+5.10+Release+Notes" class="external-link">Confluence 5.10 Release Notes</a></td>
</tr>
<tr class="even">
<td>5.9.14</td>
<td> </td>
<td> </td>
<td>18 Sep 2016 </td>
<td>6223</td>
<td> </td>
<td><a href="http://confluence.atlassian.com/display/DOC/Confluence+5.9.14+Release+Notes" class="external-link">Confluence 5.9.14 Release Notes</a></td>
</tr>
<tr class="odd">
<td>5.9.12</td>
<td> </td>
<td> </td>
<td>03 Jun 2016 </td>
<td>6220</td>
<td> </td>
<td><a href="https://confluence.atlassian.com/display/DOC/Confluence+5.9.12+Release+Notes" class="external-link">Confluence 5.9.12 Release Notes</a></td>
</tr>
<tr class="even">
<td>5.9.11</td>
<td> </td>
<td> </td>
<td>20 May 2016 </td>
<td>6219</td>
<td> </td>
<td><a href="https://confluence.atlassian.com/display/DOC/Confluence+5.9.11+Release+Notes" class="external-link">Confluence 5.9.11 Release Notes</a></td>
</tr>
<tr class="odd">
<td>5.9.10</td>
<td> </td>
<td> </td>
<td>05 May 2016 </td>
<td>6218</td>
<td> </td>
<td><a href="https://confluence.atlassian.com/display/DOC/Confluence+5.9.10+Release+Notes" class="external-link">Confluence 5.9.10 Release Notes</a></td>
</tr>
<tr class="even">
<td>5.9.9</td>
<td> </td>
<td> </td>
<td>21 Apr 2016 </td>
<td>6217</td>
<td> </td>
<td><a href="https://confluence.atlassian.com/display/DOC/Confluence+5.9.9+Release+Notes" class="external-link">Confluence 5.9.9 Release Notes</a></td>
</tr>
<tr class="odd">
<td>5.9.8</td>
<td> </td>
<td> </td>
<td> 12 Apr 2016 </td>
<td>6216</td>
<td> </td>
<td><a href="https://confluence.atlassian.com/display/DOC/Confluence+5.9.8+Release+Notes" class="external-link">Confluence 5.9.8 Release Notes</a></td>
</tr>
<tr class="even">
<td>5.9.7</td>
<td> </td>
<td> </td>
<td>17 Mar 2016 </td>
<td>6214</td>
<td> </td>
<td><a href="https://confluence.atlassian.com/display/DOC/Confluence+5.9.7+Release+Notes" class="external-link">Confluence 5.9.7 Release Notes</a></td>
</tr>
<tr class="odd">
<td>5.9.6</td>
<td> </td>
<td> </td>
<td>03 Mar 2016 </td>
<td>6213</td>
<td> </td>
<td><a href="https://confluence.atlassian.com/display/DOC/Confluence+5.9.6+Release+Notes" class="external-link">Confluence 5.9.6 Release Notes</a></td>
</tr>
<tr class="even">
<td>5.9.5</td>
<td> </td>
<td> </td>
<td>11 Feb 2016 </td>
<td>6212</td>
<td> </td>
<td><a href="https://confluence.atlassian.com/display/DOC/Confluence+5.9.5+Release+Notes" class="external-link">Confluence 5.9.5 Release Notes</a></td>
</tr>
<tr class="odd">
<td>5.9.4</td>
<td> </td>
<td> </td>
<td>08 Jan 2016 </td>
<td>6211</td>
<td> </td>
<td><a href="https://confluence.atlassian.com/display/DOC/Confluence+5.9.4+Release+Notes" class="external-link">Confluence 5.9.4 Release Notes</a></td>
</tr>
<tr class="even">
<td>5.9.3</td>
<td> </td>
<td> </td>
<td>24 Dec 2015 </td>
<td>6210</td>
<td> </td>
<td><a href="https://confluence.atlassian.com/display/DOC/Confluence+5.9.3+Release+Notes" class="external-link">Confluence 5.9.3 Release Notes</a></td>
</tr>
<tr class="odd">
<td>5.9.2</td>
<td> </td>
<td> </td>
<td>08 Dec 2015 </td>
<td>6208</td>
<td> </td>
<td><a href="https://confluence.atlassian.com/display/DOC/Confluence+5.9.2+Release+Notes" class="external-link">Confluence 5.9.2 Release Notes</a></td>
</tr>
<tr class="even">
<td>5.9.1</td>
<td> </td>
<td>Salt White</td>
<td>24 Nov 2015</td>
<td>6207</td>
<td> </td>
<td><a href="https://confluence.atlassian.com/display/DOC/Confluence+5.9+Release+Notes" class="external-link">Confluence 5.9 Release Notes</a></td>
</tr>
<tr class="odd">
<td>5.8.18</td>
<td> </td>
<td> </td>
<td>07 Dec 2015 </td>
<td>5998</td>
<td> </td>
<td><a href="https://confluence.atlassian.com/display/DOC/Confluence+5.8.18+Release+Notes" class="external-link">Confluence 5.8.18 Release Notes</a></td>
</tr>
<tr class="even">
<td>5.8.17</td>
<td> </td>
<td> </td>
<td>19 Nov 2015 </td>
<td>5997</td>
<td> </td>
<td><a href="https://confluence.atlassian.com/display/DOC/Confluence+5.8.17+Release+Notes" class="external-link">Confluence 5.8.17 Release Notes</a></td>
</tr>
<tr class="odd">
<td>5.8.16</td>
<td> </td>
<td> </td>
<td>09 Nov 2015 </td>
<td>5996</td>
<td> </td>
<td><a href="https://confluence.atlassian.com/display/DOC/Confluence+5.8.16+Release+Notes" class="external-link">Confluence 5.8.16 Release Notes</a></td>
</tr>
<tr class="even">
<td>5.8.15</td>
<td> </td>
<td> </td>
<td>22 Oct 2015 </td>
<td>5995</td>
<td> </td>
<td><a href="https://confluence.atlassian.com/display/DOC/Confluence+5.8.15+Release+Notes" class="external-link">Confluence 5.8.15 Release Notes</a></td>
</tr>
<tr class="odd">
<td>5.8.14</td>
<td> </td>
<td> </td>
<td>08 Oct 2015 </td>
<td>5994</td>
<td> </td>
<td><a href="https://confluence.atlassian.com/display/DOC/Confluence+5.8.14+Release+Notes" class="external-link">Confluence 5.8.14 Release Notes</a></td>
</tr>
<tr class="even">
<td>5.8.13</td>
<td> </td>
<td> </td>
<td>25 Sep 2015 </td>
<td>5993</td>
<td> </td>
<td><a href="https://confluence.atlassian.com/display/DOC/Confluence+5.8.13+Release+Notes" class="external-link">Confluence 5.8.13 Release Notes</a></td>
</tr>
<tr class="odd">
<td>5.8.10</td>
<td> </td>
<td> </td>
<td>28 Aug 2015 </td>
<td>5989</td>
<td> </td>
<td><a href="https://confluence.atlassian.com/display/DOC/Confluence+5.8.10+Release+Notes" class="external-link">Confluence 5.8.10 Release Notes</a></td>
</tr>
<tr class="even">
<td>5.8.9</td>
<td> </td>
<td> </td>
<td>15 Aug 2015 </td>
<td>5988</td>
<td> </td>
<td><a href="https://confluence.atlassian.com/display/DOC/Confluence+5.8.9+Release+Notes" class="external-link">Confluence 5.8.9 Release Notes</a></td>
</tr>
<tr class="odd">
<td>5.8.8</td>
<td> </td>
<td> </td>
<td>05 Aug 2015 </td>
<td>5987</td>
<td> </td>
<td><a href="https://confluence.atlassian.com/display/DOC/Confluence+5.8.8+Release+Notes" class="external-link">Confluence 5.8.8 Release Notes</a></td>
</tr>
<tr class="even">
<td>5.8.6</td>
<td> </td>
<td> </td>
<td>21 Jul 2015 </td>
<td>5984</td>
<td> </td>
<td><a href="https://confluence.atlassian.com/display/DOC/Confluence+5.8.6+Release+Notes" class="external-link">Confluence 5.8.6 Release Notes</a></td>
</tr>
<tr class="odd">
<td>5.8.5</td>
<td> </td>
<td> </td>
<td>29 Jun 2015 </td>
<td>5983</td>
<td> </td>
<td><a href="https://confluence.atlassian.com/display/DOC/Confluence+5.8.5+Release+Notes" class="external-link">Confluence 5.8.5 Release Notes</a></td>
</tr>
<tr class="even">
<td>5.8.4</td>
<td> </td>
<td> </td>
<td>03 Jun 2015 </td>
<td>5982</td>
<td> </td>
<td><a href="https://confluence.atlassian.com/display/DOC/Confluence+5.8.4+Release+Notes" class="external-link">Confluence 5.8.4 Release Notes</a></td>
</tr>
<tr class="odd">
<td>5.8.2</td>
<td> </td>
<td>Electric Blue</td>
<td>02 Jun 2015 </td>
<td>5980</td>
<td> </td>
<td><a href="https://confluence.atlassian.com/display/DOC/Confluence+5.8+Release+Notes" class="external-link">Confluence 5.8 Release Notes</a></td>
</tr>
<tr class="even">
<td>5.7.6</td>
<td> </td>
<td> </td>
<td>12 Jan 2016 </td>
<td>5792</td>
<td> </td>
<td><a href="https://confluence.atlassian.com/display/DOC/Confluence+5.7.6+Release+Notes" class="external-link">Confluence 5.7.6 Release Notes</a></td>
</tr>
<tr class="odd">
<td>5.7.5</td>
<td> </td>
<td> </td>
<td>02 Jun 2015 </td>
<td>5791</td>
<td> </td>
<td><a href="https://confluence.atlassian.com/display/DOC/Confluence+5.7.5+Release+Notes" class="external-link">Confluence 5.7.5 Release Notes</a></td>
</tr>
<tr class="even">
<td>5.7.4</td>
<td> </td>
<td> </td>
<td>06 May 2015 </td>
<td>5788</td>
<td> </td>
<td><a href="https://confluence.atlassian.com/display/DOC/Confluence+5.7.5+Release+Notes" class="external-link">Confluence 5.7.4 Release Notes</a></td>
</tr>
<tr class="odd">
<td>5.7.3</td>
<td> </td>
<td> </td>
<td>13 Apr 2015 </td>
<td>5785</td>
<td> </td>
<td><a href="https://confluence.atlassian.com/display/DOC/Confluence+5.7.5+Release+Notes" class="external-link">Confluence 5.7.4 Release Notes</a></td>
</tr>
<tr class="even">
<td>5.7.1</td>
<td> </td>
<td> </td>
<td>09 Mar 2015 </td>
<td>5781</td>
<td> </td>
<td><a href="https://confluence.atlassian.com/display/DOC/Confluence+5.7.5+Release+Notes" class="external-link">Confluence 5.7.1 Release Notes</a></td>
</tr>
<tr class="odd">
<td>5.7</td>
<td> </td>
<td> </td>
<td>27 Jan 2015 </td>
<td>5780</td>
<td> </td>
<td><a href="https://confluence.atlassian.com/display/DOC/Confluence+5.7+Release+Notes" class="external-link">Confluence 5.7 Release Notes</a></td>
</tr>
<tr class="even">
<td>5.6.6</td>
<td> </td>
<td> </td>
<td>21 Jan 2015 </td>
<td>5639</td>
<td> </td>
<td>Bugfixes</td>
</tr>
<tr class="odd">
<td>5.6.5</td>
<td> </td>
<td> </td>
<td>26 Nov 2014 </td>
<td>5638</td>
<td> </td>
<td>Bugfixes</td>
</tr>
<tr class="even">
<td>5.6.4</td>
<td> </td>
<td> </td>
<td>27-Sept-2014</td>
<td>5637</td>
<td> </td>
<td>Bugfixes</td>
</tr>
<tr class="odd">
<td>5.6.3</td>
<td> </td>
<td> </td>
<td>25-Sept-2014</td>
<td>5636</td>
<td> </td>
<td>Bugfixes</td>
</tr>
<tr class="even">
<td>5.6.1</td>
<td> </td>
<td> </td>
<td>03-Sept-2014</td>
<td>5635</td>
<td> </td>
<td>Fixing <a href="https://jira.atlassian.com/browse/CONF-34780" class="external-link">CONF-34780</a></td>
</tr>
<tr class="odd">
<td>5.6</td>
<td> </td>
<td>Lime</td>
<td>02-Sep-2014</td>
<td>5634</td>
<td> </td>
<td>Confluence Data Center</td>
</tr>
<tr class="even">
<td>5.5.7</td>
<td> </td>
<td> </td>
<td>21 Jan 2015 </td>
<td>5528</td>
<td> </td>
<td>Bugfixes</td>
</tr>
<tr class="odd">
<td>5.5.6</td>
<td> </td>
<td> </td>
<td>20-Aug-2014</td>
<td>5527</td>
<td> </td>
<td> </td>
</tr>
<tr class="even">
<td>5.5.4</td>
<td> </td>
<td> </td>
<td>30-Jul-2014</td>
<td>5517</td>
<td> </td>
<td> </td>
</tr>
<tr class="odd">
<td>5.5.3</td>
<td> </td>
<td> </td>
<td>01-Jul-2014</td>
<td>5515</td>
<td> </td>
<td> </td>
</tr>
<tr class="even">
<td>5.5.2</td>
<td> </td>
<td> </td>
<td>26-May-2014</td>
<td>5510</td>
<td> </td>
<td> </td>
</tr>
<tr class="odd">
<td>5.5.1</td>
<td> </td>
<td> </td>
<td>21-May-2014</td>
<td>5508</td>
<td> </td>
<td> </td>
</tr>
<tr class="even">
<td>5.5</td>
<td> </td>
<td> </td>
<td>30-Apr-2014</td>
<td>5501</td>
<td> </td>
<td> </td>
</tr>
<tr class="odd">
<td>5.4.4</td>
<td> </td>
<td> </td>
<td>18-Mar-2014</td>
<td>4733</td>
<td> </td>
<td> </td>
</tr>
<tr class="even">
<td>5.4.3</td>
<td> </td>
<td> </td>
<td>18-Feb-2014</td>
<td>4732</td>
<td> </td>
<td> </td>
</tr>
<tr class="odd">
<td>5.4.2</td>
<td> </td>
<td> </td>
<td>16-Jan-2014</td>
<td>4731</td>
<td> </td>
<td> </td>
</tr>
<tr class="even">
<td>5.4.1</td>
<td> </td>
<td> </td>
<td>18-Dec-2013</td>
<td>4727</td>
<td> </td>
<td> </td>
</tr>
<tr class="odd">
<td>5.4</td>
<td> </td>
<td>TARDIS blue</td>
<td>02-Dec-2013</td>
<td>4726</td>
<td> </td>
<td> </td>
</tr>
<tr class="even">
<td>5.3.4</td>
<td> </td>
<td> </td>
<td>20-Nov-2013</td>
<td>4532</td>
<td> </td>
<td> </td>
</tr>
<tr class="odd">
<td>5.3.1</td>
<td> </td>
<td> </td>
<td>22-Oct-2013</td>
<td>4528</td>
<td> </td>
<td> </td>
</tr>
<tr class="even">
<td>5.3</td>
<td> </td>
<td> </td>
<td>01-Oct-2013</td>
<td>4527</td>
<td> </td>
<td> </td>
</tr>
<tr class="odd">
<td>5.2.5</td>
<td> </td>
<td> </td>
<td>19-Sep-2013</td>
<td>4396</td>
<td> </td>
<td> </td>
</tr>
<tr class="even">
<td>5.2.4</td>
<td> </td>
<td> </td>
<td>10-Sep-2013</td>
<td>4395</td>
<td> </td>
<td> </td>
</tr>
<tr class="odd">
<td>5.2.3</td>
<td> </td>
<td>Red</td>
<td>13-Aug-2013</td>
<td>4394</td>
<td> </td>
<td>The first public 5.2.x release. See <a href="https://confluence.atlassian.com/display/DOC/Confluence+5.2+Release+Notes" class="external-link">Confluence 5.2 Release Notes</a></td>
</tr>
<tr class="even">
<td>5.1.5</td>
<td> </td>
<td> </td>
<td>6-Aug-2013</td>
<td>4252</td>
<td> </td>
<td>Security fix for <a href="https://jira.atlassian.com/browse/CONF-30221" class="uri external-link">CONF-30221</a></td>
</tr>
<tr class="odd">
<td>5.1.4</td>
<td> </td>
<td> </td>
<td>27-Jun-2013</td>
<td>4249</td>
<td> </td>
<td> </td>
</tr>
<tr class="even">
<td>5.1.3</td>
<td> </td>
<td> </td>
<td>16-May-2013</td>
<td>4226</td>
<td> </td>
<td> </td>
</tr>
<tr class="odd">
<td>5.1.2</td>
<td> </td>
<td> </td>
<td>23-Apr-2013</td>
<td>4224</td>
<td> </td>
<td> </td>
</tr>
<tr class="even">
<td>5.1.1</td>
<td> </td>
<td> </td>
<td>11-Apr-2013</td>
<td>4216</td>
<td> </td>
<td> </td>
</tr>
<tr class="odd">
<td>5.1</td>
<td> </td>
<td> </td>
<td>27-Mar-2013</td>
<td>4215</td>
<td> </td>
<td>Blueprints</td>
</tr>
<tr class="even">
<td>5.0.3</td>
<td> </td>
<td> </td>
<td>20-Mar-2013</td>
<td>4109</td>
<td> </td>
<td> </td>
</tr>
<tr class="odd">
<td>5.0.2</td>
<td> </td>
<td> </td>
<td>08-Mar-2013</td>
<td>4108</td>
<td> </td>
<td> </td>
</tr>
<tr class="even">
<td>5.0.1</td>
<td> </td>
<td> </td>
<td>27-Feb-2013</td>
<td>4107</td>
<td> </td>
<td> </td>
</tr>
<tr class="odd">
<td>5.0</td>
<td> </td>
<td> </td>
<td>26-Feb-2013</td>
<td>4104</td>
<td> </td>
<td>New UI and Space Sidebar</td>
</tr>
<tr class="even">
<td>4.3.7</td>
<td> </td>
<td> </td>
<td>29-Jan-2013</td>
<td>3398</td>
<td> </td>
<td> </td>
</tr>
<tr class="odd">
<td>4.3.6</td>
<td> </td>
<td> </td>
<td>14-Jan-2012</td>
<td>3397</td>
<td> </td>
<td> </td>
</tr>
<tr class="even">
<td>4.3.5</td>
<td> </td>
<td> </td>
<td>18-Dec-2012</td>
<td>3396</td>
<td> </td>
<td> </td>
</tr>
<tr class="odd">
<td>4.3.3</td>
<td> </td>
<td> </td>
<td>19-Nov-2012</td>
<td>3393</td>
<td> </td>
<td> </td>
</tr>
<tr class="even">
<td>4.3.2</td>
<td> </td>
<td> </td>
<td> </td>
<td>3391</td>
<td> </td>
<td> </td>
</tr>
<tr class="odd">
<td>4.3.1</td>
<td> </td>
<td> </td>
<td> </td>
<td>3390</td>
<td> </td>
<td> </td>
</tr>
<tr class="even">
<td>4.3</td>
<td> </td>
<td> </td>
<td> </td>
<td>3388</td>
<td> </td>
<td> </td>
</tr>
<tr class="odd">
<td>4.2.13</td>
<td> </td>
<td> </td>
<td>20-Aug-2012</td>
<td>3296</td>
<td> </td>
<td> </td>
</tr>
<tr class="even">
<td>4.2.11</td>
<td> </td>
<td> </td>
<td> </td>
<td>3293</td>
<td> </td>
<td> </td>
</tr>
<tr class="odd">
<td>4.2.9</td>
<td> </td>
<td> </td>
<td> </td>
<td>3291</td>
<td> </td>
<td> </td>
</tr>
<tr class="even">
<td>4.2.8</td>
<td> </td>
<td> </td>
<td> </td>
<td>3289</td>
<td> </td>
<td> </td>
</tr>
<tr class="odd">
<td>4.2.7</td>
<td> </td>
<td> </td>
<td> </td>
<td>3287</td>
<td> </td>
<td> </td>
</tr>
<tr class="even">
<td>4.2.6</td>
<td> </td>
<td> </td>
<td> </td>
<td>3285</td>
<td> </td>
<td> </td>
</tr>
<tr class="odd">
<td>4.2.5</td>
<td> </td>
<td> </td>
<td> </td>
<td>3284</td>
<td> </td>
<td> </td>
</tr>
<tr class="even">
<td><p>4.2.4</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>3281</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p>4.2.3</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>15-May-2012</p></td>
<td><p>3280</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p>4.2.2</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>3278</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p>4.2.1</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>3277</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p>4.2</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>3273</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p>4.1.10</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>14-May-2012</p></td>
<td><p>3152</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p>4.1.9</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>3148</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p>4.1.8</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>3147</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p>4.1.7</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>3145</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p>4.1.6</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>3144</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p>4.1.5</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>3143</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p>4.1.4</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>3142</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p>4.1.3</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>3129</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p>4.1.2</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>3128</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p>4.1</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>3126</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td>4.0.7</td>
<td> </td>
<td> </td>
<td> </td>
<td>3058</td>
<td> </td>
<td>Build number is identical to 4.0.5. <a href="https://confluence.atlassian.com/display/DOC/Confluence+4.0.7+Release+Notes" class="external-link">4.0.7 Release Notes</a></td>
</tr>
<tr class="even">
<td><p>4.0.5</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>3058</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p>4.0.4</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>3057</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p>4.0.3</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>3056</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p>4.0</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>3047</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="even">
<td>3.5.17</td>
<td> </td>
<td> </td>
<td> </td>
<td>2176</td>
<td> </td>
<td> </td>
</tr>
<tr class="odd">
<td><p>3.5.13</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>2171</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p>3.5.11</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>2167</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p>3.5.9</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>2166</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p>3.5.7</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>2163</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p>3.5.6</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>2162</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p>3.5.5</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>2160</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p>3.5.4</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>2156</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p>3.5.3</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>2154</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p>3.5.2</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>2153</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p>3.5.1</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>30-Mar-2011</p></td>
<td><p>2149</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p>3.5</p></td>
<td><p> </p></td>
<td><p>Kermes</p></td>
<td><p>09-Mar-2011</p></td>
<td><p>2148</p></td>
<td> </td>
<td><p>Improved user management (Embedded Crowd)</p></td>
</tr>
<tr class="even">
<td><p>3.4.9</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>2042</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p>3.4.8</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>2041</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p>3.4.7</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>2037</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p>3.4.6</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>2036</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p>3.4.5</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>2035</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p>3.4.3</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>2033</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p>3.4.2</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>2032</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p>3.4.1</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>2030</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p>3.4</p></td>
<td><p> </p></td>
<td><p>Periwinkle</p></td>
<td><p>12-Oct-2010</p></td>
<td><p>2029</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p>3.3.3</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>1925</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p>3.3.1</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>1923</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p>3.3</p></td>
<td><p> </p></td>
<td><p>Spindrift</p></td>
<td><p> </p></td>
<td><p>1911</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p>3.2.1_01</p></td>
<td><p> </p></td>
<td><p>Heliotrope</p></td>
<td><p> </p></td>
<td><p>1814</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p>3.2</p></td>
<td><p> </p></td>
<td><p>Heliotrope</p></td>
<td><p> </p></td>
<td><p>1810</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p>3.1.2</p></td>
<td><p> </p></td>
<td><p>Heliotrope</p></td>
<td><p> </p></td>
<td><p>1725</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p>3.1.1</p></td>
<td><p> </p></td>
<td><p>Heliotrope</p></td>
<td><p> </p></td>
<td><p>1724</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p>3.1</p></td>
<td><p> </p></td>
<td><p>Heliotrope</p></td>
<td><p> </p></td>
<td><p>1723</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p>3.0.2</p></td>
<td><p> </p></td>
<td><p>LemonChiffon</p></td>
<td><p>6-Oct-2009</p></td>
<td><p>1636</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p>3.0.1</p></td>
<td><p> </p></td>
<td><p>LemonChiffon</p></td>
<td><p>20-Aug-2009</p></td>
<td><p>1634</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p>3.0.0_01</p></td>
<td><p> </p></td>
<td><p>LemonChiffon</p></td>
<td><p> </p></td>
<td><p>1627</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p>3.0</p></td>
<td><p> </p></td>
<td><p>LemonChiffon</p></td>
<td><p>01-Jun-2009</p></td>
<td><p>1626</p></td>
<td> </td>
<td><p>Social Features, Macro Browser, Improved RTE</p></td>
</tr>
<tr class="odd">
<td><p>2.10.4</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>1520</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p>2.10.3</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>1519</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p>2.10.2</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>1518</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p>2.10.1</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>1517</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p><a href="#2-10" class="unresolved">2.10</a></p></td>
<td><p> </p></td>
<td><p>Gamboge</p></td>
<td><p>03-Dec-2008</p></td>
<td><p>1515</p></td>
<td> </td>
<td><p>Widget Connector, Office Connector, Quick Nav, Did-You-Mean, upgraded TinyMCE and more.</p></td>
</tr>
<tr class="even">
<td><p><a href="#2-10-m5" class="unresolved">2.10-m5</a></p></td>
<td><p> </p></td>
<td><p>Sangria</p></td>
<td><p>24-Oct-2008</p></td>
<td><p>1508</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p><a href="#2-9-2" class="unresolved">2.9.2</a></p></td>
<td><p> </p></td>
<td><p>Sangria</p></td>
<td><p>14-Oct-2008</p></td>
<td><p>1419</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p><a href="#2-9-1" class="unresolved">2.9.1</a></p></td>
<td><p> </p></td>
<td><p>Sangria</p></td>
<td><p>08-Sep-2008</p></td>
<td><p>1418</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p><a href="#2-9" class="unresolved">2.9</a></p></td>
<td><p> </p></td>
<td><p>Sangria</p></td>
<td><p>07-Aug-2008</p></td>
<td><p>1415</p></td>
<td> </td>
<td><ul>
<li><code>com .atlassian .confluence .renderer .RenderContext</code> is finally gone</li>
<li>Labels Editing (UI) totally redone</li>
</ul></td>
</tr>
<tr class="even">
<td><p><a href="#2-9-m5" class="unresolved">2.9-M5</a></p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>???</p></td>
<td><p>1408</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p><a href="#2-8-2" class="unresolved">2.8.2</a></p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>03-Jul-2008</p></td>
<td><p>1325</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p><a href="#2-8-1" class="unresolved">2.8.1</a></p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>21-May-2008</p></td>
<td><p>1322</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p><a href="#2-8" class="unresolved">2.8</a></p></td>
<td><p> </p></td>
<td><p>Myrtle</p></td>
<td><p>24-Apr-2008</p></td>
<td><p>1318</p></td>
<td> </td>
<td><p>Drop-down menu collapsible comment UI, Page ordering, JMX<br />
<code>AttachmentRemoveEvent</code> now takes <code>User</code> as remover</p></td>
</tr>
<tr class="even">
<td><p><a href="#2-7-3" class="unresolved">2.7.3</a></p></td>
<td><p> </p></td>
<td><p>Cinnabar</p></td>
<td><p>19-Mar-08</p></td>
<td><p>1116</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p><a href="#2-7-2" class="unresolved">2.7.2</a></p></td>
<td><p> </p></td>
<td><p>Cinnabar</p></td>
<td><p>05-Mar-08</p></td>
<td><p> </p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p><a href="#2-7-1" class="unresolved">2.7.1</a></p></td>
<td><p> </p></td>
<td><p>Cinnabar</p></td>
<td><p>23-Jan-08</p></td>
<td><p>1112</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p><a href="#2-7" class="unresolved">2.7</a></p></td>
<td><p> </p></td>
<td><p>Cinnabar</p></td>
<td><p>12-Dec-07</p></td>
<td><p>1109</p></td>
<td> </td>
<td><p>JIRA Trusted Auth, System Administrator permission, Attach files on create</p></td>
</tr>
<tr class="even">
<td><p><a href="#2-6-2" class="unresolved">2.6.2</a></p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>27-Nov-07</p></td>
<td><p>919</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p><a href="#2-6-1" class="unresolved">2.6.1</a></p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>03-Nov-07</p></td>
<td><p>916</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p><a href="#2-6" class="unresolved">2.6</a></p></td>
<td><p> </p></td>
<td><p>Ecru</p></td>
<td><p>27-Sep-07</p></td>
<td><p>913</p></td>
<td> </td>
<td><p>New theme, updated Joda Time, Backdate/rename news</p></td>
</tr>
<tr class="odd">
<td><p><a href="#2-6-dr1" class="unresolved">2.6-DR1</a></p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p><a href="#2-5-8" class="unresolved">2.5.8</a></p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>814</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p><a href="#2-5-7" class="unresolved">2.5.7</a></p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>813</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p><a href="#2-5-6" class="unresolved">2.5.6</a></p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p><a href="#2-5-5" class="unresolved">2.5.5</a></p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p><a href="#2-5-4" class="unresolved">2.5.4</a></p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>13-Jun-07</p></td>
<td><p>809</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p><a href="#2-5-3" class="unresolved">2.5.3</a></p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>29-May-07</p></td>
<td><p>808</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p><a href="#2-5-2" class="unresolved">2.5.2</a></p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>807</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p><a href="#2-5-1" class="unresolved">2.5.1</a></p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>806</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p><a href="#2-5" class="unresolved">2.5</a></p></td>
<td><p> </p></td>
<td><p>Myrtle</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td> </td>
<td><p>Multiple page permissions</p></td>
</tr>
<tr class="odd">
<td><p><a href="#2-4-5" class="unresolved">2.4.5</a></p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>12-Apr-07</p></td>
<td><p>708</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p><a href="#2-4-3" class="unresolved">2.4.3</a></p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>21-Mar-07</p></td>
<td><p>705</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p><a href="#2-4-2" class="unresolved">2.4.2</a></p></td>
<td><p> </p></td>
<td><p>Periwinkle</p></td>
<td><p>12-Mar-07</p></td>
<td><p>703</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p><a href="#2-3" class="unresolved">2.3</a></p></td>
<td><p>Snowy</p></td>
<td><p>Claret</p></td>
<td><p>03-Jan-07</p></td>
<td><p>641</p></td>
<td> </td>
<td><p>Clustering, People directory</p></td>
</tr>
<tr class="odd">
<td><p><a href="#2-3-dr2" class="unresolved">2.3-DR2</a></p></td>
<td><p> </p></td>
<td><p>Viridian</p></td>
<td><p>18-Sep-06</p></td>
<td><p>628</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p><a href="#2-2-10" class="unresolved">2.2.10</a></p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>30-Nov-06</p></td>
<td><p>528</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p><a href="#2-2-9" class="unresolved">2.2.9</a></p></td>
<td><p> </p></td>
<td><p>Taupe</p></td>
<td><p>07-Sep-06</p></td>
<td><p>527</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p><a href="#2-2-8" class="unresolved">2.2.8</a></p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>08-Aug-06</p></td>
<td><p>525</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p><a href="#2-3-dr1" class="unresolved">2.3-DR1</a></p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>24-Jul-06</p></td>
<td><p> </p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p><a href="#2-2-3" class="unresolved">2.2.3</a></p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>09-Jun-06</p></td>
<td><p>518</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p><a href="#2-2-2" class="unresolved">2.2.2</a></p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>516</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p><a href="#2-2-1a" class="unresolved">2.2.1a</a></p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>18-May-06</p></td>
<td><p>515</p></td>
<td> </td>
<td><p>Enabled the Recursive Plugin Classloader, Plugin Manager Fixes</p></td>
</tr>
<tr class="odd">
<td><p><a href="#2-2-1" class="unresolved">2.2.1</a></p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>17-May-06</p></td>
<td><p>514</p></td>
<td> </td>
<td><p>LDAP bugfix</p></td>
</tr>
<tr class="even">
<td><p><a href="#2-2" class="unresolved">2.2</a></p></td>
<td><p>Shoalhaven</p></td>
<td><p>Taupe</p></td>
<td><p>26-Apr-06</p></td>
<td><p>512</p></td>
<td> </td>
<td><p>User Spaces, Plugin Subsystem Improvements, Localisation, CAPTCHA</p></td>
</tr>
<tr class="odd">
<td><p><a href="#2-1-5a" class="unresolved">2.1.5a</a></p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>17-Mar-06</p></td>
<td><p>411 <strong>!</strong></p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p><a href="#2-1-5" class="unresolved">2.1.5</a></p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>16-Mar-06</p></td>
<td><p>411</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p><a href="#2-1-4" class="unresolved">2.1.4</a></p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>15-Feb-06</p></td>
<td><p>410</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p><a href="#2-1-3" class="unresolved">2.1.3</a></p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>23-Jan-06</p></td>
<td><p>408</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p><a href="#2-1-2" class="unresolved">2.1.2</a></p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>12-Jan-06</p></td>
<td><p>407</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p><a href="#2-1-1" class="unresolved">2.1.1</a></p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>23-Dec-05</p></td>
<td><p>406</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p><a href="#2-1" class="unresolved">2.1</a></p></td>
<td><p>Bogan</p></td>
<td><p>Chartreuse</p></td>
<td><p>23-Dec-05</p></td>
<td><p>405</p></td>
<td> </td>
<td><p>Autosave, concurrent edit warnings, atlassian-user</p></td>
</tr>
<tr class="even">
<td><p><a href="#2-0-3" class="unresolved">2.0.3</a></p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>11-Dec-05</p></td>
<td><p>323</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p><a href="#2-0-2" class="unresolved">2.0.2</a></p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>05-Dec-05</p></td>
<td><p>322</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p><a href="#2-0-1" class="unresolved">2.0.1</a></p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>28-Nov-05</p></td>
<td><p>321</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p><a href="#2-0" class="unresolved">2.0</a></p></td>
<td><p>Yarra</p></td>
<td><p>Chartreuse</p></td>
<td><p>16-Nov-05</p></td>
<td><p>320</p></td>
<td> </td>
<td><p>Rich text editor, labels, custom dashboard, RSS builder</p></td>
</tr>
<tr class="even">
<td><p><a href="#1-4-4" class="unresolved">1.4.4</a></p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>23-Sep-05</p></td>
<td><p>221</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p><a href="#1-4-3" class="unresolved">1.4.3</a></p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>15-Aug-05</p></td>
<td><p>219</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p><a href="#1-4-2" class="unresolved">1.4.2</a></p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>30-Jun-05</p></td>
<td><p>214</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p><a href="#1-4-1" class="unresolved">1.4.1</a></p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>02-Jun-05</p></td>
<td><p>212</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p><a href="#1-3-6" class="unresolved">1.3.6</a></p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>02-Jun-05</p></td>
<td><p>123</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p><a href="#1-4" class="unresolved">1.4</a></p></td>
<td><p>Hunter</p></td>
<td><p>Lime</p></td>
<td><p>23-May-05</p></td>
<td><p>211</p></td>
<td> </td>
<td><p>v2Renderer, new UI, page permissions, plugin API,</p></td>
</tr>
<tr class="even">
<td><p><a href="#1-3-5" class="unresolved">1.3.5</a></p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>01-Mar-05</p></td>
<td><p>122</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p><a href="#1-3-4" class="unresolved">1.3.4</a></p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>15-Feb-05</p></td>
<td><p>121</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p>1.3.3</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>08-Feb-2005</p></td>
<td><p>116</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p><a href="#1-3-2" class="unresolved">1.3.2</a></p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>21-Jan-05</p></td>
<td><p>114</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p><a href="#1-3-1" class="unresolved">1.3.1</a></p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>17-Dec-04</p></td>
<td><p>113</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p><a href="#1-3" class="unresolved">1.3</a></p></td>
<td><p>Murrumbidgee</p></td>
<td><p>Lime</p></td>
<td><p>30-Nov-04</p></td>
<td><p>112</p></td>
<td> </td>
<td><p>Mail archiving, themes, trash can, fine-grained permissions</p></td>
</tr>
<tr class="even">
<td><p><a href="#1-2-3" class="unresolved">1.2.3</a></p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>08-Oct-04</p></td>
<td><p>60</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p><a href="#1-2-2" class="unresolved">1.2.2</a></p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>23-Sep-04</p></td>
<td><p>60</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p><a href="#1-2-1" class="unresolved">1.2.1</a></p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>09-Sep-04</p></td>
<td><p>59</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p><a href="#1-2" class="unresolved">1.2</a></p></td>
<td><p>Swan</p></td>
<td><p>Jet Black</p></td>
<td><p>23-Aug-04</p></td>
<td><p>58</p></td>
<td> </td>
<td><p>Space content lists, image thumbnails, advanced search</p></td>
</tr>
<tr class="even">
<td><p><a href="#1-1-2" class="unresolved">1.1.2</a></p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>21-Jun-04</p></td>
<td><p>57</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p><a href="#1-1-1" class="unresolved">1.1.1</a></p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>18-Jun-04</p></td>
<td><p>56</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p><a href="#1-1" class="unresolved">1.1</a></p></td>
<td><p>Nymboida</p></td>
<td><p>Almond</p></td>
<td><p>09-Jun-04</p></td>
<td><p>53</p></td>
<td> </td>
<td><p>Macro management, attachment versioning, Powerpoint/Excel search</p></td>
</tr>
<tr class="odd">
<td><p>1.0.3a</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>05-May-04</p></td>
<td><p>50</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="#1-0-3" class="unresolved">1.0.3</a></p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>30-Apr-04</p></td>
<td><p>49</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p>1.0.2</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>13-Apr-04</p></td>
<td><p>48</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p><a href="#1-0-1" class="unresolved">1.0.1</a></p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>06-Apr-04</p></td>
<td><p>47</p></td>
<td> </td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p><a href="#1-0" class="unresolved">1.0</a></p></td>
<td><p>Murray</p></td>
<td><p>Puce</p></td>
<td><p>12-Mar-04</p></td>
<td><p>46</p></td>
<td> </td>
<td><p>First commercial release</p></td>
</tr>
<tr class="even">
<td><p>1.0-rc1</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>36</p></td>
<td> </td>
<td><p>Atlassian redefines 'feature freeze'</p></td>
</tr>
<tr class="odd">
<td><p>1.0-beta1</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>19-Dec-03</p></td>
<td><p>30</p></td>
<td> </td>
<td><p>Limited beta release</p></td>
</tr>
<tr class="even">
<td><p>1.0-alpha1</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>19</p></td>
<td> </td>
<td><p>First external release</p></td>
</tr>
</tbody>
</table>