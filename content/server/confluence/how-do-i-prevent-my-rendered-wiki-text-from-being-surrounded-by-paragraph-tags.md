---
aliases:
- /server/confluence/2031790.html
- /server/confluence/2031790.md
category: devguide
confluence_id: 2031790
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031790
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031790
date: '2017-12-08'
legacy_title: How do I prevent my rendered wiki text from being surrounded by paragraph
  tags?
platform: server
product: confluence
subcategory: faq
title: How do I prevent my rendered wiki text from being surrounded by paragraph tags?
---
# How do I prevent my rendered wiki text from being surrounded by paragraph tags?

### <img src="http://confluence.atlassian.com/images/icons/favicon.png" class="confluence-external-resource" /> How do I prevent my rendered wiki text from being surrounded by &lt;p&gt; tags?

When wiki text is converted to HTML, the level of conversion is determined by the <a href="http://www.atlassian.com/software/confluence/docs/api/latest/com/atlassian/renderer/v2/RenderMode.html" class="external-link">RenderMode</a> set within the <a href="http://www.atlassian.com/software/confluence/docs/api/latest/com/atlassian/renderer/v2/RenderContext.html" class="external-link">RenderContext</a>. Understanding RenderMode is quite important, so you should familiarise yourself with the documentation linked above.

There are two render modes that are useful if you want to avoid the output being placed inside paragraph tags:

`RenderMode.INLINE` will suppress the rendering of *all* block-level HTML elements, including paragraphs, blockquotes, tables and lists. Inline elements such as text decorations, links and images will still be rendered.

`RenderMode.suppress( RenderMode.F_FIRST_PARA )` will render block-level elements as usual, but if the *first* such element is a paragraph, no paragraph tags will be drawn around it. This is useful if you're placing your output inside a &lt;div&gt;.

If you are writing a macro, you will also need to return `true` from your macro's `isInline` method.
