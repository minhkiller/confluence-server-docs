---
aliases:
- /server/confluence/2031791.html
- /server/confluence/2031791.md
category: devguide
confluence_id: 2031791
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031791
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031791
date: '2017-12-08'
legacy_title: What's the easiest way to render a velocity template from Java code?
platform: server
product: confluence
subcategory: faq
title: What's the easiest way to render a velocity template from Java code?
---
# What's the easiest way to render a velocity template from Java code?

### <img src="http://confluence.atlassian.com/images/icons/favicon.png" class="confluence-external-resource" /> What's the easiest way to render a velocity template from Java code?

Use <a href="http://docs.atlassian.com/com/atlassian/confluence/atlassian-confluence/latest-server/index.html?com/atlassian/confluence/util/velocity/VelocityUtils.html" class="external-link">VelocityUtils</a>. You will need to provide VelocityUtils with the name of the template you want to render, and a map of parameters that will be made available within the template as `$variables` in velocity.

Confluence has a default set of objects for Confluence velocity templates. These are required for most Confluence velocity macros to work properly. To obtain this context, you should call <a href="http://docs.atlassian.com/com/atlassian/confluence/atlassian-confluence/latest-server/index.html?com/atlassian/confluence/renderer/radeox/macros/MacroUtils.html" class="external-link">MacroUtils</a>`.defaultVelocityContext();`.

``` java
// Create the Velocity Context
Map context = MacroUtils.defaultVelocityContext();
context.put("myCustomVar", customVar);
context.put("otherCustomVar", otherCustomVar);
// Render the Template
String result = VelocityUtils.getRenderedTemplate("/com/myplugin/templates/macro.vm", context);
```

##### RELATED TOPICS

[Rendering Velocity templates in a macro](/server/confluence/rendering-velocity-templates-in-a-macro)  
[WoW Macro explanation](/server/confluence/wow-macro-explanation)  
<a href="/server/confluence/macro-module/" class="createlink">Macro Plugins</a>
