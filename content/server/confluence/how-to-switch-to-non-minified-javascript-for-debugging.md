---
aliases:
- /server/confluence/how-to-switch-to-non-minified-javascript-for-debugging-2031862.html
- /server/confluence/how-to-switch-to-non-minified-javascript-for-debugging-2031862.md
category: devguide
confluence_id: 2031862
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031862
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031862
date: '2017-12-08'
legacy_title: How to switch to non-minified Javascript for debugging
platform: server
product: confluence
subcategory: faq
title: How to switch to non-minified JavaScript for debugging
---
# How to switch to non-minified JavaScript for debugging

Although you should always serve minified Javascript, temporarily running Confluence with non-minified Javascript can be useful for debugging.

A quick and simple way to do this is run Confluence with the following VM setting:

``` xml
-Datlassian.webresource.disable.minification=true
```

If you're using IntelliJ IDEA you can enter this at *Run→Edit Configurations→Tomcat Server→(your instance name)→VM Parameters*

Remember to remove the parameter when you're finished debugging.
