---
aliases:
- /server/confluence/space-blueprints-23298212.html
- /server/confluence/space-blueprints-23298212.md
category: devguide
confluence_id: 23298212
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=23298212
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=23298212
date: '2017-12-08'
guides: guides
legacy_title: Space Blueprints
platform: server
product: confluence
subcategory: learning
title: Space Blueprints
---
# Space Blueprints

Introduction to Space Blueprints

The Space Blueprints API (introduced in 5.3) is an extension of the existing Blueprints API (introduced in 5.1). Its main purpose is to help users bootstrap the space creation process. As a plugin developer, you can now hook into the **Create Space** dialog to add your own space blueprint. You can optionally do the following:

-   have a wizard to get user input for the new space
-   have a custom default home page for your space type
-   have a one or more templates with a predefined page hierarchy
-   promote page Blueprints for your space (show a set of Blueprints in the Create dialog as the default)
-   categorize your Space Blueprints

And lots more! It is really up to your imagination! For example the Team Space Blueprint provides a wizard to define 'team members' for a space and automatically adds the team members as space watchers and gives them explicit space permissions. 

If you haven't already, please read about the [blueprint concepts](/server/confluence/confluence-blueprints#blueprint-concepts). For a basic tutorial, start by [writing a simple Confluence Space Blueprint.](/server/confluence/write-a-simple-confluence-space-blueprint) 

### Example source

If you want to see some example code, the **Hello Blueprint** now contains a space blueprint. The source code is available on Atlassian Bitbucket: <a href="https://bitbucket.org/atlassian/hello-blueprint" class="uri external-link">bitbucket.org/atlassian/hello-blueprint</a>. To clone the repository, issue the following command:

``` bash
git clone https://bitbucket.org/atlassian/hello-blueprint.git
```

### Plugin module types used in space blueprints

Space Blueprints are built with the following plugin module types:

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><div class="tablesorter-header-inner">
Module
</div></th>
<th><div class="tablesorter-header-inner">
Purpose
</div></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="/server/confluence/web-item-plugin-module">web-item</a></p></td>
<td><p>Add your Space Blueprint to the <strong>Create Space</strong> dialog.</p>
<p>You must specify a section of <code>system.create.space.dialog/content</code> and have a <code>param</code> element with name &quot;<code>blueprintKey</code>&quot; specified, where <code>value</code> is the space blueprint module key.</p>
<p>The icon resource should be 200 x 150 pixel PNG icon resource representing the blueprint icon.</p></td>
</tr>
<tr class="even">
<td><p><a href="/server/confluence/space-blueprint-module">space-blueprint</a></p></td>
<td><p>Defines the space blueprint</p></td>
</tr>
<tr class="odd">
<td><p><a href="/server/confluence/content-template-module">content-template</a></p></td>
<td><p>Describes the default home page and content for the space</p></td>
</tr>
<tr class="even">
<td><p><a href="/server/confluence/promoted-blueprints-module">promoted-blueprints</a></p></td>
<td>Allows you promote blueprints for a space</td>
</tr>
<tr class="odd">
<td><p><a href="/server/confluence/dialog-wizard-module">dialog-wizard</a></p></td>
<td>Define a dialog wizard for users to input data during space creation</td>
</tr>
</tbody>
</table>


