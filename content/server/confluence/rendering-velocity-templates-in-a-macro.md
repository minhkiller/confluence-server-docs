---
aliases:
- /server/confluence/rendering-velocity-templates-in-a-macro-2031866.html
- /server/confluence/rendering-velocity-templates-in-a-macro-2031866.md
category: devguide
confluence_id: 2031866
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031866
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031866
date: '2017-12-08'
guides: guides
legacy_title: Rendering Velocity templates in a macro
platform: server
product: confluence
subcategory: learning
title: Rendering Velocity templates in a macro
---
# Rendering Velocity templates in a macro

When writing a [macro plugin](/server/confluence/macro-module) , it's a common requirement to render a Velocity file included with your plugin. The Velocity file should be rendered with some data provided by your plugin.

The easiest way to render Velocity templates from within a macro is to use `VelocityUtils.getRenderedTemplate` and simply return the result as the macro output. You can use it like this:

``` java
public String execute(Map params, String body, RenderContext renderContext) throws MacroException {
    // do something with params ...

    Map context = MacroUtils.defaultVelocityContext();
    context.put("page", page);
    context.put("labels", labels);
    return VelocityUtils.getRenderedTemplate("com/atlassian/confluence/example/sample-velocity.vm", context);
}
```

##### RELATED TOPICS

[Macro Module](/server/confluence/macro-module)

