---
aliases:
- /server/confluence/webdav-resource-module-2031618.html
- /server/confluence/webdav-resource-module-2031618.md
category: reference
confluence_id: 2031618
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031618
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031618
date: '2017-12-08'
legacy_title: WebDAV Resource Module
platform: server
product: confluence
subcategory: modules
title: WebDAV Resource module
---
# WebDAV Resource module

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Available:</p></td>
<td><p>Confluence 3.4 and later</p></td>
</tr>
</tbody>
</table>

WebDAV Resource modules allow you to define new kinds of content within Confluence that can be accessed remotely via the <a href="https://studio.plugins.atlassian.com/wiki/display/WBDV/Confluence+WebDAV+Plugin" class="external-link">Confluence WebDAV plugin</a>. You could use this functionality to expose your own custom entities over WebDAV, or expose existing Confluence content that is not currently accessible over WebDAV.

-   For more information about plugins in general, read [Confluence Plugin Guide](/server/confluence/confluence-plugin-guide).
-   To learn how to install and configure plugins (including macros), read <a href="#installing-a-plugin" class="unresolved">Installing a Plugin</a>.
-   For an introduction to writing your own plugins, read [Writing Confluence Plugins](/server/confluence/writing-confluence-plugins).

## Configuration

The definition for the WebDAV Resource module resides within the Confluence WebDAV plugin. In order to use the module, you will need to add a dependency on the WebDAV plugin to your plugin's `pom.xml`.

The new dependency should look like this:

``` xml
<dependency>
    <groupId>com.atlassian.confluence.extra.webdav</groupId>
    <artifactId>webdav-plugin</artifactId>
    <version>2.5</version>
    <scope>provided</scope>
</dependency>
```

Your plugin will need to be a plugin framework **version 2 plugin** for the dependency to work. See [Version 1 or Version 2 Plugin (Glossary Entry)](https://developer.atlassian.com/pages/viewpage.action?pageId=851971) and [Converting from Version 1 to Version 2 (OSGi) Plugins](https://developer.atlassian.com/display/DOCS/Converting+from+Version+1+to+Version+2+%28OSGi%29+Plugins).

You will also need to add a new element to your `atlassian-plugin.xml` that declares your WebDAV Resource module. The root element for the module is `davResourceFactory`. It allows the following attributes and child elements for configuration:

### Attributes

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Name*</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>class</p></td>
<td><p>The class which implements this plugin module. For the WebDAV Resource module, this class must implement <code>org.apache.jackrabbit.webdav.DavResourceFactory</code>.</p></td>
</tr>
<tr class="even">
<td><p>disabled</p></td>
<td><p>Indicate whether the plugin module should be disabled by default (value='true') or enabled by default (value='false').</p>
<p><strong>Default:</strong> false</p></td>
</tr>
<tr class="odd">
<td><p>i18n-name-key</p></td>
<td><p>The localisation key for the human-readable name of the plugin module.</p></td>
</tr>
<tr class="even">
<td><p>key</p></td>
<td><p>The identifier of the plugin module. This key must be unique within the plugin where it is defined.<br />
NOTE: Sometimes, in other contexts, you may need to uniquely identify a module. Do this with the <strong>complete module key</strong>. A module with key <code>fred</code> in a plugin with key <code>com.example.modules</code> will have a complete key of <code>com.example.modules:fred</code>.</p>
<p><strong>Default:</strong> N/A</p></td>
</tr>
<tr class="odd">
<td><p>name</p></td>
<td><p>The human-readable name of the plugin module.</p>
<p><strong>Default:</strong> The plugin key.</p></td>
</tr>
<tr class="even">
<td><p>workspace</p></td>
<td><p>The name of the root WebDAV context where your content will be hosted. This workspace name forms part of the URL to your WebDAV content. For example, a workspace name of 'mywebdav' would result in a WebDAV URL of <code>/plugins/servlet/confluence/mywebdav</code>. The workspace name must be unique on your Confluence server. You cannot have multiple WebDAV Resource modules enabled with the same workspace name.</p></td>
</tr>
</tbody>
</table>

**\*class, key, and workspace attributes are required**

### Elements

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Name</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>description</p></td>
<td><p>The description of the plugin module. You can specify the 'key' attribute to declare a localisation key for the value instead of text in the element body.</p></td>
</tr>
</tbody>
</table>

## Example

Here is an example `atlassian-plugin.xml` containing a definition for a single WebDAV Resource module:

``` xml
<atlassian-plugin name="My WebDAV Plugin" key="example.plugin.webdav" plugins-version="2">
    <plugin-info>
        <description>A basic WebDAV Resource module test</description>
        <vendor name="Atlassian Software Systems" url="http://www.atlassian.com"/>
        <version>1.0</version>
    </plugin-info>

    <davResourceFactory key="myResourceFactory" name="My WebDAV Resource Factory" workspace="mywebdav"
                        class="example.plugin.webdav.MyResourceFactory">
        <description>
        Exposes my plugin's content through the Confluence WebDAV plugin.
        </description>
    </davResourceFactory>
<atlassian-plugin>
```

The `MyResourceFactory` class needs to implement the `org.apache.jackrabbit.webdav.DavResourceFactory` interface. The purpose of this class is to construct new instances of objects that implement the `org.apache.jackrabbit.webdav.DavResource` interface. The factory will typically inspect the incoming request URL from the client in order to determine which DavResource should be created and returned. For example, a request to `/plugins/servlet/confluence/default/Global/DS` will return a `com.atlassian.confluence.extra.webdav.resource.SpaceResource` for the Confluence Demonstration space.

Here's an example of an implementation for MyResourceFactory:

``` java
package example.plugin.webdav;

import com.atlassian.confluence.extra.webdav.ConfluenceDavSession;
import org.apache.jackrabbit.webdav.*;

public class MyResourceFactory implements DavResourceFactory
{
    private SettingsManager settingsManager; // used by the HelloWorldResource.

    public MyResourceFactory(SettingsManager settingsManager)
    {
        this.settingsManager = settingsManager;
    }

    public DavResource createResource(DavResourceLocator davResourceLocator,
                                      DavServletRequest davServletRequest,
                                      DavServletResponse davServletResponse) throws DavException
    {
        // Delegate this call to the alternative createResource overload
        DavSession davSession = (DavSession) davServletRequest.getSession().getAttribute(ConfluenceDavSession.class.getName());
        return createResource(davResourceLocator, davSession);
    }

    /**
     * Returns a reference to the WebDAV resource identified by the current location (represented in the
     * davResourceLocator parameter). For example, a WebDAV request for the URL
     * "http://confluence-server/plugins/servlet/confluence/default/ds" would return a {@link SpaceResource}
     * representing the Demonstration Space.
     * @param davResourceLocator identifies the requested resource
     * @param davSession the current web session
     * @return A instance of {@link DavResource} representing the WebDAV resource at the requested location
     * @throws DavException thrown if any kind of non-OK HTTP Status should be returned.
     */
    public DavResource createResource(DavResourceLocator davResourceLocator, DavSession davSession) throws DavException
    {
        // this is a trivial example that always returns the HelloWorldResource. A more complete implementation would
        // probably examine the davResourceLocator.getResourcePath() value to examine the incoming request URL.
        ConfluenceDavSession confluenceDavSession = (ConfluenceDavSession)davSession;
        return new HelloWorldResource(davResourceLocator, this, confluenceDavSession.getLockManager(), confluenceDavSession, settingsManager);
    }
}
```

As part of your implementation of a WebDAV Resource Module, you will need to create classes that implement `DavResource` representing the content you want to expose over WebDAV. The Confluence WebDAV plugin provides a number of base classes that you can inherit from in order to simplify this step. Here is a brief listing of the classes that your WebDAV resources can inherit from:

-   <a href="https://studio.plugins.atlassian.com/svn/WBDV/trunk/src/main/java/com/atlassian/confluence/extra/webdav/resource/AbstractCollectionResource.java" class="external-link">AbstractCollectionResource</a> - Inherit from this class when the resource represents a collection of child resources, such as a Confluence space.
-   <a href="https://studio.plugins.atlassian.com/svn/WBDV/trunk/src/main/java/com/atlassian/confluence/extra/webdav/resource/AbstractContentResource.java" class="external-link">AbstractContentResource</a> - Inherit from this class when the resource is a single entity with no children.
-   <a href="https://studio.plugins.atlassian.com/svn/WBDV/trunk/src/main/java/com/atlassian/confluence/extra/webdav/resource/AbstractTextContentResource.java" class="external-link">AbstractTextContentResource</a> - Inherit from this class when the resource is a single entity with no children, and the content of the entity is plain text.
-   <a href="https://studio.plugins.atlassian.com/svn/WBDV/trunk/src/main/java/com/atlassian/confluence/extra/webdav/resource/AbstractAttachmentResource.java" class="external-link">AbstractAttachmentResource</a> - Inherit from this class when the resource is a Confluence page attachment.
-   <a href="https://studio.plugins.atlassian.com/svn/WBDV/trunk/src/main/java/com/atlassian/confluence/extra/webdav/resource/AbstractConfluenceResource.java" class="external-link">AbstractConfluenceResource</a> - Inherit from this class when you require more control over the behaviour of your entity. For example, if you want to customise the locking behaviour of the resource.

Here is a sample implementation of the HelloWorldResource, which inherits from `AbstractTextContentResource`.

``` java
package example.plugin.webdav;

import org.apache.jackrabbit.webdav.*;
import com.atlassian.confluence.extra.webdav.*;

public class HelloWorldResource extends AbstractTextContentResource
{
    private const String HELLO_WORLD = "Hello, World!";

    public AbstractTextContentResource(
            DavResourceLocator davResourceLocator,
            DavResourceFactory davResourceFactory,
            LockManager lockManager,
            ConfluenceDavSession davSession,
            SettingsManager settingsManager)
    {
        super(davResourceLocator, davResourceFactory, lockManager, davSession, settingsManager);
    }

    protected long getCreationtTime()
    {
        return 0;
    }

    protected byte[] getTextContentAsBytes(String encoding) throws UnsupportedEncodingException
    {
       return HELLO_WORLD.getBytes(encoding);
    }
}
```

## Notes

-   Your WebDAV Resource module must perform any and all required permission checking when returning WebDAV resources to the user. See the [Confluence Permissions Architecture](/server/confluence/confluence-permissions-architecture) guide for information on how to perform permission checks.

##### RELATED TOPICS

[Writing Confluence Plugins](/server/confluence/writing-confluence-plugins)  
<a href="#installing-a-plugin" class="unresolved">Installing a Plugin</a>  
<a href="https://studio.plugins.atlassian.com/wiki/display/WBDV/Confluence+WebDAV+Plugin" class="external-link">Confluence WebDAV Plugin</a>
