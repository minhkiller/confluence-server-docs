---
aliases:
- /server/confluence/atlassian-cache-2-overview-27561089.html
- /server/confluence/atlassian-cache-2-overview-27561089.md
category: devguide
confluence_id: 27561089
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=27561089
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=27561089
date: '2017-12-08'
guides: guides
legacy_title: Atlassian Cache 2 Overview
platform: server
product: confluence
subcategory: learning
title: Atlassian Cache 2 overview
---
# Atlassian Cache 2 overview

## Introduction

Atlassian is rolling out a new version of the Atlassian Cache API (atlassian-cache-api-2.x) that will be used within Atlassian products (initially JIRA 6.2 and Confluence 5.5). The API has been designed to be backwardly compatible with atlassian-cache-api-0.1.

The main interfaces in the API from an end-user perspective are:

-   <a href="https://docs.atlassian.com/atlassian-cache-api/2.2.0/atlassian-cache-api/apidocs/com/atlassian/cache/Cache.html" class="external-link">Cache</a> - represents a cache
-   <a href="https://docs.atlassian.com/atlassian-cache-api/2.2.0/atlassian-cache-api/apidocs/com/atlassian/cache/CacheLoader.html" class="external-link">CacheLoader</a> - used with a <a href="https://docs.atlassian.com/atlassian-cache-api/2.2.0/atlassian-cache-api/apidocs/com/atlassian/cache/Cache.html" class="external-link">Cache</a> to provide read-through semantics (see [Cache Population](#cache-population)).
-   <a href="https://docs.atlassian.com/atlassian-cache-api/2.2.0/atlassian-cache-api/apidocs/com/atlassian/cache/CacheLoader.html" class="external-link">CachedReference</a> - represents a resettable reference that is backed by a cache.
-   <a href="https://docs.atlassian.com/atlassian-cache-api/2.2.0/atlassian-cache-api/apidocs/com/atlassian/cache/CacheLoader.html" class="external-link">Supplier</a> - used with a <a href="https://docs.atlassian.com/atlassian-cache-api/2.2.0/atlassian-cache-api/apidocs/com/atlassian/cache/CacheLoader.html" class="external-link">CachedReference</a> to generate the referenced object.
-   <a href="https://docs.atlassian.com/atlassian-cache-api/2.2.0/atlassian-cache-api/apidocs/com/atlassian/cache/CacheFactory.html" class="external-link">CacheFactory</a> - responsible for creating <a href="https://docs.atlassian.com/atlassian-cache-api/2.2.0/atlassian-cache-api/apidocs/com/atlassian/cache/Cache.html" class="external-link">Cache</a> and <a href="https://docs.atlassian.com/atlassian-cache-api/2.2.0/atlassian-cache-api/apidocs/com/atlassian/cache/CachedReference.html" class="external-link">CachedReference</a> instances which may be configured using <a href="https://docs.atlassian.com/atlassian-cache-api/2.2.0/atlassian-cache-api/apidocs/com/atlassian/cache/CacheSettings.html" class="external-link">CacheSettings</a>.

## Cache types

The Cache API supports three distinct types of caches:

1.  Local - the data in a local cache is held on the JVM. The keys and values are stored by reference.
2.  Cluster - the data in a cluster cache is replicated across the cluster. The keys and values are stored by value, and hence must be Serializable (this is only enforced at run-time).
3.  Hybrid - the data in a hybrid cache is stored locally on each node in a cluster, and is kept synchronized via invalidation messages. The keys and values are stored by reference, however the keys must be Serializable (this is only enforced at run-time).

See [Cache Creation](#cache-creation) for how to specify the type of cache.

## Cache population

For populating the data in a cache, the Cache API supports two styles:

1.  Read Through Cache semantics where values for cache misses are retrieved using a supplied <a href="https://docs.atlassian.com/atlassian-cache-api/2.2.0/atlassian-cache-api/apidocs/com/atlassian/cache/CacheLoader.html" class="external-link">CacheLoader</a> when the cache is created. This is the preferred style for new development.
2.  Traditional put-style semantics where values are put into the cache using the <a href="https://docs.atlassian.com/atlassian-cache-api/2.2.0/atlassian-cache-api/apidocs/com/atlassian/cache/Cache.html#put(K,%20V)" class="external-link">put(K, V)</a> and <a href="https://docs.atlassian.com/atlassian-cache-api/2.2.0/atlassian-cache-api/apidocs/com/atlassian/cache/Cache.html#putIfAbsent(K,%20V)" class="external-link">putIfAbsent(K, V)</a> methods. This is supported for backwards compatibility.

{{% warning %}}

There is a limitation that any implementation of <a href="https://docs.atlassian.com/atlassian-cache-api/2.2.0/atlassian-cache-api/apidocs/com/atlassian/cache/CacheLoader.html" class="external-link">CacheLoader</a> that is associated with a non-local <a href="https://docs.atlassian.com/atlassian-cache-api/2.2.0/atlassian-cache-api/apidocs/com/atlassian/cache/Cache.html" class="external-link">Cache</a> must not make calls to any other non-local <a href="https://docs.atlassian.com/atlassian-cache-api/2.2.0/atlassian-cache-api/apidocs/com/atlassian/cache/Cache.html" class="external-link">Cache</a> or <a href="https://docs.atlassian.com/atlassian-cache-api/2.2.0/atlassian-cache-api/apidocs/com/atlassian/cache/CacheLoader.html" class="external-link">CachedReference</a>.

This limitation also applies to any implementation of <a href="https://docs.atlassian.com/atlassian-cache-api/2.2.0/atlassian-cache-api/apidocs/com/atlassian/cache/CacheLoader.html" class="external-link">Supplier</a> that is associated with a non-local <a href="https://docs.atlassian.com/atlassian-cache-api/2.2.0/atlassian-cache-api/apidocs/com/atlassian/cache/CacheLoader.html" class="external-link">CachedReference</a>.

{{% /warning %}}

## Cache eviction

The Cache API supports two rules for eviction of values from a cache, which are specified in the <a href="https://docs.atlassian.com/atlassian-cache-api/2.2.0/atlassian-cache-api/apidocs/com/atlassian/cache/CacheSettings.html" class="external-link">CacheSettings</a>:

1.  <a href="https://docs.atlassian.com/atlassian-cache-api/2.2.0/atlassian-cache-api/apidocs/com/atlassian/cache/CacheSettingsBuilder.html#expireAfterAccess(long,%20java.util.concurrent.TimeUnit)" class="external-link">expireAfterAccess(long, java.util.concurrent.TimeUnit)</a> - evicts entries that have not been accessed for a specified duration.
2.  <a href="https://docs.atlassian.com/atlassian-cache-api/2.2.0/atlassian-cache-api/apidocs/com/atlassian/cache/CacheSettingsBuilder.html#expireAfterWrite(long,%20java.util.concurrent.TimeUnit)" class="external-link">expireAfterWrite(long, java.util.concurrent.TimeUnit)</a> - evicts entries that have not been updated since a specified duration.

To be precise, the rules are actually hints to the underlying cache provider that may or may not be observed. The user of the cache should be prepared to handle their values being evicted at anytime.

## Cache creation

When creating a cache, it is possible to provide a <a href="https://docs.atlassian.com/atlassian-cache-api/2.2.0/atlassian-cache-api/apidocs/com/atlassian/cache/CacheSettings.html" class="external-link">CacheSettings</a> to control the configuration of a cache, which are created using the <a href="https://docs.atlassian.com/atlassian-cache-api/2.2.0/atlassian-cache-api/apidocs/com/atlassian/cache/CacheSettingsBuilder.html" class="external-link">CacheSettingsBuilder</a>. See <a href="https://docs.atlassian.com/atlassian-cache-api/2.2.0/atlassian-cache-api/apidocs/com/atlassian/cache/CacheSettingsBuilder.html" class="external-link">CacheSettingsBuilder</a> for all the settings that may be specified.

When settings are not explicitly specified, the product specific default will be used. See each product's documentation for detailed information on how defaults are determined.

The following table describes the how the different cache types are created based on the two properties <a href="https://docs.atlassian.com/atlassian-cache-api/2.2.0/atlassian-cache-api/apidocs/com/atlassian/cache/CacheSettingsBuilder.html#remote()" class="external-link">remote()</a> and <a href="https://docs.atlassian.com/atlassian-cache-api/2.2.0/atlassian-cache-api/apidocs/com/atlassian/cache/CacheSettingsBuilder.html#replicateViaCopy()" class="external-link">replicateViaCopy()</a>.

| remote() | replicateViaCopy() | Cache Type Created |
|----------|--------------------|--------------------|
| false    | false              | Local              |
| false    | true               | Local              |
| true     | false              | Hybrid             |
| true     | true               | Cluster            |

{{% warning %}}

When creating a hybrid cache, be sure to associate a <a href="https://docs.atlassian.com/atlassian-cache-api/2.2.0/atlassian-cache-api/apidocs/com/atlassian/cache/CacheLoader.html" class="external-link">CacheLoader</a> with the cache by using either of the <a href="https://docs.atlassian.com/atlassian-cache-api/2.2.0/atlassian-cache-api/apidocs/com/atlassian/cache/CacheFactory.html#getCache(java.lang.String,%20com.atlassian.cache.CacheLoader)" class="external-link">getCache(String, CacheLoader)</a> or <a href="https://docs.atlassian.com/atlassian-cache-api/2.2.0/atlassian-cache-api/apidocs/com/atlassian/cache/CacheFactory.html#getCache(java.lang.String,%20com.atlassian.cache.CacheLoader,%20com.atlassian.cache.CacheSettings)" class="external-link">getCache(String, CacheLoader, CacheSettings)</a> methods when obtaining the cache. If a <a href="https://docs.atlassian.com/atlassian-cache-api/2.2.0/atlassian-cache-api/apidocs/com/atlassian/cache/CacheLoader.html" class="external-link">CacheLoader</a> is not associated, and the <a href="https://docs.atlassian.com/atlassian-cache-api/2.2.0/atlassian-cache-api/apidocs/com/atlassian/cache/Cache.html#put(K,%20V)" class="external-link">put(K, V)</a> or <a href="https://docs.atlassian.com/atlassian-cache-api/2.2.0/atlassian-cache-api/apidocs/com/atlassian/cache/Cache.html#putIfAbsent(K,%20V)" class="external-link">putIfAbsent(K, V)</a> are used, then the cache will not operate correctly in a cluster, as each put operation will invalidate local entries in all other nodes in the cluster.

{{% /warning %}}{{% warning %}}

Whenever a cache is created with an associated <a href="https://docs.atlassian.com/atlassian-cache-api/2.2.0/atlassian-cache-api/apidocs/com/atlassian/cache/CacheLoader.html" class="external-link">CacheLoader</a> by using either of the <a href="https://docs.atlassian.com/atlassian-cache-api/2.2.0/atlassian-cache-api/apidocs/com/atlassian/cache/CacheFactory.html#getCache%28java.lang.String,%20com.atlassian.cache.CacheLoader%29" class="external-link">getCache(String, CacheLoader)</a> or <a href="https://docs.atlassian.com/atlassian-cache-api/2.2.0/atlassian-cache-api/apidocs/com/atlassian/cache/CacheFactory.html#getCache%28java.lang.String,%20com.atlassian.cache.CacheLoader,%20com.atlassian.cache.CacheSettings%29" class="external-link">getCache(String, CacheLoader, CacheSettings)</a> methods, then the returned <a href="https://docs.atlassian.com/atlassian-cache-api/2.2.0/atlassian-cache-api/apidocs/com/atlassian/cache/Cache.html" class="external-link">Cache</a> object should be retained for future use. If the returned <a href="https://docs.atlassian.com/atlassian-cache-api/2.2.0/atlassian-cache-api/apidocs/com/atlassian/cache/Cache.html" class="external-link">Cache</a> object is not retained for re-use, then every subsequent call to obtain a new <a href="https://docs.atlassian.com/atlassian-cache-api/2.2.0/atlassian-cache-api/apidocs/com/atlassian/cache/Cache.html" class="external-link">Cache</a> object will result in the current contents of the underlying cache to be evicted.

{{% /warning %}}

## Cache data sharing guidelines

This section provides guidelines for developers wanting to share data stored in a cache. Sharing can be between:

-   Different plugins.
-   Different versions of a plugin (after an upgrade/downgrade).

### Local cache data

Local cache keys and values are stored by reference. Therefore, to ensure that the data can be accessed between plugins and plugin restarts, only use classes provided by:

-   Java JDK (e.g. `java.lang.String`); OR
-   The product core (such as Confluence core classes).

Note that a plugin can serialize custom classes to a portable format that is stored as a `java.lang.String` or `byte[]`.

### Cluster cache data

Cluster cache keys and values are stored by value, which means they are serialized on insertion to the cache, and deserialized on extraction.

Therefore, to ensure that the data can be shared, both product and plugin developers must ensure that the objects placed into a cluster cache implement serialization with support for versioning. See <a href="http://docs.oracle.com/javase/7/docs/platform/serialization/spec/version.html" class="external-link">Versioning of Serializable Objects</a>.

It's recommended that plugin developers use classes from the Java JDK and product core. If custom classes are required to be shared between plugins, then a common shared library should be used for defining them.

### Hybrid cache data

Hybrid cache keys are stored by value, and values are stored by reference.

Therefore:

-   Keys should adhere to the guidelines in [Cluster Cache Data](#cluster-cache-data), AND
-   Values should adhere to the guidelines in [Local Cache Data](#local-cache-data).

### Cache code examples

The following example shows how to obtain a cache called "old-style" using defaults.

``` java
Cache<String, String> cache = cacheManager.getCache("old-style");

// Returns null
cache.get("money");
```

The following example shows how to obtain a cache called "with-loader" that uses a CacheLoader.

``` java
Cache<String, String> cache =
    cacheManager.getCache(
    "with-loader",
    new CacheLoader<String, String>() {
        public String load(String key) {
            return "value for " + key;
        }
    });

// Returns "value for money"
cache.get("money");
```

The following example shows how to obtain a hybrid cache called "adam" that expires values after 60 seconds of no access.

``` java
CacheSettings required =
    new CacheSettingsBuilder().
        remote().
        expireAfterAccess(60, TimeUnit.SECONDS).
        build();

Cache<String, String> c =
    cacheManager.getCache("adam", someCacheLoader, required);
```

## CachedReference code examples

The following example shows how to use a <a href="https://docs.atlassian.com/atlassian-cache-api/2.2.0/atlassian-cache-api/apidocs/com/atlassian/cache/CacheLoader.html" class="external-link">CachedReference</a>.

``` java
Supplier<Date> generateTheAnswer = new Supplier<>()
{
    public Date get()
    {
        return new Date();
    }
}

CachedReference<Date> cr = 
    factory.getCachedReference(
        "mycache", generateTheAnswer);
cr.get(); // will call generateTheAnswer
cr.get(); // will get from the cache
cr.reset();
cr.get(); // will call generateTheAnswer
```
