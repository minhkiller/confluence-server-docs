---
aliases:
- /server/confluence/log-into-confluence-39368892.html
- /server/confluence/log-into-confluence-39368892.md
category: devguide
confluence_id: 39368892
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39368892
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39368892
date: '2017-12-08'
legacy_title: Log into Confluence
platform: server
product: confluence
subcategory: other
title: Log into Confluence
---
# Log into Confluence

To log into Confluence, visit <a href="http://localhost:1990/confluence" class="uri external-link">http://localhost:1990/confluence</a> in a web browser.  Login as the administrator (username = admin, password = admin)

<img src="https://wiki2.collaboration.is.keysight.com/download/attachments/22282494/image2015-9-28%2015%3A49%3A45.png?version=1&amp;modificationDate=1455032501047&amp;api=v2" class="confluence-external-resource" height="250" />
