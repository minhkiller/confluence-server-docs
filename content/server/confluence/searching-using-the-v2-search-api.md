---
aliases:
- /server/confluence/searching-using-the-v2-search-api-2031828.html
- /server/confluence/searching-using-the-v2-search-api-2031828.md
category: devguide
confluence_id: 2031828
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031828
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031828
date: '2017-12-08'
guides: guides
legacy_title: Searching Using the V2 Search API
platform: server
product: confluence
subcategory: learning
title: Searching using the v2 search API
---
# Searching using the v2 search API

The v2 search API provides a fast way of searching content within Confluence. We highly recommend that all plugin authors switch to this API where possible.

To illustrate how to use this API, we have included a simple code snippet for a basic search that:

-   searches for all content labelled with **administration** in the space with key **DOC**.
-   sorts these results with the latest modified content displayed first.
-   limits the number of results to 10.

``` java
SearchQuery query = BooleanQuery.composeAndQuery(new LabelQuery("administration"), new InSpaceQuery("DOC"));
SearchSort sort = new ModifiedSort(SearchSort.Order.DESCENDING); // latest modified content first
SearchFilter securityFilter = SiteSearchPermissionsSearchFilter.getInstance();
ResultFilter resultFilter = new SubsetResultFilter(10);

Search search = new Search(query, sort, securityFilter, resultFilter);

SearchResults searchResults;
try
{
    searchResults = searchManager.search(search);
}
catch (InvalidSearchException e)
{
    // discard search and assign empty results
    searchResults = LuceneSearchResults.EMPTY_RESULTS;
}

// iterating over search results
for (SearchResult searchResult : searchResults.getAll())
{
    System.out.println("Title: " + searchResult.getDisplayTitle());
    System.out.println("Content: " + searchResult.getContent());
    System.out.println("SpaceKey: " + searchResult.getSpaceKey());
}

// total number of results found
System.out.println("Total number of results: " + searchResults.getUnfilteredResultsCount());
```

Further comments:

-   Please ensure you include `com.atlassian.confluence.search.v2.searchfilter.SiteSearchPermissionsSearchFilter` in your search. This is a bundled filter that will handle permission checking and content filtering automatically for you.
-   The number of results returned has been limited with the use of `com.atlassian.confluence.search.v2.filter.SubsetResultFilter`. This class efficiently filters search results during search time.
-   The search is executed using `searchManager.search(search)`. This invocation returns search results populated with data from your index.
    -   To iterate over the search results returned, you can get a reference to the list of search results with `searchResults.getAll()` or an iterator to this list using `searchResults.iterator()`.
    -   Common information about a search result like title, body and space key can be extracted from the search result using `getDisplayTitle()`, `getContent()` and `getSpaceKey()` respectively. For more accessors see the [API documentation](/server/confluence/java-api-reference) for `com.atlassian.confluence.search.v2.SearchResult`.
    -   This invocation does not go to the database to construct any search results. If you want `com.atlassian.bonnie.Searchable` objects from the database to be returned by the search, call `searchManager.searchEntities(search)` instead.
-   An exception `com.atlassian.confluence.search.v2.InvalidSearchException` is thrown when either:
    -   there is an error mapping a v2 search object to the corresponding Lucene search object, or
    -   no mapper could be found to map one of the search objects. (The mapper plugin responsible for mapping this search may have been uninstalled.)
-   You should simply discard the search if an exception is thrown as described above.

##### RELATED TOPICS

-   [Java API Reference](/server/confluence/java-api-reference)
