---
aliases:
- /server/confluence/confluence-server-rest-api-2031699.html
- /server/confluence/confluence-server-rest-api-2031699.md
category: reference
confluence_id: 2031699
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031699
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031699
date: '2017-12-08'
legacy_title: Confluence Server REST API
platform: server
product: confluence
subcategory: api
title: Confluence Server REST API
---
# Confluence Server REST API

|            |                                 |
|------------|---------------------------------|
| Applicable | Confluence Server 5.5 and later |

The Confluence Server REST API is for admins who want to script interactions with Confluence and developers who want to integrate with or build on top of the Confluence platform.

For REST API documentation, go to <a href="https://docs.atlassian.com/atlassian-confluence/REST/latest-server/" class="external-link">Confluence Server REST API reference</a>.

{{% tip %}}

You can find information on the **Confluence Cloud REST API** [here](https://developer.atlassian.com/display/CONFCLOUD/Confluence+REST+API).

{{% /tip %}}

## CRUD Operations

Confluence's REST APIs provide access to resources (data entities) via URI paths. To use a REST API, your application will make an HTTP request and parse the response. By default, the response format is JSON. Your methods will be the standard HTTP methods: GET, PUT, POST and DELETE. The REST API is based on open standards, so you can use any web development language to access the API.

For some examples of what you can do with the REST API, see [Confluence REST API Examples](/server/confluence/confluence-rest-api-examples).

## Pagination

Confluence's REST API provides a way to paginate your calls to limit the amount of data you are fetching. Read more about this in [Pagination in the REST API](/server/confluence/pagination-in-the-rest-api).

## Expansions

Confluence's REST API also provides a way to fetch more data in a single call through REST API Expansions. Read more about this in [Expansions in the REST API](/server/confluence/expansions-in-the-rest-api).

## Advanced Search Through CQL

Confluence's REST API provides an [advanced search](/server/confluence/advanced-searching-using-cql) that lets you use structured queries to search for content within Confluence. Your search results will take the same form as the Content model returned by the Content REST API. 

## Content and Space Properties

Content and Space Properties are JSON key-value storages that you can access through the REST API. This is a great way, for example, to store metadata about a piece (or pieces) of content. Read more about [Content Properties.](/server/confluence/content-properties-in-the-rest-api)

## REST API Policy

Read the [Atlassian REST API Policy ](https://developer.atlassian.com/display/HOME/Atlassian+REST+API+policy)for information on compatibility, versioning and deprecation. 

## REST API Browser

The Atlassian REST API Browser is a tool for discovering the REST APIs available in Atlassian applications, including Confluence. This is available as part of the Atlassian SDK or can be <a href="https://marketplace.atlassian.com/plugins/com.atlassian.labs.rest-api-browser" class="external-link">downloaded from the Atlassian Marketplace</a>.

See [Using the REST API Browser](https://developer.atlassian.com/display/DOCS/Using+the+REST+API+Browser).
