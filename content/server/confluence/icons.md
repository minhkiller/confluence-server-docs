---
aliases:
- /server/confluence/icons-6849929.html
- /server/confluence/icons-6849929.md
category: devguide
confluence_id: 6849929
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=6849929
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=6849929
date: '2017-12-08'
guides: guides
legacy_title: Icons
platform: server
product: confluence
subcategory: learning
title: Icons
---
# Icons

Confluence provides an icon sprite which can be reused by using recommended markup. It will ensure that extra requests for images aren't made uncessarily and you will also get new versions of the icons, when they change.

Simple add a span with class `icon` and one of the icon types below to display a Confluence icon:

-   icon-space
-   icon-personal-space
-   icon-user
-   icon-home-page
-   icon-page
-   icon-add-page
-   icon-blogpost
-   icon-mail
-   icon-status
-   icon-show-more
-   icon-show-less
-   icon-file-pdf
-   icon-file-image
-   icon-file-html
-   icon-file-xml
-   icon-file-java
-   icon-file-text
-   icon-file-zip
-   icon-file-word
-   icon-file-excel
-   icon-file-powerpoint
-   icon-file-unknown
-   icon-remove
-   icon-edit
-   icon-tick
-   icon-cross
-   icon-add-fav
-   icon-remove-fav

  
The following example markup will display the page icon: ![](/server/confluence/images/page-16.png)

``` xml
<span class="icon icon-page">Test Page</span>
```
