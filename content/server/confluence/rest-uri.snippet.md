---
aliases:
- /server/confluence/-rest-uri-2031623.html
- /server/confluence/-rest-uri-2031623.md
category: devguide
confluence_id: 2031623
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031623
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031623
date: '2017-12-08'
legacy_title: _REST URI
platform: server
product: confluence
subcategory: other
title: _REST URI
---
# \_REST URI

URIs for a Confluence REST API resource have the following structure:  
With context:

``` xml
http://host:port/context/rest/api-name/api-version/resource-name
```

Or without context:

``` xml
http://host:port/rest/api-name/api-version/resource-name
```

NOTE: In Confluence 3.1 and Confluence 3.2, the only available `api-name` is `prototype`.  
  
**Examples:**  
With context:

``` xml
http://myhost.com:8080/confluence/rest/prototype/1/space/ds
http://localhost:8080/confluence/rest/prototype/latest/space/ds
```

Or without context:

``` xml
http://confluence.myhost.com:8095/rest/prototype/1/space/ds
http://confluence.myhost.com:8095/rest/prototype/latest/space/ds
```

##### RELATED TOPICS

[Confluence REST APIs - Prototype Only](/server/confluence/confluence-rest-apis-prototype-only)

