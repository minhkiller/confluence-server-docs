---
aliases:
- /server/confluence/preparing-for-confluence-6.7-52432450.html
- /server/confluence/preparing-for-confluence-6.7-52432450.md
category: devguide
confluence_id: 52432450
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=52432450
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=52432450
date: '2017-12-08'
legacy_title: Preparing for Confluence 6.7
platform: server
product: confluence
subcategory: development resources
title: Preparing for Confluence 6.7
---
# Preparing for Confluence 6.7

 

This documentation is intended for Confluence developers who want to ensure that their existing plugins and add-ons are compatible with Confluence 6.7.

We plan to release frequent milestones during the development of Confluence 6.7. Watch this page to find out when a new milestone is available and what's changed.  We won't be publishing formal release notes over at <a href="http://confluence.atlassian.com/display/DOC/Confluence+Development+Releases" class="external-link">Development Releases</a> until we release a beta.

{{% note %}}

**Latest milestone**

|              |           |                                                                                                         |
|--------------|-----------|--------------------------------------------------------------------------------------------------------:|
| 23 Jan 2018  | 6.7.0-rc1  |  <a href="https://www.atlassian.com/software/confluence/download-eap" class="external-link">Download</a>|

{{% /note %}}

**Changes in Confluence 6.7**

**Issues with this milestone?**

Hit the Feedback button on the Confluence EAP header or <a href="https://jira.atlassian.com/secure/CreateIssueDetails!init.jspa?pid=10470&amp;issuetype=10000&amp;components=43692&amp;summary=Confluence+6.7+EAP+Feedback" class="external-link">raise an issue</a> to tell us about it. 

Looking for updated documentation? Check out the <a href="https://confluence.atlassian.com/display/CONFEAP/Confluence+EAP+Documentation" class="external-link">Confluence EAP</a> space for the latest docs. 

Did you know we've got a new developer community? Head to  <a href="https://community.developer.atlassian.com/" class="uri external-link">community.developer.atlassian.com/</a>  to check it out!  We'll be posting in the **announcements**  category if when new EAP releases are available.  

## Release candidate - 23 January

This release contains an upgrade to AUI 7.6.1, some bug fixes and small visual changes. 

## Beta 1 - 16 January

In this beta:

- Upgrade to AUI 7.6.0-beta2
- Bug fixes and more small visual changes

For a complete run down of all the features and changes in this beta, head to our <a href="https://confluence.atlassian.com/display/DOC/Confluence+6.7.0-beta+release+notes" class="external-link">Confluence 6.7.0-beta release notes</a>. 

## Milestone 50 - 9 January
In this milestone:

- Upgrade to AUI 7.5.0-alpha3
- Visual changes to the following areas:
 - New Confluence logo
 - Create dialog
 - Editor toolbars
 - Space sidebars
 - View page / blog
 - Icons

**New Confluence icon library**

The new Confluence icon library aims to centralise and standardize the way we interact with icons in Confluence. The library provides a number of CSS classes which can be used to display icons in your pages and plugins. The library supports interaction with these CSS classes only, any other type of interaction with our icons (by directly referencing images for example) is not supported, and should be considered volatile. 

The Confluence icon library is a P2 plugin and is available from Confluence 6.7.0. To use it in your plugin, place a dependency on the plugin in your `atlassian-plugin.xml` file, for example:

``` java
<web-resource name="Common web resources" key="common-resources">
    <description>Common assets used by all aspects of this plugin</description>
    <resource name="mystyle.css" type="download" location="assets/styles/mystyle.css"/>
    <dependency>com.atlassian.confluence.plugins.confluence-icons:confluence-icon-resources</dependency>
</web-resource>
```
See <a href="https://bitbucket.org/atlassian/confluence-icons/src" class="external-link">https://bitbucket.org/atlassian/confluence-icons/src</a> for the full list of CSS classes included in the library.


## Milestone 43 - 3 January
No notable changes in this milestone.

## Milestone 28 - 18 December
No notable changes in this milestone.

## Milestone 20 - 11 December

This is our first public milestone for Confluence 6.7. We're making it available a little earlier than usual as there are some [exciting changes to the look and feel](#exciting-changes-to-the-look-and-feel) on the way. Keep an eye on this page, as we'll be releasing a milestone every week in the lead up to Confluence 6.7.

In this milestone:

-   Code block macro now includes YAML syntax highlighting
-   Upgrade to AUI 7.4
-   Changes to the following components:  
    -   fonts and line height
    -   colours
    -   button styles
    -   modals
    -   tabs
    -   flags and messages
    -   profile pictures (now round in most places).

Some icons have also been updated, however, we're still working on these and plan to improve the way we implement icons throughout Confluence, so stay tuned for more information on this in an upcoming milestone. 

## Updated look and feel for Confluence

We're bringing a refreshed look and feel to many of our Server and Data Center products, and Confluence is the first cab off the rank. This work is based on the new <a href="https://atlassian.design/" class="external-link">Atlassian design</a> and will include things like the updated colour palette, typography and icons. We're not currently working on any significant changes to navigation, or features like Home, that you may have seen in our Cloud products. 

We'll be implementing many of these design changes via an upgrade to AUI 7.x. The <a href="https://docs.atlassian.com/aui/latest/docs/upgrade-guide.html" class="external-link">AUI Upgrade Guide</a> has details of all the recent changes. In case you missed it, there have also been some changes to AUI licensing, see <a href="https://community.developer.atlassian.com/t/the-license-for-aui-7-is-changing/10071" class="external-link">The license for AUI 7 is changing</a> in our developer community for more info. 

We've aimed to minimise the impact to add-ons as much as possible, however as we release Confluence milestones, you should check your add-ons, and may want to update any non-AUI or non-product UI components to match the design guidelines. 

We're super excited to be bringing you these changes, and know that customers are going to love the fresh, modern look. 

 Here's a sneak peek of what we're working towards. These sketches are just indicative, so things may change along the way.

<img src="/server/confluence/images/popular-feed@2x.png" height="250" />  <img src="/server/confluence/images/editor@2x.png" height="250" />  

## Mobile app beta

A new mobile app for Confluence Server and Data Center is in the works. To find out how to try the beta, and what's involved to make your macros appear in mobile, head to [Preparing for the Confluence Server mobile app](/server/confluence/preparing-for-the-confluence-server-mobile-app). 

## Editor changes

We're in the process of upgrading the Confluence editor from TinyMCE 3 to TinyMCE 4, and expect this work to land in or before Confluence 7.0. While this work is in progress we will be taking effort to make these changes in a backwards compatible way, so as not to impact upcoming Confluence 6.x releases. While we are working on it, however, it is possible that your plugin might experience some issues with the changes.

In preparation for the upgrade to TinyMCE 4 we are splitting the editor context into 3 separate contexts internally. "editor" will remain the official context to contribute resources as a plugin developer. Internally, we will also be conditionally loading either "editor-v3" or "editor-v4" context depending on whether TinyMCE 3 or TinyMCE 4 is active, during the transition. TinyMCE 3 will continue to be the current editor until the announcement of TinyMCE 4. This means that any code loaded currently in "editor" context will continue to work normally in 6.7. However, if a plugin is loading the "editor" context on its own, for a custom editor experience, the resources loaded by "editor-v3" may not be loaded unless the `atlassian-plugin.xml` resources being used declare explicit dependencies on those resources.

If your plugin is currently requesting the "editor" context, you can verify that your plugin will continue to work normally by also requesting the "editor-v3" context ahead of the "editor" context (the order is important). This is a backwards compatible change, and can be easily added to any plugin that is doing this, without adverse effects to previous Confluence versions. The specific bugs we are aware of in this case have to do with link insert and autocomplete, as well as mentions autocomplete. They do not work at all and this is a quick way to check if your plugin editor is affected. There is no guarantee that we will remove the need to do this before the final 6.7 release, so we strongly recommend that you load editor-v3, as well, if you are loading editor, so that your plugin continues to work. Again, this does not affect your plugin if it is merely adding resources to `<context>editor</context>` in your `atlassian-plugin.xml`. However, you might experience other issues.  
  
If you do experience issues with your plugin, it would be good to hear from you in an <a href="http://go.atlassian.com/eap-editor-feedback" class="external-link" title="http://go.atlassian.com/eap-editor-feedback">EAP Feedback ticket</a>, so that we can take appropriate action to help to resolve it.

We'll keep you updated throughout the process, and will provide additional communication about any API changes that might be necessary before the final upgrade release. We will also be providing additional EAP milestones so you can test the editor upgrade with your plugins.
