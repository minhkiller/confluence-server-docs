---
aliases:
- /server/confluence/eventlistener-example-2031800.html
- /server/confluence/eventlistener-example-2031800.md
category: reference
confluence_id: 2031800
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031800
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031800
date: '2017-12-08'
legacy_title: EventListener Example
platform: server
product: confluence
subcategory: modules
title: EventListener example
---
# EventListener example

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Available:</p></td>
<td><p>Confluence 1.4 and later</p></td>
</tr>
<tr class="even">
<td><p>Changed:</p></td>
<td><p>In Confluence 3.3 and later, Confluence events are annotation-based. This means that you have two options when writing event listeners. Option 1 is the Event Listener plugin module, using the <code>com.atlassian.event.EventListener</code> class. Option 2 is to declare your listener as a <a href="/server/confluence/component-module">component</a> and use annotation-based event listeners, annotating your methods to be called for specific event types. The component will need to register itself at the time it gets access to the EventPublisher, typically in the constructor. More information and examples are in <a href="/server/confluence/event-listener-module">Event Listener Module</a>.</p></td>
</tr>
</tbody>
</table>

This page gives the example code for an event listener, as described in the page about [event listener plugins](/server/confluence/event-listener-module).

Below is an example of an EventListener that listens for the LoginEvent and LogoutEvent.

``` java
package com.atlassian.confluence.extra.userlister;

import com.atlassian.confluence.event.events.security.LoginEvent;
import com.atlassian.confluence.event.events.security.LogoutEvent;
import com.atlassian.event.Event;
import com.atlassian.event.EventListener;
import org.apache.log4j.Logger;

public class UserListener implements EventListener {
    private static final Logger log = Logger.getLogger(UserListener.class);

    private Class[] handledClasses = new Class[]{ LoginEvent.class, LogoutEvent.class};


    public void handleEvent(Event event) {
        if (event instanceof LoginEvent) {
            LoginEvent loginEvent = (LoginEvent) event;
            log.info(loginEvent.getUsername() + " logged in (" +  loginEvent.getSessionId() + ")");
        } else if (event instanceof LogoutEvent) {
            LogoutEvent logoutEvent = (LogoutEvent) event;
            log.info(logoutEvent.getUsername() + " logged out (" +  logoutEvent.getSessionId() + ")");
        }
}

    public Class[] getHandledEventClasses() {
        return handledClasses;
    }
    
}
```
