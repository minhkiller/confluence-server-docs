---
aliases:
- /server/confluence/approval-workflow-2031635.html
- /server/confluence/approval-workflow-2031635.md
category: reference
confluence_id: 2031635
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031635
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031635
date: '2017-12-08'
legacy_title: Approval Workflow
platform: server
product: confluence
subcategory: modules
title: Approval Workflow
---
# Approval Workflow

This page describes the details of an approval workflow.

-   Users may be members of an 'author' group which is allowed to edit pages, an 'approver' group which is allowed to approve edited pages, or both groups (in which case they can't approve their own changes) or neither (in which case they are just consumers of the content).

<!-- -->

-   When an 'author' edits a page, the page goes into a 'editing in progress' state.

<!-- -->

-   When an author views an 'editing in progress' page, they are presented with an option to submit the page for review. This puts the page into the 'waiting for approval' state.

<!-- -->

-   Members of the approver group have access to a page in confluence which automatically lists the pages waiting for approval.

<!-- -->

-   When an 'approver' visits a 'waiting for approval' page, they are presented with options to accept or reject the changes. If they accept the changes, the page goes to the 'accepted' state, where pages spend most of their life, otherwise it goes to the 'rejected' state.

<!-- -->

-   Members of the 'author' group have access to a page in Confluence where they can see all the pages which they edited which have been rejected, or are waiting for approval. They don't see pages other authors have edited.

<!-- -->

-   When an author visits a page in the 'rejected' or 'waiting for approval' state, they have the option of withdrawing the change, which moves the page to the accepted state, and rolls back to the most recent approved version.

<!-- -->

-   When an author edits a page in the rejected state, it moves to the 'editing in progress' state.

All of this can be done with the <a href="#workflow-plugin-prototype" class="unresolved">Workflow Plugin Prototype</a>.

But we probably also want to show consumers the most recently approved version of a page, not the one currently under review. Without core Confluence changes, the best we can do is show users a banner which says "This content is being reviewed. The most recent approved content is *here*".
