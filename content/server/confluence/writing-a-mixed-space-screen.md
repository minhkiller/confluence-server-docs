---
aliases:
- /server/confluence/writing-a-mixed-space-screen-18252361.html
- /server/confluence/writing-a-mixed-space-screen-18252361.md
category: devguide
confluence_id: 18252361
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=18252361
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=18252361
date: '2017-12-08'
legacy_title: Writing a mixed Space screen
platform: server
product: confluence
subcategory: learning
title: Writing a mixed Space screen
---
# Writing a mixed Space screen

<table>
<colgroup>
<col style="width: 30%" />
<col style="width: 70%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Applicable:</p></td>
<td><p>This tutorial applies to <strong>Confluence 5.0.</strong></p></td>
</tr>
<tr class="even">
<td><p>Level of experience:</p></td>
<td><p>This is an <strong>advanced</strong> tutorial. You need to understand the Confluence Actions, templates and decorators.</p></td>
</tr>
</tbody>
</table>

This tutorial will explain how to display a Space screen in both situations:

-   **Doc Theme**  
    Displayed as a top-level Space tab  
    ![](/server/confluence/images/image2013-3-18-17:47:21.png)
-   **Default Theme in Confluence 5.0**  
    Displayed as a Space Tool  
    ![](/server/confluence/images/image2013-3-18-17:47:35.png)

We call it a mixed screen because:

-   In the Doc Theme, top-level tabs should display types of content, like Pages, Blog or Mail.
-   If you want to display settings for your add-on, they should appear under the Space Admin tab.

However we understand it makes sense for add-on developers to mix those two patterns.

## Preparation

The example below is available on the following BitBucket repository:

<a href="https://bitbucket.org/atlassian_tutorial/space-plugin-example" class="uri external-link">bitbucket.org/atlassian_tutorial/space-plugin-example</a>

## Web-items

As for the two previous tutorials, one web-item is required for each place:

**atlassian-plugin.xml**

``` xml
    <!-- Mixed Screen: Top-level Space tab in Doc Theme -->
    <web-item key="space-tab-link-for-mixed-screen" name="Top-level tab for the mixed screen" section="system.space" weight="50">
        <label key="mixed.screen.title"/>
        <link id="mixed-screen-tab-id">/plugins/${project.artifactId}/mixed.action?key=$generalUtil.urlEncode($helper.spaceKey)</link>
        <condition class="com.atlassian.confluence.plugin.descriptor.web.conditions.SpaceSidebarCondition" invert="true"/>
    </web-item>
    <!-- Mixed Screen: Space Tools tab -->
    <web-item key="space-tools-link-for-mixed-screen" name="Space Tools tab for the mixed screen" section="system.space.tools/addons" weight="50">
        <label key="mixed.screen.title"/>
        <link id="mixed-screen-space-tools-id">/plugins/${project.artifactId}/mixed.action?key=$generalUtil.urlEncode($helper.spaceKey)</link>
    </web-item>
```

## Action

An XWork definition will allow you to display the action:

**atlassian-plugin.xml**

``` xml
    <xwork name="Example Actions" key="example-actions">
        <description>Examples of actions</description>
        <package name="space-links-xwork-package" extends="default" namespace="/plugins/${project.artifactId}">
            <default-interceptor-ref name="validatingStack"/>
            <action name="mixed" class="com.atlassian.examples.MixedAction" method="doDefault">
                <result name="input" type="velocity">/templates/mixed-space-screen.vm</result>
            </action>
        </package>
    </xwork>
```

**MixedAction.java**

``` java
public class MixedAction extends SpaceAdminAction
{
    @Override
    public String doDefault()
    {
        return INPUT;
    }
}
```

## Use two decorators

**mixed-space-screen.vm**

``` xml
<html>
    <head>
        <title>$action.getText("mixed.screen.title")</title>
        <meta name="decorator" content="main"/>
    </head>

        #applyDecorator("root")
            #decoratorParam("helper" $action.helper)
            <!-- context is the web-item key. It must start with "space-" to display the space tabs. -->
            #decoratorParam("context" "space-tab-link-for-mixed-screen")
            #applyDecorator ("root")
                #decoratorParam ("context" "spacetoolspanel")
                #decoratorParam("helper" $action.helper)
                #decoratorParam("selectedSpaceToolsWebItem" "space-tools-link-for-mixed-screen")
                <body>
                    <p>This screen displays both as a top-level tab when the Doc Theme is activated, and as a Space Tools when the default theme is used.</p>
                </body>
            #end
        #end
</html>
```

## Result

The action is available at <a href="http://localhost:1990/confluence/plugins/space-links/mixed.action?key=ds" class="uri external-link">localhost:1990/confluence/plugins/space-links/mixed.action?key=ds</a>. The two images of the introduction show how the action displays.

### Related Content

[Web UI Modules](https://developer.atlassian.com/display/CONFDEV/Web+UI+Modules)

