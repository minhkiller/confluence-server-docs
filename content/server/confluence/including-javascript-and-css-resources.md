---
aliases:
- /server/confluence/including-javascript-and-css-resources-2031769.html
- /server/confluence/including-javascript-and-css-resources-2031769.md
category: devguide
confluence_id: 2031769
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031769
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031769
date: '2017-12-08'
guides: guides
legacy_title: Including Javascript and CSS resources
platform: server
product: confluence
subcategory: learning
title: Including Javascript and CSS resources
---
# Including Javascript and CSS resources

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Available:</p></td>
<td><p>Confluence 2.10 and later</p></td>
</tr>
<tr class="even">
<td><p>Deprecated:</p></td>
<td><p>DWR was deprecated in Confluence 3.3</p></td>
</tr>
</tbody>
</table>

Good style for web applications requires that JavaScript and CSS for web pages are kept separate to the HTML they enhance. Confluence itself is moving towards this model, and the tools that Confluence uses to do this are also available to plugin developers.

{{% note %}}

If you are developing a theme plugin and would like to include css resources, see [Theme Stylesheets](/server/confluence/creating-a-theme#usingstylesheets) instead.

{{% /note %}}

## Including a Custom JavaScript or CSS File from a Plugin

In your `atlassian-plugin.xml`, you should add a Web Resource module. See [Web Resource Module](/server/confluence/web-resource-module).

For each resource, the location of the resource should match the path to the resource in your plugin JAR file. Resource paths are namespaced to your plugin, so they can't conflict with resources in other plugins with the same location (unlike say i18n or Velocity resources). However, you may find it convenient to use a path name which is specific to your plugin to be consistent with these other types.

To include your custom web resource in a page where your plugin is used, you use the `#requireResource` Velocity macro like this:

``` java
#requireResource("com.acme.example.plugin:web-resource-key")
```

Where `"com.acme.example.plugin:web-resource-key"` should be your plugin key, a colon, and the key of the [web resource module](/server/confluence/web-resource-module) in your plugin.

Only one instance of each script or stylesheet will be included, and they will appear in the order they are requested in the Velocity rendering process.

{{% note %}}

The rich text editor does not currently use dynamic stylesheets provided by a macro rendered in this way.

{{% /note %}}

## Web Resource Configuration

Within your Web Resource plugin module, you will define one or more resource definitions. See [Adding Plugin and Module Resources](/server/confluence/adding-plugin-and-module-resources).

Note that you can declare the media type (for CSS resources) and whether the resource should be wrapped in an <a href="http://www.quirksmode.org/css/condcom.html" class="external-link">Internet Explorer conditional comment</a>. This feature is also described in [Adding Plugin and Module Resources](/server/confluence/adding-plugin-and-module-resources).

Here is a short example:

``` xml
<web-resource key="my-macro-resources">
    <resource type="download" name="macro.js" location="path/inside/jar/to/js/macro.js"/>
    <resource type="download" name="more-macro-stuff.js" location="path/inside/jar/to/js/more-macro-stuff.js"/>
    <resource type="download" name="macro.css" location="path/inside/jar/to/css/macro.css"/>
    <resource type="download" name="macro-ie.css" location="path/inside/jar/to/css/macro-ie.css">
        <param name="ieonly" value="true"/>
        <param name="title" value="IE styles for My Awesome Macro"/>
    </resource>
    <resource type="download" name="macro-print.css" location="path/inside/jar/to/css/macro-print.css">
        <param name="media" value="print"/>
    </resource>
    <dependency>confluence.web.resources:ajs</dependency> <!-- depends on jQuery/AJS -->
</web-resource>
```

See below for the libraries provided by Confluence which you can include as a dependency.

{{% note %}}

Resource dependencies are not supported in 2.10. You will need to define the depending resources explicitly

{{% /note %}}

## Including a JavaScript Library provided by Confluence

Confluence currently includes several JavaScript libraries which plugins can use. The versions of these libraries are subject to change, but only across major versions of Confluence.

In the Confluence source code, these libraries are included in a plugin XML file called `web-resources.xml`.

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Library</p></th>
<th><p>Web resource key</p></th>
<th><p>Confluence 3.2</p></th>
<th><p>Confluence 3.3+</p></th>
<th><p>Details</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>jQuery + AJS</p></td>
<td><pre><code>confluence
  .web
  .resources:ajs</code></pre></td>
<td><p>1.2.6</p></td>
<td><p>1.4.2</p></td>
<td><p>Atlassian's JS abstraction on top of jQuery provides a few additional pieces of functionality.</p></td>
</tr>
<tr class="even">
<td><p>jQuery</p></td>
<td><pre><code>confluence
  .web
  .resources:jquery</code></pre></td>
<td><p>1.2.6</p></td>
<td><p>1.4.2</p></td>
<td><p>For compatibility with prototype, you must use 'jQuery()' not '$' to access jQuery.</p></td>
</tr>
</tbody>
</table>

To include one of these libraries in all pages in which your Velocity template appear, simply use the `#requireResource` macro as above. For example, if your macro requires jQuery, add the following to its Velocity template:

``` java
#requireResource("confluence.web.resources:jquery")
```

##### Deprecated libraries

Use of Scriptaculous, Prototype and DWR is deprecated. Use of these libraries in Confluence core will be gradually replaced with jQuery over the next few releases. Plugin developers should start doing the same with their front-end code, because these libraries will at some point, be removed in a future release of Confluence.

{{% warning %}}

The 'Prototype' and 'Scriptaculous' libraries will no longer be available in Confluence 3.5.

{{% /warning %}}{{% warning %}}

DWR has been deprecated as of 3.3. Support for the client side Javascript proxies has been moved into the Confluence Legacy Web Resources plugin. This plugin is disabled by default. If you need any of the following web resources you have to enable this plugin:

-   DWR framework
-   DWR Javascript proxies for label (add, remove, suggest) or editor operations (heartbeat, draft saving, editor preferences)

{{% /warning %}}

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 25%" />
<col style="width: 20%" />
<col style="width: 35%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Library</p></th>
<th><p>Web resource key</p></th>
<th><p>Current version</p></th>
<th><p>Details</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Scriptaculous</p></td>
<td><pre><code>confluence
  .web
  .resources:scriptaculous</code></pre></td>
<td><p>1.5rc3</p></td>
<td><p><strong>Deprecated. Do not use.</strong> Includes effects, dragdrop, controls and util.</p></td>
</tr>
<tr class="even">
<td><p>Prototype</p></td>
<td><pre><code>confluence
  .web
  .resources:prototype</code></pre></td>
<td><p>1.4.0_pre11</p></td>
<td><p><strong>Deprecated. Do not use.</strong> Version found in the scriptaculous lib directory. Also includes a domready extension, see domready.js.</p></td>
</tr>
<tr class="odd">
<td><p>DWR</p></td>
<td><pre><code>confluence
  .web
  .resources:dwr</code></pre></td>
<td><p>1.1.4</p></td>
<td><p><strong>Deprecated. Do not use.</strong> Includes engine and util.</p></td>
</tr>
</tbody>
</table>

## Running Scripts when the Page Loads

The recommended way to load scripts when the page is ready, known as 'on-DOM-ready', is to use the Atlassian JavaScript (AJS) abstraction. This avoids depending on a particular JavaScript library which may not remain in Confluence.

``` javascript
AJS.toInit(function () {
    // ... your initialisation code here
});
```

This has the additional benefit of ensuring any functions or variables you declare here are not in the global scope, which is important for best interoperability with other plugins in Confluence.

## Achieving Progressive Enhancement

We recommend you separate your markup, styles and JavaScript when developing a Confluence plugin, according to the design principles of progressive enhancement. To assist with this, there are a few hooks in AJS and in Confluence in general to make this easier.

##### Dynamic Content in JavaScript

If you need to pass information from Velocity to JavaScript, such as for localised text, you can use `AJS.params`. This automatically looks up values inside fieldsets marked with a class of "parameters" inside your markup. For example, given the following markup:

``` xml
<fieldset class="parameters hidden">
    <input type="hidden" id="deleteCommentConfirmMessage" value="$action.getText('remove.comment.confirmation.message')">
</fieldset>
```

You can have your JavaScript access the localised text without embedding it by using `AJS.params`:

``` javascript
if (confirm(AJS.params.deleteCommentConfirmMessage)) {
    // ...
}
```

##### Getting the Context Path

Usually, you can use relative paths in stylesheets and JavaScript to avoid the need to know the context path. However, Confluence makes this available through a meta tag in the header which looks like this:

``` xml
<meta name="ajs-context-path" content="/confluence">
```

Since 3.4 the best way of accessing this path is via the AJS.Data JavaScript method:

``` javascript
var relativeUrl = AJS.Data.get("context-path") + "/path/to/content";
```

## More Information

##### Couldn't you do this already? What's changed in Confluence 2.8?

Since Confluence 2.6, you've been able to use `#includeJavascript`, which puts the script tag inline, exactly where that Velocity macro appears. You've also always been able to include inline scripts or styles in your macros. However, there are a couple of problems with this that we've solved in 2.8:

1.  The JavaScript might override other script already present in the page, including scripts used by Confluence.
2.  Inline JavaScript or styles might appear multiple times in the page, wasting bandwidth and potentially causing conflicts.

Many plugin authors found that including JavaScript in their plugins meant the plugin broke in some places, such as in the preview window, if two copies of the macro were on the same page.

By using the new `#requireResource`, you're guaranteed to get only one instance of the script appearing on a page, and it will be cached by browsers until your plugin is upgraded.

##### Do I have to use Velocity to request these resources? What about in Java?

You can achieve the same result in Java via the WebResourceManager. Use the same method as described above for Velocity: `webResourceManager.requireResource(String)`  
The WebResourceManager is a bean you can get [injected by Spring](/server/confluence/accessing-confluence-components-from-plugin-modules). Do this within the scope of the request.

In most cases, using Velocity makes more sense, because the declaration of the JS and CSS should be close to the code which uses it.

##### RELATED TOPICS

[Web Resource Module](/server/confluence/web-resource-module)  
[Adding Plugin and Module Resources](/server/confluence/adding-plugin-and-module-resources)  
[Writing Confluence Plugins](/server/confluence/writing-confluence-plugins)  
<a href="#installing-a-plugin" class="unresolved">Installing a Plugin</a>
