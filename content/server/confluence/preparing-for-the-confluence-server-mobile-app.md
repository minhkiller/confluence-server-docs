---
aliases:
- /server/confluence/preparing-for-the-confluence-server-mobile-app-52430665.html
- /server/confluence/preparing-for-the-confluence-server-mobile-app-52430665.md
category: devguide
confluence_id: 52430665
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=52430665
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=52430665
date: '2017-12-08'
legacy_title: Preparing for the Confluence Server mobile app
platform: server
product: confluence
subcategory: development resources
title: Preparing for the Confluence Server mobile app
---
# Preparing for the Confluence Server mobile app

This documentation is intended for Confluence developers who want to ensure that their existing plugins and add-ons are compatible with the new Confluence Server mobile app.

We will be releasing frequent updates to the app during development. Watch this page to find out about changes that may be important to your add-ons.

See <a href="https://confluence.atlassian.com/display/DOC/Confluence+Server+mobile+app+beta" class="external-link">Confluence Server mobile app beta</a> for information on compatible Confluence versions.

{{% note %}}

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p><strong>Android beta</strong></p>
<p>This is a public beta. All your users need to do is hit the button below to get the app.</p>
<p><a href="https://play.google.com/apps/testing/com.atlassian.confluence.server" class="external-link">Get the app</a></p></td>
<td><p><strong>iOS beta</strong></p>
<p>This is a private beta. We'll need your email address to invite you to join.</p>
<p><a href="https://docs.google.com/forms/d/e/1FAIpQLScM-SsJduXHMS69wOP4hEDfGGkAgynQN7BfhFlxhByS5K7_pw/viewform" class="external-link">Sign up now</a></p></td>
</tr>
</tbody>
</table>

{{% /note %}} 
{{% note %}}

**Issues with the beta?**

Give us feedback directly in the app 

or <a href="https://jira.atlassian.com/secure/CreateIssueDetails!init.jspa?pid=10470&amp;issuetype=10000&amp;components=43692&amp;summary=Confluence+Server+mobile+beta+feedback" class="external-link">raise an issue</a> to tell us about it. 

{{% /note %}}

Did you know we've got a new developer community? Head to <a href="https://community.developer.atlassian.com/" class="uri external-link">community.developer.atlassian.com/</a> to check it out!  We'll be posting in the **announcements** category when new EAP releases are available. 

## About the app

The Confluence Server mobile app will be available for Confluence Server and Confluence Data Center. Its functionality is similar to the Confluence Cloud app, and allows viewing, creating and editing content.  The Android beta is available now, with an iOS beta soon to follow. 

As with mobile web, some macros won't display in the app and users will need to view the page in their browser (or switch to full desktop mode on their device).  We're working hard to make sure as many bundled macros display in the app as possible. If your add-on includes macros, you may want to make sure they display in the app also.  

## How to make your macro display in the mobile app

Confluence will automatically display a "View desktop version" link whenever a macro can't be displayed on a page or blog post. 

To allow your macro display in the app, follow the steps below. 

### Configure your macro to support mobile mode

In the `atlassian-plugin.xml`, add the device type.  Here's an example from the JIRA Chart macro:

``` java
<xhtml-macro name='jirachart' class='com.atlassian.confluence.plugins.jiracharts.JiraChartMacro'
     key='jirachart-xhtml'
     icon="/download/resources/confluence.extra.jira/images/jira-chart-macro-icon.png"
     documentation-url="help.jirachart.macro">
  <category name="development"/>
  <device-type>mobile</device-type> 
  <parameters>
     <parameter name="server" type="string" required="true">
        <option key="showKeyInPlaceholder" value="false"/>
        <option key="showValueInPlaceholder" value="false"/>
     </parameter>
  </parameters>
</xhtml-macro>
```

If your plugin is using the Transformer form plugin you can skip this step, as the mobile app already supports the Transformer form plugin.  

### Render static content

Use the value of `context.getOutputDeviceType()` of execute method in `YourMacro` class to render mobile mode, as shown in this example. 

``` java
public class YourMacro implements Macro {
    public String execute(Map<String, String> parameters, String body, ConversionContext context) throws MacroExecutionException {
        if("mobile".equals(context.getOutputDeviceType()) {
            //add your code to render in mobile here
        }
    }
}
```

At the moment, the mobile app only supports static content. You can use current PDF or email rendering in mobile. 

## If you're using a Transformer to render the view

### Configure your macro to support mobile mode

The mobile app already supports the Transformer form plugin, so you don't need to add any specific configuration to support it. 

### Render static content

This only applies for chain="storageToView" transformer (display mode). 

Use the value of `context.getOutputDeviceType()` of transform method in `YourTransformer` class to render mobile mode, as shown in this example. 

``` java
public class YourTransformer implemenent Transformer {
    public String transform(Reader reader, ConversionContext context) throws XhtmlException {
        if("mobile".equals(context.getOutputDeviceType()) {
            //add your code to render in mobile here
        }
    }
}
```

## How to test your macro

Download the Confluence Server beta app to test your macro. 

See <a href="https://confluence.atlassian.com/display/DOC/Confluence+Server+mobile+app+beta" class="external-link">Confluence Server mobile app beta</a> for more details.
