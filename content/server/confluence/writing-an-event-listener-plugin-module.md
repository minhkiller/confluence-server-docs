---
aliases:
- /server/confluence/writing-an-event-listener-plugin-module-2031813.html
- /server/confluence/writing-an-event-listener-plugin-module-2031813.md
category: reference
confluence_id: 2031813
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031813
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031813
date: '2017-12-08'
legacy_title: Writing an Event Listener Plugin Module
platform: server
product: confluence
subcategory: modules
title: Writing an Event Listener Plugin module
---
# Writing an Event Listener Plugin module

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Available:</p></td>
<td><p>Confluence 1.4 and later</p></td>
</tr>
<tr class="even">
<td><p>Changed:</p></td>
<td><p>In Confluence 3.3 and later, Confluence events are annotation-based. This means that you have two options when writing event listeners. Option 1 is the Event Listener plugin module, using the <code>com.atlassian.event.EventListener</code> class. Option 2 is to declare your listener as a <a href="/server/confluence/component-module">component</a> and use annotation-based event listeners, annotating your methods to be called for specific event types. The component will need to register itself at the time it gets access to the EventPublisher, typically in the constructor. More information and examples are in <a href="/server/confluence/event-listener-module">Event Listener Module</a>.</p></td>
</tr>
</tbody>
</table>

### Overview

For an introduction to event listener plugin modules, please read [Event Listener Module](/server/confluence/event-listener-module).

### Writing an Event Listener as a plugin module within Confluence

Writing an event listener is a four-step process:

{{% note %}}

1.  Identify the events you wish to listen for
2.  Create the EventListener Java class
    -  Implement `getHandledEventClasses()`
    -  Implement `handleEvent()`
3.  Add the listener module to your `atlassian-plugin.xml` file

{{% /note %}}

#### Identify the events you wish to listen for

The easiest thing here is to consult the <a href="https://docs.atlassian.com/atlassian-confluence/latest-server/" class="external-link">Javadoc</a>, in the package **com.atlassian.confluence.event.events** . When you implement an EventListener you will provide an array of Class objects which represent the events you wish to handle.

The naming of most events are self explanitory (GlobalSettingsChangedEvent or ReindexStartedEvent for example), however there are some which need further clarification:

<table>
<colgroup>
<col style="width: 30%" />
<col style="width: 70%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Event Class</p></th>
<th><p>Published</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="http://www.atlassian.com/software/confluence/docs/api/latest/com/atlassian/confluence/event/events/label/LabelCreateEvent.html" class="external-link">LabelCreateEvent</a></p></td>
<td><p>On the creation of the <strong>first</strong> label to the target Content Entity Object.</p></td>
</tr>
<tr class="even">
<td><p><a href="http://www.atlassian.com/software/confluence/docs/api/latest/com/atlassian/confluence/event/events/label/LabelCreateEvent.html" class="external-link">LabelRemoveEvent</a></p></td>
<td><p>On the removal of the <strong>last</strong> label from the target Content Entity Object.</p></td>
</tr>
<tr class="odd">
<td><p><a href="http://www.atlassian.com/software/confluence/docs/api/latest/com/atlassian/confluence/event/events/label/LabelAddEvent.html" class="external-link">LabelAddEvent</a></p></td>
<td><p>On the addition of <strong>any</strong> label to the target Content Entity Object.</p></td>
</tr>
<tr class="even">
<td><p><a href="http://www.atlassian.com/software/confluence/docs/api/latest/com/atlassian/confluence/event/events/label/LabelAddEvent.html" class="external-link">LabelDeleteEvent</a></p></td>
<td><p>On the deletion of <strong>any</strong> label from the target Content Entity Object.</p></td>
</tr>
</tbody>
</table>

#### Create the EventListener

The EventListener interface defines two methods which must be implemented: `getHandledEventClasses()` and `handleEvent()`.

**Implement getHandledEventClasses()**

The getHandledEventClasses() method holds an array of class objects representing the events you wish to listen for.

-   Your listener will *only* receive events of the types specified in getHandledEventClasses()
-   You must specify all the event types you need - specifying a superclass will *not* include its subclasses
-   Returning an empty array will cause your listener to receive every event Confluence produces

So, if you want your listener to receive only `SpaceCreatedEvent` and `SpaceRemovedEvent`

``` java
    private static final Class[] HANDLED_EVENTS = new Class[] { 
        SpaceCreateEvent.class, SpaceRemovedEvent.class 
    };

    public Class[] getHandledEventClasses()
    {
        return HANDLED_EVENTS;
    }
```

Alternatively, to receive all possible events:

``` java
    /**
     * Returns an empty array, thereby handling every ConfluenceEvent
     * @return
     */
    public Class[] getHandledEventClasses()
    {
        return new Class[0];
    }
```

**Implement `handleEvent()`**

The implementation below simply relies upon the toString() implementation of the event and logs it to a log4j appender.

``` java
    public void handleEvent(Event event)
    {
        if (!initialized)
           initializeLogger();

        log.info(event);
    }
```

Most often, a handleEvent(..) method will type check each event sent through it and execute some conditional logic.

``` java
    public void handleEvent(Event event)
    {
        if (event instanceof LoginEvent)
        {
            LoginEvent loginEvent = (LoginEvent) event;
            // ... logic associated with the LoginEvent
        }
        else if (event instanceof LogoutEvent)
        {
            LogoutEvent logoutEvent = (LogoutEvent) event;
            // ... logic associated with the LogoutEvent
        }
    }
```

A full example of an EventListener class that listens for login and logout events can be found in [EventListener Example](/server/confluence/eventlistener-example).

#### Add the EventListener as a module to your plugin by creating an atlassian-plugin.xml

The atlassian-plugin.xml file has been described [elsewhere](/server/confluence/writing-confluence-plugins) in detail. This is an example of a listener plugin module included in an `atlassian-plugin.xml` file.

``` xml
<atlassian-plugin name='Optional Listeners' key='confluence.extra.auditor'>
    <plugin-info>
        <description>Audit Logging</description>
        <vendor name="Atlassian Software Systems" url="http://www.atlassian.com"/>
        <version>1.0</version>
    </plugin-info>

    <listener name='Audit Log Listener' class='com.atlassian.confluence.extra.auditer.AuditListener' key='auditListener'>
        <description>Provides an audit log for each event within Confluence.</description>
    </listener>
</atlassian-plugin>
```

