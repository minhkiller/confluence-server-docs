---
aliases:
- /server/confluence/tutorials-2031688.html
- /server/confluence/tutorials-2031688.md
category: devguide
confluence_id: 2031688
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031688
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031688
date: '2017-12-08'
legacy_title: Tutorials
platform: server
product: confluence
subcategory: learning
title: Tutorials
---
# Tutorials

The following are tutorials for Confluence Server. If you're looking for tutorials for Confluence Cloud development, you can [find them here](https://developer.atlassian.com/display/CONFCLOUD/Tutorials).

**Beginner**

-   [Confluence Blueprints](/server/confluence/confluence-blueprints)
-   [Write a simple Confluence Space Blueprint](/server/confluence/write-a-simple-confluence-space-blueprint)
-   [Adding Confluence Keyboard Shortcuts](/server/confluence/adding-confluence-keyboard-shortcuts)
-   [Create a Confluence 'Hello World' Macro](https://developer.atlassian.com/display/DOCS/Create+a+Confluence+%27Hello+World%27+Macro)
-   [Write a simple Confluence Blueprint plugin](/server/confluence/write-a-simple-confluence-blueprint-plugin)
-   [Writing a Confluence Theme](/server/confluence/writing-a-confluence-theme)
-   [Extending Autoconvert](/server/confluence/extending-autoconvert)

#### Intermediate

-   [Extending the Highlight Actions Panel](/server/confluence/extending-the-highlight-actions-panel)
-   [Adding new Confluence to HipChat notifications](/server/confluence/adding-new-confluence-to-hipchat-notifications)
-   [Adding items to the Info Banner](/server/confluence/adding-items-to-the-info-banner)
-   [Extending the Confluence Insert Link Dialog](/server/confluence/extending-the-confluence-insert-link-dialog)
-   [Extending the Image Properties Dialog](/server/confluence/extending-the-image-properties-dialog)
-   [Write an intermediate blueprint plugin](/server/confluence/write-an-intermediate-blueprint-plugin)
-   [Writing a Macro Using JSON](/server/confluence/writing-a-macro-using-json)
-   [Posting Notifications in Confluence](/server/confluence/posting-notifications-in-confluence)
-   [Searching Using the V2 Search API](/server/confluence/searching-using-the-v2-search-api)
-   [Adding a Custom Action to Confluence](/server/confluence/adding-a-custom-action-to-confluence)
-   [Unit Testing Plugins](/server/confluence/unit-testing-plugins)
-   [Writing a Blueprint - Intermediate](/server/confluence/writing-a-blueprint-intermediate)

#### Advanced

-   [Write an advanced blueprint plugin](/server/confluence/write-an-advanced-blueprint-plugin)
-   [Extending the V2 search API](/server/confluence/extending-the-v2-search-api)
-   [Adding a field to CQL](/server/confluence/adding-a-field-to-cql)
-   [Defining a Pluggable Service in a Confluence Plugin](/server/confluence/defining-a-pluggable-service-in-a-confluence-plugin)
-   [Creating an Admin Configuration Form](https://developer.atlassian.com/display/DOCS/Creating+an+Admin+Configuration+Form)
-   [Writing a Blueprint - Advanced](/server/confluence/writing-a-blueprint-advanced)

### Other Tutorials

Looking for other products and for the cross-product frameworks? Please refer to the [complete list of Atlassian tutorials](https://developer.atlassian.com/display/DOCS/Tutorials).

### Legacy Tutorials

These tutorials apply to Confluence versions that have reached end of life. 

-   [Adding an Option to the Editor Insert Menu](/server/confluence/adding-an-option-to-the-editor-insert-menu)
-   [Preventing XSS issues with macros in Confluence 4.0](/server/confluence/preventing-xss-issues-with-macros-in-confluence-4-0)
-   [Writing Soy Templates in Your Plugin](/server/confluence/writing-soy-templates-in-your-plugin)
-   [Upgrading and Migrating an Existing Confluence Macro to 4.0](/server/confluence/upgrading-and-migrating-an-existing-confluence-macro-to-4-0)
-   [Providing an Image as a Macro Placeholder in the Editor](/server/confluence/providing-an-image-as-a-macro-placeholder-in-the-editor)
-   [Extending the Macro Property Panel](/server/confluence/extending-the-macro-property-panel)
-   [Sending Emails in a Plugin](/server/confluence/sending-emails-in-a-plugin)
-   [Writing Macros for Pre-4.0 Versions of Confluence](/server/confluence/writing-macros-for-pre-4-0-versions-of-confluence)
-   [Creating a Template Bundle](/server/confluence/creating-a-template-bundle)
-   [Adding Menu Items to Confluence](/server/confluence/adding-menu-items-to-confluence)
