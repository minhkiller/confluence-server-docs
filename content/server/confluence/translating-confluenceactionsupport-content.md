---
aliases:
- /server/confluence/translating-confluenceactionsupport-content-2031626.html
- /server/confluence/translating-confluenceactionsupport-content-2031626.md
category: reference
confluence_id: 2031626
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031626
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031626
date: '2017-12-08'
legacy_title: Translating ConfluenceActionSupport Content
platform: server
product: confluence
subcategory: modules
title: Translating ConfluenceActionSupport content
---
# Translating ConfluenceActionSupport content

Guide for translating the values for each property in a `ConfluenceActionSupport_<KEY>.properties` file, where &lt;KEY&gt; is the international language identifier:

### Translating Strings Without Variables Or Links

These links can be translated directly. Using German in this example

``` java
submit.query.name=Submit Query
```

can be translated directly into

``` java
submit.query.name=Anfrage senden
```

### Translating Strings Containing Variables Or Links

Some strings use variables or hyperlinks to provide contextual information. Variables are shown as {`NUMBER`} while hyperlinks are shown as `<a href="{NUMBER}">LINK ALIAS</a>`. Translations must take into account the positioning of variables, and check that links occur over the relevant phrase. Using German again as an example

``` xml
search.include.matches.in.other.spaces=There are <b>{0} matches</b> in <b>other spaces</b>. <a href="{1}">Include these matches</a>.
```

This tag uses a variable to show the number of matches, and a link the user can click to include those matches. The German version must place the 'matches' variable in the adjusted location, and reapply the hyperlink to the relevant phrase.

``` xml
search.include.matches.in.other.spaces=Es wurden <b>{0} Resultate</b> in <b>anderen Spaces</b> gefunden. <a href="{1}">Diese Resultate einschliessen</a>.
```
