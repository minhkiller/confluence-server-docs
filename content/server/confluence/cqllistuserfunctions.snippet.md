---
aliases:
- /server/confluence/-cqllistuserfunctions-29952578.html
- /server/confluence/-cqllistuserfunctions-29952578.md
category: devguide
confluence_id: 29952578
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=29952578
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=29952578
date: '2017-12-08'
legacy_title: _CQLListUserFunctions
platform: server
product: confluence
subcategory: other
title: _CQLListUserFunctions
---
# \_CQLListUserFunctions

-   [currentUser()](https://developer.atlassian.com/display/CONFDEV/CQL+Function+Reference#CQLFunctionReference-currentUser())
