---
aliases:
- /server/confluence/2031795.html
- /server/confluence/2031795.md
category: devguide
confluence_id: 2031795
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031795
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031795
date: '2017-12-08'
legacy_title: What class should my macro extend?
platform: server
product: confluence
subcategory: faq
title: What class should my macro extend?
---
# What class should my macro extend?

### <img src="http://confluence.atlassian.com/images/icons/favicon.png" class="confluence-external-resource" /> What class should my macro extend?

It should extend <a href="http://www.atlassian.com/software/confluence/docs/api/latest/com/atlassian/renderer/v2/macro/BaseMacro.html" class="external-link">com.atlassian.renderer.v2.macro.BaseMacro</a>, **not** <a href="http://www.atlassian.com/software/confluence/docs/api/latest/com/atlassian/renderer/macro/BaseMacro.html" class="external-link">com.atlassian.renderer.macro.BaseMacro</a>.
