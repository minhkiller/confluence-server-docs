---
aliases:
- /server/confluence/preparing-for-confluence-6.5-51921199.html
- /server/confluence/preparing-for-confluence-6.5-51921199.md
category: devguide
confluence_id: 51921199
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=51921199
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=51921199
date: '2017-12-08'
legacy_title: Preparing for Confluence 6.5
platform: server
product: confluence
subcategory: development resources
title: Preparing for Confluence 6.5
---
# Preparing for Confluence 6.5

 

This documentation is intended for Confluence developers who want to ensure that their existing plugins and add-ons are compatible with Confluence 6.5.

We plan to release frequent milestones during the development of Confluence 6.5. Watch this page to find out when a new milestone is available and what's changed.  We won't be publishing formal release notes over at <a href="http://confluence.atlassian.com/display/DOC/Confluence+Development+Releases" class="external-link">Development Releases</a> until we release a beta.

{{% note %}}

**Latest milestone**

|              |           |                                                                                                         |
|--------------|-----------|--------------------------------------------------------------------------------------------------------:|
| 23 Oct 2017  | 6.5.0-rc1 |  <a href="https://www.atlassian.com/software/confluence/download-eap" class="external-link">Download</a>|

{{% /note %}}

**Issues with this milestone?**

Hit the Feedback button on the Confluence EAP header or <a href="https://jira.atlassian.com/secure/CreateIssueDetails!init.jspa?pid=10470&amp;issuetype=10000&amp;components=43692&amp;summary=Confluence+6.5+EAP+Feedback" class="external-link">raise an issue</a> to tell us about it. 

Looking for updated documentation? Check out the <a href="https://confluence.atlassian.com/display/CONFEAP/Confluence+EAP+Documentation" class="external-link">Confluence EAP</a> space for the latest docs. 

Did you know we've got a new developer community? Head to <a href="https://community.developer.atlassian.com/" class="uri external-link">community.developer.atlassian.com/</a> to check it out!  We'll be posting in the **announcements** category if when new EAP releases are available. 

**How do you want to find out about these releases?**

We'd love to improve how we tell you about Early Access Program (EAP) releases. <a href="https://goo.gl/forms/QoYMmJVdceWsnfjT2" class="external-link">Tell us what you think </a>in this quick survey.

## Release candidate 1 - 23 October

This release candidate contains minor fixes and improvements, including the editor resource restructuring known issue described below.   

## Beta 1 - 16 October

In this beta:

-   Start and stop scripts are now available to help you run Synchrony standalone for Data Center.  You'll find the new scripts in your Confluence `<install-directory>/bin/synchrony` directory. Simply move the scripts to your Synchrony node, insert the values for your environment and you're ready to go. Head to <a href="https://confluence.atlassian.com/display/CONFEAP/Configuring+Synchrony+for+Data+Center" class="external-link">Configuring Synchrony for Data Center</a> in our EAP documentation to find out more. 
-   Applinks upgrade to 5.4.1

For a complete run down of all the features and changes in this beta, head to our <a href="https://confluence.atlassian.com/display/DOC/Confluence+6.5.0-beta+release+notes" class="external-link">Confluence 6.5.0-beta release notes</a>. 

#### Known issue: Editor resource restructuring

We restructured some of the editor resources in 6.5. These are meant to be backwards and forwards compatible; however, we found an issue if you are loading the Confluence Editor manually in your plugin. Some features might not be working correctly, such as Links or Mentions. We're on it and this will be fixed for RC and Final. See <a href="https://jira.atlassian.com/browse/CONFSERVER-53917" class="external-link">CONFSERVER-53917</a>. 

## Milestone 22 - 9 October

In this milestone:

-   New system add-on to provide targeted in-app notifications in Confluence (this add-on also recently shipped in Jira 7.4). Notifications will be predominantly targeted to administrators and will provide information on new Confluence versions and upcoming license renewals.
-   Synchrony upgraded to 2.1.x 

## Milestone 16 - 3 October

We've made some changes to how you configure your database in the setup wizard:

-   The option to use a datasource will only appear if you have already configured a datasource in the server.xml.  
-   You can now enter database details in a simple form, or can choose enter your own connection URL (which was the previous method).
-   We've added a few more checks to make sure the database is configured up correctly and is using the correct character set, collation, isolation level etc to help customers avoid future problems. 

We've also completed a few library and other upgrades including:

-   Tomcat upgraded to 8.0.46
-   Jodatime upgraded to 2.9.9  

## Milestone 12 - 25 September

There are no notable changes in this milestone.

## Milestone 8 - 18 September

We've made some changes to our Support Tools. When you upgrade Confluence to 6.5.0-m08 or later:

-   the following plugins will be uninstalled:
    -   Confluence Required Health Checks Plugin
    -   Confluence Healthcheck plugin
    -   Support Healthcheck Plugin

    -   Support Tools Plugin

-   a new plugin called Atlassian Troubleshooting and Support Tools will be installed.

You can find the new Troubleshooting and support tools under ![](/server/confluence/images/cog.png) &gt; **Troubleshooting and support tools**.  The functionality of the plugin (health checks, log analyzer, support zip etc) has not changed.

## Milestone 4 - 12 September

This is our first public milestone for Confluence 6.5. Not much has changed yet, but keep an eye on this page, as we'll be releasing a milestone every week in the lead up to Confluence 6.5.  

-   Improved email threading. Confluence will group all the email notifications about changes to a specific page together. Other notifications such as sharing a page, or requesting access to a page are not grouped.

## New Azure deployment template for Confluence Data Center

Our new Azure deployment template is ready for you to take for a test drive. The template makes provisioning a new Confluence Data Center cluster in Microsoft Azure simple. Head to <a href="https://confluence.atlassian.com/display/DOC/Getting+started+with+Confluence+Data+Center+on+Azure" class="external-link">Getting started with Confluence Data Center</a> in Azure to find out how it works.

We've still got some work to do, including an app for the Azure marketplace, but we'd love your feedback on our work so far, so <a href="https://jira.atlassian.com/secure/CreateIssueDetails!init.jspa?pid=10470&amp;issuetype=10000&amp;components=43692&amp;summary=Confluence+Azure+Beta+Feedback" class="external-link">raise an issue</a> to tell us what you think.
