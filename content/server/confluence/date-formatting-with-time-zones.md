---
aliases:
- /server/confluence/date-formatting-with-time-zones-2031847.html
- /server/confluence/date-formatting-with-time-zones-2031847.md
category: devguide
confluence_id: 2031847
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031847
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031847
date: '2017-12-08'
guides: guides
legacy_title: Date formatting with time zones
platform: server
product: confluence
subcategory: learning
title: Date formatting with time zones
---
# Date formatting with time zones

Confluence 2.3 supports a time zone preference for a user. This means all dates in the system must be formatted using the same process to appear in the user's time zone correctly. This document describes how dates are formatted in Confluence. It may be useful to plugin developers who need to format dates in a special way inside Confluence.

### DateFormatter

The new class introduced in Confluence 2.3, <a href="http://docs.atlassian.com/com/atlassian/confluence/atlassian-confluence/2.3/com/atlassian/confluence/core/DateFormatter.html" class="external-link">DateFormatter</a>, allows formatting in the user's timezone. See the full javadoc for details, but methods include:

-   `String format(Date date)` - Formats the date and returns it as a string, using the date formatting pattern.
-   `String formatDateTime(Date date)` - Formats the date and returns it as a string, using the date-time formatting pattern.
-   `String formatServerDate(Date date)` - Same as format(Date), but doesn't perform time zone conversion.

Most methods format the time in the user's time zone. The 'server' methods format the time in the server's time zone.

### Accessing the DateFormatter in Velocity

In Velocity, using the DateFormatter is easy because it is in the Velocity context. In a normal Velocity template (\*.vm), such as an action result, you might use it like this:

``` javascript
$dateFormatter.format($action.myBirthdayDate)
```

If you want to use the DateFormatter in a Velocity decorator (\*.vmd), such as a custom layout or theme, you need to access it via its getter on the action:

``` javascript
$action.dateFormatter.format( $page.lastModificationDate )
```

### Accessing the DateFormatter in code

The DateFormatter is constructed by the ConfluenceUserPreferences object, which can be obtained from the UserAccessor. The code below gives a demonstration:

``` java
ConfluenceUserPreferences preferences = userAccessor.getConfluenceUserPreferences(user);
DateFormatter dateFormatter = preferences.getDateFormatter(formatSettingsManager);
System.out.println(dateFormatter.formatDateTime(date));
```

The `userAccessor` and `formatSettingsManager` are Spring beans which can be [injected into your object](/server/confluence/spring-ioc-in-confluence). You can usually get the `user` from the context of your macro or plugin, or using <a href="http://docs.atlassian.com/com/atlassian/confluence/atlassian-confluence/2.3/com/atlassian/confluence/user/AuthenticatedUserThreadLocal.html#getUser()" class="external-link">AuthenticatedUserThreadLocal.getUser()</a>.
