---
aliases:
- /server/confluence/keyboard-shortcut-module-2031724.html
- /server/confluence/keyboard-shortcut-module-2031724.md
category: reference
confluence_id: 2031724
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031724
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031724
date: '2017-12-08'
legacy_title: Keyboard Shortcut Module
platform: server
product: confluence
subcategory: modules
title: Keyboard Shortcut module
---
# Keyboard Shortcut module

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Available:</p></td>
<td><p>Confluence 3.4 and later</p></td>
</tr>
</tbody>
</table>

## Purpose of this Module Type

A Keyboard Shortcut plugin module defines a keyboard shortcut within Confluence. A Confluence keyboard shortcut allows you to perform potentially any action in Confluence using one or more keyboard strokes - for example, going to the dashboard, editing a page, adding a comment, formatting text while using the editor, and so on.

## Configuration

The root element for the Keyboard Shortcut plugin module is `keyboard-shortcut`. It allows the following attributes and child elements for configuration:

#### Attributes

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Name*</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>key</p></td>
<td><p>The unique identifier of the plugin module. You refer to this key to use the resource from other contexts in your plugin, such as from the plugin Java code or JavaScript resources.</p>
<pre><code>&lt;component-import
  key=&quot;appProps&quot;
  interface=&quot;com.atlassian.sal.api.ApplicationProperties&quot;/&gt;</code></pre>
<p>In the example, <code>appProps</code> is the key for this particular module declaration, for <code>component-import</code>, in this case.</p>
<p><strong>Default:</strong> N/A</p></td>
</tr>
<tr class="even">
<td><p>i18n-name-key</p></td>
<td>The localisation key for the human-readable name of the plugin module.</td>
</tr>
<tr class="odd">
<td><p>name</p></td>
<td><p>The human-readable name of the plugin module.</p>
<p><strong>Default:</strong> The plugin key.</p></td>
</tr>
<tr class="even">
<td><p>hidden</p></td>
<td><p>When <code>hidden='true'</code>, the keyboard shortcut will not appear in the <a href="#keyboard-shortcuts-dialog-box" class="unresolved">Keyboard Shortcuts dialog box</a>.<br />
NOTE: Despite not appearing in the dialog box, hidden keyboard shortcuts can still be accessed via the relevant keystrokes.</p>
<p><strong>Default:</strong> false</p></td>
</tr>
</tbody>
</table>

**\*key is required**

{{% note %}}

There is a known issue with the i18n-name-key. Please see <a href="https://jira.atlassian.com/browse/CONF-24450" class="external-link">CONF-24450</a>.

{{% /note %}}

#### Elements

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Name*</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>order</p></td>
<td><p>A value that determines the order in which the shortcut appears on the <a href="#keyboard-shortcuts-dialog-box" class="unresolved">Keyboard Shortcuts dialog box</a>, with respect to other <code>keyboard-shortcut</code> plugin modules. This element is also used to override existing shortcuts displayed on the Keyboard Shortcuts dialog box (see <a href="#overriding-existing-keyboard-shortcuts">below</a>).<br />
NOTE: For each <code>keyboard-shortcut</code> plugin module, we recommend using gaps in <code>order</code> values (for example, <code>10</code>, <code>20</code>, <code>30</code>, etc.) rather than consecutive values. This will allow you to insert new keyboard shortcuts more easily into the keyboard shortcuts dialog box.</p></td>
</tr>
<tr class="even">
<td><p>description</p></td>
<td><p>A human-readable description of this Keyboard Shortcut module.<br />
NOTE: Please see <a href="https://jira.atlassian.com/browse/CONF-24450" class="external-link">CONF-24450</a> if you are having problems with internationalisation.</p></td>
</tr>
<tr class="odd">
<td><p>shortcut</p></td>
<td><p>The sequence of keystrokes required to activate the keyboard shortcut. These should be presented in the order that the keys are pressed on a keyboard. For example, <code>gb</code> represents a keyboard shortcut activated by pressing '<code>g</code>' then '<code>b</code>' on the keyboard.</p></td>
</tr>
<tr class="even">
<td><p>operation</p></td>
<td><p>A <a href="http://api.jquery.com/category/selectors/" class="external-link">jQuery selector</a> that specifies the target of the keyboard shortcut. The target is typically a component of the current page that performs an action. The <code>operation</code> element is accompanied by a <code>type</code> attribute that specifies the type of keyboard shortcut operation. Available types are:</p>
<ul>
<li><strong>click</strong><br />
Clicks an element identified by a jQuery selector</li>
<li><strong>execute</strong><br />
Executes a JavaScript function specified by the operation parameter</li>
<li><strong>followLink</strong><br />
Sets the window.location to the href value of the link specified by the jQuery selector.</li>
<li><strong>goTo</strong><br />
Changes the window.location to go to a specified location</li>
<li><strong>moveToAndClick</strong><br />
Scrolls the window to an element and clicks that element using a jQuery selector</li>
<li><strong>moveToAndFocus</strong><br />
Scrolls the window to a specific element and focuses that element using a jQuery selector</li>
<li><strong>moveToNextItem</strong><br />
Scrolls to and adds focused class to the next item in the jQuery collection</li>
<li><strong>moveToPrevItem</strong><br />
Scrolls to and adds focused class to the previous item in the jQuery collection</li>
</ul></td>
</tr>
<tr class="odd">
<td><p>context</p></td>
<td><p>The context defines which pages the shortcut will be active on.</p>
<ul>
<li>If this element contains <code>global</code> the keyboard shortcut will be active on all Confluence pages. The shortcut will also appear in the 'Global Shortcuts' sub-section of the Keyboard Shortcuts dialog box under the 'General' tab.</li>
<li>If this element contains <code>viewcontent</code> the keyboard shortcut will be active when viewing a page or blog post. The shortcut will also appear in the 'Page / Blog Post Actions' sub-section of the Keyboard Shortcuts dialog box under the 'General' tab.</li>
<li>If this element contains <code>viewinfo</code> the keyboard shortcut will be active when viewing the info for a page. The shortcut will also appear in the 'Page / Blog Post Actions' sub-section of the Keyboard Shortcuts dialog box under the 'General' tab.</li>
</ul></td>
</tr>
</tbody>
</table>

**\*all elements are required**

### Overriding Existing Keyboard Shortcuts

You can override an existing keyboard shortcut defined either within Confluence itself or in another plugin.

To do this create a `keyboard-shortcut` plugin module with exactly the same `shortcut` element's keystroke sequence as that of the keyboard shortcut you want to override. Then, ensure that an `order` element is added, whose value *is greater than* that defined in the keyboard shortcut being overridden.

NOTE: The `order` element will affect the position of your overriding keyboard shortcut on the Keyboard Shortcuts dialog box. It will also prevent the overridden keyboard shortcut from being accessed via the keyboard.

### Internationalisation

You may need to push your i18n resource to the front end for the shortcut description to be displayed correctly. See <a href="https://jira.atlassian.com/browse/CONF-24450" class="external-link">CONF-24450</a> for instructions on how to do this.

## Examples

These examples are taken from Confluence's pre-defined keyboard shortcuts:

``` xml
...
    <keyboard-shortcut key="goto.space" i18n-name="admin.keyboard.shortcut.goto.space.name" name="Browse Space">
        <order>20</order>
        <description key="admin.keyboard.shortcut.goto.space.desc">Browse Space</description>
        <shortcut>gs</shortcut>
        <operation type="followLink">#space-pages-link</operation>
        <context>global</context>
    </keyboard-shortcut>
...

...
    <keyboard-shortcut key="managewatchers" i18n-name="admin.keyboard.shortcut.managewatchers.name" name="Manage Watchers">
        <order>60</order>
        <description key="admin.keyboard.shortcut.managewatchers.desc">Manage Watchers</description>
        <shortcut>w</shortcut>
        <operation type="click">#manage-watchers-menu-item</operation>
        <context>viewcontent</context>
    </keyboard-shortcut>
...

...
    <keyboard-shortcut key="quicksearch" i18n-name="admin.keyboard.shortcut.quicksearch.name" name="Quick Search">
        <order>40</order>
        <description key="admin.keyboard.shortcut.quicksearch.desc">Quick Search</description>
        <shortcut>/</shortcut>
        <operation type="moveToAndFocus">#quick-search-query</operation>
        <context>global</context>
    </keyboard-shortcut>
...
```

##### RELATED TOPICS

[Writing Confluence Plugins](/server/confluence/writing-confluence-plugins)  
<a href="#installing-a-plugin" class="unresolved">Installing a Plugin</a>
