---
aliases:
- /server/confluence/2031784.html
- /server/confluence/2031784.md
category: devguide
confluence_id: 2031784
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031784
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031784
date: '2017-12-08'
legacy_title: How do I get the location of the confluence.home directory?
platform: server
product: confluence
subcategory: faq
title: How do I get the location of the confluence home directory?
---
# How do I get the location of the confluence home directory?

### <img src="http://confluence.atlassian.com/images/icons/favicon.png" class="confluence-external-resource" /> How do I get the location of the confluence.home directory?

First you need the <a href="http://www.atlassian.com/software/confluence/docs/api/latest/com/atlassian/confluence/setup/BootstrapManager.html" class="external-link">BootstrapManager</a> then simply call the following method:

**Confluence 5.5 and earlier:**

``` java
String confluenceHome = bootstrapManager.getConfluenceHome();
```

**Confluence 5.6 and later:**

``` java
String confluenceHome = bootstrapManager.getSharedHome();
```

or

``` java
String confluenceHome = bootstrapManager.getLocalHome();
```

{{% note %}}

The BootstrapManager also has a `getConfiguredLocalHome` method. This method is used during system startup to determine the location of confluence.home from first principles. There is no reason for you to call this method.

{{% /note %}}

**Related:**

[How do I get a reference to a component](https://developer.atlassian.com/confdev/development-resources/confluence-developer-faq/how-do-i-get-a-reference-to-a-component)
