---
aliases:
- /server/confluence/preparing-for-confluence-5.7-29464000.html
- /server/confluence/preparing-for-confluence-5.7-29464000.md
category: devguide
confluence_id: 29464000
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=29464000
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=29464000
date: '2017-12-08'
legacy_title: Preparing for Confluence 5.7
platform: server
product: confluence
subcategory: development resources
title: Preparing for Confluence 5.7
---
# Preparing for Confluence 5.7

This documentation is intended for Confluence developers who want to ensure that their existing plugins and add-ons are compatible with Confluence 5.7, and to learn about the new features for developing add-ons in Confluence 5.7.

Please watch the <a href="http://confluence.atlassian.com/display/DOC/Confluence+Development+Releases" class="external-link">Development Releases</a> page for pre-release versions of Confluence 5.7 that you can use for testing purposes. This page will be updated as further EAP development releases are available.

## Removal of `setContent` on `ContentEntityObject`

The `setContent` method allowed developers to set wiki markup content as the body of a `ContentEntityObject` (such as a page or blog post) and automatically converted it to the Confluence storage format. It has been deprecated since Confluence 3.5. The suggested replacement is the `setBodyAsString` method. There are 2 possible migration paths:

#### Use the Confluence storage format instead

This is the recommended way. Instead of passing in wiki markup, you should pass in Confluence storage format to the `setBodyAsString` method instead. For example:

``` java
page.setContent("{code}some code{code}");
```

becomes:

``` java
page.setBodyAsString("<ac:structured-macro ac:name=\"code\"><ac:plain-text-body><![CDATA[some code]]></ac:plain-text-body></ac:structured-macro>");
```

For more information, see the <a href="https://confluence.atlassian.com/display/DOC/Confluence+Storage+Format" class="external-link">Confluence Storage Format</a> page.

#### Use the content body conversion service

If for some reason you prefer to keep the wiki markup format, you will have to use the content body conversion service to do the conversion from wiki markup to Confluence storage format for you.

You will need to import the content body conversion service in your `atlassian-plugin.xml` file:

**atlassian-plugin.xml**

``` xml
<component-import key="apiContentBodyService">
    <interface>com.atlassian.confluence.api.service.content.ContentBodyConversionService</interface>
</component-import>
```

Then, to use it in your Java code, call the `convert` method, passing in a wiki markup `ContentBody` and converting it to the `STORAGE` `ContentRepresentation`.

``` java
final String wikiMarkup = "{code}some code{code}";
final ContentBody xhtmlBody = contentBodyConversionService.convert(
        new ContentBody(ContentRepresentation.WIKI, wikiMarkup), ContentRepresentation.STORAGE
);
page.setBodyAsString(xhtmlBody.getValue());
```

## Changes to the files experience

We're improving how Confluence works with files. Major changes will include:

-   new lightbox preview
-   new upload and insert experience
-   commenting on files, and more.

As development on Confluence 5.7 continues we'll update this page with information on breaking changes and other things that may impact plugins. 

#### Attachments will soon be ContentEntityObjects

As part of our work to improve the files experience, `Attachment` objects will be modified to extend from `ContentEntityObject`. This means that attachments will be stored in a different database table, and may, in rare cases, have some of their IDs changed. You may need to change the way you handle attachments if your plugin stores attachment IDs to reference attachments.

###### The change to ContentEntityObject

Modifying `Attachment` to extend `ContentEntityObject` is a big step for Confluence and will have big benefits for plugin developers. `ContentEntityObject`s get the benefits of better versioning, commenting, permissioning and API support (including the ability for P2 and Connect plugins to [store custom data](https://developer.atlassian.com/display/CONFDEV/Persistence+in+Confluence#PersistenceinConfluence-Contentproperties) against them).  Since `Attachment` will only extend an additional class, this change will not affect binary compatibility of plugins using the `Attachment` object. In other words, you will not need to recompile your plugin against the latest version of Confluence for this change.

###### Underlying storage

As of 5.7 attachment metadata that was stored in the `attachment` table will transition to the `content` and `contentproperties` tables. All Confluence APIs will transition to referencing these objects from the new tables. As an API consumer, you should not be affected by this change. If your plugin directly references the `attachment` table, you'll need to transition it to use the public API manager, `AttachmentManager,` to avoid any issues. Attachments that were stored in the database (legacy) will still be in the `attachmentdata` table.

###### Attachment ID changes

With the change of attachment's underlying storage, the IDs of attachments may need to be renumbered. This change will happen as part of an upgrade task in Confluence 5.7. If you are currently storing data against an `Attachment`, and doing so by referencing it's ID, you will need to make modifications to ensure your IDs are correctly updated. For backwards compatibility the old attachment ID will be stored as a content property on the updated `Attachment` object (with key '`OLD_ATTACHMENT_ID`'). We also plan to add API methods that allow for mapping old `Attachment` IDs to their new IDs for use in a plugin upgrade task. Unfortunately this did not make it into the 5.7 final release but there are plans to include it in a point release. See <a href="https://jira.atlassian.com/browse/CONF-36292" class="external-link">CONF-36292</a> for more information.

A convenience library has been created to aid with handling the attachment ID changes. You can find it on the Atlassian maven repository with the following group and artifact IDs:

**pom.xml**

``` xml
<dependency>
  <groupId>com.atlassian.labs.confluence.lib</groupId>
  <artifactId>attachment-ceo-compat</artifactId>
  <version>0.9.3</version>
</dependency>
```

The source code and documentation on how to use it can be found on <a href="https://bitbucket.org/slancashire/confluence-attachment-to-ceo-compat/src/f455faf06c226a38c5957b2245feac49073d6688/src/main/java/com/atlassian/labs/confluence/lib/attachmentcompat/AttachmentCompatibility.java?at=master" class="external-link">BitBucket</a>.

Note that only some attachments that were created before Confluence 2.3 should have their IDs remapped, so most instances will probably not have any IDs remapped at all.

#### New file preview

The existing fancybox preview has been deprecated. Plugins can now use the new lightbox preview `$imageLink.previewer({ src: srcOfImage });`

We plan to make parts of the preview pluggable, and will provide more details in the coming months.

## Phase out `ActiveObjects` with `@StringLength` greater than 450

Support for ActiveObjects with `@StringLength` greater than 450 is being phased out. Previously, plugins using Active Objects could specify a value of `@StringLength`, on their `String`-typed entity columns, of up to 767.

For releases after Confluence 5.7, the maximum supported will be 450 due to a limitation of SQL Server.

#### Warning in Confluence 5.7

In Confluence 5.7, a warning like the following will be logged for plugins using `@StringLength` with values between 450 and 767, but the **plugins will continue to work**:

``` text
[net.java.ao.schema.SchemaGenerator] - @StringLength is 700. Since 0.28.4 the suggested max string length has been reduced to 450 characters due to a limitation in MS SQL Server. Starting from 0.29.0 exceeding this maximum string length will throw an Exception
```

#### For releases post 5.7

For releases after Confluence 5.7, plugins with `@StringLength` greater than 450 won't work, and will be unable to access their data unless modified.

You need to change them to `@StringLength(StringLength.UNLIMITED)`. This will automatically change the column to use the appropriate `CLOB` or `TEXT` type, and will automatically migrate the existing data.

## Changes to comments

#### Inline comments

From Confluence 5.7, we'll support two types of commenting: page comments (that appear at the bottom of the page) and inline comments.

Inline comments are stored as normal comments with an extra flag to indicate they need to be shown inline. Inline comments, unlike page comments, have a location and a resolved state.

The list of resolved comments can be viewed via the <img src="/server/confluence/images/ellipsis.png" class="confluence-thumbnail" width="22" /> menu on a page or blog post.

{{% note %}}

We're still finalising the API for inline comments; we'll have more information on it very soon.

{{% /note %}}

#### Quick reload notifications

We have updated the in-page notifications UI. It now appears on the top right of the page, and can now work with the Inline comments.

## Changes to the editor

We've made some visual changes to the editor. In particular we've changed how the page title and breadcrumbs appear when editing a page.   Plugin developers should test their plugins to ensure they work correctly in the new editor.

## Larger user avatar images

We're now storing larger user avatar images (up to 256 x 256). If your plugin is using user avatars, you can now take advantage of these larger images.

## Support for Java 8

In this release, we've added support for the Java Runtime Engine (JRE) 8, while maintaining support for JRE 7. Plugins must still be compiled using the Java Development Kit (JDK) 7, as Java 8 bytecode isn't supported in this release.

We expect to drop support for the JRE 7 in Confluence 5.8, and only support JRE 8. This is in line with Oracle's decision to stop providing public updates for JRE 7 in <a href="http://www.oracle.com/technetwork/java/eol-135779.html" class="external-link">April 2015</a>.

Confluence 5.8 will support the Java 8 bytecode, and we will continue to have binary compatibility - so you won't need to recompile your plugins using JDK 8. 

#### Fully Qualified Host Name Resolution

Under Java 8, the local host's fully qualified host name resolution may be unreliable due to changes in the JDK. The solution is to either:

-   Use the `atlassian-localhost` library (preferred); OR

<!-- -->

-   Update the `/etc/hosts` file so that the first alias is the mapping for the fully qualified host name for `127.0.0.1`.

