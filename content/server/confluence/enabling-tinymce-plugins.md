---
aliases:
- /server/confluence/enabling-tinymce-plugins-2031658.html
- /server/confluence/enabling-tinymce-plugins-2031658.md
category: devguide
confluence_id: 2031658
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031658
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031658
date: '2017-12-08'
guides: guides
legacy_title: Enabling TinyMCE Plugins
platform: server
product: confluence
subcategory: learning
title: Enabling TinyMCE plugins
---
# Enabling TinyMCE plugins

{{% note %}}

This documentation refers to a feature in the Confluence 3.1 release cycle. It will not work in Confluence 3.0 or before. Please check out our <a href="http://confluence.atlassian.com/display/DOC/Development+Releases" class="external-link">Milestone release notes</a> to learn more about our milestone process for Confluence 3.1

{{% /note %}}

<a href="http://tinymce.moxiecode.com/" class="external-link">TinyMCE</a> is the WYSIWYG editor we use in Confluence. You can now customise and enable <a href="http://wiki.moxiecode.com/index.php/TinyMCE:Plugins" class="external-link">TinyMCE plugins</a> in Confluence by converting it as an Atlassian plugin. You simply need to define a plugin descriptor and provide a small snippet of javascript to configure your plugin.

{{% note %}}

Please note that this does not mean that all TinyMCE plugins are guaranteed to work in Confluence. Confluence is using a customised version of TinyMCE, so the plugins may not work 'out of the box' and could require some changes.

{{% /note %}}

### Defining the TinyMCE Plugin Resources

You will need to define the TinyMCE plugin resources (e.g. editor\_plugin.js) in a [web resource module](/server/confluence/web-resource-module) with an added context of 'wysiwyg-editor'. Below is an example plugin descriptor for the TinyMCE Search & Replace plugin.

``` xml
 <atlassian-plugin name='TinyMCE Search Replace Plugin' key='tinymce.searchreplace.plugin'>
   ...
    <resource name="searchreplace/" type="download" location="searchreplace/"/>

    <web-resource name='TinyMCE Search Replace Web Resources' key='tinymce-searchreplace-resources'>
        <resource name="searchreplace.js" type="download" location="searchreplace/editor_plugin.js"/>
        <resource name="searchreplace-adapter.js" type="download" location="searchreplace/confluence-adapter.js"/>
        <context>wysiwyg-editor</context>
    </web-resource>
</atlassian-plugin>
```

### Configuring TinyMCE Plugin Settings

To enable the TinyMCE plugin, you will need to configure the settings object that is <a href="http://wiki.moxiecode.com/index.php/TinyMCE:Configuration" class="external-link">typically passed to TinyMCE's init()</a> method. To do this, simply add some javascript to register your configuration logic with  
`AJS.Editor.Adapter.addTinyMcePluginInit`. The following code enables the search/replace plugin and adds the search and replace buttons to the second row of the toolbar.

**searchreplace-adapter.js**

``` javascript
 AJS.Editor.Adapter.addTinyMcePluginInit(function(settings) {
    settings.plugins += ",searchreplace";
    settings.theme_advanced_buttons2 += ",search,replace";
});
```

Please note that if you are enabling several buttons on the toolbar, the buttons may not appear on pages using the <a href="https://plugins.atlassian.com/plugin/details/276" class="external-link">Clickr theme</a>. The theme has a fixed screen width, hence it is better to display the buttons on the second row of the toolbar.
