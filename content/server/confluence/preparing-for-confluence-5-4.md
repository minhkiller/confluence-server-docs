---
aliases:
- /server/confluence/preparing-for-confluence-5.4-24085034.html
- /server/confluence/preparing-for-confluence-5.4-24085034.md
category: devguide
confluence_id: 24085034
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=24085034
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=24085034
date: '2017-12-08'
legacy_title: Preparing for Confluence 5.4
platform: server
product: confluence
subcategory: development resources
title: Preparing for Confluence 5.4
---
# Preparing for Confluence 5.4

This documentation is intended for Confluence developers who want to ensure that their existing plugins and add-ons are compatible with Confluence 5.4, and to learn about the new features for developing add-ons in Confluence 5.4.

### New instructional text type - JIRA Macro

A new instructional text type is now available for templates and blueprints - JIRA Macro. When clicked, the placeholder launches the JIRA Issues macro dialog allowing users search for or create an issue in JIRA. See [Instructional text in blueprints](/server/confluence/instruction-text-in-blueprints) for more details.

### Plugin framework upgrade - Spring upgrade

Confluence 5.4 will include a newer version of the Atlassian Plugins Framework, which includes an upgrade of Spring from version 2 to 3. In this upgrade, if your plugin uses Spring classes and you have not explicitly set a version range, your plugin will not load. This is because BND, a tool that generates manifests for OSGi bundles at compile time, will generate an import statement for Spring with version range "\[2.5,3)" (this reads as "greater than or equal to 2.5, but less than 3"). Your plugin will try to find the relevant Spring packages, but will limit its search to that range (i.e. versions less than 3). The package that is supplied is now version 3, so the search will fail.  
  
To fix this, you must explicitly add a version range. We suggest "2.5," which reads as "greater than or equal to 2.5," removing the upper bound of 3. The complete import statement will look something like this:

``` java
...
    org.springframework*;version=2.5
...
```

If you know the exact packages that are required by your plugin, you should list them out explicitly rather than relying on a wildcard expression.

### API for Page Properties Report

You can now get page metadata for pages which use the Page Properties macro using a REST API. 

### Upgrade to Joda-Time 2.3

Confluence is now using Joda-Time 2.3. 

### New pluggable select text panel

The select text panel (as seen in the new highlight to quote in comment or create JIRA issue features) is pluggable. See the tutorial on [Extending the Highlight Actions Panel](/server/confluence/extending-the-highlight-actions-panel).

### New pluggable banner for pages and blogs

The new banner area (the location for the new JIRA Links button) is pluggable. See the tutorial on [Adding items to the Info Banner](/server/confluence/adding-items-to-the-info-banner). 

Milestone releases for developers

Please watch the <a href="http://confluence.atlassian.com/display/DOC/Confluence+Development+Releases" class="external-link">Development Releases</a> page for pre-release versions of Confluence 5.4 that you can use for testing purposes.

## Have questions?

If you have any questions about preparing for Confluence 5.3, please register for an account on <a href="https://answers.atlassian.com/" class="external-link">Atlassian Answers</a>, our community Q&A resource, and <a href="https://answers.atlassian.com/questions/ask/?tags=confluence-development" class="external-link">ask a question</a>. We'll be keeping an eye on things to help out as soon as possible.
