---
aliases:
- /server/confluence/java-api-reference-6291656.html
- /server/confluence/java-api-reference-6291656.md
category: reference
confluence_id: 6291656
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=6291656
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=6291656
date: '2017-12-08'
legacy_title: Java API Reference
platform: server
product: confluence
subcategory: japi
title: Java API reference
---
# Java API reference

See the <a href="http://docs.atlassian.com/atlassian-confluence/latest-server" class="external-link">Confluence Server Javadoc</a>.

For examples see the [Confluence Plugin Guide](/server/confluence/confluence-plugin-guide) or one of our [Tutorials](/server/confluence/tutorials).
