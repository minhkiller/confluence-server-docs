---
aliases:
- /server/confluence/2031736.html
- /server/confluence/2031736.md
category: devguide
confluence_id: 2031736
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031736
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031736
date: '2017-12-08'
legacy_title: What is the best way to load a class or resource from a plugin?
platform: server
product: confluence
subcategory: faq
title: What is the best way to load a class or resource from a plugin?
---
# What is the best way to load a class or resource from a plugin?

### <img src="http://confluence.atlassian.com/images/icons/favicon.png" class="confluence-external-resource" /> What is the best way to load a resource from the classpath?

Because of the different ways that application servers deal with class-loading, just calling `this.getClass().getResourceAsStream()` might not work the same everywhere Confluence is deployed. To help, we have a utility method that checks the various classloaders in a predictable order:

``` java
InputStream in = com.atlassian.core.util.ClassLoaderUtils.getResourceAsStream(filename, this)
```

#### Inside Plugins

Because plugins may be dynamically loaded, each plugin may have its own classloader, separate from the main Confluence application. This makes loading resources like properties files from inside a plugin JAR a little tricky.

If the class from which you are loading the resource is in the *same jar* as the resource file itself (i.e. it's all part of the same plugin), you can use `ClassLoaderUtils` as above, and everything will work fine.

However, if you are trying to load the file from a different plugin, or from the main application code, you'll need an instance of the `pluginManager` from spring:

``` java
InputStream in = pluginManager.getDynamicResourceAsStream(filename)
```

#### An example of modules and resources

When writing your own ModuleDescriptor you might want developers that depend on your ModuleDescriptor write something that looks like this in their atlassian-plugin.xml:

**atlasian-plugin.xml example definition**

``` xml
<swagger-enforcer key="meRestSwaggerEnforcer">
    <resource name="my-rest-swagger" type="swagger" location="swagger/swagger.json" />
</swagger-enforcer>
```

They would then have a ModuleDescriptor with an init() method that looks something like this:

**ModuleDescriptor\#init**

``` java
@Override
public void init(@Nonnull Plugin plugin, @Nonnull Element element) throws PluginParseException {
    super.init(plugin, element);
 
    for (ResourceDescriptor resourceDescriptor : getResourceDescriptors(RESOURCE_DESCRIPTOR_TYPE)) {
        final InputStream potentialResource = plugin.getClassLoader().getResourceAsStream(resourceDescriptor.getLocation());
        Preconditions.checkState(potentialResource != null, "Could not find the swagger resource: " + resourceDescriptor.getLocation());


        // DO SOMETHING HERE
    }
}
```

There are two important function invocations in this example:

-   **AbstractModuleDescriptor\#getResourceDescriptors(java.lang.String)**  
    This method makes it easy to extract resource declarations from Atlassian Plugins 2 Module Descriptor XML definitions. You should use this for your own resource declarations.
-   **plugin.getClassLoader().getResourceAsStream(resourceDescriptor.getLocation());**  
    This is the preferred method of extracting a resource from another plugin.

Use this to load resources from other plugins that have been defined inside atlassian-plugin.xml files.

