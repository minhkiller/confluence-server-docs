---
aliases:
- /server/confluence/packaging-and-installing-a-theme-plugin-2031655.html
- /server/confluence/packaging-and-installing-a-theme-plugin-2031655.md
category: reference
confluence_id: 2031655
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031655
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031655
date: '2017-12-08'
legacy_title: Packaging and Installing a Theme Plugin
platform: server
product: confluence
subcategory: modules
title: Packaging and Installing a Theme plugin
---
# Packaging and Installing a Theme plugin

## The Theme Plugin Module

The theme module defines the theme itself. When someone in Confluence selects a theme either globally or within a space, they are selecting from the available theme modules.

``` xml
<theme key="dinosaurs" name="Dinosaur Theme"
       class="com.atlassian.confluence.themes.BasicTheme">
    <description>A nice theme for the kids</description>
    <colour-scheme key="com.example.themes.dinosaur:earth-colours"/>
    <layout key="com.example.themes.dinosaur:main"/>
    <layout key="com.example.themes.corporate:mail-template"/>
</theme>
```

The class of a theme must implement `com.atlassian.confluence.themes.Theme`. The `com.atlassian.confluence.themes.BasicTheme` class provided with Confluence gathers together all the resources listed within the module definition into a theme.

A theme can contain an optional `colour-scheme` element that defines which colour-scheme module this theme will use, and any number of `layout` elements that define which layouts should be applied in this theme. Refer to these modules by the **complete module key**.

It is possible for a theme to use modules that aren't in the same plugin as the theme. Just keep in mind that your theme will be messed up if some plugin that the theme depends on is removed.

## Installing the Theme

Themes are installed as 'plugin modules'. The plugin module is a collection of files, usually zipped up in a JAR archive, which tells Confluence how to find the decorators and colour-scheme of your theme.

You can use [plugins](/server/confluence/confluence-plugin-guide) in Confluence for many purposes, one of which is themes. In every case, the central configuration file, which describes the plugin to Confluence, is named `atlassian-plugin.xml`.

There are two steps to creating the plugin module.

1.  Create the central configuration file for the theme: [atlassian-plugin.xml](#writing-the-atlassian-plugin-xml-file-for-your-theme)
2.  Create the JAR archive for your theme: [bundling your theme](#bundling-the-theme)

### Writing the `atlassian-plugin.xml` file for your theme

The structure of an `atlassian-plugin.xml` file is fairly self-explanatory. In the code segment below you will find a full example of an `atlassian-plugin.xml` file, showing:

-   each of the decorators you have defined to customize Confluence
-   your colour scheme

in a way which Confluence can use to override the default theme. In other words, this XML tells Confluence to look in certain locations for replacement decorators when processing a request.

``` xml
<atlassian-plugin key="com.atlassian.confluence.themes.tabless" name="Plain Theme">
   <plugin-info>
       <description>
         This theme demonstrates a plain look and feel for Confluence. 
         It is useful as a building block for your own themes.
       </description>
       <version>1.0</version>
       <vendor name="Atlassian Software Systems Pty Ltd" url="http://www.atlassian.com/"/>
   </plugin-info>

    <theme key="tabless" name="Tabless Theme" class="com.atlassian.confluence.themes.BasicTheme">
        <description>plain Confluence theme.</description>
        <layout key="com.atlassian.confluence.themes.tabless:main"/>
        <layout key="com.atlassian.confluence.themes.tabless:global"/>
        <layout key="com.atlassian.confluence.themes.tabless:space"/>
        <layout key="com.atlassian.confluence.themes.tabless:page"/>
        <layout key="com.atlassian.confluence.themes.tabless:blogpost"/>
        <layout key="com.atlassian.confluence.themes.tabless:mail"/>
        <colour-scheme key="com.atlassian.confluence.themes.tabless:earth-colours"/>
        // Optional: for themes which need configuration.
        <param name="space-config-path" value="/themes/tabless/configuretheme.action"/>
        <param name="global-config-path" value="/admin/themes/tabless/configuretheme.action"/>
    </theme>

    <layout key="main" name="Main Decorator" class="com.atlassian.confluence.themes.VelocityDecorator"
            overrides="/decorators/main.vmd">
        <resource type="velocity" name="decorator"
                  location="com/atlassian/confluence/themes/tabless/main.vmd"/>
    </layout>

    <layout key="global" name="Global Decorator" class="com.atlassian.confluence.themes.VelocityDecorator"
            overrides="/decorators/global.vmd">
        <resource type="velocity" name="decorator"
                  location="com/atlassian/confluence/themes/tabless/global.vmd"/>
    </layout>

    <layout key="space" name="Space Decorator" class="com.atlassian.confluence.themes.VelocityDecorator"
            overrides="/decorators/space.vmd">
        <resource type="velocity" name="decorator"
                  location="com/atlassian/confluence/themes/tabless/space.vmd"/>
    </layout>

    <layout key="page" name="Page Decorator" class="com.atlassian.confluence.themes.VelocityDecorator"
            overrides="/decorators/page.vmd">
        <resource type="velocity" name="decorator"
                  location="com/atlassian/confluence/themes/tabless/page.vmd"/>
    </layout>

    <layout key="blogpost" name="Blogpost Decorator" class="com.atlassian.confluence.themes.VelocityDecorator"
            overrides="/decorators/blogpost.vmd">
        <resource type="velocity" name="decorator"
                  location="com/atlassian/confluence/themes/tabless/blogpost.vmd"/>
    </layout>

    <layout key="mail" name="Mail Decorator" class="com.atlassian.confluence.themes.VelocityDecorator"
            overrides="/decorators/mail.vmd">
        <resource type="velocity" name="decorator"
                  location="com/atlassian/confluence/themes/tabless/mail.vmd"/>
    </layout>


    <colour-scheme key="earth-colours" name="Brown and Red Earth Colours"
                    class="com.atlassian.confluence.themes.BaseColourScheme">
         <colour key="topbar" value="#440000"/>
         <colour key="spacename" value="#999999"/>
         <colour key="headingtext" value="#663300"/>
         <colour key="link" value="#663300"/>
         <colour key="border" value="#440000"/>
         <colour key="navbg" value="#663300"/>
         <colour key="navtext" value="#ffffff"/>
         <colour key="navselectedbg" value="#440000"/>
         <colour key="navselectedtext" value="#ffffff"/>
    </colour-scheme>

</atlassian-plugin>
```

The class which each decorator, or layout, is mapped to must implement `com.atlassian.confluence.themes.VelocityDecorator`.

The layout entry must provide an `overrides` attribute which defines which decorator within Confluence is being overridden by the theme.

Importantly, when telling Confluence to override a particular decorator with another one, the location of the custom decorator is specified. For example:

``` xml
<layout key="page" name="Page Decorator" class="com.atlassian.confluence.themes.VelocityDecorator"
            overrides="/decorators/page.vmd">
        <resource type="velocity" name="decorator"
                  location="com/atlassian/confluence/themes/tabless/page.vmd"/>
</layout>
```

The location attribute needs to be represented in the JAR archive you will use to bundle your theme.

### Bundling the Theme

Your decorators should be placed in a directory hierarchy which makes sense to you. The `atlassian-plugin.xml` file should be placed at the top level of the directory structure, afterwards the decorators should be placed in directories which make a meaningful division of what they do. It is your choice as to how the structure is laid out, each decorator could even be placed alongside atlassian-plugin.xml}}. The essential thing is for the location attribute of each decorator to accurately tell Confluence how to load it.

Thus, a recursive directory listing of the example theme above gives:

``` bash
atlassian-plugin.xml
com/atlassian/confluence/themes/tabless/
com/atlassian/confluence/themes/tabless/global.vmd
com/atlassian/confluence/themes/tabless/space.vmd
com/atlassian/confluence/themes/tabless/mail.vmd
com/atlassian/confluence/themes/tabless/blogpost.vmd
com/atlassian/confluence/themes/tabless/main.vmd
com/atlassian/confluence/themes/tabless/page.vmd 
```
