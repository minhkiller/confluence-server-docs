---
aliases:
- /server/confluence/cql-operators-reference-29951942.html
- /server/confluence/cql-operators-reference-29951942.md
category: reference
confluence_id: 29951942
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=29951942
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=29951942
date: '2017-12-08'
legacy_title: CQL Operators Reference
platform: server
product: confluence
subcategory: api
title: CQL operators reference
---
# CQL operators reference

## CQL Operators

An operator in CQL is one or more symbols or words which compares the value of a [field](/server/confluence/advanced-searching-using-cql#field) on its left with one or more values (or [functions](/server/confluence/advanced-searching-using-cql#functions)) on its right, such that only true results are retrieved by the clause. Some operators may use the [NOT](/server/confluence/cql-keywords-reference#n-o-t) keyword.

**List of Operators:**

#### EQUALS: =

The "`=`" operator is used to search for content where the value of the specified field exactly matches the specified value. (Note: cannot be used with [text](/server/confluence/advanced-searching-using-cql#text) fields; see the [CONTAINS](#contains) operator instead.)

To find content where the value of a specified field exactly matches *multiple* values, use multiple "`=`" statements with the [AND](/server/confluence/cql-keywords-reference#a-n-d) operator.

###### Examples

-   Find all content that were created by jsmith:

    ``` sql
    creator = jsmith
    ```

-   Find all content that has the title "Advanced Searching"

    ``` sql
    title = "Advanced Searching"
    ```

#### NOT EQUALS: !=

The "`!=`" operator is used to search for content where the value of the specified field does not match the specified value. (Note: cannot be used with [text](#text) fields; see the [DOES NOT MATCH](#does-not-match) ("`!~`") operator instead.)

Note: typing `field != value` is the same as typing `NOT field = value`.

{{% note %}}

Currently a negative expression cannot be the first clause in a CQL statement

{{% /note %}}

###### Examples

-   Find all content in the DEV space that was created by someone other than jsmith:

    ``` sql
    space = DEV and not creator = jsmith
    ```

    or:

    ``` sql
    space = DEV and creator != jsmith
    ```

-   Find all content that was created by me but doesn't mention me

    ``` sql
    creator = currentUser() and mention != currentUser()
    ```

#### GREATER THAN: &gt;

The "`>`" operator is used to search for content where the value of the specified field is greater than the specified value. Cannot be used with [text](#text) fields.

Note that the "`>`" operator can only be used with fields which support range operators (e.g. date fields and numeric fields). To see a field's supported operators, check the individual [field](/server/confluence/advanced-searching-using-cql#field) reference.

###### Examples

-   Find all content created in the last 4 weeks

    ``` sql
    created > now("-4w")
    ```

-   Find all attachments last modified since the start of the month

    ``` sql
    created > startOfMonth() and type = attachment
    ```

#### GREATER THAN EQUALS: &gt;=

The "`>=`" operator is used to search for content where the value of the specified field is greater than or equal to the specified value. Cannot be used with [text](#text) fields.

Note that the "`>=`" operator can only be used with fields which support range operators (e.g. date fields). To see a field's supported operators, check the individual [field](/server/confluence/advanced-searching-using-cql#field) reference.

###### Examples

-   Find all content created on or after 31/12/2008:

    ``` sql
    created >= "2008/12/31"
    ```

#### LESS THAN: &lt;

The "`<`" operator is used to search for content where the value of the specified field is less than the specified value. Cannot be used with [text](/server/confluence/advanced-searching-using-cql#text) fields.

Note that the "`<`" operator can only be used with fields which support range operators (e.g. date fields). To see a field's supported operators, check the individual [field](/server/confluence/advanced-searching-using-cql#field) reference.

###### Examples

-   Find all pages lastModified before the start of the year

    ``` sql
    lastModified < startOfYear() and type = page
    ```

#### LESS THAN EQUALS: &lt;=

The "`<=`" operator is used to search for content where the value of the specified field is less than or equal to than the specified value. Cannot be used with [text](#text) fields.

Note that the "`<=`" operator can only be used with fields which support range operators (e.g. date fields). To see a field's supported operators, check the individual [field](/server/confluence/advanced-searching-using-cql#field) reference.

###### Examples

-   Find blogposts created in the since the start of the fortnight

    ``` sql
    created >= startOfWeek("-1w") and type = blogpost
    ```

#### IN

The "`IN`" operator is used to search for content where the value of the specified field is one of multiple specified values. The values are specified as a comma-delimited list, surrounded by parentheses.

Using "`IN`" is equivalent to using multiple [EQUALS](#equals) (=) statements with the OR keyword, but is shorter and more convenient. That is, typing `creator IN (tom, jane, harry)` is the same as typing `creator = "tom" `[OR](/server/confluence/cql-keywords-reference#o-r)` creator = "jane" `[OR](/server/confluence/cql-keywords-reference#o-r) `creator = "harry"`.

###### Examples

-   Find all content that mentions either jsmith or jbrown or jjones:

    ``` sql
    mention in (jsmith,jbrown,jjones)
    ```

-   Find all content where the creator or contributor is either Jack or Jill:

    ``` sql
    creator in (Jack,Jill) or contributor in (Jack,Jill)
    ```

#### NOT IN

The "`NOT IN`" operator is used to search for content where the value of the specified field is not one of multiple specified values.

Using "`NOT IN`" is equivalent to using multiple

[NOT\_EQUALS](#not-equals) `(`!=`)` statements, but is shorter and more convenient. That is, typing `creator NOT IN (tom, jane, harry)` is the same as typing `creator != "tom" `[AND](/server/confluence/cql-keywords-reference#a-n-d) creator != "jane" [AND](#and) `creator != "harry"`.

###### Examples

-   Find all content where the creator is someone other than Jack, Jill or John:

    ``` sql
    space = DEV and creator not in (Jack,Jill,John)
    ```

#### CONTAINS: ~

The "`~`" operator is used to search for content where the value of the specified field matches the specified value (either an exact match or a "fuzzy" match -- see examples below). The "~" operator can only be used with text fields, for example:

-   title
-   text

Note: when using the "`~`" operator, the value on the right-hand side of the operator can be specified using [Confluence text-search syntax](/server/confluence/performing-text-searches-using-cql).

###### Examples

-   Find all content where the title contains the word "win" (or simple derivatives of that word, such as "wins"):

    ``` sql
    title ~ win
    ```

-   Find all content where the title contains a [wild-card](/server/confluence/cql-operators-reference) match for the word "win":

    ``` sql
    title ~ "win*"
    ```

-   Find all content where the text contains the word "advanced" and the word "search":

    ``` sql
    text ~ "advanced search"
    ```

#### DOES NOT CONTAIN: !~

The "`!~`" operator is used to search for content where the value of the specified field is not a "fuzzy" match for the specified value. The "!~" operator can only be used with text fields, for example:

-   title
-   text

Note: when using the "`!~`" operator, the value on the right-hand side of the operator can be specified using [Confluence text-search syntax](/server/confluence/performing-text-searches-using-cql).

###### Examples

-   Find all content where the title does not contain the word "run" (or derivatives of that word, such as "running" or "ran"):

    ``` sql
    space = DEV and title !~ run
    ```
