---
aliases:
- /server/confluence/language-pack-flags-2031775.html
- /server/confluence/language-pack-flags-2031775.md
category: reference
confluence_id: 2031775
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031775
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031775
date: '2017-12-08'
legacy_title: Language Pack Flags
platform: server
product: confluence
subcategory: modules
title: Language Pack Flags
---
# Language Pack Flags

Below are flags that can be used with Language Pack Plugins in Confluence. For individual country names, see the [DOC:attachments list](https://developer.atlassian.com///pages/viewpageattachments.action?pageId=161665).

{{% warning %}}

These images are only for us within Confluence plugins and may not be redistributed with any other code. For license details, see [license.txt](https://dac-lf.prod.atl-paas.net/server/confluence/attachments/2031775/2228554.txt)

{{% /warning %}}

<table>
<colgroup>
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
</colgroup>
<tbody>
<tr class="odd">
<td><img src="/server/confluence/images/flag-zimbabwe.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-zambia.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-yemen.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-wallis-and-futuna.png" width="48" height="48" /></td>
</tr>
<tr class="even">
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
</tr>
<tr class="odd">
<td><img src="/server/confluence/images/flag-wales.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-virgin-islands.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-vietnam.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-venezuela.png" width="48" height="48" /></td>
</tr>
<tr class="even">
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
</tr>
<tr class="odd">
<td><img src="/server/confluence/images/flag-vatican-city.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-vanuatu.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-uzbekistan.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-usa.png" width="48" height="48" /></td>
</tr>
<tr class="even">
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
</tr>
<tr class="odd">
<td><img src="/server/confluence/images/flag-uruquay.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-united-arab-emirates.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-ukraine.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-uganda.png" width="48" height="48" /></td>
</tr>
<tr class="even">
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
</tr>
<tr class="odd">
<td><img src="/server/confluence/images/flag-tuvalu.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-turks-and-caicos-islands.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-turkmenistan.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-turkey.png" width="48" height="48" /></td>
</tr>
<tr class="even">
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
</tr>
<tr class="odd">
<td><img src="/server/confluence/images/flag-tunisia.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-trinidad-and-tobago.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-tonga.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-togo.png" width="48" height="48" /></td>
</tr>
<tr class="even">
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
</tr>
<tr class="odd">
<td><img src="/server/confluence/images/flag-tibet.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-thailand.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-tanzania.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-tajikistan.png" width="48" height="48" /></td>
</tr>
<tr class="even">
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
</tr>
<tr class="odd">
<td><img src="/server/confluence/images/flag-taiwan.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-syria.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-switzerland.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-sweden.png" width="48" height="48" /></td>
</tr>
<tr class="even">
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
</tr>
<tr class="odd">
<td><img src="/server/confluence/images/flag-swaziland.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-suriname.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-sudan.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-sri-lanka.png" width="48" height="48" /></td>
</tr>
<tr class="even">
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
</tr>
<tr class="odd">
<td><img src="/server/confluence/images/flag-spain.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-south-korea.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-south-georgia.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-south-africa.png" width="48" height="48" /></td>
</tr>
<tr class="even">
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
</tr>
<tr class="odd">
<td><img src="/server/confluence/images/flag-somalia.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-solomon-islands.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-slovenia.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-slovakia.png" width="48" height="48" /></td>
</tr>
<tr class="even">
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
</tr>
<tr class="odd">
<td><img src="/server/confluence/images/flag-singapore.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-sierra-leone.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-seychelles.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-serbia-montenegro.png" width="48" height="48" /></td>
</tr>
<tr class="even">
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
</tr>
<tr class="odd">
<td><img src="/server/confluence/images/flag-senegal.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-scotland.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-saudi-arabia.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-sao-tome-and-principe.png" width="48" height="48" /></td>
</tr>
<tr class="even">
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
</tr>
<tr class="odd">
<td><img src="/server/confluence/images/flag-san-marino.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-samoa.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-saint-vincent-and-the-grenadines.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-saint-pierre-and-miquelon.png" width="48" height="48" /></td>
</tr>
<tr class="even">
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
</tr>
<tr class="odd">
<td><img src="/server/confluence/images/flag-saint-lucia.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-saint-kitts-and-nevis.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-saint-helena.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-rwanda.png" width="48" height="48" /></td>
</tr>
<tr class="even">
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
</tr>
<tr class="odd">
<td><img src="/server/confluence/images/flag-russia.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-romania.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-qatar.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-puerto-rico.png" width="48" height="48" /></td>
</tr>
<tr class="even">
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
</tr>
<tr class="odd">
<td><img src="/server/confluence/images/flag-portugal.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-poland.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-pitcairn-islands.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-philippines.png" width="48" height="48" /></td>
</tr>
<tr class="even">
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
</tr>
<tr class="odd">
<td><img src="/server/confluence/images/flag-peru.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-paraquay.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-papua-new-guinea.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-panama.png" width="48" height="48" /></td>
</tr>
<tr class="even">
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
</tr>
<tr class="odd">
<td><img src="/server/confluence/images/flag-palau.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-pakistan.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-oman.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-norway.png" width="48" height="48" /></td>
</tr>
<tr class="even">
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
</tr>
<tr class="odd">
<td><img src="/server/confluence/images/flag-northern-mariana-islands.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-north-korea.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-norfolk-island.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-niue.png" width="48" height="48" /></td>
</tr>
<tr class="even">
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
</tr>
<tr class="odd">
<td><img src="/server/confluence/images/flag-nigeria.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-niger.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-nicaragua.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-new-zealand.png" width="48" height="48" /></td>
</tr>
<tr class="even">
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
</tr>
<tr class="odd">
<td><img src="/server/confluence/images/flag-netherlands-antilles.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-netherlands.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-nepal.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-nauru.png" width="48" height="48" /></td>
</tr>
<tr class="even">
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
</tr>
<tr class="odd">
<td><img src="/server/confluence/images/flag-namibia.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-mozambique.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-morocco.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-montserrat.png" width="48" height="48" /></td>
</tr>
<tr class="even">
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
</tr>
<tr class="odd">
<td><img src="/server/confluence/images/flag-mongolia.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-monaco.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-moldova.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-micronesia.png" width="48" height="48" /></td>
</tr>
<tr class="even">
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
</tr>
<tr class="odd">
<td><img src="/server/confluence/images/flag-mexico.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-mauritius.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-mauretania.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-martinique.png" width="48" height="48" /></td>
</tr>
<tr class="even">
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
</tr>
<tr class="odd">
<td><img src="/server/confluence/images/flag-marshall-islands.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-malta.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-mali.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-maledives.png" width="48" height="48" /></td>
</tr>
<tr class="even">
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
</tr>
<tr class="odd">
<td><img src="/server/confluence/images/flag-malaysia.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-malawi.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-madagascar.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-macedonia.png" width="48" height="48" /></td>
</tr>
<tr class="even">
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
</tr>
<tr class="odd">
<td><img src="/server/confluence/images/flag-macau.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-luxembourg.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-lithuania.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-liechtenstein.png" width="48" height="48" /></td>
</tr>
<tr class="even">
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
</tr>
<tr class="odd">
<td><img src="/server/confluence/images/flag-libya.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-liberia.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-lesotho.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-lebanon.png" width="48" height="48" /></td>
</tr>
<tr class="even">
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
</tr>
<tr class="odd">
<td><img src="/server/confluence/images/flag-latvia.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-laos.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-kyrgyzstan.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-kuwait.png" width="48" height="48" /></td>
</tr>
<tr class="even">
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
</tr>
<tr class="odd">
<td><img src="/server/confluence/images/flag-kiribati.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-kenya.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-kazakhstan.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-jordan.png" width="48" height="48" /></td>
</tr>
<tr class="even">
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
</tr>
<tr class="odd">
<td><img src="/server/confluence/images/flag-jersey.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-japan.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-jamaica.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-italy.png" width="48" height="48" /></td>
</tr>
<tr class="even">
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
</tr>
<tr class="odd">
<td><img src="/server/confluence/images/flag-israel.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-isle-of-man.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-ireland.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-iraq.png" width="48" height="48" /></td>
</tr>
<tr class="even">
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
</tr>
<tr class="odd">
<td><img src="/server/confluence/images/flag-iran.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-indonesia.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-india.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-iceland.png" width="48" height="48" /></td>
</tr>
<tr class="even">
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
</tr>
<tr class="odd">
<td><img src="/server/confluence/images/flag-hungary.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-hong-kong.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-honduras.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-haiti.png" width="48" height="48" /></td>
</tr>
<tr class="even">
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
</tr>
<tr class="odd">
<td><img src="/server/confluence/images/flag-guyana.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-guinea-bissau.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-guinea.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-guernsey.png" width="48" height="48" /></td>
</tr>
<tr class="even">
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
</tr>
<tr class="odd">
<td><img src="/server/confluence/images/flag-guatemala.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-guam.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-grenada.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-greenland.png" width="48" height="48" /></td>
</tr>
<tr class="even">
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
</tr>
<tr class="odd">
<td><img src="/server/confluence/images/flag-greece.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-great-britain.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-gibraltar.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-ghana.png" width="48" height="48" /></td>
</tr>
<tr class="even">
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
</tr>
<tr class="odd">
<td><img src="/server/confluence/images/flag-germany.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-georgia.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-gambia.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-gabon.png" width="48" height="48" /></td>
</tr>
<tr class="even">
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
</tr>
<tr class="odd">
<td><img src="/server/confluence/images/flag-french-polynesia.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-france.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-finland.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-fiji.png" width="48" height="48" /></td>
</tr>
<tr class="even">
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
</tr>
<tr class="odd">
<td><img src="/server/confluence/images/flag-faroe-islands.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-falkland-islands.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-ethiopia.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-estonia.png" width="48" height="48" /></td>
</tr>
<tr class="even">
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
</tr>
<tr class="odd">
<td><img src="/server/confluence/images/flag-eritrea.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-equatorial-guinea.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-equador.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-england.png" width="48" height="48" /></td>
</tr>
<tr class="even">
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
</tr>
<tr class="odd">
<td><img src="/server/confluence/images/flag-el-salvador.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-egypt.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-east-timor.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-dominican-republic.png" width="48" height="48" /></td>
</tr>
<tr class="even">
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
</tr>
<tr class="odd">
<td><img src="/server/confluence/images/flag-dominica.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-djibouti.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-denmark.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-czech-republic.png" width="48" height="48" /></td>
</tr>
<tr class="even">
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
</tr>
<tr class="odd">
<td><img src="/server/confluence/images/flag-cyprus.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-cuba.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-croatia.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-cote-divoire.png" width="48" height="48" /></td>
</tr>
<tr class="even">
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
</tr>
<tr class="odd">
<td><img src="/server/confluence/images/flag-costa-rica.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-cook-islands.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-congo-republic.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-congo-democratic-republic.png" width="48" height="48" /></td>
</tr>
<tr class="even">
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
</tr>
<tr class="odd">
<td><img src="/server/confluence/images/flag-comoros.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-colombia.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-china.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-chile.png" width="48" height="48" /></td>
</tr>
<tr class="even">
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
</tr>
<tr class="odd">
<td><img src="/server/confluence/images/flag-chad.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-central-african-republic.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-cayman-islands.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-cape-verde.png" width="48" height="48" /></td>
</tr>
<tr class="even">
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
</tr>
<tr class="odd">
<td><img src="/server/confluence/images/flag-canada.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-cameroon.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-cambodia.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-burundi.png" width="48" height="48" /></td>
</tr>
<tr class="even">
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
</tr>
<tr class="odd">
<td><img src="/server/confluence/images/flag-burma.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-burkina-faso.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-bulgaria.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-brunei.png" width="48" height="48" /></td>
</tr>
<tr class="even">
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
</tr>
<tr class="odd">
<td><img src="/server/confluence/images/flag-british-virgin-islands.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-british-indian-ocean-territory.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-brazil.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-botswana.png" width="48" height="48" /></td>
</tr>
<tr class="even">
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
</tr>
<tr class="odd">
<td><img src="/server/confluence/images/flag-bosnia-and-herzegovina.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-bolivia.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-bhutan.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-bermuda.png" width="48" height="48" /></td>
</tr>
<tr class="even">
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
</tr>
<tr class="odd">
<td><img src="/server/confluence/images/flag-benin.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-belize.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-belgium.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-belarus.png" width="48" height="48" /></td>
</tr>
<tr class="even">
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
</tr>
<tr class="odd">
<td><img src="/server/confluence/images/flag-barbados.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-bangladesh.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-bahrain.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-bahamas.png" width="48" height="48" /></td>
</tr>
<tr class="even">
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
</tr>
<tr class="odd">
<td><img src="/server/confluence/images/flag-azerbaijan.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-austria.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-australia.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-aruba.png" width="48" height="48" /></td>
</tr>
<tr class="even">
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
</tr>
<tr class="odd">
<td><img src="/server/confluence/images/flag-armenia.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-argentina.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-antigua-and-barbuda.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-anguilla.png" width="48" height="48" /></td>
</tr>
<tr class="even">
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
</tr>
<tr class="odd">
<td><img src="/server/confluence/images/flag-angola.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-andorra.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-american-samoa.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-algeria.png" width="48" height="48" /></td>
</tr>
<tr class="even">
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
</tr>
<tr class="odd">
<td><img src="/server/confluence/images/flag-albania.png" width="48" height="48" /></td>
<td><img src="/server/confluence/images/flag-afghanistan.png" width="48" height="48" /></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><div class="gallery-caption">

</div></td>
<td><div class="gallery-caption">

</div></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>
