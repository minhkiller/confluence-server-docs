---
aliases:
- /server/confluence/2031636.html
- /server/confluence/2031636.md
category: devguide
confluence_id: 2031636
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031636
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031636
date: '2017-12-08'
legacy_title: I am trying to compile a plugin, but get an error about the target release
platform: server
product: confluence
subcategory: faq
title: I am trying to compile a plugin, but get an error about the target release
---
# I am trying to compile a plugin, but get an error about the target release

**I am trying to compile a plugin, but get an error about the "target release"**

When compiling plugins and using version 1.5 of the JDK, the following error may appear:

``` bash
javac: target release 1.3 conflicts with default source release 1.5
```

##### SOLUTION

The solution is essentially to tell your compiler to target Java 1.3. How to do this will differ depending on what compiler you are using, but generally, something like this will work:

``` bash
javac -target 1.3 <other options here>
```

If you are using Maven to build your project, try adding the following to your `project.properties` or `build.properties` file:

``` bash
# Set the javac target to 1.3
maven.compile.target=1.3
maven.compile.source=1.3
```

If the solutions above do not resolve this issue and you are using an older version of Confluence, try the following approach:  
Open the *src/etc/plugins/build.xml* file and in the line that looks similar to the following one, change its `target` parameter from "1.3" to "1.5":

``` xml
<javac destdir="${library}/classes" target="1.3" debug="${debug}" deprecation="false" optimize="false" failonerror="true">
```

##### RELATED TOPICS

[Confluence Plugin Guide](/server/confluence/confluence-plugin-guide) <br>
<a href="#faq-home" class="unresolved">FAQ Home</a>
