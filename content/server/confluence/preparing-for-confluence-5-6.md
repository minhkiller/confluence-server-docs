---
aliases:
- /server/confluence/preparing-for-confluence-5.6-28870185.html
- /server/confluence/preparing-for-confluence-5.6-28870185.md
category: devguide
confluence_id: 28870185
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=28870185
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=28870185
date: '2017-12-08'
legacy_title: Preparing for Confluence 5.6
platform: server
product: confluence
subcategory: development resources
title: Preparing for Confluence 5.6
---
# Preparing for Confluence 5.6

This documentation is intended for Confluence developers who want to ensure that their existing plugins and add-ons are compatible with Confluence 5.6, and to learn about the new features for developing add-ons in Confluence 5.6.

This page will be updated as further EAP development releases are available.

### Improved clustering solution for Confluence

As previously mentioned, we have been working on a new clustering solution for Confluence. Important changes for plugin developers include:

-   Changes to the home directory - in a clustered environment Confluence will have a [local home and a shared home](/server/confluence/how-do-i-ensure-my-add-on-works-properly-in-a-cluster#cluster-home-directory-home-directory). Plugins will need to decide the most appropriate place to store any data they place on the file system. 
-   Introduction of Hazelcast - data will be evenly partitioned across all the nodes in a cluster, instead of being fully replicated on each node. 
-   New cluster lock service - the locking primitives provided with Java (`java.util.concurrent.Lock`, `synchronized`, etc.) will not properly ensure serialised access to data in a cluster. Instead, you need to use the cluster-wide lock that is provided through the Beehive <a href="https://docs.atlassian.com/beehive/0.0.6/apidocs/com/atlassian/beehive/ClusterLockService.html" class="external-link">ClusterLockService</a> API.
-   Show compatibility on Marketplace - you can use the  `atlassian-data-center-compatible` parameter to your `atlassian-plugin.xml` descriptor file to indicate that your plugin is cluster compatible.   
    Note: plugins should not cache licenses or license states as this will prevent license changes being correctly propagated to all nodes in the cluster. 

See [How do I ensure my add-on works properly in a cluster?](/server/confluence/how-do-i-ensure-my-add-on-works-properly-in-a-cluster) for more information. 

### Licensing library changes

Confluence's licensing libraries have been upgraded.  Plugins should use the  the `com.atlassian.confluence.license.LicenseService#retrieve()` method to retrieve the currently installed Confluence license.  The method `com.atlassian.confluence.license.LicenseService#retrieveAtlassianLicense()` can also be used to retrieve the encompassing `com.atlassian.extras.api.AtlassianLicense`.

Plugins should not use:

1.  `com.atlassian.confluence.setup.ConfluenceLicenseRegistry` (deprecated)
2.  `com.atlassian.extras.core.LicenseManagerFactory` (no longer exists, plugins using this class will not run)
3.  `com.atlassian.config.bootstrap.AtlassianBootstrapManager#getString` - passing in `com.atlassian.confluence.setup.ConfluenceBootstrapConstants#LICENSE_MESSAGE_KEY` and/or `com.atlassian.confluence.setup.ConfluenceBootstrapConstants#LICENSE_HASH_KEY`

Milestone releases for developers

Please watch the <a href="http://confluence.atlassian.com/display/DOC/Confluence+Development+Releases" class="external-link">Development Releases</a> page for pre-release versions of Confluence 5.6 that you can use for testing purposes.

## Have questions?

If you have any questions about preparing for Confluence 5.6, please register for an account on <a href="https://answers.atlassian.com/" class="external-link">Atlassian Answers</a>, our community Q&A resource, and <a href="https://answers.atlassian.com/questions/ask/?tags=confluence-development" class="external-link">ask a question</a>. We'll be keeping an eye on things to help out as soon as possible.

