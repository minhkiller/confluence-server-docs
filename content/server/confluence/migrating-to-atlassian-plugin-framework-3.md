---
aliases:
- /server/confluence/migrating-to-atlassian-plugin-framework-3-21463080.html
- /server/confluence/migrating-to-atlassian-plugin-framework-3-21463080.md
category: devguide
confluence_id: 21463080
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=21463080
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=21463080
date: '2017-12-08'
legacy_title: Migrating to Atlassian Plugin Framework 3
platform: server
product: confluence
subcategory: updates
title: Migrating to Atlassian Plugin Framework 3
---
# Migrating to Atlassian Plugin Framework 3

This page gives guidelines for migrating your plugin to version 3.x of the Atlassian Plugin Framework.

## Breaking changes

### The version number of Atlassian Plugin Framework

The new major version number of the Atlassian Plugin Framework means some plugins that import `com.atlassian.plugin*` packages will break.

Most plugins will import with a version set like this: 

`com.atlassian.plugin;version="2.8"`

These will be fine. Some, however, will have their import set like this: 

`com.atlassian.plugin;version="[2.8,3.0)"`

These will break.

To avoid this, change the import version for the second pattern -  `[2.8,4.0)` - since we don't plan to break any APIs in the `3.x` cycle of the plugin framework.

### The default constructor of `AbstractModuleDescriptor`

This constructor was removed. A number of our own module descriptors were using the default constructor. That means their modules were using the `LegacyModuleFactory,` which is not the module factory configured for their products. In most cases, to fix this you just need to change the constructor. Sometimes you need to make small changes in the module descriptor when loading module classes, and add an actual `component-import` for the module factory exposed by the host product.

### Removed `com.atlassian.plugin.util.collect.CollectionUtil` (previously deprecated)

This class doesn't bring anything that `Guava` doesn't. We have now removed it, in favor of using `Guava`.

## Web Resource and Web Fragment split

The following two modules have been split into their own repositories, with their own version control:

`com.atlassian.plugins:atlassian-plugins-webresource`

`com.atlassian.plugins:atlassian-plugins-webfragment`

This means that those libraries don't necessarily have the same version as the Atlassian Plugin Framework any more. They are usually already declared in the product POMs. If not you will need to declare them.
