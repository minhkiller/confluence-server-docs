---
aliases:
- /server/confluence/macro-tutorials-for-confluence-2031696.html
- /server/confluence/macro-tutorials-for-confluence-2031696.md
category: devguide
confluence_id: 2031696
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031696
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031696
date: '2017-12-08'
legacy_title: Macro Tutorials for Confluence
platform: server
product: confluence
subcategory: learning
title: Macro tutorials for Confluence
---
# Macro tutorials for Confluence

-   [Creating a New Confluence Macro](/server/confluence/creating-a-new-confluence-macro)
-   [Customising the Display of Macro Parameters in the Editor Placeholder](/server/confluence/customising-the-display-of-macro-parameters-in-the-editor-placeholder)
-   [Extending the Macro Property Panel](/server/confluence/extending-the-macro-property-panel)
-   [Making your macro render in Confluence mobile](/server/confluence/making-your-macro-render-in-confluence-mobile)
-   [Providing an Image as a Macro Placeholder in the Editor](/server/confluence/providing-an-image-as-a-macro-placeholder-in-the-editor)
-   [Upgrading and Migrating an Existing Confluence Macro to 4.0](/server/confluence/upgrading-and-migrating-an-existing-confluence-macro-to-4-0)
-   [Writing a Macro Using JSON](/server/confluence/writing-a-macro-using-json)
-   [Writing Macros for Pre-4.0 Versions of Confluence](/server/confluence/writing-macros-for-pre-4-0-versions-of-confluence)

We also have [Plugin points for the editor in 4.0](/server/confluence/plugin-points-for-the-editor-in-4-0).
