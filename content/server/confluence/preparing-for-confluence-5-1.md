---
aliases:
- /server/confluence/preparing-for-confluence-5.1-18251993.html
- /server/confluence/preparing-for-confluence-5.1-18251993.md
category: devguide
confluence_id: 18251993
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=18251993
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=18251993
date: '2017-12-08'
legacy_title: Preparing for Confluence 5.1
platform: server
product: confluence
subcategory: development resources
title: Preparing for Confluence 5.1
---
# Preparing for Confluence 5.1

This documentation is intended for Confluence developers who want to ensure that their existing plugins and add-ons are compatible with Confluence 5.1, and to learn about the new features for developing add-ons in Confluence 5.1.

Confluence 5.1 introduces blueprints - add-ons which create different types of content.

## Introducing Confluence blueprints

In Confluence 5.0 we introduced the new Create experience. Add-on developers can make use of this create experience by creating add-ons which create different types of content.  The goal of Confluence Blueprints is to:

-   Show users what types of content they can create
-   Guides them to be able to create the content
-   Helps them structure the content

We know that Confluence is awesome for building 'mini applications', like meeting notes, release notes, image galleries, even lists of documents and files. But customers don't always know how to create these things. Confluence Blueprints helps "bootstrap" the page creation process. It creates content that is consistently formatted, automatically organized, and easy to find.

A typical blueprint would

-   extend the Confluence create experience to possibly include a "wizard-like" interface to creating a page
-   could come bundled with one or more templates or macros
-   optionally include a wizard which helps pre-populate the template or macro with content
-   can provide an "index" page: a page that lists all content for a particular label/type.   

Calling all add-on developers! Confluence **blueprints** means that you can build these as well and exchange them through the <a href="https://marketplace.atlassian.com/" class="external-link">Atlassian Marketplace</a>, either as an enhancement to your existing plugin or as a standalone plugin.

To help you get started we have a few tutorials available. See [Confluence Blueprints](/server/confluence/confluence-blueprints) for details. 

## Instructional text in templates

Blueprints (and other add-ons) can make use of the new "Instructional Text" feature for templates. The purpose of instructional text is to:

-   Make it easy for a template creator to include information on how to fill out the template.
-   Make it easy for the template end-users to fill out a template by:
    -   automatically clearing all "instructional text" as the user types in a specific text block, or
    -   automatically triggering a @mention prompt for user selection.

Learn more about [how to create instructional text](/server/confluence/instruction-text-in-blueprints).  

## Extending the image properties dialog

Confluence 5.1 also introduces a new pluggable image properties dialog. If you are building an add-on which extends an image in any way (such as an image map, image manipulation etc..) you should consider using the image properties dialog for this. See [extending the Image Properties Dialog](/server/confluence/extending-the-image-properties-dialog) for details.

## Labels tidied up

We have simplified and improved the labels code, and added some REST resources:

-   We have deleted two Velocity (vm) files. Please adjust any plugins that use them:
    -   `label-listitems.vm` - This provided a for-each loop over another template. You should be able to get the same functionality from `labels-list.vm`.
    -   `labels-dialog.vm` - We now use the Soy version of this template.
-   Labels now follow the <a href="https://developer.atlassian.com/design/labels.html" class="external-link">ADG/AUI Labels</a> guidelines. Your plugin should use these guidelines too, so that it automatically inherits AUI changes in future releases.

## Atlassian UI updates

Following on from Confluence 5.0, this release includes further improvements to the Confluence UI. In Confluence 5.0, we introduced the [new Atlassian Design Guidelines](https://developer.atlassian.com/design/) (ADG) which included a major upgrade of [AUI](https://developer.atlassian.com/display/AUI) (the Atlassian User Interface) and jQuery that Confluence supports.

-   Labels now follow the <a href="https://developer.atlassian.com/design/labels.html" class="external-link">ADG/AUI Labels</a> guidelines.

## Other note-worthy modifications

-   The [Creating a Template Bundle](/server/confluence/creating-a-template-bundle) plugin point will be removed for Confluence 5.1. This will be replaced the new pluggable create experience mentioned above.

## Milestone releases for developers

Please watch the <a href="http://confluence.atlassian.com/display/DOC/Confluence+Development+Releases" class="external-link">Development Releases</a> page for pre-release versions of Confluence 5.1 that you can use for testing purposes.

## Advance notices

Below are notices for future release consideration for plugin developers:

-   The "Add" menu drop-down web item is hidden for the default theme in Confluence 5.0 and later, and will probably be removed from other theme in future versions of Confluence. The new "Create" experience (see above) will replace the need for the Add button on the view page, consolidating all types of content in one place. There will be no migration path for the old Add drop-down.

## Have questions?

If you have any questions about preparing for Confluence 5.1, please register for an account on <a href="https://answers.atlassian.com/" class="external-link">Atlassian Answers</a>, our community Q&A resource, and <a href="https://answers.atlassian.com/questions/ask/?tags=confluence-development" class="external-link">ask a question</a>. We'll be keeping an eye on things to help out as soon as possible.
