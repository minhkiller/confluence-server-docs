---
aliases:
- /server/confluence/preparing-for-confluence-5.10-39977451.html
- /server/confluence/preparing-for-confluence-5.10-39977451.md
category: devguide
confluence_id: 39977451
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39977451
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39977451
date: '2017-12-08'
legacy_title: Preparing for Confluence 5.10
platform: server
product: confluence
subcategory: development resources
title: Preparing for Confluence 5.10
---
# Preparing for Confluence 5.10

This documentation is intended for Confluence developers who want to ensure that their existing plugins and add-ons are compatible with Confluence 5.10, and to learn about the new features for developing add-ons in Confluence 5.10.

Please watch the <a href="http://confluence.atlassian.com/display/DOC/Confluence+Development+Releases" class="external-link">Development Releases</a> page for pre-release versions of Confluence 5.10 that you can use for testing purposes. This page will be updated as further EAP development releases are available.

**A quick note on upcoming versions:**

-   Confluence 6.0, with all its collaborative editing goodness, is still on the way.  We're releasing 5.10 first to make some great new features available.
-   You may find a few references to 6.0 in our javadoc - we'll get that sorted out very soon. 
-   The documentation theme, which has already been removed in Confluence Cloud, is still available in Confluence 5.10. It will be removed in Confluence 6.0 as originally announced.  

## !!! Marketplace compatibility 

We needed to make a change to our Confluence version details on Marketplace this morning due to an issue with the build number.  If you updated your add-on compatibility to 5.10.0 before 10am on 9 June AEST you'll need to update it again. We're sorry for the hassle! 

## AUI upgrade

Confluence is now running AUI 5.9.15.  Check out the <a href="https://bitbucket.org/atlassian/aui/src/d65128d66f0c1fac5749da3bdf9c446f1db0a96f/changelog.md?fileviewer=file-view-default" class="external-link">AUI change log</a> to find out what's changed. 

## Applinks upgrade

Confluence is now using Applications Links 5.2.  This version includes significant improvements that help administrators set up and troubleshoot application links. Read more about the changes in our <a href="https://confluence.atlassian.com/display/APPLINKS/Application+links+diagnostics" class="external-link">Application links</a> documentation. 

Note: Trusted application and Basic authentication are now deprecated. Existing application links will continue to work after upgrading to Confluence 5.10, but admins will not be able to create new links or change existing link to use Trusted application or Basic authentication.

## Quartz scheduler bean is now deprecated

Quartz scheduler bean and related `<job>` and `<trigger>` plugin modules are now deprecated. You should use `atlassian-scheduler schedulingService` bean or `<job-config>` plugin module instead.  See [Job Config Module](/server/confluence/job-config-module) for more info. 

## New audit log

Confluence's new audit log allows administrators to look back at changes to global settings, permissions, users, groups and more. Add-ons can send events to the log using either REST API or, if they are P2, by calling the Java API directly.

## API docs have moved

You can now find our API docs here:

-   Java API - <a href="https://docs.atlassian.com/atlassian-confluence/latest-server/" class="uri external-link">docs.atlassian.com/atlassian-confluence/latest-server/</a>
-   REST API for Confluence Server - <a href="https://docs.atlassian.com/atlassian-confluence/REST/latest-server/" class="uri external-link">docs.atlassian.com/atlassian-confluence/REST/latest-server/</a>
-   REST API for Confluence Cloud - <a href="https://docs.atlassian.com/atlassian-confluence/REST/latest/" class="uri external-link">docs.atlassian.com/atlassian-confluence/REST/latest/</a>

## Reminder to Require Dependencies

As we continue to move to a more modular design for our front end resources, it becomes even more important to make sure you are requiring all the dependencies that your plugin needs. Some resources that may have been included on the page by chance previously may no longer be included or might be out of order or included later, because some of them may be loaded asynchronously for the individual features or components that require them. This gives us a performance benefit as it gives a faster page load time and also avoids loading resources that are not always required.

What this means is that you should always explicitly define the dependencies required for your resources in your atlassian-plugin.xml, especially for AUI Components that are not included in Core. For example, on <a href="https://docs.atlassian.com/aui/latest/docs/date-picker.html" class="uri external-link">docs.atlassian.com/aui/latest/docs/date-picker.html</a>, you can see that this dependency needs to be explicitly required in your atlassian-plugin.xml file: &lt;dependency&gt;`com.atlassian.auiplugin:aui-date-picker`&lt;/dependency&gt;. The good news is that if you are already doing this, you shouldn't have anything to worry about.  

## Reminder: Deprecated javascript globals

In Confluence 5.9 we deprecated the use of all Confluence provided javascript globals. 

The globals listed below will be replaced with one or more individual AMD modules.

| window Global |
|---------------|
| Confluence    |
| AJS           |
| WRM           |

As of Confluence 6.0 these will no longer be available for use as javascript globals.
