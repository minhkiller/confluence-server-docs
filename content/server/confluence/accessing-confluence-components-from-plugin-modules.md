---
aliases:
- /server/confluence/accessing-confluence-components-from-plugin-modules-2031833.html
- /server/confluence/accessing-confluence-components-from-plugin-modules-2031833.md
category: devguide
confluence_id: 2031833
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031833
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031833
date: '2017-12-08'
guides: guides
legacy_title: Accessing Confluence Components from Plugin Modules
platform: server
product: confluence
subcategory: learning
title: Accessing Confluence Components from Plugin Modules
---
# Accessing Confluence Components from Plugin Modules

Confluence is built around <a href="http://projects.spring.io/spring-framework/" class="external-link">Spring</a>, an open-source component framework for Java.

If you are familiar with Spring, then you may only wish to know that Confluence plugin modules (and their implementing classes) are autowired by name. Thus, if you want to access a Confluence component from your plugin, just include the appropriate setter method in your implementing class.

If you want to write Confluence plugins but are unfamiliar with Spring, the rest of this page should give you more than enough information on how to have your plugin interact with Confluence.

### Interacting with Confluence

When you are writing anything but the simplest Confluence plugin, you will need to interact with the Confluence application itself in order to retrieve, change or store information. This document describes how this can be done.

#### Manager Objects

At the core of Confluence is a group of "Manager" objects. For example, the `pageManager` is in charge of Confluence pages, the `spaceManager` of spaces, the `attachmentManager` of attachments, and so on.

#### Dependency Injection

Traditionally, in a component-based system, components are retrieved from some kind of central repository. For example, in an EJB-based system, you would retrieve the bean from the application server's JNDI repository.

Confluence works the other way round. When a plugin module is instantiated, Confluence determines which components the module needs, and delivers them to it.

Confluence determines which components a module needs by reflecting on the module's methods. There are two different mechanisms that are used, based on whether you are using a v1 or v2 plugin.

###### Setter-based injection (v1 plugins)

With setter-based injection, any method with a signature that matches a standard JavaBeans-style setter of the same name as a Confluence component will have that component passed to it when the module is initialised.

So, if your plugin module needs to access the `pageManager`, all you need to do is put the following setter method on your module's implementing class:

``` java
public void setPageManager(PageManager pageManager)
{
    this.pageManager = pageManager;
}
```

###### Constructor-based injection (v2 plugins)

With constructor-based injection, the constructor which has the greatest number of arguments which can be satisfied by Confluence or plugin components will be called by Spring when constructing your module.

So, if your plugin module needs to access the `pageManager`, you need to have a constructor which takes a PageManager instance and assigns it to a field for later use:

``` java
public class MyModule {
    private final PageManager pageManager;

    public MyModule(PageManager pageManager) {
        this.pageManager = pageManager;
    }

    // ...
}
```

With constructor injection, you need to make sure there are no circular dependencies among your modules. If there are, you plugin will fail to start up and you will see an error message in the Confluence log file with information about the dependency problem.

### Manager Classes

There are several dozen managers for different areas of functionality in Confluence. The following table lists some of the more commonly used ones:

<table>
<colgroup>
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Manager class</p></th>
<th><p>Responsibility</p></th>
<th><p>Sample methods</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="http://docs.atlassian.com/atlassian-confluence/latest-server/com/atlassian/confluence/pages/PageManager.html" class="external-link">PageManager</a></p></td>
<td><p>Pages, blogs</p></td>
<td><p>getPage(), getBlogPost(), getRecentlyAddedPages(), findNextBlogPost(), saveContentEntity()</p></td>
</tr>
<tr class="even">
<td><p><a href="http://docs.atlassian.com/atlassian-confluence/latest-server/com/atlassian/confluence/spaces/SpaceManager.html" class="external-link">SpaceManager</a></p></td>
<td><p>Spaces</p></td>
<td><p>getSpace(), getPersonalSpace(), createSpace()</p></td>
</tr>
<tr class="odd">
<td><p><a href="http://docs.atlassian.com/atlassian-confluence/latest-server/com/atlassian/confluence/user/UserAccessor.html" class="external-link">UserAccessor</a></p></td>
<td><p>Users, groups, preferences</p></td>
<td><p>getUser(), addUser(), addMembership(), hasMembership(), getConfluenceUserPreferences(), getUserProfilePicture()</p></td>
</tr>
<tr class="even">
<td><p><a href="http://docs.atlassian.com/atlassian-confluence/latest-server/com/atlassian/confluence/pages/CommentManager.html" class="external-link">CommentManager</a></p></td>
<td><p>Comments</p></td>
<td><p>getComment(), updateCommentContent()</p></td>
</tr>
<tr class="odd">
<td><p><a href="http://docs.atlassian.com/atlassian-confluence/latest-server/com/atlassian/confluence/labels/LabelManager.html" class="external-link">LabelManager</a></p></td>
<td><p>Labels</p></td>
<td><p>addLabel(), removeLabel(), getCurrentContentForLabel()</p></td>
</tr>
<tr class="even">
<td><p><a href="http://docs.atlassian.com/atlassian-confluence/latest-server/com/atlassian/confluence/pages/AttachmentManager.html" class="external-link">AttachmentManager</a></p></td>
<td><p>Attachment storage and retrieval</p></td>
<td><p>getAttachments(Content), getAttachmentData(Attachment), saveAttachment()</p></td>
</tr>
<tr class="odd">
<td><p><a href="https://docs.atlassian.com/atlassian-confluence/2.8.0/com/atlassian/confluence/core/LuceneSmartListManager.html" class="external-link">SmartListManager</a></p></td>
<td><p>Searching (2.8 and earlier)</p></td>
<td><p>getListQueryResults()</p></td>
</tr>
<tr class="even">
<td><p><a href="http://docs.atlassian.com/atlassian-confluence/latest-server/com/atlassian/confluence/search/v2/SearchManager.html" class="external-link">SearchManager</a></p></td>
<td><p>Searching (2.9 and later)</p></td>
<td><p>search(), convertToEntities(), searchEntities()</p></td>
</tr>
<tr class="odd">
<td><p><a href="http://docs.atlassian.com/atlassian-confluence/latest-server/com/atlassian/confluence/core/ContentEntityManager.html" class="external-link">ContentEntityManager</a></p></td>
<td><p>Saving and retrieving all content. Parent interface of PageManager, CommentManager, etc.</p></td>
<td><p>saveContentEntity(), getVersionHistorySummaries()</p></td>
</tr>
<tr class="even">
<td><p><a href="http://docs.atlassian.com/atlassian-confluence/latest-server/com/atlassian/confluence/setup/settings/SettingsManager.html" class="external-link">SettingsManager</a></p></td>
<td><p>Global, space, plugin configuration</p></td>
<td><p>getGlobalSettings(), updateSpaceSettings(), getPluginSettings()</p></td>
</tr>
<tr class="odd">
<td><p><a href="http://docs.atlassian.com/atlassian-confluence/latest-server/com/atlassian/confluence/util/i18n/I18NBean.html" class="external-link">I18NBean</a></p></td>
<td><p>Getting localised text</p></td>
<td><p>getText(String), getText(String, Object[]), getText(String, List)</p></td>
</tr>
<tr class="even">
<td><p><a href="http://docs.atlassian.com/atlassian-confluence/latest-server/com/atlassian/confluence/security/PermissionManager.html" class="external-link">PermissionManager</a></p></td>
<td><p>Checking permissions (do this before calling a manager)</p></td>
<td><p>hasPermission(), hasCreatePermission(), isConfluenceAdministrator(), getPermittedEntities()</p></td>
</tr>
<tr class="odd">
<td><p><a href="http://docs.atlassian.com/atlassian-confluence/latest-server/com/atlassian/confluence/security/SpacePermissionManager.html" class="external-link">SpacePermissionManager</a></p></td>
<td><p>Adding or modifying space permissions</p></td>
<td><p>savePermission(), getGlobalPermissions(), getGroupsWithPermissions()</p></td>
</tr>
<tr class="even">
<td><p><a href="http://docs.atlassian.com/atlassian-event/0.6-SNAPSHOT/apidocs/com/atlassian/event/EventManager.html" class="external-link">EventManager</a></p></td>
<td><p>Register listeners or publish events</p></td>
<td><p>publishEvent(), registerListener()</p></td>
</tr>
<tr class="odd">
<td><p><a href="http://docs.atlassian.com/atlassian-confluence/latest-server/com/atlassian/confluence/plugin/descriptor/web/ConfluenceWebInterfaceManager.html" class="external-link">WebInterfaceManager</a></p></td>
<td><p>Rendering web-sections and web-items in Velocity</p></td>
<td><p>getDisplayableSections(), getDisplayableItems()</p></td>
</tr>
</tbody>
</table>

Note that these are all interfaces. The actual implementation will be injected in your class by Spring, if you include the appropriate setter method in your class as described above.

Do not directly use implementations or cast the injected class to a particular implementation. Implementation classes are subject to change across versions without warning. Where possible, interface methods will be marked as deprecated for two major versions before being removed.

### Service Classes

Managers in Confluence are responsible for the data integrity of their domain, but they are not generally responsible for validation or security. Invalid calls will typically result in a runtime exception. Historically, this wasn't a major problem, but as time went by there was more duplication of this functionality across actions, remote API methods and plugins. In recent releases, a service layer is being introduced in Confluence to address this.

The services will follow a command pattern, where the service is responsible for creating a command that can then be validated and executed. The following nascent services are available:

<table>
<colgroup>
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Service class</p></th>
<th><p>Responsibility</p></th>
<th><p>Sample commands</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="http://docs.atlassian.com/atlassian-confluence/latest-server/com/atlassian/confluence/content/service/CommentService.html" class="external-link">CommentService</a></p></td>
<td><p>Comments</p></td>
<td><p>CreateCommentCommand, DeleteCommentCommand, EditCommentCommand</p></td>
</tr>
<tr class="even">
<td><p><a href="http://docs.atlassian.com/atlassian-confluence/latest-server/com/atlassian/confluence/content/service/PageService.html" class="external-link">PageService</a></p></td>
<td><p>Pages, blog posts</p></td>
<td><p>MovePageCommand</p></td>
</tr>
<tr class="odd">
<td><p>SearchService (2.9+)</p></td>
<td><p>Performing searches</p></td>
<td><p> </p></td>
</tr>
</tbody>
</table>

These simpler services don't follow the command pattern, nor do they perform any data modification. They are generally used to simplify other functionality:

<table>
<colgroup>
<col style="width: 25%" />
<col style="width: 75%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Service class</p></th>
<th><p>Responsibility</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="http://docs.atlassian.com/atlassian-confluence/latest-server/com/atlassian/confluence/util/http/HttpRetrievalService.html" class="external-link">HttpRetrievalService</a></p></td>
<td><p>Http Connectivity to External Services</p></td>
</tr>
</tbody>
</table>

### More information

-   [Spring IoC in Confluence](/server/confluence/spring-ioc-in-confluence), a longer guide to Spring in Confluence.
-   Confluence [Java API Reference](/server/confluence/java-api-reference) which include the bean interfaces and classes.
-   [Confluence Developer FAQ](/server/confluence/confluence-developer-faq)
