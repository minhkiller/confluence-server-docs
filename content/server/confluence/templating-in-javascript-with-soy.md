---
aliases:
- /server/confluence/templating-in-javascript-with-soy-2031762.html
- /server/confluence/templating-in-javascript-with-soy-2031762.md
category: devguide
confluence_id: 2031762
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031762
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031762
date: '2017-12-08'
guides: guides
legacy_title: Templating in JavaScript with Soy
platform: server
product: confluence
subcategory: learning
title: Templating in JavaScript with Soy
---
# Templating in JavaScript with Soy

{{% note %}}

This is only available in Confluence 4.0+.

{{% /note %}}

This page describes how you can write a Soy template in your plugin to use in JavaScript. This can be useful when generating a dialog or some piece of content to display on the page.

## References

-   <a href="http://code.google.com/closure/templates/docs/commands.html#template" class="external-link">Soy Commands</a>
-   <a href="http://code.google.com/closure/templates/docs/concepts.html" class="external-link">Soy Concepts</a>

## Quick Guide

### 1. Create a soy file

Under the plugins resource directory, create a soy file. Each soy file needs to have a namespace declaration at the top of the file. It must be declared before any templates are declared. You can think of the namespace as a javascript namespace equivalent. You can put it under your plugin's namespace or use the existing `Confluence.Templates`.

### 2. Write your template

Next comes your actual template declaration, which is simply declared with {template .templateName}. If your template needs parameters, you must declare them in the 'javadoc' part before your template. You can then use the param in the template with a {$paramName} syntax. Here is a simple example soy file:

**example.soy**

``` java
{namespace Confluence.Templates.Example}
/**
 * Renders a Hello message.
 * @param name the name of the user
 */
{template .helloWorld}
<div>Hello {$name}!</div>
{/template}
```

### 3. Add your template to a web-resource

Since a soy file gets transformed in a js file, we need to declare it with all the other js files in a &lt;web-resource&gt; element of a plugin xml. You will also need to copy the soy transformer config into the `<web-resource>` element. Here is an example:

``` xml
<web-resource key="example-resources" name="Example Resources">
...
<transformation extension="soy">
<transformer key="soyTransformer"/>
</transformation>        ...
<resource type="download" name="example-soy.js" location="/soy/example.soy"/>
....
</web-resource>
```

### 4. Use the template in your javascript

You can now invoke the template by simply calling a javascript function with your previously declared namespace. In this example it would be something like:

``` javascript
// display a hello message on the page 
var template = Confluence.Templates.Example.helloWorld({name: "John Smith"});
AJS.messages.info(jQuery("body"), {body: template, closeable: true}); 
```

## i18n in your templates

You will often need to use i18n in your templates. To do so, simply use the {getText('key')} syntax.

{{% note %}}

GOTCHA: You must use single quotes for string literals in soy. Double quotes are for html attribute values.

{{% /note %}}

Here is an example template using i18n.

``` java
/**
 * A template for a dialog help link
 * @param href key of the doc link url
 */
{template .helpLink}
<div class="dialog-help-link">
<a href="{$href}" target="_blank">{getText('help.name')}</a>
</div>
{/template}
```

## Calling another template from a template

You may want to reuse an existing template from another template. To do this you can simply use the call command to invoke another template. You can optionally pass in all the params from one template to another or configure individual parameters that get passed through. Here is an example with a single parameter being passed through.

``` java
/**
 * Move page dialog help link
 */
{template .helpLink}
{call Confluence.Templates.Dialog.helpLink}
{param href: docLink('help.moving.page') /}
{/call}
{/template}
```
