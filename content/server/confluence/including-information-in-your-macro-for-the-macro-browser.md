---
aliases:
- /server/confluence/including-information-in-your-macro-for-the-macro-browser-2031776.html
- /server/confluence/including-information-in-your-macro-for-the-macro-browser-2031776.md
category: reference
confluence_id: 2031776
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031776
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031776
date: '2017-12-08'
legacy_title: Including Information in your Macro for the Macro Browser
platform: server
product: confluence
subcategory: modules
title: Including Information in your Macro for the Macro Browser
---
# Including Information in your Macro for the Macro Browser

The Macro Browser helps users to browse and insert macros while adding or editing content. If you are a plugin author, you need to include metadata in your macro so that works correctly and makes use of the new Macro Browser framework.

NOTE: As of Confluence 4.0, all macros must contain metadata in order to function correctly in Confluence.

## Including Macro Browser Information in your Macro

You need to include information in your macro so that it appears and behaves correctly in the macro browser and displays the correct parameter input fields. This will require simple additions to your macro definition in the `atlassian-plugin.xml` file.

### Macro Descriptor Attributes

The following macro attributes contain information specifically for the macro browser.

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Name</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>documentation-url</p></td>
<td><p>The absolute URL pointing to the macro's documentation.</p></td>
</tr>
<tr class="even">
<td><p>icon</p></td>
<td><p>The relative URL to the application for the macro icon. To display well in the macro browser, the image should be 80 pixels by 80 pixels, with no transparency.<br />
Note: if you have your icon defined as a <a href="/server/confluence/adding-plugin-and-module-resources">downloadable resource</a>, you can refer to this by specifying &quot;<code>/download/resources/PLUGIN_KEY/RESOURCE_NAME</code>&quot; as the icon attribute.</p></td>
</tr>
<tr class="odd">
<td><p>hide-body</p></td>
<td><p>This attribute is available for macros that falsely declare that they have body (most likely cause they extend BaseMacro) when they don't.<br />
For example the Gallery macro. This attribute helps hide the body text field in the macro browser. </p>
<p><strong>Default:</strong> false.</p></td>
</tr>
<tr class="even">
<td><p>hidden</p></td>
<td><p>If set to true, the macro is not visible in the macro browser for selection. Plugin authors may want to hide macros that are for their plugin's internal use and shouldn't really be used by users.</p>
<p><strong>Default:</strong> false.</p></td>
</tr>
</tbody>
</table>

### Macro Elements

The following macro elements contain information specifically for the macro browser. They should be placed inside your `<macro>` element.

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Name*</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>category</p></td>
<td><p>The category the macro should appear in. Valid categories are listed <a href="#macro-categories">below</a>.</p></td>
</tr>
<tr class="even">
<td><p>alias</p></td>
<td><p>Defines an alias for the macro. This means that the macro browser will open for the defined aliases as if it were this macro.<br />
For example if <code>dynamictasklist</code> is an alias of <code>tasklist</code>, editing an existing <code>dynamictasklist</code> macro will open it as a <code>tasklist</code> macro.</p></td>
</tr>
<tr class="odd">
<td><p>parameters</p></td>
<td><p>Defines a group of parameter elements. See example below.</p></td>
</tr>
<tr class="even">
<td><p>parameter</p></td>
<td><p>This defines a single macro parameter. It must be an element of the parameters element. Its contents are described <a href="#parameter-options">below</a>.</p></td>
</tr>
</tbody>
</table>

**\*parameters is required.**

#### Macro Categories

The following categories for macros have been defined (see MacroCategory.java). A macro with no category will show up in the default 'All' category.

-   `formatting`
-   `confluence-content`
-   `media`
-   `visuals`
-   `navigation`
-   `external-content`
-   `communication`
-   `reporting`
-   `admin`
-   `development`

#### Parameter Options

Each `<parameter>` element **must** have the following attributes:

-   **name** - A unique name of the parameter, or "" for the default (unnamed) parameter.
-   **type**- The type of parameter. Currently the following parameter types are supported in the macro browser's UI:
    -   `boolean` - displays a check box.
    -   `enum` - displays a select field.
    -   `string` - displays an input field (this is the default if unknown type).
    -   `spacekey` - displays an autocomplete field for search on space names.
    -   `attachment` - displays an autocomplete field for search on attachment filenames.
    -   `username` - displays an autocomplete field for search on username and full name.
    -   `confluence-content` - displays an autocomplete field for search on page and blog titles.

These are optional:

-   **required** - whether it is a required parameter, defaults to 'false'.
-   **multiple** - whether it takes multiple values, defaults to 'false'.
-   **default** - the default value for the parameter.

It can also have the following optional child elements:

-   `<alias name="xxx"/>` - alias for the macro parameter.
-   `<value name="xxx"/>` - describes a single 'enum' value - only applicable for enum typed parameters.

##### Hidden Parameter

It is possible to define a hidden parameter, to send information along with your form, invisible for users to see. Take a look at the examples below how to define this.

Within the `atlassian-plugin.xml `define a `string` parameter.

``` xml
<macro name="example-formatting" key="example-formatting" class="some.path.ExampleFormatting.java"
       icon="/images/icons/macrobrowser/example-formatting.png">
    <category name="formatting"/>
    <parameters>
        <parameter name="willbehidden" type="string"/>
    </parameters>
</macro>

<web-resource key="hidden-field-parameter" name="Add a hidden field">
    <resource type="download" name="hidden-parameter-field.js" location="js/hidden-parameter-field.js" />
    <dependency>confluence.editor.actions:editor-macro-browser</dependency>
    <context>macro-browser</context>
</web-resource>
```

Inside the `js/hidden-parameter-field.js`

``` javascript
AJS.MacroBrowser.getMacroJsOverride("example-formatting").fields.string = {
  "willbehidden": function(param) {
    var parameterField = AJS.MacroBrowser.ParameterFields["_hidden"] (param, {});
    if (!parameterField.getValue()) {
      parameterField.setValue('hidden field value');
    }
    return parameterField;
  }
};
```

{{% note %}}

Autocompletion on the parameter types may show up that `hidden` is also a supported type. Keep in mind that choosing this type will not make the field hidden.

{{% /note %}}

#### Example

The following is an example of the Recently Updated Macro defined:

``` xml
<macro name="recently-updated" key="recently-updated" icon="/images/icons/macrobrowser/recently-updated.png"
documentation-url="http://confluence.atlassian.com/display/DOC/Recently+Updated+Macro">
  <category name="confluence-content"/>
  <parameters>
    <parameter name="spaces" type="spacekey" multiple="true">
      <alias name="space"/>
    </parameter>
    <parameter name="labels" type="string">
      <alias name="label"/>
    </parameter>
    <parameter name="width" type="percentage" default="100%"/>
    <parameter name="types" type="string">
      <alias name="type"/>
    </parameter>
    <parameter name="max" type="int" default="100">
      <alias name="maxResults"/>
    </parameter>
    <parameter name="sort" type="enum">
      <value name="title"/>
      <value name="creation"/>
      <value name="modified"/>
    </parameter>
    <parameter name="reverse" type="boolean" default="false"/>
  </parameters>
</macro>
```

WARNING: *Note that this example contains parameter types which aren't all supported in the macro browser UI, but may be in future releases*. 

#### Macro Icon Example

To provide an icon for your macro -  
1) Create a resource for `icons/images` if you don't already have one. e.g.

``` xml
<resource key="icons" name="icons/" type="download" location="myplugin/images/icons"/>
```

This must be a top level resource in your atlassian-plugin.xml and must be defined before the macro.

2) Ensure your plugin should contain the resource directory `myplugin/images/icons`

3) Set the icon attribute on the macro e.g.

``` xml
icon="/download/resources/pluginkey/icons/iconfile.png"
```

### i18n Conventions

Instead of having to define i18n keys for each element in the macro definition, the following convention is used to lookup i18n keys for the macro browser.

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Key</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>{pluginKey}.{macroName}.label</p></td>
<td><p>Macro label/display name</p></td>
</tr>
<tr class="even">
<td><p>{pluginKey}.{macroName}.desc</p></td>
<td><p>Macro description</p></td>
</tr>
<tr class="odd">
<td><p>{pluginKey}.{macroName}.param.{paramName}.label</p></td>
<td><p>Macro parameter label</p></td>
</tr>
<tr class="even">
<td><p>{pluginKey}.{macroName}.param.{paramName}.desc</p></td>
<td><p>Macro parameter description</p></td>
</tr>
<tr class="odd">
<td><p>{pluginKey}.{macroName}.body.label</p></td>
<td><p>Macro body label (defaults to 'Body Text' if not provided)</p></td>
</tr>
<tr class="even">
<td><p>{pluginKey}.{macroName}.body.desc</p></td>
<td><p>Macro body description</p></td>
</tr>
</tbody>
</table>

You will need to place the keys in a .properties file with a [resource of type i18n](/server/confluence/adding-plugin-and-module-resources) in your plugin.
