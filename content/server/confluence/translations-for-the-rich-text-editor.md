---
aliases:
- /server/confluence/translations-for-the-rich-text-editor-2031662.html
- /server/confluence/translations-for-the-rich-text-editor-2031662.md
category: reference
confluence_id: 2031662
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031662
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031662
date: '2017-12-08'
legacy_title: Translations for the Rich Text Editor
platform: server
product: confluence
subcategory: modules
title: Translations for the Rich Text Editor
---
# Translations for the Rich Text Editor

The Rich Text Editor provided by Confluence is <a href="http://tinymce.moxiecode.com/" class="external-link">TinyMCE</a>. In Confluence version 2.2.10 and above it is possible to provide translations for the tooltips and labels in the Rich Text Editor.

Most of the editor's internationalised text consists of its tooltips. There are also a few labels such as those in the Image Properties dialog. If you are using Confluence in a language other than English, you will want to translate these messages as well as the standard Confluence text.

Confluence fully supports internationalisation of the rich text editor:

-   The translations for the rich text editor can be part of a Confluence language pack plugin. The TinyMCE properties can be included in the ConfluenceActionSupport properties file, along with the standard Confluence properties.
-   If your language pack does not contain translations for the rich text editor, the text will show in English.

{{% note %}}

In Confluence versions 2.5.4 and earlier, Rich Text Editor translations can not be installed as a language pack. Refer to <a href="#earlier-documentation" class="unresolved">earlier documentation</a> for a workaround.

{{% /note %}}

### Creating a new translation

The core editing strings for the Rich Text Editor translations are found in the <a href="https://svn.atlassian.com/svn/public/contrib/confluence/atlassian-tinymce-plugin/trunk/src/main/resources/com/atlassian/confluence/extra/tinymceplugin/tinymce.properties" class="external-link">tinymce.properties file</a>.

Add a new i18n plugin resource to `atlassian-plugin.xml` like this:

``` xml
<resource name="i18n" type="i18n" location="com/atlassian/confluence/tinymceplugin/tinymce"/>
```

Now, put your translations (as described below) in `tinymce_locale.properties` (where locale is the target locale - e.g. de\_DE) under the directory `src/main/resources/com/atlassian/confluence/tinymceplugin`.

### Example

Below is a partial listing of the core TinyMCE properties. The properties consist of 'key=value' pairs. To translate from English to another language, you would replace the text to the right of the '=' sign with the translation.

``` java
# English

## TinyMCE core
tinymce.bold_desc=Bold (Ctrl+B)
tinymce.italic_desc=Italic (Ctrl+I)
tinymce.underline_desc=Underline (Ctrl+U)
tinymce.striketrough_desc=Strikethrough
.
.
.
## paste plugin
tinymce.paste.paste_text_desc=Paste as Plain Text
tinymce.paste.paste_text_title=Use CTRL+V on your keyboard to paste the text into the window.
tinymce.paste.paste_text_linebreaks=Keep linebreaks
.
.
.
```
