---
aliases:
- /server/confluence/cql-keywords-reference-29951944.html
- /server/confluence/cql-keywords-reference-29951944.md
category: reference
confluence_id: 29951944
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=29951944
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=29951944
date: '2017-12-08'
legacy_title: CQL Keywords Reference
platform: server
product: confluence
subcategory: api
title: CQL keywords reference
---
# CQL keywords reference

## A keyword in CQL is a word or phrase that

-   joins two or more clauses together to form a complex CQL query, or
-   alters the logic of one or more clauses, or
-   alters the logic of [operators](/server/confluence/advanced-searching-using-cql#operators), or
-   has an explicit definition in a CQL query, or
-   performs a specific function that alters the results of a CQL query.

**List of Keywords:**

#### AND

Used to combine multiple clauses, allowing you to refine your search.

Note: you can use [parentheses](/server/confluence/advanced-searching-using-cql#parentheses) to control the order in which clauses are executed.

###### Examples

-   Find all blogposts with the label "performance"

    ``` sql
    label = "performance" and type = "blogpost"
    ```

-   Find all pages created by jsmith in the DEV space

    ``` sql
    type = page and creator = jsmith and space = DEV
    ```

-   Find all content that mentions jsmith but was not created by jsmith

    ``` sql
    mention = jsmith and creator != jsmith
    ```

#### OR

Used to combine multiple clauses, allowing you to expand your search.

Note: you can use [parentheses](/server/confluence/advanced-searching-using-cql#parentheses) to control the order in which clauses are executed.

(Note: also see [IN](/server/confluence/cql-operators-reference#i-n), which can be a more convenient way to search for multiple values of a field.)

###### Examples

-   Find all content in the IDEAS space or with the label idea

    ``` sql
    space = IDEAS or label = idea
    ```

-   Find all content last modified before the start of the year or with the label needs\_review

    ``` sql
    lastModified < startOfYear() or label = needs_review
    ```

#### NOT

Used to negate individual clauses or a complex CQL query (a query made up of more than one clause) using [parentheses](/server/confluence/advanced-searching-using-cql#parentheses), allowing you to refine your search.

(Note: also see [NOT EQUALS](/server/confluence/cql-operators-reference#e-q-u-a-l-s) ("!="), [DOES NOT CONTAIN](#does-not-contain) ("!~") and [NOT IN](/server/confluence/cql-operators-reference#i-n).)

###### Examples

-   Find all pages with the "cql" label that aren't in the dev space

    ``` sql
    label = cql and not space = dev 
    ```

#### ORDER BY

Used to specify the fields by whose values the search results will be sorted.

By default, the field's own sorting order will be used. You can override this by specifying ascending order ("`asc`") or descending order ("`desc`").

Not all fields support Ordering. Generally, ordering is not supported where a piece of content can have multiple values for a field, for instance ordering is not supported on labels.

###### Examples

-   Find content in the DEV space ordered by creation date

    ``` sql
    space = DEV order by created
    ```

-   Find content in the DEV space ordered by creation date with the newest first, then title

    ``` sql
    space = DEV order by created desc, title
    ```

-   Find pages created by jsmith ordered by space, then title

    ``` sql
    creator = jsmith order by space, title asc
    ```
