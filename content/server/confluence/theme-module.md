---
aliases:
- /server/confluence/theme-module-2031725.html
- /server/confluence/theme-module-2031725.md
category: reference
confluence_id: 2031725
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031725
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031725
date: '2017-12-08'
legacy_title: Theme Module
platform: server
product: confluence
subcategory: modules
title: Theme module
---
# Theme module

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Available:</p></td>
<td><p>Confluence 1.3 and later</p></td>
</tr>
</tbody>
</table>

Themes define a look and feel for Confluence. Confluence ships with several themes that you can use, such as the default theme and or the left-nav theme. Theme plugins, on the other hand, allow you to create your totally customized look and feel. A theme can be applied to an entire Confluence site or to individual spaces.

### Stylesheet themes

Creating a theme which only relies on stylesheets and images is much simpler than customising HTML, and more likely to work in future versions of Confluence.

-   [Creating a Stylesheet Theme](/server/confluence/creating-a-stylesheet-theme)

### Custom HTML themes

Creating a new theme with custom HTML consists of two steps:

1.  [Creating a theme](/server/confluence/creating-a-theme) with decorators and colour schemes, which defines how each page looks.
2.  [Packaging and Installing a Theme Plugin](/server/confluence/packaging-and-installing-a-theme-plugin) - themes are part of our [plugin](/server/confluence/confluence-plugin-guide) system.

### Installing your theme

To install it within Confluence, please read <a href="#installing-a-plugin" class="unresolved">Installing a Plugin</a>.

### Example themes

There are several other themes that you can use as examples to learn from and extend.

Stylesheet themes:

-   [Easy Blue Theme](/server/confluence/creating-a-stylesheet-theme)

Custom HTML themes:

-   <a href="#clickr-theme" class="unresolved">Clickr Theme</a>
-   <a href="#comment-tab-theme" class="unresolved">Comment Tab Theme</a>
-   [DOC:Left-nav Theme](https://dac-lf.prod.atl-paas.net/server/confluence/attachments/2031725/2228310.jar)

We've also prodvided a [DOC:Confluence Space export](https://dac-lf.prod.atl-paas.net/server/confluence/attachments/2031725/2228558.zip) for theme developers. You can import this space into your development Confluence, and check each page to make sure all of the Confluence content looks good in your new theme.

You may also want to read <a href="#including-cascading-stylesheets-in-themes" class="unresolved">Including Cascading Stylesheets in Themes</a>.

