---
aliases:
- /server/confluence/adding-confluence-keyboard-shortcuts-13631990.html
- /server/confluence/adding-confluence-keyboard-shortcuts-13631990.md
category: devguide
confluence_id: 13631990
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=13631990
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=13631990
date: '2017-12-08'
legacy_title: Adding Confluence Keyboard Shortcuts
platform: server
product: confluence
subcategory: learning
title: Adding Confluence keyboard shortcuts
---
# Adding Confluence keyboard shortcuts

{{% tip %}}

Level of experience: Beginner

This tutorial's level of difficulty is at 'beginner' level, so you can follow it even if you have never developed a plugin before. Our tutorials are classified as 'beginner', 'intermediate' and 'advanced'.

{{% /tip %}}

## Overview

This tutorial shows you how to use a [Keyboard Shortcut Module](https://developer.atlassian.com/display/CONFDEV/Keyboard+Shortcut+Module) in your plugin to add keyboard shortcuts to different parts of the Confluence UI and navigation between pages. To do this, you will create a very simple Confluence plugin. Your plugin consists of the following components:

-   Plugin descriptor - to enable the plugin module in Confluence.
-   An internationalisation properties file - for multi-language support.
-   A JavaScript resource - to work around a current bug in the keyboard shortcuts module.

All these components will be contained within a single JAR file. Each component is further discussed in the examples below.

This plugin is designed to show two ways of adding new keyboard shortcuts to Confluence:

1.  Navigates to a different page.
2.  Scrolls the window to an element and clicks that element.

## Source code

The source code of the plugin used in this tutorial is available in Bitbucket. You can browse the source code here: <a href="https://bitbucket.org/appfusions/confluence-tutorial-keyboard-shortcuts" class="external-link">confluence-tutorial-keyboard-shortcuts</a>.

## Step 1. Create the plugin project

On the command line type: `atlas-create-confluence-plugin`

You'll be prompted to enter two values, the **group ID** and **artifact ID**. For this tutorial, we used the following values:

-   Group ID: **com.appfusions.confluence.plugins**
-   Artifact ID: **confluence-tutorial-keyboard-shortcuts**

This will create a basic plugin with some Java code and tests. In the <a href="https://bitbucket.org/appfusions/confluence-tutorial-keyboard-shortcuts" class="external-link">sample code</a>, all Java code including tests have been removed as they are not needed for the plugin. 

## Step 2. Edit the atlassian-plugin.xml file

In this step, you must now register the plugin module in your plugin descriptor, `atlassian-plugin.xml`. In other words, add the following code to your `atlassian-plugin.xml` file between the `<atlassian-plugin>` tags, but below the `<plugin-info>` tag group.

``` xml
  <keyboard-shortcut key="goto.four.oh.four" i18n-name-key="keyboard.shortcut.four.oh.four" name="Page not found">
    <order>200</order>
    <description key="keyboard.shortcut.four.oh.four.desc">Page not found</description>
    <shortcut>404</shortcut>
    <operation type="goTo">/four-oh-four</operation>
    <context>global</context>
  </keyboard-shortcut>
```

Let's break down that XML code. In this example we are creating a keyboard shortcut which when triggered navigates to, for example, http://localhost:1990/confluence/four-oh-four in the development system - a page which does not exist. Admittedly, this is likely to be of limited practical use - unless you are a lover of 404 pages - but it demonstrates the potential of keyboard shortcuts.

To do this, we've done a number of things.

### The Keyboard Shortcut module

In the code above, this line involves 3 attributes:

``` xml
<keyboard-shortcut key="goto.four.oh.four" i18n-name-key="keyboard.shortcut.four.oh.four" name="Page not found">
```

-   The first, **key="goto.four.oh.four"** sets an internal name for the new item.
-   The second, **i18n-name-key="keyboard.shortcut.four.oh.four" ** is the internationalisation key for the item - this is displayed in the admin section as the name of the module in the plugin.
-   The third, **name="Page not found"** defines the default name of the item.

### Controlling where the keyboard shortcut is displayed

Examine these line of code:

``` xml
<order>200</order>
<description key="keyboard.shortcut.four.oh.four.desc">Page not found</description>
```

In Confluence, if you go to the **Browse** menu and select** Keyboard Shortcuts** you will see a dialog box with lists of keyboard shortcuts:

<img src="/server/confluence/images/image2012-8-23-21:37:36.png" width="650" />

Each item has a a value for **order** between 10 and 100. A smaller value means the label for your keyboard shortcut appears higher in the list. A value of 200 means that the item will almost certainly be placed at the bottom of the list.

The description contains an attribute **key**. This is the internationalisation key contained in a properties file (explained later in the tutorial). The value of this element defines the label for the keyboard shortcut in this dialog box.

### Controlling how the keyboard shortcut is triggered

This line defines how the keyboard shortcut is triggered:  `<shortcut>404</shortcut>`

This example is triggered by the keys in this order: "4" then a "0" then a "4".

### Defining what the keyboard shortcut does

This line defines what the keyboard shortcut does:  `<operation type="goTo">/four-oh-four</operation>`

In this case, the `window.location` is changed to a path below the server base URL. So for a Confluence installation at http://locahost:1990/confluence, this will correspond to http://localhost:1990/confluence/four-oh-four

### Controlling when the keyboard shortcut can be triggered

In this line we allow the keyboard shortcuts to be triggered globally within Confluence: `<context>global</context>`

The context tag also accepts the page or content values for other situations.

### Adding new resource files and internationalisation

We will want to specify a text label to display in our Confluence keyboard shortcuts. You could just hard-code this information into your `atlassian-plugin.xml` file, but by adding it in a new resource file, we can make our plugin compatible with internationalisation. To do so, simply add a new file in the resources directory of your plugin (called `confluence-tutorial-keyboard-shortcuts.properties`) and enter a couple of lines of code into it:

``` xml
keyboard.shortcut.four.oh.four = Page Not Found
keyboard.shortcut.four.oh.four.desc = Display 404 page:
```

The first line - `keyboard.shortcut.four.oh.four` - describes the module name in **Confluence Admin \| Plugins:**

<img src="/server/confluence/images/image2012-8-23-21:39:8.png" width="650" />

**Note:** Displaying friendly names rather than module keys can be useful when there are many modules in a Confluence plugin.

The second line - `keyboard.shortcut.four.oh.four.desc` -  is the label for the keyboard shortcut in the Keyboard Shortcut dialog displayed earlier.

Additionally, we will need to reference this resource file (`confluence-tutorial-keyboard-shortcuts.properties`) in our `atlassian-plugin.xml` file. To do this, add this line of code above the `<keyboard-shortcut>` code block:

``` xml
<resource type="i18n" name="i18n" location="confluence-tutorial-keyboard-shortcuts"  />
```

This will access your `confluence-tutorial-keyboard-shortcuts.properties` file and retrieve the text for our button label. 

If you would like to know more about internationalisation, see our <a href="http://confluence.atlassian.com/display/DISC/i18n+in+Confluence+Plugin+Development" class="external-link">Confluence documentation on the topic</a>.

## Step 3. Build, install and run the plugin

### Build and install

The **atlas-run** command will compile the plugin project and then launch a local instance of Confluence.

### Run

Once Confluence has loaded, access the local instance with this URL:

``` xml
http://localhost:1990/confluence/
```

1.  Log in with username "admin" and password "admin.
2.  Type "4" then "0" then "4" - your browser should now display the following:  
      
    <img src="/server/confluence/images/image2012-8-23-21-39-42.png" height="250" />

-   Go back to the **Dashboard** and select **Keyboard Shortcuts** from the **Browse** menu (or just type "**?**").  You'll see your new keyboard shortcut has been added.    
      
    <img src="/server/confluence/images/image2012-8-23-21:40:45.png" width="650" />

But wait, the internationalisation has strangely failed. The `keyboard.shortcut.four.oh.four.desc` label displays the key rather than the value it should contain.

You guessed it, there's a little bug - <a href="https://jira.atlassian.com/browse/CONF-24450" class="external-link">CONF-24450</a> -  in keyboard shortcuts.  Don't worry though, we can quickly work around this issue...

## Step 4. Add the workaround

Create a new JavaScript web resource file at `src/main/resources/CONF-24450-workaround.js` and add the code:

``` java
AJS.toInit(function($) {
    AJS.I18n.get("com.appfusions.confluence.plugins.confluence-tutorial-keyboard-shortcuts");
});
```

Additionally, you will need to reference this web resource file in our `atlassian-plugin.xml` file. To do this, add this line of code above the `<keyboard-shortcut>` code block:

``` xml
 <web-resource key="keyboard.shortcut.CONF-24450-workaround" name="Keyboard Shortcuts" i18n-name-key="keyboard.shortcut.CONF-24450-workaround">
    <resource name="CONF-24450-workaround.js" type="download" location="js/CONF-24450-workaround.js"/>
    <context>main</context>
    <context>admin</context>
  </web-resource>
```

The `AJS.I18n.get(...)` line adds the keys in the internationalisation file into the `AJS.I18n.keys` object in JavaScript.

**Note of interest:** This also makes for a cunning alternative to the [Web Resource Transformer Module](https://developer.atlassian.com/display/CONFDEV/Web+Resource+Transformer+Module) for adding internationalisation in JavaScript. A further tutorial plugin for this is available in Bitbucket here: <a href="https://bitbucket.org/dvdsmpsn/confluence-tutorial-awesome-plugin" class="external-link">confluence-tutorial-awesome-plugin</a>.

## Step 5. Redeploy the plugin

At the command prompt, type `atlas-package ; atlas-install-plugin` This will repackage the plugin and install it on your Confluence. Alternatively, you may simply rebuild the plugin, and [QuickReload](https://developer.atlassian.com/docs/developer-tools/automatic-plugin-reinstallation-with-quickreload)  will automatically reload it for you.

The keyboard shortcut should now be displayed correctly. Type "**?**" in your browser window to check.  You should see this:

<img src="/server/confluence/images/image2012-8-23-21:41:38.png" width="650" />

The "Display 404 page" label is now displayed correctly.

## Step 6. For extra points...

Add a new keyboard shortcut that triggers the **Browse Menu** to be toggled open or closed when you type **CTRL+B**:

<img src="/server/confluence/images/image2012-8-23-21:43:4.png" width="340" />

In your `atlassian-plugin.xml` add a new **keyboard-shortcut** code block as follows:

``` xml
  <keyboard-shortcut key="moveToAndClick.toggle.browse" i18n-name-key="keyboard.shortcut.toggle.browse" name="Toggle Browse Menu">
    <order>200</order>
    <description key="keyboard.shortcut.toggle.browse.desc">Toggle Browse Menu</description>
    <shortcut>[Ctrl+B]</shortcut>
    <operation type="moveToAndClick">#browse-menu-link</operation> 
    <context>global</context>
  </keyboard-shortcut>
```

This is very similar except for two key points:

-   The shortcut now triggers when two keys are pressed simultaneously 
-   The operation type is now **moveToAndClick** - which should be fairly self explanatory.  
    -   This moves the web browser window so that the Browse menu is visible
    -   The value `#browse-menu-link` is a CSS selector for the id attribute on the **Browse** menu anchor element

### Redeploy and run the plugin

At the command prompt, type `atlas-package ; atlas-install-plugin`

In a web browser window, go to your local Confluence installation and type **CTRL+B** several times - you should see the **Browse** dropdown opening and closing. This example shows how you can quickly add your own productivity- or accessibility-enhancing keyboard shortcuts to expand upon the default keyboard shortcuts.

{{% tip %}}

Congratulations, you have completed this tutorial.

{{% /tip %}}
