---
aliases:
- /server/confluence/2031792.html
- /server/confluence/2031792.md
category: devguide
confluence_id: 2031792
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031792
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031792
date: '2017-12-08'
legacy_title: How do I associate my own properties with a ContentEntityObject?
platform: server
product: confluence
subcategory: faq
title: How do I associate my own properties with a ContentEntityObject?
---
# How do I associate my own properties with a ContentEntityObject?

### <img src="http://confluence.atlassian.com/images/icons/favicon.png" class="confluence-external-resource" /> How do I associate my own properties with a ContentEntityObject?

You will need the <a href="http://www.atlassian.com/software/confluence/docs/api/latest/com/atlassian/confluence/core/ContentEntityManager.html" class="external-link">ContentEntityManager</a> (see [DOC: how to retrieve it](#doc-how-to-retrieve-it)). This manager allows you to store and retrieve arbitrary String values associated with a ContentEntityObject.

Properties are stored as simple key/value pairs. We recommend that anyone writing a third-party plugin use the standard Java "reverse domain name" syntax to ensure their keys are unique. Keys may be no longer than 200 characters.

``` java
// Set the property
contentPropertyManager.setText(page, "com.example.myProperty", "This is the value")
// Retrieve it
String myProperty = contentPropertyManager.getText(page, "com.example.myProperty")
```

`getText` and `setText` can store strings of arbitrary length (up to the size-limit for CLOBs in your database). There is also a `getString` and `setString` which is slightly more efficient, but limited to 255 characters per value.
