---
aliases:
- /server/confluence/creating-a-theme-2031656.html
- /server/confluence/creating-a-theme-2031656.md
category: reference
confluence_id: 2031656
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031656
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031656
date: '2017-12-08'
legacy_title: Creating a Theme
platform: server
product: confluence
subcategory: modules
title: Creating a Theme
---
# Creating a Theme

## Using Decorators

A decorator defines Confluence page layout. By modifying a decorator file, you can move "Attachments' tab from the left of the screen to the right or remove it completely. Decorator files are written in the Velocity templating language and have the VMD extension. You can familiarise yourself with Velocity at the [Velocity Template Overview](/server/confluence/velocity-template-overview) and decorators in general at the <a href="http://www.opensymphony.com/sitemesh" class="external-link">Sitemesh homepage</a>.

### Decorators, Contexts and Modes

Confluence comes bundled with a set of decorator files that you can customize. Instead of having one decorator file for each screen, we've grouped together similar screens (example: view and edit page screens) to simplfy editing layouts.

There is some terminology that we use when talking about decorators that should be defined. We've grouped all the screens in Confluence into major categories which we call *contexts*. Within each context are various *modes* (ways of viewing that particular layout).

The following table summarises how decorators use contexts and modes:

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Decorator</p></th>
<th><p>Mode</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>main.vmd</p></td>
<td><p><strong>Context:</strong> n/a (header and footer formatting).</p>
<p><strong>Comment:</strong> main.vmd is used to control the header and footer of each page, not the page specific presentation logic.</p></td>
</tr>
<tr class="even">
<td><p>page.vmd</p></td>
<td><p>'view', 'edit', 'edit-preview', 'view-information', and 'view-attachments'</p>
<p><strong>Context:</strong> page.</p></td>
</tr>
<tr class="odd">
<td><p>blogpost.vmd</p></td>
<td><p>'view', 'edit', 'edit-preview', and 'remove'</p>
<p><strong>Context:</strong> blogpost (news).</p>
<p><strong>Comment:</strong> We prefer to use 'news' as an end-user term; all templates and classes use 'blogpost' to indicate RSS related content.</p></td>
</tr>
<tr class="even">
<td><p>mail.vmd</p></td>
<td><p>'view', 'view-thread' and 'remove'</p>
<p><strong>Context:</strong> mail.</p></td>
</tr>
<tr class="odd">
<td><p>global.vmd</p></td>
<td><p>'dashboard', 'view-profile', 'edit-profile', 'change-password-profile', 'edit-notifications-profile'</p>
<p><strong>Context:</strong> global.</p></td>
</tr>
<tr class="even">
<td><p>space.vmd</p></td>
<td><p>list-alphabetically, list-recently-updated, list-content-tree, create-page</p>
<p><strong>Context:</strong> space-pages.</p>
<p><strong>Comment:</strong> space.vmd handles a wide range of options, this context is accessed by clicking on 'browse space' in the default theme of Confluence (tabbed theme).</p></td>
</tr>
<tr class="odd">
<td><p> </p></td>
<td><p>view-mail-archive</p>
<p><strong>Context:</strong> space-mails.</p></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td><p>view-blogposts, create-blogpost</p>
<p><strong>Context:</strong> space-blogposts.</p></td>
</tr>
<tr class="odd">
<td><p> </p></td>
<td><p>view-templates</p>
<p><strong>Context:</strong> space-templates.</p></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td><p>view-space-operations&quot;</p>
<p><strong>Context:</strong> space-operations.</p></td>
</tr>
<tr class="odd">
<td><p> </p></td>
<td><p>view-space-administration, list-permission-pages</p>
<p><strong>Context:</strong> space-administration.</p></td>
</tr>
</tbody>
</table>

### Example

As an example on how to use the table above, say we found the 'Attachments' tab on the view page screen annoying and wanted to remove it. We could make this layout change in the page.vmd file - where the 'view' mode is handled (as shown below).

``` java
#*
     Display page based on mode: currently 'view', 'edit', 'preview-edit', 'info' and 'attachments.
     See the individual page templates (viewpage.vm, editpage.vm, etc.) for the setting of the mode parameter.
   *#
   ## VIEW
   #if ($mode == "view")

       <make layout modifications here>

   #elseif ...
```

{{% note %}}

When creating your own decorators, it is critical that you preserve the lines `#parse ("/pages/page-breadcrumbs.vm")` or `#parse ("/breadcrumbs.vm")`. These include files pass important information about the space to other space decorators and hence must be included.

{{% /note %}}

### The Theme Helper Object

When editing decorator files you will come across a variable called **$helper** - this is the **theme helper object**.

The following table summarises what this object can do:

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Behaviour</p></th>
<th><p>Explanation</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>$helper.domainName</p></td>
<td><p>displays the base URL of your Confluence instance on your page. This is useful for constructing links to your own Confluence pages.</p></td>
</tr>
<tr class="even">
<td><p>$helper.spaceKey()</p></td>
<td><p>returns the current space key or null if in a global context.</p></td>
</tr>
<tr class="odd">
<td><p>$helper.spaceName</p></td>
<td><p>returns the name of the current space</p></td>
</tr>
<tr class="even">
<td><p>$helper.renderConfluenceMacro(&quot;{create-space-button}&quot;)</p></td>
<td><p>renders a call to a <a href="#confluence-macro" class="unresolved">Confluence Macro</a> for the velocity context</p></td>
</tr>
<tr class="odd">
<td><p>$helper.getText(&quot;key.key1&quot;)</p></td>
<td><p>looks up a key in a properties file matching <code>key.key1=A piece of text</code> and returns the matching value (&quot;A piece of text&quot;)</p></td>
</tr>
<tr class="even">
<td><p>$helper.action</p></td>
<td><p>returns the <a href="http://opensymphony.com/xwork" class="external-link">XWork</a> action which processed the request for the current page.</p></td>
</tr>
</tbody>
</table>

If you are on a page or space screen you also have access to the actual page and space object by using `$helper.page` and `$helper.space` respectively.

If you want to delve more into what other methods are available in this object, please see our API's for ThemeHelper.

### Velocity macros

Finally, the last thing you need to decipher decorator files is an understanding of macros. A velocity macro looks like this:

``` py
#myVelocityMacro()
```

In essence, each macro embodies a block of code. We've used these macros to simplify decorator files and make them easier to modify.

For example, the `#editPageLink()` macro will render the edit page link you see on the 'View Page Screen'. All the logic which checks whether a certain user has permissions to edit pages and hence see the link are hidden in this macro. As the theme writer, you need only care about calling it.

The easiest way to acquaint yourself with the macros is to browse through your macros.vm file, located in `/template/includes/macros.vm` (under the base Confluence installation).

### Writing your own Velocity Macros

<a href="http://jakarta.apache.org/velocity/user-guide.html#Velocimacros" class="external-link">Velocity macros</a> are very useful for abstracting out common presentation logic into a function call and for keeping decorators clean. If you wish to use them for your theme you can either:

##### Write your own Macros file

Write your own Velocity macros library file, as we've done with macros.vm. If you elect to do this you must locate the velocity.properties file beneath WEB-INF/classes and tell the Velocity engine where your library file can be located, relative to the base installation of Confluence.

``` java
velocimacro.library = template/includes/macros.vm
```

##### Use Inline Velocity Macros.

Inline velocity macros, when loaded once, can be called from anywhere. See decorators/mail.vmd for examples of inline decorators.

## Using Stylesheets

Stylesheets can be defined for a theme and they will automatically be included by Confluence when pages are displayed with your theme. You simply need to add a resource of type download to your theme module. Please note that the **resource name must end with .css** for it to be automatically included by Confluence.

``` xml
<theme key="mytheme" .... >
   ...
   <resource type="download" name="my-theme.css" location="styles/my-theme.css"/>
   ...
</theme>
```

Now, in the HTML header of any page using your theme, a link tag to your theme stylesheets will be created by Confluence. If you have a look at the source of `combined.css`, it will contain imports to all your theme stylesheets.

``` xml
<html>
<head>
   ...
   <link type="text/css" href="/confluence/s/.../_/styles/combined.css?spaceKey=FOO" rel="stylesheet">
</head>

...

</html>
```

Theme stylesheets are included **after** all the default Confluence styles and colour schemes. This is to ensure that your theme styles can override and take precedence over the base styles provided by Confluence.

## Using Colour Schemes

Users can customise their own colour scheme (regardless of the theme selected) for a particular space under Space Administration.

<img src="/server/confluence/images/space-colour-scheme.png" class="image-center" />

You may choose to respect these user configured colour schemes in your theme or ignore them completely by overriding them in your theme stylesheets. If you would like to respect the configured colour schemes for your new UI elements, you should specify a velocity stylesheet resource in your theme module.

``` xml
<theme key="mytheme" .... >
   ...
   <resource type="stylesheet" name="my-theme-colors.vm" location="templates/clickr/my-theme-colors.vm"/>
   ...
</theme>
```

Please note that the **resource name must end with .vm**, and the **type must be 'stylesheet'** for it to be automatically rendered as a velocity template by Confluence. This velocity stylesheet will essentially contain css for colours with references to the <a href="http://docs.atlassian.com/atlassian-confluence/latest/com/atlassian/confluence/themes/ColorSchemeBean.html" class="external-link">colour scheme bean</a> (which is available to you via the action). For example:

``` java
\#breadcrumbs a {
    color: $action.colorScheme.linkColor;
}
#myNewElement {
    color: $action.colorScheme.headingTextColor;
}
.myNewElementClass {
    border-color: $action.colorScheme.borderColor;
}
...
```

{{% note %}}

As the velocity stylesheet is rendered as a velocity template, you will need to escape any \#ids (e.g. breadcrumbs) that match velocity macro names.

{{% /note %}}

Additionally, you may choose to provide your theme with a pre-defined colour scheme (which users will be able to select under Space Administration). This pre-defined colour scheme will take precedence if no custom user one is defined for the space. To define a theme's colour scheme, you need to add a colour scheme module and link to it in the theme module. For example:

``` xml
<theme key="mytheme" .... >
   ...
   <colour-scheme key="com.atlassian.confluence.themes.mytheme:earth-colours"/>
   ... 
</theme>

...

<colour-scheme key="earth-colours" name="Brown and Red Earth Colours" class="com.atlassian.confluence.themes.BaseColourScheme">
     <colour key="property.style.topbarcolour" value="#440000"/>
     <colour key="property.style.spacenamecolour" value="#999999"/>
     <colour key="property.style.headingtextcolour" value="#663300"/>
     <colour key="property.style.linkcolour" value="#663300"/>
     <colour key="property.style.bordercolour" value="#440000"/>
     <colour key="property.style.navbgcolour" value="#663300"/>
     <colour key="property.style.navtextcolour" value="#ffffff"/>
     <colour key="property.style.navselectedbgcolour" value="#440000"/>
     <colour key="property.style.navselectedtextcolour" value="#ffffff"/>
</colour-scheme>
```

The class of a colour scheme must implement `com.atlassian.confluence.themes.ColourScheme`. The `com.atlassian.confluence.themes.BaseColourScheme` class provided with Confluence sets the colours based on the module's configuration.

The available colours correspond to those that you would configure under Space Administration &gt; Colour Scheme:

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Key</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>property.style.topbarcolour</p></td>
<td><p>The strip across the top of the page.</p>
<p><strong>Default value:</strong> #003366</p></td>
</tr>
<tr class="even">
<td><p>property.style.breadcrumbstextcolour</p></td>
<td><p>The breadcrumbs text in the top bar of the page.</p>
<p><strong>Default value:</strong> #ffffff</p></td>
</tr>
<tr class="odd">
<td><p>property.style.spacenamecolour</p></td>
<td><p>The text of the current space name, or Confluence in the top left.</p>
<p><strong>Default value:</strong> #999999</p></td>
</tr>
<tr class="even">
<td><p>property.style.headingtextcolour</p></td>
<td><p>All heading tags throughout the site.</p>
<p><strong>Default value:</strong> #003366</p></td>
</tr>
<tr class="odd">
<td><p>property.style.linkcolour</p></td>
<td><p>All links throughout the site.</p>
<p><strong>Default value:</strong> #003366</p></td>
</tr>
<tr class="even">
<td><p>property.style.bordercolour</p></td>
<td><p>Table borders and dividing lines.</p>
<p><strong>Default value:</strong> #3c78b5</p></td>
</tr>
<tr class="odd">
<td><p>property.style.navbgcolour</p></td>
<td><p>Background of tab navigation buttons.</p>
<p><strong>Default value:</strong> #3c78b5</p></td>
</tr>
<tr class="even">
<td><p>property.style.navtextcolour</p></td>
<td><p>Text of tab navigational buttons.</p>
<p><strong>Default value:</strong> #ffffff</p></td>
</tr>
<tr class="odd">
<td><p>property.style.navselectedbgcolour</p></td>
<td><p>Background of tab navigation buttons when selected or hovered.</p>
<p><strong>Default value:</strong> #003366</p></td>
</tr>
<tr class="even">
<td><p>property.style.navselectedtextcolour</p></td>
<td><p>Text of tab navigation buttons when selected or hovered.</p>
<p><strong>Default value:</strong> #ffffff</p></td>
</tr>
<tr class="odd">
<td><p>property.style.topbarmenuselectedbgcolour</p></td>
<td><p>Background of top bar menu when selected or hovered.</p>
<p><strong>Default value:</strong> #336699</p></td>
</tr>
<tr class="even">
<td><p>property.style.topbarmenuitemtextcolour</p></td>
<td><p>Text of menu items in the top bar menu.</p>
<p><strong>Default value:</strong> #003366</p></td>
</tr>
<tr class="odd">
<td><p>property.style.menuselectedbgcolour</p></td>
<td><p>Background of page menu when selected or hovered.</p>
<p><strong>Default value:</strong> #6699cc</p></td>
</tr>
<tr class="even">
<td><p>property.style.menuitemtextcolour</p></td>
<td><p>Text of menu items in the page menu.</p>
<p><strong>Default value:</strong> #535353</p></td>
</tr>
<tr class="odd">
<td><p>property.style.menuitemselectedbgcolour</p></td>
<td><p>Background of menu items when selected or hovered.</p>
<p><strong>Default value:</strong> #6699cc</p></td>
</tr>
<tr class="even">
<td><p>property.style.menuitemselectedtextcolour</p></td>
<td><p>Text of menu items when selected or hovered.</p>
<p><strong>Default value:</strong> #ffffff</p></td>
</tr>
</tbody>
</table>
