---
aliases:
- /server/confluence/writing-a-blueprint-advanced-16973838.html
- /server/confluence/writing-a-blueprint-advanced-16973838.md
category: devguide
confluence_id: 16973838
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=16973838
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=16973838
date: '2017-12-08'
legacy_title: Writing a Blueprint - Advanced
platform: server
product: confluence
subcategory: learning
title: Writing a Blueprint - Advanced
---
# Writing a Blueprint - Advanced

If you've followed the steps in [Confluence Blueprints](/server/confluence/confluence-blueprints) and [Writing a Blueprint - Intermediate](/server/confluence/writing-a-blueprint-intermediate) and find that your Blueprint needs more flexibility, you're probably going to want a custom action and may have to interact with some other components made available by the Blueprint API.

## Adding a dependency on the confluence-create-content-plugin

The Blueprint API is provided by a plugin called the confluence-create-content-plugin, so to interact with the API you will have to add a dependency in your project's `pom.xml`.

``` javascript
<dependency>
    <groupId>com.atlassian.confluence.plugins</groupId>
    <artifactId>confluence-create-content-plugin</artifactId>
    <!-- 1.5.25 is the version released with Confluence 5.1.4 -->
    <version>1.5.25</version>
    <scope>provided</scope>
</dependency>
```

## Adding an event listener

If the only functionality you need to add to the default behaviour is to take some action after the Blueprint is created, you can listen for the `BlueprintPageCreateEvent`.

``` javascript
public class MyBlueprintListener
{
    private static final ModuleCompleteKey MY_BLUEPRINT_KEY = new ModuleCompleteKey(
        "com.atlassian.confluence.plugins.myplugin", "myplugin-blueprint");

    public MyBlueprintListener(EventPublisher eventPublisher)
    {
        eventPublisher.register(this);
    }

    @EventListener
    public void onBlueprintCreateEvent(BlueprintPageCreateEvent event)
    {
        ModuleCompleteKey moduleCompleteKey = event.getBluePrintKey();
        if (MY_BLUEPRINT_KEY.equals(moduleCompleteKey))
        {
            // Take some action when a My Plugin Blueprint is created
            System.out.println("Created your kind of blueprint");
        }
    }
}
```

You will also need to register the class as a component in your `atlassian-plugin.xml`

``` javascript
<component key="my-listener" class="myplugin.MyBlueprintListener" />
```

## Blueprint API components

{{% warning %}}

We are currently in the process of updating this Blueprints API for v2.

Please contact us if you require access to these managers in your plugin.

{{% /warning %}}

If you need to interact with the Blueprints API at a low level, you can import and use these components:

<table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<thead>
<tr class="header">
<th>Component Key</th>
<th>Interface</th>
<th>Methods</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><code>blueprintManager</code></td>
<td><code>com.atlassian.confluence.plugins.createcontent.actions.BlueprintManager</code></td>
<td><ul>
<li><code>createAndPinIndexPage</code> checks for and creates a index page if required for this blueprint, and pins it to the Space Sidebar.</li>
<li><code>setBlueprintCreatedByUser</code> flags that a Blueprint of a certain type has been created by a given user</li>
</ul></td>
</tr>
<tr class="even">
<td><code>contentGenerator</code></td>
<td><code>com.atlassian.confluence.plugins.createcontent.actions.BlueprintContentGenerator</code></td>
<td><ul>
<li><p><code>createIndexPageObject</code> creates a content page object given a plugin template and a render context.</p></li>
<li><p><code>generateBlueprintPageObject</code> creates an index page object given a plugin template and a render context.</p></li>
</ul></td>
</tr>
</tbody>
</table>


