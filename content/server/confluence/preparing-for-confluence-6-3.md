---
aliases:
- /server/confluence/preparing-for-confluence-6.3-49016220.html
- /server/confluence/preparing-for-confluence-6.3-49016220.md
category: devguide
confluence_id: 49016220
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=49016220
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=49016220
date: '2017-12-08'
legacy_title: Preparing for Confluence 6.3
platform: server
product: confluence
subcategory: development resources
title: Preparing for Confluence 6.3
---
# Preparing for Confluence 6.3

This documentation is intended for Confluence developers who want to ensure that their existing plugins and add-ons are compatible with Confluence 6.3.

We plan to release frequent milestones during the development of Confluence 6.3. Watch this page to find out when a new milestone is available and what's changed.  We won't be publishing formal release notes over at <a href="http://confluence.atlassian.com/display/DOC/Confluence+Development+Releases" class="external-link">Development Releases</a> until we release a beta.

{{% note %}}

**Latest milestone**

|              |           |                                                                                                         |
|--------------|-----------|--------------------------------------------------------------------------------------------------------:|
| 03 Jul 2017  | 6.3.0-rc1 |  <a href="https://www.atlassian.com/software/confluence/download-eap" class="external-link">Download</a>|

{{% /note %}}

**Issues with this milestone?**

Hit the Feedback button on the Confluence EAP header or <a href="https://jira.atlassian.com/secure/CreateIssueDetails!init.jspa?pid=10470&amp;issuetype=10000&amp;components=43692&amp;summary=Confluence+6.3+EAP+Feedback" class="external-link">raise an issue</a> to tell us about it. 

Looking for updated documentation? Check out the <a href="https://confluence.atlassian.com/display/CONFEAP/Confluence+EAP+Documentation" class="external-link">Confluence EAP</a> space for the latest docs. 

Did you know we've got a new developer community? Head to <a href="https://community.developer.atlassian.com/" class="uri external-link">community.developer.atlassian.com/</a> to check it out!  We'll be posting in the **announcements** category if when new EAP releases are available. 

**How do you want to find out about these releases?**

We'd love to improve how we tell you about Early Access Program (EAP) releases. <a href="https://goo.gl/forms/QoYMmJVdceWsnfjT2" class="external-link">Tell us what you think </a>in this quick survey.

## Release Candidate - 3 July

-   Added three new languages - Estonian, Icelandic and Slovenian. 
-   Added support for MySQL 5.7

## Beta 3 - 26 June

-   Improved translations for several languages
-   Bug fixes and other improvements

## Beta 2 - 19 June

-   Bug fixes and other improvements. 
-   For the full release notes see <a href="https://confluence.atlassian.com/display/DOC/Confluence+6.3.0+beta+Release+Notes" class="external-link">Confluence 6.3.0 beta Release Notes</a>

## Beta 1 (internal)

This was an internal release. 

## Milestone 8 - 13 June

-   **Recently worked on** is now listed in the profile menu. This is to provide easy access to recently worked on pages for sites that use a page as the space homepage, rather than the dashboard. 
-   Upgraded Applinks to 5.3.2

## Milestone 5 - 5 June 

-   The number of users who can edit a page simultaneously has been limited to 12. This is a proactive change to help customers avoid performance issues if a large number of people edit at the same time. The limit can be modified using the `confluence.collab.edit.user.limit` <a href="https://confluence.atlassian.com/display/CONFEAP/Configuring+System+Properties" class="external-link">system property</a>.

## Milestone 4 - 29 May

-   Added support for PostgreSQL 9.6
-   Removed support for Solaris operating systems - see <a href="https://confluence.atlassian.com/display/DOC/End+of+Support+Announcements+for+Confluence" class="external-link">see announcement</a>

## Milestone 3 - 22 May

This is our first public milestone for Confluence 6.3. Not much has changed yet, but keep an eye on this page, as we'll be releasing a milestone every week in the lead up to Confluence 6.3.  

-   Updated Tomcat to 8.0.43

