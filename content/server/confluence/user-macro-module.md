---
aliases:
- /server/confluence/user-macro-module-2031854.html
- /server/confluence/user-macro-module-2031854.md
category: reference
confluence_id: 2031854
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031854
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031854
date: '2017-12-08'
legacy_title: User Macro Module
platform: server
product: confluence
subcategory: modules
title: User Macro module
---
# User Macro module

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Available:</p></td>
<td><p>Confluence 2.3 and later</p></td>
</tr>
</tbody>
</table>

{{% tip %}}

You can create user macros without writing a plugin, by defining the user macro in the Confluence Administration Console. See <a href="https://confluence.atlassian.com/display/DOC/Writing+User+Macros" class="external-link">Writing User Macros</a>.

{{% /tip %}}

## Adding a user macro plugin

User Macros are a kind of Confluence plugin module.

-   For more information about plugins in general, read [Confluence Plugin Guide](/server/confluence/confluence-plugin-guide).
-   To learn how to install and configure plugins (including macros), read <a href="#installing-a-plugin" class="unresolved">Installing a Plugin</a>.
-   For an introduction to writing your own plugins, read [Writing Confluence Plugins](/server/confluence/writing-confluence-plugins)

{{% note %}}

User macro plugin modules are available in Confluence 2.3 or later

{{% /note %}}{{% note %}}

In order to upload your plugin via the Universal Plugin Manager (the default plugin manager in Confluence 3.4 and later) you will need to set the **atlassian-plugin** attribute **pluginsVersion='2***'* as shown in the example bellow.

{{% /note %}}

## User Macro Plugin Modules

User macro plugin modules allow plugin developers to define simple macros directly in the `atlassian-plugin.xml` file, without writing any additional Java code. User macro plugin modules are functionally identical to <a href="#writing-user-macros" class="unresolved">Writing User Macros</a> configured through the administrative console, except that they can be packaged and distributed in the same way as normal plugins.

{{% note %}}

User macros installed by plugin modules **do not** appear in the user macro section of the administrative console, and are not editable from within the user interface. They appear just as normal plugin modules in the plugin interface.

{{% /note %}}

### Configuring a Macro Plugin Module

Macro plugin modules are configured entirely inside the `atlassian-plugin.xml` file, as follows:

``` xml
<atlassian-plugin name='Hello World Macro' key='confluence.extra.helloworld' pluginsVersion='2'>
    <plugin-info>
        <description>Example user macro</description>
        <vendor name="Atlassian Software Systems" url="http://www.atlassian.com"/>
        <version>1.0</version>
    </plugin-info>

    <user-macro name='helloworld' key='helloworld' hasBody='true' bodyType='raw' outputType='html'>
        <description>Hello, user macro</description>
        <template><![CDATA[Hello, $body!]]></template>
    </user-macro>

    <!-- more macros... -->
</atlassian-plugin>
```

-   The &lt;template&gt; section is required, and defines the velocity template that will be used to render the macro
-   All the velocity variables available in <a href="https://confluence.atlassian.com/display/DOC/Writing+User+Macros" class="external-link">Writing User Macros</a> are available in user macro plugin modules
-   The name and key of the macro must be specified the same as [Macro Module](/server/confluence/macro-module)
-   No class attribute is required
-   The attributes of the &lt;user-macro&gt; element match the corresponding configuration for user macros:

### Available Attributes

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Attribute</p></th>
<th><p>Allowed Values</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>hasBody</p></td>
<td><ul>
<li>true - the macro expects a body (i.e. {hello}World{hello})</li>
<li>false - the macro does not expect a body (i.e. Hello, {name})<br />
<strong>Default:</strong> false.</li>
</ul></td>
</tr>
<tr class="even">
<td><p>bodyType</p></td>
<td><ul>
<li>raw - the body will not be processed before being given to the template</li>
<li>escapehtml - HTML tags will be escaped before being given to the template</li>
<li>rendered - the body will be rendered as wiki text before being given to the template<br />
<strong>Default:</strong> raw.</li>
</ul></td>
</tr>
<tr class="odd">
<td><p>outputType</p></td>
<td><ul>
<li>html - the template produces HTML that should be inserted directly into the page</li>
<li>wiki - the template produces Wiki text that should be rendered to HTML before being inserted into the page<br />
<strong>Default:</strong> html.</li>
</ul></td>
</tr>
</tbody>
</table>
