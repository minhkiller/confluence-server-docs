---
aliases:
- /server/confluence/preparing-for-confluence-6.0-40508412.html
- /server/confluence/preparing-for-confluence-6.0-40508412.md
category: devguide
confluence_id: 40508412
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=40508412
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=40508412
date: '2017-12-08'
legacy_title: Preparing for Confluence 6.0
platform: server
product: confluence
subcategory: development resources
title: Preparing for Confluence 6.0
---
# Preparing for Confluence 6.0

This documentation is intended for Confluence developers who want to ensure that their existing plugins and add-ons are compatible with Confluence 6.0, and to learn about the new features for developing add-ons in Confluence 6.0.

Please watch the <a href="http://confluence.atlassian.com/display/DOC/Confluence+Development+Releases" class="external-link">Development Releases</a> page for pre-release versions of Confluence 6.0 that you can use for testing purposes. This page will be updated as further EAP development releases are available - we plan to release regular milestones in the lead up to the final release.

## Collaborative editing 

The feature you've all been waiting for is ready for a test drive!  Read more about how <a href="https://confluence.atlassian.com/display/CONFEAP/collaborative+editing" class="external-link">collaborative editing</a> works for users or head to [Collaborative editing for Confluence Server](/server/confluence/collaborative-editing-for-confluence-server) to see how it works under the hood and find out how you can make sure your add-on is compatible. 

These are early milestones, and are not suitable for production sites (no matter how much you may be tempted) - so give it a run in a test site only.   

##### Memory requirements

You may need to give your test server more memory than for previous Confluence releases. In this EAP release Synchrony's max heap size is 1 GB. As of Confluence 6.0.0-m77 this is configurable via the `reza.memory.max` system property.

##### Ports and firewalls / proxies

Your firewall / proxies must be configured to allow:

-   WebSocket connection (if you're using NGINX see <a href="https://www.nginx.com/blog/websocket-nginx/" class="external-link">Using NGINX as a WebSocket Proxy</a> for more info). As of Confluence 6.0.0-rc2 an experimental XML HTTP Request (XHR) fallback feature is available by setting the system property `synchrony.enable.xhr.fallback=true`. This may be used if it is discovered in production that some users can't access Confluence via WebSockets. Note that the XHR fallback isn't compatible with using the Synchrony proxy via Confluence. See <a href="https://jira.atlassian.com/browse/CONF-44250" class="external-link">CONF-44250</a>.
-   Port 8091 for Synchrony's connections. We are in the process of developing an internal Synchrony proxy that will mean customers don't need to open port 8091. This will be available in a future milestone.
-   As of Confluence 6.0.0-m77 we support running Confluence with a reverse proxy. See <a href="https://confluence.atlassian.com/display/CONFKB/How+to+use+NGINX+to+proxy+requests+for+Confluence" class="external-link">How to use NGINX to proxy requests for Confluence</a> for an example of how to include Synchrony.

##### HTTP and HTTPS

-   Early EAP releases only supported the default HTTP connector configuration. If you've customized the Tomcat connector configuration (as outlined in <a href="https://confluence.atlassian.com/display/DOC/Running+Confluence+Over+SSL+or+HTTPS" class="external-link">Running Confluence Over SSL or HTTPS</a> for example) collaborative editing will likely fail. This was resolved in later milestones.
-   As of Confluence 6.0.0-m77, we support running Confluence with a reverse proxy with SSL. See <a href="https://confluence.atlassian.com/display/CONFEAP/Running+Confluence+behind+NGINX+with+SSL" class="external-link">Running Confluence behind NGINX with SSL</a> for more information. Updated docs for Apache mod\_proxy will be available soon.

##### Database considerations

Collaborative editing will fail with an error if:

-   you're using an unsupported or custom database driver. See <a href="https://confluence.atlassian.com/display/DOC/Database+JDBC+Drivers" class="external-link">Database JDBC Drivers</a> for the list of drivers we support.
-   you have a custom datasource connection, particularly if you've used a `driverClassName` that is not in our list of supported <a href="https://confluence.atlassian.com/display/DOC/Database+JDBC+Drivers" class="external-link">database JDBC Drivers</a>. See <a href="https://confluence.atlassian.com/display/CONFKB/How+to+convert+a+datasource+to+a+direct+JDBC+connection" class="external-link">How to convert a datasource to a direct JDBC connection</a> to switch to a direct JDBC connection, and <a href="https://confluence.atlassian.com/display/DOC/Troubleshooting+External+Database+Connections" class="external-link">Troubleshooting External Database Connections</a> for more info.

##### Not yet available for Data Center

This EAP release is not ready for Confluence Data Center. We'll make this available in a future milestone.

## Documentation theme

Confluence 6.0.0-**m77** contains the upgrade task that will remove the documentation theme, and switch any spaces using the documentation theme, to the default theme. See <a href="https://confluence.atlassian.com/display/CONFEAP/Documentation+theme+migration+FAQ" class="external-link">Documentation theme migration FAQ</a> for more information. 

## Guava upgrade

Confluence 6.0.0-**m68** contains an upgrade to Guava 18.0.  The previous version of Guava being used in the product was 11.0.2, which is a very old version released back in 2011. 

Full release notes from version 11 to 18 can be found at <a href="https://github.com/google/guava/wiki/ReleaseHistory" class="uri external-link">github.com/google/guava/wiki/ReleaseHistory</a>.

### Breaking changes

There are a lot of breaking changes between 11.0.2 and 18.0, but the most noticeable ones are listed below.

-   **Cache becomes LoadingCache**  
    Cache was deprecated in 11.0.2, but was still usable.  It has now been fully removed, and all usages should be replaced with `LoadingCache` or `CacheBuilder.build()`.  See <a href="https://github.com/google/guava/wiki/Release11" class="uri external-link">github.com/google/guava/wiki/Release11</a> for more information.
-   **Cache no longer supports MapMaker**  
    Details on migrating from MapMaker to a standard LoadingCache can be found in <a href="https://github.com/google/guava/wiki/MapMakerMigration" class="uri external-link">github.com/google/guava/wiki/MapMakerMigration</a>.
-   **Stopwatch changes**  
    Stopwatch constructors have been removed in favour of `Stopwatch.createStated()` and `Stopwatch.createUnstarted()` methods.  All `elapsedXXX` methods have also been removed in favour of `stopwatchInstance.elapsed(TimeUnit)`. See the notes in <a href="https://github.com/google/guava/wiki/Release15" class="uri external-link">github.com/google/guava/wiki/Release15</a>.

## Hibernate upgrade

Confluence 6.0.0-**m68** contains an upgrade to Hibernate 5.2.2.  The previous version of Hibernate being used in the product was 2.1.8, which is a very old version released over a decade ago.

A compatibility layer has been introduced to adapt hibernate 2 interfaces to hibernate 5, and to transpile HQL queries to work around common incompatibilities.

A full change log from version 2.1.8 to 5.2.2 can be found at <a href="https://github.com/hibernate/hibernate-orm/blob/4.3/changelog.txt" class="uri external-link">github.com/hibernate/hibernate-orm/blob/4.3/changelog.txt</a> and <a href="https://github.com/hibernate/hibernate-orm/blob/master/changelog.txt" class="uri external-link">github.com/hibernate/hibernate-orm/blob/master/changelog.txt</a>.

## Other library upgrades

We've upgraded the following libraries and plugins:

-   Hibernate 5
-   Google gson 2.7
-   Atlassian gzipfilter 3.0
-   AUI 5.10.1
-   Atlassian Gadgets 4.2.7
-   SLF4 1.7.21
-   JSON 0.11
-   Atlassian http 1.1.4
-   commons codec 1.10 
-   commons validator 1.5.1
-   Botocss 6.3
-   Atlassian beehive 0.2.1
-   Active Objects 1.2.2

## Plans to remove deprecated functions 

We plan remove a number of previously deprecated functions in Confluence 6.0. More information will be provided here once the work is complete. 

## Change to plugin license storage plugin

We have removed `com.atlassian.upm:plugin-license-storage-plugin:2.15.3` from Confluence 6.0 as its functionality is entirely provided by UPM now and the old plugin was incompatible with Guava 18.

If you see errors like `Could not initialize class com.atlassian.upm.osgi.impl.Versions` you'll need to update your add-on to depend on a version of UPM that provides a recent version of the third party license API.  See <https://developer.atlassian.com/market/add-on-licensing-for-developers/tutorial-adding-licensing-support-to-your-add-on> for how to add licensing support to your add-on for Confluence 6.0.

If you were using classes like `com.atlassian.upm.osgi.impl.Versions` for any other reason, it is still available via the following dependency

**pom.xml**

``` xml
<dependency>
    <groupId>com.atlassian.upm</groupId>
    <artifactId>upm-common</artifactId>
    <version>2.21.3</version>
    <scope>provided</scope>
</dependency>
```

## Atlassian Gzip Filter is no longer available to plugins

`com.atlassian.gzipfilter*` is no longer available to plugins. Any plugin using the shaded version of `com.atlassian.gzipfilter.org.commons.lang` should upgrade to commons-lang3 following the standard upgrade guide on the <a href="https://commons.apache.org/proper/commons-lang/index.html" class="external-link">Apache commons website</a>. This typically involves simply changing the imports from `com.atlassian.gzipfilter.org.commons.lang` to `org.apache.commons.lang3`, which is exported by Confluence.

## Changes to slf4j internals

`org.slf4j.impl` and `org.slf4j.bridge` are no longer exported. Plugins can remove usages of slf4j internals and also remove declared imports of the internals in the `<Import-Package>` section of the maven-confluence-plugin configuration in pom.xml as `slf4j` will still work perfectly without them in a plugin.

## XML api changes 

-   Confluence no longer uses the `com.sun.bind` implementation of JAXB (from `com.sun.xml.bind:jaxb-impl`) and now just uses the version bundled in Java 8. Plugins no longer need to import the implementation packages. Confluence also no longer exports a dependency on `javax.xml.bind:jaxb-api`. Plugins can depend on the version bundled in Java.
-   Confluence now exports enough packages from `org.w3c.dom` for its batik jars to be usable from a plugin, and `org.w3c.dom.xpath` is now correctly exported from the bootstrap loader. As part of this change, Confluence no longer exports a dependency on `xml-apis:xml-apis`, but instead exports `org.apache.xmlgraphics:batik-ext`. Some plugins may have compilation errors due to this change.
-   Confluence no longer exports `org.w3c.css:sac` but exports `xml-apis:xml-apis-ext` instead.
-   Confluence no longer provides an implementation of the interface `org.w3c.css.sac.Parser` from `net.sourceforge.cssparser:cssparser`. Only the implementation in `org.apache.xmlgraphics:batik-css:1.7` is available from the system bundle by default.

## Deprecated javascript globals

In Confluence 5.9 we deprecated the use of all Confluence provided javascript globals. 

| window Global |
|---------------|
| Confluence    |
| AJS           |
| WRM           |

**Update:** these deprecated javascript globals will now **not** be removed in 6.0, but will be removed in a future release. We'll provide further info to help you migrate to appropriate AMD modules before this change happens. 

## Reminder to Require Dependencies

As we continue to move to a more modular design for our front end resources, it becomes even more important to make sure you are requiring all the dependencies that your plugin needs. Some resources that may have been included on the page by chance previously may no longer be included or might be out of order or included later, because some of them may be loaded asynchronously for the individual features or components that require them. This gives us a performance benefit as it gives a faster page load time and also avoids loading resources that are not always required.

What this means is that you should always explicitly define the dependencies required for your resources in your atlassian-plugin.xml, especially for AUI Components that are not included in Core. For example, on <a href="https://docs.atlassian.com/aui/latest/docs/date-picker.html" class="uri external-link">docs.atlassian.com/aui/latest/docs/date-picker.html</a>, you can see that this dependency needs to be explicitly required in your atlassian-plugin.xml file: &lt;dependency&gt;`com.atlassian.auiplugin:aui-date-picker`&lt;/dependency&gt;. The good news is that if you are already doing this, you shouldn't have anything to worry about.

