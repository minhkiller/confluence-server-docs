---
aliases:
- /server/confluence/dtds-and-schemas-2031633.html
- /server/confluence/dtds-and-schemas-2031633.md
category: devguide
confluence_id: 2031633
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031633
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031633
date: '2017-12-08'
guides: guides
legacy_title: DTDs and Schemas
platform: server
product: confluence
subcategory: learning
title: DTDs and schemas
---
# DTDs and schemas

This page contains information about DTDs and schemas used internally by Confluence.

## XHTML storage format in Confluence 4

See <a href="http://confluence.atlassian.com/display/DOC/Confluence+Storage+Format" class="external-link">Confluence Storage Format</a>.

## Attached files

| Name                                                                                                                                 | Version | Date             |
|--------------------------------------------------------------------------------------------------------------------------------------|---------|------------------|
| [hibernate-mapping-2.0.dtd](https://developer.atlassian.com/confdev/files/2031633/2228229/1/1230848560692/hibernate-mapping-2.0.dtd) | 1       | 2009-01-01 22:22 |
