---
aliases:
- /server/confluence/i18n-architecture-2031841.html
- /server/confluence/i18n-architecture-2031841.md
category: devguide
confluence_id: 2031841
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031841
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031841
date: '2017-12-08'
guides: guides
legacy_title: I18N Architecture
platform: server
product: confluence
subcategory: learning
title: I18N architecture
---
# I18N architecture

This document sheds some light on how Confluence looks up a message text from one of the resource bundles.

## Loading of Resources

Currently the only implementation of the <a href="http://docs.atlassian.com/atlassian-confluence/latest/com/atlassian/confluence/util/i18n/I18NBean.html" class="external-link">I18NBean</a> interface is the <a href="http://docs.atlassian.com/atlassian-confluence/latest/com/atlassian/confluence/util/i18n/DefaultI18NBean.html" class="external-link">DefaultI18NBean</a>. When it is instantiated, it attempts to load resources from the following locations:

1.  Load `/com/atlassian/confluence/core/ConfluenceActionSupport.properties` and `/external-links.properties` and create a <a href="http://docs.atlassian.com/atlassian-confluence/latest/com/atlassian/confluence/util/i18n/CombinedResourceBundle.html" class="external-link">CombinedResourceBundle</a> containing both of them.

2.  Load all language pack resources which match the current user's locale, which would be:
    -   `/com/atlassian/confluence/core/ConfluenceActionSupport_<lang_country_variant>.properties`
    -   `/com/atlassian/confluence/core/ConfluenceActionSupport_<lang_country>.properties`
    -   `/com/atlassian/confluence/core/ConfluenceActionSupport_<lang>.properties`

        {{% warning %}}

Warning

The files are only loaded if a language pack for the specific locale is installed.

        {{% /warning %}}

3.  Load all resources of type 'i18n' which match the current user's locale:
    -   `<resource location>_<lang_country_variant>.properties`
    -   `<resource location>_<lang_country>.properties`
    -   `<resource location>_<lang>.properties`
    -   `<resource location>.properties`

## Resource Bundle Structure

<a href="http://docs.atlassian.com/atlassian-confluence/latest/com/atlassian/confluence/util/i18n/DefaultI18NBean.html" class="external-link">DefaultI18NBean</a> internally creates a list of <a href="http://docs.atlassian.com/atlassian-confluence/latest/com/atlassian/confluence/util/i18n/CombinedResourceBundle.html" class="external-link">CombinedResourceBundles</a> per locale, combining resource bundles from language packs and i18n resources of the same locale. When you call one of the <a href="http://docs.atlassian.com/atlassian-confluence/latest/com/atlassian/confluence/util/i18n/DefaultI18NBean.html#getText(com.atlassian.confluence.util.i18n.Message)" class="external-link">DefaultI18NBean.getText()</a> methods it will go through the bundles in the following order:

1.  lang\_country\_variant
2.  lang\_country
3.  lang
4.  default

On a lookup of a combined resource bundle, the last occurrence of a given key takes precedence during the lookup, which results in the following lookup order:

1.  i18n resource
2.  language pack

The order within i18n resources and language packs with the same locale is not defined, as they are loaded from the plugins which are loaded in an arbitrary order. This is not an issue in most cases, as you usually have no overlapping keys between your resources anyway.

## Example

Given the following situation:

-   The current user's locale is 'de\_DE\_foo'
-   There are language packs installed for the locales 'de\_DE\_foo', 'de\_DE' and 'de'
-   There is one resource of type 'i18n' available from one of the plugins. The location for that resource is 'com.example.resources'

Lookups will always happen from top to bottom until a message for a given key is found.

##### RELATED TOPICS

[Confluence Internals](/server/confluence/confluence-internals)