---
aliases:
- /server/confluence/javascript-components-6849920.html
- /server/confluence/javascript-components-6849920.md
category: devguide
confluence_id: 6849920
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=6849920
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=6849920
date: '2017-12-08'
guides: guides
legacy_title: JavaScript Components
platform: server
product: confluence
subcategory: learning
title: JavaScript Components
---
# JavaScript Components

This page documents some of Confluence's JavaScript components you can use in your plugin.

## Autocomplete Input Fields

Confluence provides a number of autocomplete input components for the various content types. You simply need the right class on the input field. You can also configure the autocomplete with some 'data-' attributes (see table below). Events are fired on the input element via jQuery's event system, so clients can listen for the documented events.

![](/server/confluence/images/pageautocomplete.png)

<table>
<colgroup>
<col style="width: 25%" />
<col style="width: 75%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Attribute name</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>data-template</p></td>
<td><p>a template used to populate the value for the input</p></td>
</tr>
<tr class="even">
<td><p>data-none-message</p></td>
<td><p>a message to display when no results returned</p></td>
</tr>
<tr class="odd">
<td><p>data-max</p></td>
<td><p>maximum number of search results, defaults to 10 if not defined</p></td>
</tr>
<tr class="even">
<td><p>data-alignment</p></td>
<td><p>alignment of the autocomplete dropdown relative to the input field. Defaults to &quot;left&quot; alignment</p></td>
</tr>
<tr class="odd">
<td><p>data-dropdown-target</p></td>
<td><p>a target element selector to place the autocomplete dropdown in.</p>
<ul>
<li>If none specified it will be placed in a div immediately after the input field.</li>
</ul></td>
</tr>
<tr class="even">
<td><p>data-target</p></td>
<td><p>a target element selector to update its value with the value provided by data-template.</p>
<ul>
<li>This is typically useful when you want to display the user's full name in the input field but submit the username</li>
<li>to the server, so another input element needs to be updated.</li>
</ul></td>
</tr>
</tbody>
</table>

Events Thrown:

-   `open.autocomplete-content`
-   `selected.autocomplete-content`

### Space Autocomplete

``` xml
<input class="autocomplete-space" data-max="10" data-none-message="No results" data-template="{name}">
```

### Attachment Autocomplete

``` xml
<input class="autocomplete-attachment" data-max="10" data-none-message="No results" data-template="{fileName}">
```

### Page Autocomplete

``` xml
<input class="autocomplete-page" data-max="10" data-none-message="No results" data-template="{title}">
```

### Blog post Autocomplete

``` xml
<input class="autocomplete-blogpost" data-max="10" data-none-message="No results" data-template="{title}">
```

### Page and Blog post Autocomplete

``` xml
<input class="autocomplete-confluence-content" data-none-message="No results" data-template="{title}">
```

### All Content Autocomplete

This includes pages, blogs, spaces, users and attachments.

``` xml
<input class="autocomplete-search" data-none-message="No results" data-template="{title}">
```

### User Autocomplete

This component throws user specific events:

-   `open.autocomplete-user`
-   `selected.autocomplete-user`

``` xml
<input class="autocomplete-user" data-none-message="No users found" data-template="{username}">
```

## User Hover

You can easily turn a user link to provide more information on the user when hovering over it. Simply add the class `confluence-userlink` and a data attribute of `data-username` to your link tag.

![](/server/confluence/images/userhover.png)

Events thrown:

-   `hover-user.open`
-   `hover-user.follow`

``` xml
<a class="confluence-userlink" data-username="admin">A. D. Ministrator</a>
```
