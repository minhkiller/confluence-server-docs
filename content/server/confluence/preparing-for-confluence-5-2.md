---
aliases:
- /server/confluence/preparing-for-confluence-5.2-18252992.html
- /server/confluence/preparing-for-confluence-5.2-18252992.md
category: devguide
confluence_id: 18252992
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=18252992
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=18252992
date: '2017-12-08'
legacy_title: Preparing for Confluence 5.2
platform: server
product: confluence
subcategory: development resources
title: Preparing for Confluence 5.2
---
# Preparing for Confluence 5.2

This documentation is intended for Confluence developers who want to ensure that their existing plugins and add-ons are compatible with Confluence 5.2, and to learn about the new features for developing add-ons in Confluence 5.2.

## Upgraded Atlassian Plugin Framework

This version of Confluence contains a milestone of Atlassian Plugin Framework 3.0.0. See [Migrating to Atlassian Plugin Framework 3](/server/confluence/migrating-to-atlassian-plugin-framework-3) for details on the breaking changes.

This also includes the first milestone release of [Atlassian Connect](https://developer.atlassian.com/display/AC/Atlassian+Connect), a new model for creating add-ons for Confluence Cloud. Read about it [here](https://developer.atlassian.com/display/AC/Atlassian+Connect) and join the <a href="https://groups.google.com/forum/?fromgroups#!forum/atlassian-connect-dev" class="external-link">Atlassian Connect Google Group</a> to learn more.

## New pluggable 'About Confluence' dialog

You can add information about your plugin to the new 'About Confluence' dialog in the Confluence user interface. This dialog appears when people click the help icon <img src="/server/confluence/images/21529042.png" class="confluence-external-resource" /> and choose 'About Confluence'. To add text and/or license information to the dialog, use the new [About Page Panel Module](/server/confluence/about-page-panel-module).  

## Renamable users

With Confluence 5.2, we are introducing schema and API changes necessary to support the ability to change usernames. To make that possible, users now have a new, unique, permanent key as well as the already-existing, unique, changeable username.

As usernames will be able to change, your plugin will need to migrate its existing data to user keys instead of usernames.

See [Renamable Users in Confluence 5.2](/server/confluence/renamable-users-in-confluence-5-2) and [Writing a Plugin Upgrade Task to Migrate User Data to New User Keys](/server/confluence/writing-a-plugin-upgrade-task-to-migrate-user-data-to-new-user-keys).

{{% note %}}

The complete rename user functionality will be delivered in 5.3. However the schema and API changes necessary will be released in 5.2 and recommend developers begin updating their add-ons now.

{{% /note %}}

## IndexQueueFlushCompleteEvent no longer fired for empty queues

The `IndexQueueFlushCompleteEvent` was always fired when the index queue was periodically flushed, even if the queue was empty. This event is now only fired if the queue is not empty.

## Lucene upgrade

With Confluence 5.2, we have upgraded the Lucene search library to from version 2.9 to version 4.3, as part of larger efforts to make search better and faster.

This means your implementations of `Extractor` will be broken and will need updating. See [How to fix broken Extractors](/server/confluence/how-to-fix-broken-extractors).

## System installation date

The Confluence system installation date can now be programmatically accessed using the `getInstallationDate()` method on the `SystemInformationService`'s` ConfluenceInfo` object. System content is used to estimate the date if it was not recorded when Confluence was installed.

## Storage format changes for page layouts

Confluence 5.2 provides more flexible page layouts with a more concise storage format. See <a href="https://confluence.atlassian.com/display/DOC/Confluence+Storage+Format#ConfluenceStorageFormat-Pagelayouts" class="external-link">Confluence Storage Format</a> for information on the storage format changes for page layouts.

### Milestone releases for developers

Please watch the <a href="http://confluence.atlassian.com/display/DOC/Confluence+Development+Releases" class="external-link">Development Releases</a> page for pre-release versions of Confluence 5.2 that you can use for testing purposes.

### Have questions?

If you have any questions about preparing for Confluence 5.2, please register for an account on <a href="https://answers.atlassian.com/" class="external-link">Atlassian Answers</a>, our community Q&A resource, and <a href="https://answers.atlassian.com/questions/ask/?tags=confluence-development" class="external-link">ask a question</a>. We'll be keeping an eye on things to help out as soon as possible.

