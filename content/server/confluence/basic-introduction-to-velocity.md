---
aliases:
- /server/confluence/basic-introduction-to-velocity-2031651.html
- /server/confluence/basic-introduction-to-velocity-2031651.md
category: devguide
confluence_id: 2031651
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031651
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031651
date: '2017-12-08'
guides: guides
legacy_title: Basic Introduction to Velocity
platform: server
product: confluence
subcategory: learning
title: Basic introduction to Velocity
---
# Basic introduction to Velocity

### Example Usage

A variable in velocity looks like this:

``` xml
 $foo 
```

To set a variable:

``` xml
#set ($message = "Hello")
```

A basic *if* statement:

``` ruby
#if ($message == "Hello")
   Message received and is "Hello"
#end 
```

{{% tip %}}

A velocity variable which evaluates to null will simply render as the variable name. See the <a href="http://velocity.apache.org/engine/1.6/user-guide.html" class="external-link">Velocity User's Guide</a>

{{% /tip %}}

### Related Content

-   [What's the easiest way to render a velocity template from Java code?](/server/confluence/whats-the-easiest-way-to-render-a-velocity-template-from-java-code) (Confluence Server Development)
    -   <a href="/label/CONFDEV/faq_conf_dev" class="aui-label-split-main">faq_conf_dev</a>
    -   <a href="/label/CONFDEV/velocity-related" class="aui-label-split-main">velocity-related</a>
-   [Confluence UI architecture](/server/confluence/confluence-ui-architecture) (Confluence Server Development)
    -   <a href="/label/CONFDEV/sitemesh" class="aui-label-split-main">sitemesh</a>
    -   <a href="/label/CONFDEV/vm" class="aui-label-split-main">vm</a>
    -   <a href="/label/CONFDEV/velocity-related" class="aui-label-split-main">velocity-related</a>
    -   <a href="/label/CONFDEV/front-end" class="aui-label-split-main">front-end</a>
    -   <a href="/label/CONFDEV/golden" class="aui-label-split-main">golden</a>
    -   <a href="/label/CONFDEV/ticket" class="aui-label-split-main">ticket</a>
-   [Confluence Objects Accessible From Velocity](/server/confluence/confluence-objects-accessible-from-velocity) (Confluence Server Development)
    -   <a href="/label/CONFDEV/context" class="aui-label-split-main">context</a>
    -   <a href="/label/CONFDEV/velocity" class="aui-label-split-main">velocity</a>
    -   <a href="/label/CONFDEV/template" class="aui-label-split-main">template</a>
    -   <a href="/label/CONFDEV/plugin" class="aui-label-split-main">plugin</a>
    -   <a href="/label/CONFDEV/velocity-related" class="aui-label-split-main">velocity-related</a>
-   [Disable Velocity Caching](/server/confluence/disable-velocity-caching) (Confluence Server Development)
    -   <a href="/label/CONFDEV/velocity" class="aui-label-split-main">velocity</a>
    -   <a href="/label/CONFDEV/faq_conf_dev" class="aui-label-split-main">faq_conf_dev</a>
    -   <a href="/label/CONFDEV/velocity-related" class="aui-label-split-main">velocity-related</a>
    -   <a href="/label/CONFDEV/cache" class="aui-label-split-main">cache</a>
