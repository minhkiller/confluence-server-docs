---
aliases:
- /server/confluence/-cqllistdatefunctions-29952582.html
- /server/confluence/-cqllistdatefunctions-29952582.md
category: devguide
confluence_id: 29952582
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=29952582
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=29952582
date: '2017-12-08'
legacy_title: _CQLListDateFunctions
platform: server
product: confluence
subcategory: other
title: _CQLListDateFunctions
---
# \_CQLListDateFunctions

-   [endOfDay()](https://developer.atlassian.com/display/CONFDEV/CQL+Function+Reference#CQLFunctionReference-endOfDay)
-   [endOfMonth()](https://developer.atlassian.com/display/CONFDEV/CQL+Function+Reference#CQLFunctionReference-endOfMonth)
-   [endOfWeek()](https://developer.atlassian.com/display/CONFDEV/CQL+Function+Reference#CQLFunctionReference-endOfWeek)
-   [endOfYear()](https://developer.atlassian.com/display/CONFDEV/CQL+Function+Reference#CQLFunctionReference-endOfYear)
-   [startOfDay()](https://developer.atlassian.com/display/CONFDEV/CQL+Function+Reference#CQLFunctionReference-startOfDay)
-   [startOfMonth()](https://developer.atlassian.com/display/CONFDEV/CQL+Function+Reference#CQLFunctionReference-startOfMonth)
-   [startOfWeek()](https://developer.atlassian.com/display/CONFDEV/CQL+Function+Reference#CQLFunctionReference-startOfWeek)
-   [startOfYear()](https://developer.atlassian.com/display/CONFDEV/CQL+Function+Reference#CQLFunctionReference-startOfYear)
