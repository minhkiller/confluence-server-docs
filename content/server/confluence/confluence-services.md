---
aliases:
- /server/confluence/confluence-services-2031639.html
- /server/confluence/confluence-services-2031639.md
category: devguide
confluence_id: 2031639
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031639
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031639
date: '2017-12-08'
guides: guides
legacy_title: Confluence Services
platform: server
product: confluence
subcategory: learning
title: Confluence services
---
# Confluence services

{{% note %}}

These are guidelines related to the development of Confluence. The guidelines mainly apply to Atlassian employees, but reading them should provide insight for third-party plugin developers as well, so we decided to make them public.

{{% /note %}}

Here's a quick overview of some of the services defined in Confluence (for more details of what a service is, see the [High Level Architecture Overview](/server/confluence/high-level-architecture-overview)).

### Current Services

#### Database Service

This service is defined in `databaseSubsystemContext.xml` and `productionDatabaseContext.xml`. It provides database connectivity and Hibernate ORM to the application.

The reason for splitting the service into two files is to allow for easier testing. `productionDatabaseContext.xml` extracts the database configuration from the bootstrap configuration, and brings up Confluence with the Tangosol Coherence clustered cache. If you substitute that one file with `testDatabaseContext.xml` you will instead get a pre-configured in-memory HSQL database and in-memory caching.

Because configuring Hibernate dynamically is non-trivial, the database service is unavoidably dependent on every class we want to persist via Hibernate. You can see this in the list of `.hbm.xml` files loaded in `databaseSubsystemContext.xml`.

#### Bandana Service

Provides a generic configuration/preferences storing service using <a href="http://xstream.codehaus.org" class="external-link">XStream</a> to serialize POJO configuration objects to XML. Confluence's bandana service persists to the database.

#### Cache Service

Provides centralised management of in-memory cached data. The backing cache implementation is provided by <a href="http://ehcache.sourceforge.net/" class="external-link">ehcache</a> in the standard edition of Confluence, and <a href="http://www.oracle.com/technology/products/coherence/index.html" class="external-link">Oracle Coherence</a> in the clustered edition. For more information about the cache service, see [Confluence Caching Architecture](/server/confluence/confluence-caching-architecture).

NOTE: For more information about standard and clustered editions of Confluence, please refer to the <a href="#coherence-license-changes" class="unresolved">Coherence license changes</a> document.

#### Event Service

Provides a simple service for producing and consuming events. Defined in `eventServiceContext.xml`. Confluence's event service is cluster-aware, distinguishing between events that are limited to a single node of the cluster, and events that must be broadcast to every node.

#### Plugin Service

Provides the Atlassian plugin framework, in `pluginServiceContext.xml`. Confluence's plugin service is customised to deal with bundled plugins (plugins that are provided with the application but that may be upgraded by the end user), and to behave mostly sanely in a cluster.

The plugin system hasn't been entirely service-ised yet, as all the different plugin module loaders result in dependencies back to whatever subsystem they're being plugged into.

#### Task Queue Service

A central manager for queues in Confluence. I'm not entirely sure this should exist as it currently adds no value whatsoever beyond being a lookup mechanism, which Spring does already.

### Not Services

Things that should be services, but aren't.

#### Quartz Scheduling

Pretty obvious next candidate for servicization, but possibly tricky because the Spring/Quartz integration might not be very friendly.

#### Backup/Restore

Something to keep in mind if we clean up the backup/restore code

#### User Management

I wasn't going to mess with user-management while there was a different atlassian-user task in the release pipeline.

#### Wiki Rendering

This seems like a reasonably trivial candidate to convert to a service. There's only one dependency on non-service code (the image renderer depends on the attachment manager).

#### Mail (sending and receiving)

The sending and receiving of email is currently a mess of singleton configurations, clients sticking mail jobs directly on the queue, and very little going through Spring at all. This should be fixed.

#### External Network Access

It would be nice to have Confluence provide a service for accessing the outside world so we can throttle number of connections, provide central configuration of time-outs and authentication, and so on.

#### Image Manipulation

Right now we have a thumbnail manager that lives with attachments, but it would be nice to make this more generic, and at least support multiple thumbnail sizes.
