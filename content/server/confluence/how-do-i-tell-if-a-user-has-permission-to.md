---
aliases:
- /server/confluence/2031704.html
- /server/confluence/2031704.md
category: devguide
confluence_id: 2031704
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031704
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031704
date: '2017-12-08'
legacy_title: How do I tell if a user has permission to...?
platform: server
product: confluence
subcategory: faq
title: How do I tell if a user has permission to...?
---
# How do I tell if a user has permission to...?

### <img src="http://confluence.atlassian.com/images/icons/favicon.png" class="confluence-external-resource" /> How do I tell if a user has permission to...?

When you're writing a Confluence plugin, it's important to check that the user has permission to do the operations your plugin is performing. Confluence does not enforce security for you, it's up to your code to perform these checks.

There are two places you might want to check permissions:

-   [In Java Code](#in-java-code)
-   [In Velocity Templates](#in-velocity-templates)

#### In Java Code:

You will need:

1.  the `User` object of the user whose permissions you want to check ([How do I find the logged in user?](/server/confluence/how-do-i-find-the-logged-in-user))
2.  the `permissionManager` component from Spring ([How do I get a reference to a component?](/server/confluence/how-do-i-get-a-reference-to-a-component))

The PermissionManager has quite a few methods (<a href="https://docs.atlassian.com/atlassian-confluence/latest-server/com/atlassian/confluence/security/PermissionHelper.html" class="external-link">Javadoc</a>), but the most important are:

``` java
    /**
     * Determine whether a user has a particular permission against a given target.
     *
     * @param user the user seeking permission, or null if the anonymous user is being checked against
     * @param permission the permission to check
     * @param target the object that the permission is being checked against. If this object is null, the method
     *        will return false
     * @return true if the user has this permission, false otherwise
     * @throws IllegalStateException if the permission being checked against does not apply to the target
     */
    boolean hasPermission(User user, Permission permission, Object target);

    /**
     * Determine whether a user has permission to create an entity of a particular type within a given container.
     *
     * <p>The container is the natural container of the object being created. For example, a comment is contained
     * in a page, which is contained within TARGET_APPLICATION.
     *
     * @param user the user seeking permission, or null if the anonymous user is being checked against
     * @param container the target that the object is being created within. If this object is null, the method
     *        will return false
     * @param typeToCreate the type of object being created (see above)
     * @return true if the user has permission, false otherwise
     * @see com.atlassian.confluence.core.ContentEntityObject#getType()
     * @throws IllegalStateException if the permission being checked against does not apply to the target
     */
    boolean hasCreatePermission(User user, Object container, Class typeToCreate);
```

**Simple Permissions**

Generally you're going to be asking the question: "Does some user have permission to do something to some target?" For example: "Does BOB have permission to VIEW this PAGE?", "Does JANE have permission to REMOVE this ATTACHMENT?" These questions map to the `hasPermission()` method above.

The various values of "something" are all constants of the `Permission` class listed in <a href="https://docs.atlassian.com/atlassian-confluence/latest-server/com/atlassian/confluence/security/Permission.html" class="external-link">this Javadoc</a>. At the time this document was written, the permission 'verbs' are:

``` java
    Permission.VIEW
    Permission.EDIT
    Permission.EXPORT
    Permission.REMOVE
    Permission.SET_PERMISSIONS
    Permission.ADMINISTER
```

So to check if your user has permission to edit a particular page, the call is:

``` java
    permissionManager.hasPermission(myUser, Permission.EDIT, thePage)
```

For global permissions, the 'target object' is considered to be the Confluence application itself. There is a special target, `TARGET_APPLICATION` that represents the application as a whole. So to check if someone is a global administrator, call:

``` java
    permissionManager.hasPermission(myUser, Permission.ADMINISTER, PermissionManager.TARGET_APPLICATION
```

**Create Permissions**

Checking if someone has the ability to create an object (page, blogpost, space, etc) is a little more complicated. Every object is created *inside* some other object. Comments and Attachments are created inside Pages or BlogPosts. Pages are created inside Spaces. And Spaces are crated inside TARGET\_APPLICATION.

So to check if someone can create something, the question is: "Does this user have permission to create this KIND OF OBJECT, in this CONTAINER?" In Java, kinds of objects are represented by their class, so to see if a user can create a comment inside a particular page, you'd call:

``` java
    permissionManager.hasCreatePermission(myUser, containingPage, Comment.class)
```

And to check if the user has permission to create spaces globally:

``` java
    permissionManager.asCreatePermission(myUser, PermissionManager.TARGET_APPLICATION, Space.class)
```

#### In Velocity Templates

While all of the above is very powerful, it's a bit complicated to deal with in a Velocity file. There is an object in the default velocity context called `$permissionHelper` which has a bunch of useful methods on it. All the methods do pretty much what you'd expect them to do, so I'll just link to the <a href="https://docs.atlassian.com/atlassian-confluence/latest-server/com/atlassian/confluence/security/PermissionHelper.html" class="external-link">Javadoc</a> and give a simple example:

``` xml
   #if ($permissionHelper.canEdit($remoteUser, $action.page))
       <b>You have Edit Permission for this Page</b>
   #end
```