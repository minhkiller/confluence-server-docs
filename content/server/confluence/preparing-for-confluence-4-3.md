---
aliases:
- /server/confluence/preparing-for-confluence-4.3-10422088.html
- /server/confluence/preparing-for-confluence-4.3-10422088.md
category: devguide
confluence_id: 10422088
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=10422088
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=10422088
date: '2017-12-08'
legacy_title: Preparing for Confluence 4.3
platform: server
product: confluence
subcategory: development resources
title: Preparing for Confluence 4.3
---
# Preparing for Confluence 4.3

This documentation is intended for Confluence plugin developers who want to ensure that their existing plugins and add-ons are compatible with Confluence 4.3.

## What's changing in Confluence 4.3?

### Changes to ContentUiSupport

In Confluence 4.3 and later, the ContentUiSupport interface has a new implementation that supports rendering attachment icons as well. This is now the preferred way to render all icons in server side code.

This change required the ContentUiSupport class to accept a more generic ConfluenceEntityObject instead of its more specific subclass ContentEntityObject. For plugin developers, this means you will need to recompile your code against Confluence 4.3, if any of the following is true:

-   Your code calls any of these methods:

    ``` java
    public String getContentTypeI18NKey(com.atlassian.confluence.core.ContentEntityObject)
    public String getIconCssClass(com.atlassian.confluence.core.ContentEntityObject)
    public String getIconPath(com.atlassian.confluence.core.ContentEntityObject, int)
    ```

-   You have implemented a class that extends the ContentUiSupport class.

No changes to your source code should be required.

### Changes to Notification ContentEntityObject

As of Confluence 4.3, the following method has been removed:

``` java
String[] getDropWatch(com.atlassian.confluence.mail.notification.listeners.NotificationData)
```

This method was used prior to 4.3 to provide the text and href of the link in notification emails that would "drop the watch" on the content that the notification referenced. That link is now provided by a WebItem with section key "email.footer.links.right". To include the drop-watch link in a custom-rendered email message, retrieve it with webInterfaceManager.getDisplayableItems("email.footer.links.right", context) where context is the Map of the email Velocity render-context.

### Removal of PermissionSetter

The <a href="http://docs.atlassian.com/atlassian-confluence/4.2.5/com/atlassian/confluence/util/actions/PermissionSetter.html" class="external-link">PermissionSetter</a> class has been removed in **Confluence 4.2.6** and later.

Reason for removal: Confluence 4.2.6 and later have a new feature allowing administrators to set the default permissions for a space. (See <a href="https://jira.atlassian.com/browse/CONF-2559" class="external-link">CONF-2559</a>.) We therefore added a new method to the <a href="http://docs.atlassian.com/atlassian-confluence/4.2.6/com/atlassian/confluence/spaces/SpaceManager.html" class="external-link">SpaceManager</a> for creating a global space with no permissions (createPrivateSpace) and we modified the existing method to apply the default space permissions configured in the global admininstration screen (createSpace). There are equivalent methods for creating personal spaces too.

By the time you have a reference to the newly created space, it will already have the relevant permissions applied, making the PermissionSetter obsolete. Note also that PermissionSetter had no way of removing permissions that already exist.

You will need to adjust your code to call the appropriate SpaceManager methods, then modify the space permissions further via calls to the SpacePermissionManager directly. We recommend adding support for space permission defaults as well, so your users can benefit from the easy administration this provides.

### Change to the page template storage format

Prior to Confluence 4.3, page templates were stored in wiki markup format. Starting with Confluence 4.3, page templates are stored in the <a href="https://confluence.atlassian.com/display/DOC/Confluence+Storage+Format" class="external-link">Confluence XHTML-based storage format</a>.

Reason for change: Confluence 4.3 and later now use the rich text editor for editing page templates, requiring the storage format for templates to change to the XHTML-based storage format.

## What's new in Confluence 4.3?

### Experimental API for in-app notifications and tasks

Confluence 4.3 introduces the workbox, for managing notifications and tasks in the app. We have an API available that will be released as *experimental*, wanting your feedback. Here is your chance to help us shape the workbox notifications and tasks API.

Resources to help you get started:

-   <a href="http://docs.atlassian.com/mywork-confluence-host-plugin/REST/mywork-confluence-host-plugin-0.2.11/" class="external-link">Confluence Notifications and Tasks REST API</a>
-   <a href="http://docs.atlassian.com/mywork-api/1.0.1/" class="external-link">Confluence Notifications and Tasks Java API</a>
-   <a href="https://bitbucket.org/cofarrell/mywork-example" class="external-link">A sample project using notifications and tasks</a> to help you get started
-   [Posting Notifications in Confluence](/server/confluence/posting-notifications-in-confluence)

We want your feedback! You can provide feedback for the API by raising an issue in the <a href="https://jira.atlassian.com/browse/CONF" class="external-link">Confluence project on jira.atlassian.com</a>.

### Confluence Mobile

Confluence 4.3 introduces a brand new mobile interface for iOS devices. From Confluence 4.3.2 and onwards, plugin developers can update their macros to render in this new mobile-friendly view. For more information, see our tutorial for [Making your macro render in Confluence mobile](/server/confluence/making-your-macro-render-in-confluence-mobile).

## What other resources do we provide?

Please watch the <a href="http://confluence.atlassian.com/display/DOC/Development+Releases" class="external-link">Development Releases</a> page for pre-release versions of Confluence 4.3 that you can use for testing purposes.
