---
aliases:
- /server/confluence/2031793.html
- /server/confluence/2031793.md
category: devguide
confluence_id: 2031793
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031793
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031793
date: '2017-12-08'
legacy_title: How do I convert wiki text to HTML?
platform: server
product: confluence
subcategory: faq
title: How do I convert wiki text to HTML?
---
# How do I convert wiki text to HTML?

### <img src="http://confluence.atlassian.com/images/icons/favicon.png" class="confluence-external-resource" /> How do I convert wiki text to HTML?

This depends on where you want to do it:

#### In a macro...

You will need the <a href="http://www.atlassian.com/software/confluence/docs/api/latest/com/atlassian/renderer/v2/SubRenderer.html" class="external-link">SubRenderer</a> (see [how to retrieve it](/server/confluence/how-do-i-get-a-reference-to-a-component)).

The SubRenderer has two `render` methods: one that allows you to specify a specific <a href="http://www.atlassian.com/software/confluence/docs/api/latest/com/atlassian/renderer/v2/RenderMode.html" class="external-link">RenderMode</a> for the rendered content, and another that uses the current RenderMode from the <a href="http://www.atlassian.com/software/confluence/docs/api/latest/com/atlassian/renderer/v2/RenderContext.html" class="external-link">RenderContext</a>.

{{% note %}}

If you just want the body of your macro rendered, you can have this done for you by the macro subsystem by having your macro's `getBodyRenderMode` method return the appropriate RenderMode.

{{% /note %}}

#### In some other component...

You will need the <a href="http://www.atlassian.com/software/confluence/docs/api/latest/com/atlassian/renderer/WikiStyleRenderer.html" class="external-link">WikiStyleRenderer</a> (see <a href="http://confluence.atlassian.com/pages/viewpage.action?pageId=157440" class="external-link">how to retrieve a component</a>).

The WikiStyleRenderer has a `convertWikiToHtml` method that takes the wiki text you wish to convert, and a <a href="http://www.atlassian.com/software/confluence/docs/api/latest/com/atlassian/renderer/v2/RenderContext.html" class="external-link">RenderContext</a>. If you are converting the text in the context of some ContentEntityObject (for example within a page or blog post), then you can call `contentEntityObject.toPageContext()` to retrieve its RenderContext. Otherwise pass in a `new PageContext()`.
