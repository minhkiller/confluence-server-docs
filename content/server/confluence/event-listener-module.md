---
aliases:
- /server/confluence/event-listener-module-2031834.html
- /server/confluence/event-listener-module-2031834.md
category: reference
confluence_id: 2031834
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031834
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031834
date: '2017-12-08'
legacy_title: Event Listener Module
platform: server
product: confluence
subcategory: modules
title: Event Listener module
---
# Event Listener module

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Available:</p></td>
<td><p>Confluence 1.4 and later</p></td>
</tr>
<tr class="even">
<td><p>Changed:</p></td>
<td><p>In Confluence 3.3 and later, Confluence events are annotation-based. This means that you have two options when writing event listeners. Option 1 is the Event Listener plugin module, using the <code>com.atlassian.event.EventListener</code> class. Option 2 is to declare your listener as a <a href="/server/confluence/component-module">component</a> and use annotation-based event listeners, annotating your methods to be called for specific event types. The component will need to register itself at the time it gets access to the EventPublisher, typically in the constructor. More information and examples are below.</p></td>
</tr>
</tbody>
</table>

Every time something important happens within Confluence (a page is added or modified, the configuration is changed, etc.), an 'event' is triggered. Listeners allow you to extend Confluence by installing code that responds to those events.

{{% note %}}

Plugin Events

It is possible to listen for plugin install/uninstall/enable/disable events, however this will be unreliable when trying to listen for events about your own plugin. You will *not* receive a PluginDisableEvent or PluginUninstallEvent for the plugin itself. To trigger actions for these events, one (or more) of your modules (macro, event listener, etc.) should implement the [Making your Plugin Modules State Aware](/server/confluence/making-your-plugin-modules-state-aware) interface instead.

{{% /note %}}{{% note %}}

Synchronous Events

Confluence events are currently processed synchronously. That is, Confluence will wait for your event to finish processing before returning from the method that was the source of the event. This makes it **very important** that any event listener you write completes as quickly as possible.

{{% /note %}}

## Adding a Listener Plugin

Listeners are a kind of Confluence plugin module.

-   For more information about plugins in general, read [Confluence Plugin Guide](/server/confluence/confluence-plugin-guide).
-   To learn how to install and configure plugins (including macros), read <a href="#installing-a-plugin" class="unresolved">Installing a Plugin</a>.
-   For an introduction to writing your own plugins, read [Writing Confluence Plugins](/server/confluence/writing-confluence-plugins)

## Option 1. Annotation Based Event Listeners

{{% note %}}

Events 2.0

From **Confluence 3.3** you can take advantage of annotation-based event listeners.

{{% /note %}}

Using annotation-based event listeners allows you to annotate methods to be called for specific event types. The annotated methods must take a single parameter specifying the type of event that should trigger the method.

You must also register the class with the `EventPublisher`, which can be injected to the listener.

INFO: As an annotation-based event listener is not required to implement the `com.atlassian.event.EventListener` interface, you cannot use the `listener` module descriptor.

In order to use the annotation-based event listeners you must register your listener as a [component](/server/confluence/component-module). For example:

``` xml
<atlassian-plugin key="listenerExample" name="Listener Examples" plugins-version="2">
    <plugin-info>
        <description>${project.description}</description>
        <version>${project.version}</version>
        <vendor name="${project.organization.name}" url="${project.organization.url}" />
    </plugin-info>

    <component name="Annotated Event Listener" class="com.atlassian.confluence.plugin.example.listener.AnnotatedListener" key="annotatedListener"/>

</atlassian-plugin>
```

See the example code for how to implement this listener: [Annotation Based Event Listener Example](/server/confluence/annotation-based-event-listener-example)

## Option 2. The Listener Plugin Module

### The Listener Plugin XML

Each listener is a plugin module of type 'listener', packaged with whatever Java classes and other resources the listener requires in order to run. Here is an example `atlassian-plugin.xml` file containing a single listener:

``` xml
<atlassian-plugin name='Optional Listeners' key='confluence.extra.auditor'>
    <plugin-info>
        <description>Audit Logging</description>
        <vendor name="Atlassian Software Systems" url="http://www.atlassian.com"/>
        <version>1.0</version>
    </plugin-info>

    <listener name='Audit Log Listener' class='com.example.listener.AuditListener'
              key='auditListener'>
        <description>Provides an audit log for each event within Confluence.</description>
    </listener>
</atlassian-plugin>
```

The listener module definition has no configuration requirements beyond any other module: just give it a name, a key, and provide the name of the class that implements the listener.

### The Listener Class

The **class** attribute of the listener module definition must refer to a Java class that implements the `com.atlassian.confluence.event.EventListener` interface. This is the interface:

``` java
package com.atlassian.confluence.event;

import com.atlassian.confluence.event.events.ConfluenceEvent;

/**
 * Defines a listener for Confluence events.
 */
public interface EventListener
{
    /**
     * Perform some action as a response to a Confluence event. The EventManager will
     * ensure that this is only called if the class of the event matches one of the
     * classes returned by getHandledEventClasses
     *
     * @param event some event triggered within Confluence
     */
    void handleEvent(ConfluenceEvent event);

    /**
     * Determine which event classes this listener is interested in.
     * 
     * The EventManager performs rudimentary filtering of events by their class. If
     * you want to receive only a subset of events passing through the system, return
     * an array of the Classes you wish to listen for from this method.
     *
     * For the sake of efficiency, only exact class matches are performed. Sub/superclassing
     * is not taken into account.
     *
     * Returning an empty array will allow you to receive every event.
     *
     * @return An array of the event classes that this event listener is interested in,
     *         or an empty array if the listener should receive all events. <b>Must not</b>
     *         return null.
     */
    Class[] getHandledEventClasses();
}
```

A more detailed example, with sample code, can be found in [Writing an Event Listener Plugin Module](/server/confluence/writing-an-event-listener-plugin-module).

## Events and Event Types

All events within Confluence extend from <a href="https://docs.atlassian.com/confluence/6.4.2/com/atlassian/confluence/event/events/ConfluenceEvent.html" class="external-link">com.atlassian.com.event.events.ConfluenceEvent</a>. In general, we use the following convention for naming each type of ConfluenceEvent:

> &lt;Object&gt;&lt;Operation&gt;Event

For example, we have the following event types relating to space events: `SpaceCreateEvent`, `SpaceUpdateEvent`, `SpaceRemoveEvent`. In the above description *space* would correspond to &lt;Object&gt; and *create, update,* or *remove* would correspond to &lt;Operation&gt;.

Occasionally, an operation is so singular that its meaning will be obvious without use of this naming convention; for example a **LoginEvent** or **ConfigurationEvent**.

##### Limitations of Events

-   Events are a notification that something has occurred. The event system is not designed to allow a listener to veto the action that caused the event.
-   There is no loop detection. If you write a listener for the SpaceModifiedEvent that itself causes a SpaceModifiedEvent to be generated, you are responsible for preventing the ensuing infinite loop.
