---
aliases:
- /server/confluence/writing-an-space-index-page-for-a-content-type-17563743.html
- /server/confluence/writing-an-space-index-page-for-a-content-type-17563743.md
category: devguide
confluence_id: 17563743
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=17563743
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=17563743
date: '2017-12-08'
legacy_title: Writing an space index page for a content type
platform: server
product: confluence
subcategory: learning
title: Writing an space index page for a content type
---
# Writing an space index page for a content type

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Applicable:</p></td>
<td><p>This tutorial applies to <strong>Confluence 5.0.</strong></p></td>
</tr>
<tr class="even">
<td><p>Level of experience:</p></td>
<td><p>This is an <strong>intermediate</strong> tutorial. You should have completed at least one beginner tutorial before working through this tutorial. See the<a href="https://developer.atlassian.com/display/DOCS/Tutorials"> list of developer tutorials</a>.</p></td>
</tr>
</tbody>
</table>

If your plugin provides another content type than Pages or Blog, it is probably useful to build an index page for it.

{{% note %}}

This tutorial isn't about Blueprints

Index pages are created automatically for Blueprints.

Example: A Space Admin creates a template, then a user presses Create and chooses the template. The template is created and its parent page is an Index Page.

Templates do not generate a custom content type: They are simple Pages. As opposed to this, a plugin managing room reservations could want to display an index page for the bookings. In our example, we will name it "Custom Content".

{{% /note %}}

## Preparation

The example below is available on the following BitBucket repository:

<a href="https://bitbucket.org/atlassian_tutorial/space-plugin-example" class="uri external-link">bitbucket.org/atlassian_tutorial/space-plugin-example</a>

You need to create a Confluence plugin and update the version of Confluence to 5.0-beta5.

## Declaring the link

The link in the sidebar can be declared in atlassian-plugin.xml:

**atlassian-plugin.xml**

``` xml
    <web-item key="plugin-content-main-link" name="Plugin Content" section="system.space.sidebar/main-links" weight="30">
        <label key="plugin.content.title"/>
        <link id="plugin-content-main-link-id">/plugins/${project.artifactId}/view-custom-content.action?key=$generalUtil.urlEncode($helper.spaceKey)</link>
        <styleClass>plugin-content-main-link</styleClass>
    </web-item>
```

## Creating an action

The link declared above needs to lead to the index page:

``` xml
    <xwork name="Example Actions" key="example-actions">
        <description>Examples of actions</description>
        <package name="space-links-xwork-package" extends="default" namespace="/plugins/${project.artifactId}">
            <default-interceptor-ref name="validatingStack"/>
            <action name="view-custom-content" class="com.atlassian.examples.MyAction" method="doViewCustomContent">
                <result name="input" type="velocity">/templates/view-custom-content.vm</result>
            </action>
        </package>
    </xwork>
```

``` java
public class MyAction extends AbstractSpaceAction
{
    public String doViewCustomContent()
    {
        return INPUT;
    }
}
```

## Writing the index page

**view-custom-content.vm**

``` xml
<html>
    <head>
        <title>$action.getText("plugin.content.title")</title>
        <meta name="decorator" content="main"/>
    </head>

    #applyDecorator("root")
        #decoratorParam("context" "space")
        #decoratorParam("mode" "collector")
        #decoratorParam("collector-key" "plugin-content-main-link") ## Highlights the link in the sidebar
        <body>
            <p>Place custom content rendering here.</p>
            <p>
                <a href="$req.contextPath/plugins/space-links/add-link.action?key=${space.key}">Go to the Space Tools screen</a>
            </p>
        </body>
    #end
</html> 
```

## Result

You can see the link in the sidebar next to Pages and Blog, and the index page on the right.

<img src="/server/confluence/images/image2013-2-7-18:17:47.png" width="500" />

### Known Bug: Gray header

<img src="/server/confluence/images/image2013-2-7-18:19:59.png" class="confluence-thumbnail" width="300" />

The header is gray on Confluence 5.0-beta5. If you use \#decoratorParam("mode" "collector") in the template, the header will automatically come with the right color in Confluence 5.0. If you need to use another mode, add this declaration to your css:

``` css
.(your-own-mode)-mode.spacetools.with-space-sidebar #main-header {
    background-color: transparent;
}
```

