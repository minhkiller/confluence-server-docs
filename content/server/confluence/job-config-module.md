---
aliases:
- /server/confluence/job-config-module-40000535.html
- /server/confluence/job-config-module-40000535.md
category: reference
confluence_id: 40000535
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=40000535
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=40000535
date: '2017-12-08'
legacy_title: Job Config Module
platform: server
product: confluence
subcategory: modules
title: Job Config module
---
# Job Config module

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Available:</p></td>
<td><p>Confluence 5.10</p></td>
</tr>
</tbody>
</table>

Job Config module defines an atlassian-scheduler `JobConfig` within a plugin. Atlassian Scheduler is used for creating scheduled tasks in Atlassian applications.

-   For more information about plugins in general, read [Confluence Plugin Guide](/server/confluence/confluence-plugin-guide).
-   To learn how to install and configure plugins (including macros), read [Installing an add-on](https://developer.atlassian.com/display/MARKET/Installing+an+add-on).
-   For an introduction to writing your own plugins, read [Writing Confluence Plugins](/server/confluence/writing-confluence-plugins)

## Job Config Module

Defines an atlassian-scheduler `JobConfig` within a plugin.  Here's an example:

``` xml
<component key="myJobRunner" class="my.JobRunner"/>

<job-config name="My job" key="myJobId">
    <job key="myJobRunner" perClusterJob="true" clusteredOnly="true"/>
    <schedule cron-expression="0 * * * * ?" jitterSecs="10"/>
    <managed editable="true" keepingHistory="true" canRunAdhoc="true" canDisable="true"/>
</job-config>
```

Job runner classes must implement `JobRunner`.

## Configuration

### `job-config`

-   the **name** represents how this job config will be referred to in the Confluence interface.
-   the** key** represents the internal, system name for the job.

### `job`

-   the **key** points to the `JobRunner` component.
-   when **perClusterJob** is true, this job will only run once in a cluster rather than on every node.
-   when **clusteredOnly** is true this job won't be scheduled in non-clustered environment. This is optional.

### `schedule`

-   either a **cron-expression** or **repeat-interval **must be present.
-   **repeat-interval **allows you to use an interval, instead of a **cron-expression**.
    -   This example repeats every hour (3600000 milliseconds) and only 5 times:

        ``` xml
        <schedule repeat-interval="3600000" repeat-count="5"/>
        ```

    -   **repeat-count** is optional when **repeat-interval** is used.
    -   to repeat indefinitely, set **repeat-count** to `-1` or omit it
    -   to run the job only once set **repeat-count** to `0`.

-   **jitterSecs** (optional) introduces some random jitter.  This is only for Confluence Cloud. It prevents many thousands of servers from attempting to do similar things all at once, by providing a jitter value less than the size of the schedule frequency, we spread the load caused by a process over time.

### `managed`

-   **managed** is optional. If this is omitted the job won't appear in the <a href="https://confluence.atlassian.com/display/DOC/Scheduled+Jobs" class="external-link">Scheduled Jobs</a> administration page in Confluence.
-   If any of the following attributes are omitted, their values are assumed to be `false`: 
    -   when true, **editable** allows the job's schedule can be edited. This is forced to false if schedule type is interval.
    -   when true, **keepingHistory** means the job's history persists and survives server restarts. If false the job's history is only stored in memory and will be lost on the next server restart.
    -   when true, **canRunAdhoc** allows the job to be executed manually on the Scheduled Jobs administration screen.
    -   when true, **canDisable** allows the job to be enabled or disabled on the Scheduled Jobs administration screen.

## Migration from [&lt;job&gt;](/server/confluence/job-module) and [&lt;trigger&gt;](/server/confluence/trigger-module)

-   For ease of use (see CONF-20162), there's nothing to replace `<job>`. Use `<component>` as above instead.
-   The XML of `<job-config>` is similar to `<trigger>` to make the migration easier.
-   For managed jobs, the `key` of `<job-config>` is used to lookup the i18n job name (`scheduledjob.desc.<key>`), and display it in the Confluence admin UI. Previously it was the `key` of `<job>.`





























































































































































































































































































































