---
aliases:
- /server/confluence/adding-a-custom-action-to-confluence-2818704.html
- /server/confluence/adding-a-custom-action-to-confluence-2818704.md
category: devguide
confluence_id: 2818704
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2818704
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2818704
date: '2017-12-08'
legacy_title: Adding a Custom Action to Confluence
platform: server
product: confluence
subcategory: learning
title: Adding a custom action to Confluence
---
# Adding a custom action to Confluence

|                       |                 |
|-----------------------|-----------------|
| Level of experience   | BEGINNER        |
| Time estimate         | 0:30            |
| Atlassian application | CONFLUENCE 4.X+ |

## Tutorial overview

This tutorial shows you how to create a plugin to add a custom action to Confluence. The custom action adds a "draft" label to a Confluence page accessible from the **Tools** drop-down menu. 

![Finished custom action in Tools menu](/server/confluence/images/image2013-7-23-14:39:28.png "Finished custom action in Tools menu")

**Concepts covered in this tutorial:**

-   Creating a plugin skeleton for Confluence
-   Adding a `Web Item` and `XWork` module to your plugin
-   The Confluence `PageAware, Label, Labelable` and `LabelManger` interfaces

{{% note %}}

About these instructions

You can use any supported combination of OS and IDE to create this plugin. These instructions were written using Eclipse Indigo on Mac OS X. If you are using another OS or IDE combination, you should use the equivalent operations for your specific environment.

This tutorial was last tested with **Confluence 5.1.4**. 

{{% /note %}}

### Prerequisite knowledge

To complete this tutorial, you should understand basic Java development - classes, interfaces, methods, and how to use the compiler.

### Plugin Source

We encourage you to work through this tutorial. If you want to skip ahead or check your work when you are done, you can find the plugin source code on Atlassian Bitbucket. Bitbucket serves a public Git repository containing the tutorial's code. To clone the repository, issue the following command:

``` bash
$ git clone git@bitbucket.org:atlassian_tutorial/adding-a-custom-action-to-confluence.git
```

Alternatively, you can download the source using the **Downloads** page here: https://bitbucket.org/atlassian_tutorial/confluence-custom-action-tutorial/downloads

## Step 1. Create and prune the plugin skeleton

In this step, you'll create a plugin skeleton using `atlas-` commands. Since you won't need some of the files created in the skeleton, you'll also delete them in this step.

1.  Open a terminal and navigate to your Eclipse workspace directory.  
      
2.  Enter the following command to create a Confluence plugin skeleton:

    ``` bash
    $ atlas-create-confluence-plugin
    ```

3.  When prompted, enter the following information to identify your plugin:

    <table>
    <tbody>
    <tr class="odd">
    <td>group-id</td>
    <td><code>com.atlassian.plugins.tutorial</code><br />
    </td>
    </tr>
    <tr class="even">
    <td>artifact-id</td>
    <td><code>AddDraftLabelAction</code></td>
    </tr>
    <tr class="odd">
    <td>version</td>
    <td><code>1.0-SNAPSHOT</code></td>
    </tr>
    <tr class="even">
    <td>package</td>
    <td><code>com.atlassian.plugins.tutorial</code></td>
    </tr>
    </tbody>
    </table>

4.  Confirm your entries when prompted with `Y` or `y`.

    Your terminal notifies you of a successful build:

    ``` bash
    [INFO] ------------------------------------------------------------------------
    [INFO] BUILD SUCCESSFUL
    [INFO] ------------------------------------------------------------------------
    [INFO] Total time: 1 minute 11 seconds
    [INFO] Finished at: Thu Jul 18 11:30:23 PDT 2013
    [INFO] Final Memory: 82M/217M
    [INFO] ------------------------------------------------------------------------
    ```

5.  Change to the project directory created by the previous step.

    ``` bash
    $ cd AddDraftLabelAction/
    ```

6.  Delete the test directories.

    Setting up testing for your macro isn't part of this tutorial. Use the following commands to delete the generated test skeleton:

    ``` bash
    $ rm -rf ./src/test/java
    $ rm -rf ./src/test/resources/
    ```

7.  Delete the unneeded Java class files.

    You'll build a single class for your custom action in later steps.

    ``` bash
    $ rm ./src/main/java/com/atlassian/plugins/tutorial/*.java
    ```

8.  Edit the `src/main/resources/atlassian-plugin.xml` file.  
      
9.  Remove the generated `myPluginComponent` component declaration.

    ``` xml
    <!-- publish our component -->
    <component key="myPluginComponent" class="com.example.plugins.tutorial.confluence.simplebp.MyPluginComponentImpl" public="true">
       <interface>com.example.plugins.tutorial.confluence.simplebp.MyPluginComponent</interface>
    </component>
    ```

10. Save and close `atlassian-plugin.xml`.

### Import your project into your IDE

1.   Make your project available to Eclipse.

    ``` bash
    atlas-mvn eclipse:eclipse
    ```

    You'll see a successful build message: 

    ``` bash
    [INFO] ------------------------------------------------------------------------
    [INFO] BUILD SUCCESSFUL
    [INFO] ------------------------------------------------------------------------
    [INFO] Total time: 54 seconds
    [INFO] Finished at: Tue Jul 16 11:03:59 PDT 2013
    [INFO] Final Memory: 82M/224M
    [INFO] ------------------------------------------------------------------------
    ```

2.  Start Eclipse.  
    You can open a new terminal window to perform this action.

    ``` bash
    $ cd ~/eclipse
    $ ./eclipse
    ```

3.  Click **File &gt; Import**.   
    Eclipse starts the **Import** wizard.  
      
4.  Expand the **General** folder tree to choose **Existing Projects into Workspace**.  
      
5.  Click **Next.**
6.  Click **Browse** and enter the root directory of your workspace.  
    Your Atlassian plugin folder should now appear under **Projects**.
7.  Ensure your plugin checkbox is ticked and click **Finish**.   
    Eclipse imports your project, and it's now visible in your Package Explorer view. 

## Step 2. Create an `AddDraftLabelAction` stub class 

You've created the plugin skeleton. Here you'll create a stub class in Java called `AddDraftLabelAction`. This class uses the existing class,  `ConfluenceActionSupport` , to leverage existing methods for managing page labels like  `LabelManager` . This class also implements  `PageAware`  interface methods to ensure your custom action only engages with current, valid pages. 

This step uses Eclipse, but you should be able to use any comparable features in your IDE.

1.  In Eclipse, locate the `com.atlassian.plugins.tutorial` package under `src/main/java`.  
      
2.  Right-click the package and choose **New &gt; Class**.  
      
3.  Name the class `AddDraftLabelAction`.  
      
4.  Under **Superclass**, choose `com.atlassian.confluence.core.ConfluenceActionSupport`.  
      
5.  Under **Interfaces**, add `com.atlassian.confluence.pages.actions.PageAware`.  
      
6.  Tick the checkboxes to create method stub code with **Constructors from superclass** and **Inherited abstract methods.**  
    <img src="/server/confluence/images/image2013-7-23-11:53:53.png" title="New AddDraftLabelAction class in Eclipse" alt="New AddDraftLabelAction class in Eclipse" width="500" />

7.  Click  **Finish.**  

    Eclipse builds `AddDraftLabelAction` with stub code.

    Here's an example of the class with stub code:

    ``` java
    package com.atlassian.plugins.tutorial;

    import com.atlassian.confluence.core.ConfluenceActionSupport;
    import com.atlassian.confluence.pages.AbstractPage;
    import com.atlassian.confluence.pages.actions.PageAware;


    public class AddDraftLabelAction extends ConfluenceActionSupport implements
            PageAware {

        public AddDraftLabelAction() {
            // TODO Auto-generated constructor stub
        }


        @Override
        public AbstractPage getPage() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public boolean isLatestVersionRequired() {
            // TODO Auto-generated method stub
            return false;
        }


        @Override
        public boolean isPageRequired() {
            // TODO Auto-generated method stub
            return false;
        }

        @Override
        public boolean isViewPermissionRequired() {
            // TODO Auto-generated method stub
            return false;
        }


        @Override
        public void setPage(AbstractPage arg0) {
            // TODO Auto-generated method stub

        }


    }
    ```

## Step 3. Modify the stub code and add an `execute` method

You've built the foundation for your class, now you'll flesh out the stub code. Your class foundation is built from `ConfluenceActionSupport` and `PageAware`, so it inherits methods that deal with validating or identifying a specific Confluence page. Here you'll add additional interfaces - `Label, Labelable,` and `LabelManager`- so that your custom action can add a label to a page.

1.  Add the `Label, Labelable,` and `LabelManager` interfaces to your class.

    These interfaces provide access to methods and fields for managing and creating labels in Confluence. 

    ``` java
    import com.atlassian.confluence.labels.Label;
    import com.atlassian.confluence.labels.LabelManager;
    import com.atlassian.confluence.labels.Labelable;
    ```

2.  Add `page` and `labelManager` as instance variables.

    ``` java
    private AbstractPage page;
    private LabelManager labelManager;
    ```

3.  Complete the stub code for `getPage()` and `setPage()`.

    `getPage()` obtains the current Confluence page, and sets the `return` to `page`. With `page` defined, `setPage()` defines which page your custom action should interact with.  

    ``` java
        @Override
        public AbstractPage getPage() {
            return page;
        }
        
        public void setPage(AbstractPage page)
        {
            this.page = page;
        }
    ```

4.  Modify the stub code to `return true` for methods with a boolean `return`.

    These methods are all inherited from the `PageAware` interface. Now your custom action will only execute if the Confluence page is within the scope of user permissions, is defined as a page, and is the most recent version.

    ``` java
        @Override
        public boolean isLatestVersionRequired() 
        {
            return true;
        }

        @Override
        public boolean isPageRequired() 
        {
            return true;
        }


        @Override
        public boolean isViewPermissionRequired() 
        {
            return true;
        }
    ```

5.  Invoke the `setLabelManager` method.

    ``` java
        public void setLabelManager(LabelManager labelManager)
        {
            this.labelManager = labelManager;
        }
    ```

6.  Add the `execute` method to instantiate a new draft label.

    Your `execute` method returns the "success" message as a `String`, which your `XWork` module in the `atlassian-plugin.xml` file uses to trigger a page refresh.

    ``` java
        public String execute()
        {
            Label label = new Label("draft");
            labelManager.addLabel(page, label);
            return "success";
        }
    ```

      

7.  Save your changes.

    Here's what your finalized `AddDraftLabelAction.java` file should look like:

    ``` java
    package com.atlassian.plugins.tutorial;

    import com.atlassian.confluence.core.ConfluenceActionSupport;
    import com.atlassian.confluence.pages.AbstractPage;
    import com.atlassian.confluence.pages.actions.PageAware;
    import com.atlassian.confluence.labels.Label;
    import com.atlassian.confluence.labels.LabelManager;
    import com.atlassian.confluence.labels.Labelable;

    public class AddDraftLabelAction extends ConfluenceActionSupport implements
            PageAware {

        private AbstractPage page;
        private LabelManager labelManager;

        public AddDraftLabelAction() {
            // TODO Auto-generated constructor stub
        }
     
        @Override
        public AbstractPage getPage() 
        {
            return page;
        }
        
        public void setPage(AbstractPage page)
        {
            this.page = page;
        }

        @Override
        public boolean isLatestVersionRequired() 
        {
            return true;
        }

        @Override
        public boolean isPageRequired() 
        {
            return true;
        }

        @Override
        public boolean isViewPermissionRequired() 
        {
            return true;
        }
        
        public void setLabelManager(LabelManager labelManager)
        {
            this.labelManager = labelManager;       
        }
     
        public String execute()
        {
            Label label = new Label("draft");
            labelManager.addLabel(page, label);
            return "success";
        }


    }
    ```

## Step 4. Add a `web-item`  module to the  `atlassian-plugin.xml` plugin descriptor

You've built the skeleton for your plugin and finalized a Java class. Now you'll modify the `atlassian-plugin.xml` file to describe how Confluence should interact with your code. This file bases these dependencies on your  `pom.xml` . The  `atlassian-plugin.xml`  file was generated when you created your plugin skeleton. 

In this step you'll add a `Web Item` module to your plugin. This module type allows you to customize application menus in Confluence with links to your custom action. You can add modules to Confluence with the command `atlas-create-confluence-module`, but in this particular case it's more convenient to simply modify the `atlassian-plugin.xml` file directly. 

1.  In your IDE, open `atlassian-plugin.xml`.  
    This file is located under `src/main/resources`.  
      
2.  Locate the closing `</plugin-info`&gt; tag.   
      
3.  Add the following directly after the closing `</plugin-info>` tag.

    This section defines the `<label>` users will see in the **Tools** menu, and when the custom action is accessible ( `system.content.action/primary`). There are four sections in the **Tools **menu: primary, marker, secondary and modify. Designating your plugin's location as primary will place it at the top of the menu.

    ``` xml
        <web-item name="add-draft-label-action-web-ui" key="add-draft-label-action-web-ui" section="system.content.action/primary" weight="10">
            <description key="item.add-draft-label-action-web-ui.link.desc">Adds the "Add 'draft' label" action into the Tools menu.</description>
            <icon width="16" height="16">
              <link linkId="add-draft-label-action-web-ui-icon">/images/icons/pluginIcon.png</link>
            </icon>
            <label key="Add ''draft'' label"/>
            <link linkId="add-draft-label-action">/plugins/add-draft-label/add-label.action?pageId=$page.id</link>
        </web-item>
    ```

4.  Save your changes.

## Step 5. Add a `XWork-Webwork` module to `atlassian-plugin.xml`

Now, you'll add an XWork-Webwork module. Your `Web Item` component module defines the label and placement of the custom action. In this step you'll define what the custom action should *do* within Confluence using an `XWork-Webwork` module. The `xwork` module will rely on your Java class, `AddDraftLabelAction`. This module informs Confluence to reference your Java class to execute on the page. After the draft label is applied, `ApplyDraftLabelAction.java` returns a String literal value of "success". This module then uses that message to trigger a "redirect" to the current page; which is the same as a refresh.

1.  In `atlassian-plugin.xml` locate the closing tag for your newly-added `</web-item>`.  
      
2.  Add the following on the line after `</web-item>`.

    ``` xml
        <xwork name="add-draft-label-action-xwork" key="add-draft-label-action-xwork">
            <description key="item.add-draft-label-action-xwork.link.desc">Defines what the "Add 'draft' label" action does.</description>
            <package name="add-draft-label" extends="default" namespace="/plugins/add-draft-label">
                <default-interceptor-ref name="defaultStack"/>
                <action name="add-label" class="com.atlassian.plugins.tutorial.AddDraftLabelAction">
                    <result name="success" type="redirect">${page.urlPath}</result>
                </action>
            </package>
        </xwork>
    ```

3.  Save your changes and close the file.  
      
4.  Update your project changes. If you're an Eclipse user, your directions are as follows:

    ``` bash
    $ atlas-mvn eclipse:eclipse
    ```

Here's an example of the complete `atlassian-plugin.xml`:

``` xml
<atlassian-plugin key="${project.groupId}.${project.artifactId}" name="${project.name}" plugins-version="2">
    <plugin-info>
        <description>${project.description}</description>
        <version>${project.version}</version>
        <vendor name="${project.organization.name}" url="${project.organization.url}" />
        <param name="plugin-icon">images/pluginIcon.png</param>
        <param name="plugin-logo">images/pluginLogo.png</param>
    </plugin-info>
    
    <web-item name="add-draft-label-action-web-ui" key="add-draft-label-action-web-ui" section="system.content.action/primary" weight="10">
        <description key="item.add-draft-label-action-web-ui.link.desc">Adds the "Add 'draft' label" action into the Tools menu.</description>
        <label key="Add ''draft'' label"/>
        <link linkId="add-draft-label-action">/plugins/add-draft-label/add-label.action?pageId=$page.id</link>
    </web-item>
    
     <xwork name="add-draft-label-action-xwork" key="add-draft-label-action-xwork">
        <description key="item.add-draft-label-action-xwork.link.desc">Defines what the "Add 'draft' label" action does.</description>
        <package name="add-draft-label" extends="default" namespace="/plugins/add-draft-label">
            <default-interceptor-ref name="defaultStack"/>
            <action name="add-label" class="com.atlassian.plugins.tutorial.AddDraftLabelAction">
                <result name="success" type="redirect">${page.urlPath}</result>
            </action>
        </package>
    </xwork>   
    
    <!-- add our i18n resource -->
    <resource type="i18n" name="i18n" location="AddDraftLabelAction"/>
    
    <!-- add our web resources -->
    <web-resource key="AddDraftLabelAction-resources" name="AddDraftLabelAction Web Resources">
        <dependency>com.atlassian.auiplugin:ajs</dependency>
        
        <resource type="download" name="AddDraftLabelAction.css" location="/css/AddDraftLabelAction.css"/>
        <resource type="download" name="AddDraftLabelAction.js" location="/js/AddDraftLabelAction.js"/>
        <resource type="download" name="images/" location="/images"/>
        <context>AddDraftLabelAction</context>
    </web-resource>
    
    
    <!-- import from the product container -->
    <component-import key="applicationProperties" interface="com.atlassian.sal.api.ApplicationProperties" />
    
</atlassian-plugin>
```

## Step 6. Build, install, and run your plugin

In this step you'll install your plugin and run Confluence. Then, to test your custom action, you'll add a page and add a "draft" label to the page from the **Tools** menu.

1.  Open a terminal window and navigate to the plugin root folder. 

    ``` bash
    $ cd AddDraftLabelAction
    ```

2.  Start up Confluence from your project root using the `atlas-run` command.

    ``` bash
    atlas-run
    ```

    This command builds your plugin code, starts a Confluence instance, and installs your plugin. This may take a few minutes.

3.  Locate the URL for Confluence.  
    Your terminal outputs the location of your local Confluence instance, defaulted to <a href="http://localhost:1990/confluence" class="uri external-link">http://localhost:1990/confluence</a>.

    ``` bash
    [INFO] confluence started successfully in 71s at http://localhost:1990/confluence
    [INFO] Type CTRL-D to shutdown gracefully
    [INFO] Type CTRL-C to exit
    ```

4.  Open your browser and navigate to your local Confluence instance.  
      
5.  Login with `admin` / `admin`.  
      
6.  Click **Create**.  
      
7.  Choose **Blank Page** and click **Create**.  
      
8.  Add some content and click **Save**.  
      
9.  Click **Tools** &gt; **Add 'draft' label**.  
    <img src="/server/confluence/images/image2013-7-23-13:32:55.png" width="500" />
10. Verify that the 'draft' label was applied.  
    <img src="/server/confluence/images/confirm-label-applied.png" title="Confirm the label was applied" alt="Confirm the label was applied" width="500" />
