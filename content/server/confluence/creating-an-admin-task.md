---
aliases:
- /server/confluence/creating-an-admin-task-16974311.html
- /server/confluence/creating-an-admin-task-16974311.md
category: devguide
confluence_id: 16974311
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=16974311
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=16974311
date: '2017-12-08'
legacy_title: Creating an Admin Task
platform: server
product: confluence
subcategory: learning
title: Creating an admin task
---
# Creating an admin task

{{% note %}}

This is a Confluence 5.0 feature. See [Preparing for Confluence 5.0](/server/confluence/preparing-for-confluence-5-0) for details.

{{% /note %}}

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Applicable:</p></td>
<td><p>This tutorial applies to <strong>Confluence 5.0</strong>.</p></td>
</tr>
<tr class="even">
<td><p>Level of experience:</p></td>
<td><p><strong>Intermediate</strong>. Our tutorials are classified as 'beginner', 'intermediate' and 'advanced'. This one is at 'intermediate' level. If you have never developed a plugin before, you may find this one a bit difficult.</p></td>
</tr>
</tbody>
</table>

An Admin Task is visible on the console when the admin clicks the cog, then Administration:

<img src="/server/confluence/images/image2013-1-10-17:29:8.png" class="confluence-thumbnail" width="100" />

<img src="/server/confluence/images/image2013-1-10-17:29:57.png" class="image-center" width="500" />

## Required Knowledge

If you are unfamiliar with plugin development, we recommend that you read the guide to developing with the Atlassian Plugin SDK before you read this document. You may also want to read [Common Coding Tasks](https://developer.atlassian.com/display/DOCS/Common+Coding+Tasks) for an overview of the plugin framework. Some experience with Maven is assumed for plugin development. If you haven't used Maven before, you may want to read the <a href="http://maven.apache.org/guides/getting-started/index.html" class="external-link">Maven documentation</a> to familiarise yourself with it.

## General Information

An admin task is configured like this:

**atlassian-plugin.xml**

``` xml
    <resource type="i18n" name="i18n" location="plugin-i18n"/>

    <web-item key="plugin-admin-task" section="system.admin.tasks/general" weight="150">
        <label key="plugin.admin.task.title"/>
        <description key="plugin.admin.task.description"/>
        <link id="configure-mail">/admin/mail/viewmailservers.action</link>
        <condition class="com.atlassian.examples.DefaultMailServerExistsCondition" invert="true"/>
    </web-item>
```

{{% note %}}

Please use the &lt;condition&gt;

To ensure a smooth experience for admins, please do not surcharge the admin console. In particular, **please make use of the &lt;condition&gt;** to automatically dismiss the task as soon as it seems the users knows how to configure your plugin.

{{% /note %}}{{% note %}}

Is an Admin Task the best medium for your information?

 The most common admin task for add-ons is to let the administrator discover and configure your features. UPM has a specific plugin point for this usecase. Please look at the details for `post.install.url` and `post.update.url `on the following page: [Plugin metadata files used by UPM and Marketplace](https://developer.atlassian.com/display/MARKET/Plugin+metadata+files+used+by+UPM+and+Marketplace).

{{% /note %}}

Admin tasks have a few components:

<table>
<colgroup>
<col style="width: 15%" />
<col style="width: 35%" />
<col style="width: 15%" />
<col style="width: 35%" />
</colgroup>
<thead>
<tr class="header">
<th>Component</th>
<th>Description</th>
<th>Available for core tasks</th>
<th><p>Available for plugins</p>
<p>See example below and on <a href="/server/confluence/web-item-plugin-module">Web Item Plugin Module</a> for details about each tag</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Checkbox</td>
<td>The administrator can mark tasks as viewed, so they appear in &quot;View all completed tasks&quot;. Some tasks should not be dismissed and have no checkbox.</td>
<td>Yes. Optional</td>
<td>Yes. Mandatory</td>
</tr>
<tr class="even">
<td>Title</td>
<td>Bold line of text.</td>
<td>Yes</td>
<td>Yes. Use &lt;label&gt;</td>
</tr>
<tr class="odd">
<td>Description</td>
<td>Gray text under the title.</td>
<td>Yes</td>
<td>Yes. Use &lt;description&gt;</td>
</tr>
<tr class="even">
<td>Current Value</td>
<td>Some tasks are associated to one or several values which are displayed under the description.</td>
<td>Yes</td>
<td>No. Plugins can't display the current value of the task.</td>
</tr>
<tr class="odd">
<td>Configure</td>
<td>An edit icon with a link labelled &quot;Configure&quot;. The url is chosen by the plugin.</td>
<td>Yes</td>
<td>Yes. Plugins can't rename the title of the link. Alternatively, plugins can use the description to redirect to their screen and not use the link. Use &lt;link&gt;.</td>
</tr>
<tr class="even">
<td>Condition</td>
<td>The task only appears on the Admin Console if some conditions are fulfilled.</td>
<td>Yes</td>
<td>Yes. If the condition is false, the task is not displayed at all, even in &quot;completed tasks&quot;. Use &lt;condition&gt;.</td>
</tr>
</tbody>
</table>

## Download the example from BitBucket

The source for this tutorial is available on BitBucket: https://bitbucket.org/atlassianlabs/confluence-5.0-plugin-points

## Create a Confluence plugin

Create a Confluence plugin using the SDK:

``` bash
atlas-create-confluence-plugin
cd my-plugin
atlas-run
```

If you encounter issues creating the Confluence plugin, please review the Beginner tutorials and/or submit issues to <a href="https://ecosystem.atlassian.net/browse/AMPS" class="external-link">the AMPS bugtracker</a>.

## Add the WebItem for the task

In atlassian-plugin.xml, add the following item:

**atlassian-plugin.xml**

``` xml
    <resource type="i18n" name="i18n" location="plugin-i18n"/>

    <web-item key="plugin-admin-task" section="system.admin.tasks/general" weight="150">
        <label key="plugin.admin.task.title"/>
        <description key="plugin.admin.task.description"/>
        <link id="configure-mail">/admin/mail/viewmailservers.action</link>
        <condition class="com.atlassian.examples.DefaultMailServerExistsCondition" invert="true"/>
    </web-item>
```

Add the translations in plugin-i18n.properties:

**plugin-i18n.properties**

``` java
plugin.admin.task.title=Did you know you could let Confluence send e-mail notifications?
plugin.admin.task.description=The Mail Notification plugin hasn''t been configured yet. Check it out!
```

## Write a condition for your task

In the file src/main/java/com/atlassian/examples/DefaultMailServerExistsCondition.java, add the following code:

**DefaultMailServerExistsCondition.java**

``` java
public class DefaultMailServerExistsCondition implements Condition
{
    final MailServerExistsCriteria mailServerExistsCriteria;
    public DefaultMailServerExistsCondition(MailServerExistsCriteria mailServerExistsCriteria)
    {
        this.mailServerExistsCriteria = mailServerExistsCriteria;
    }
    @Override public void init(Map<String, String> params) throws PluginParseException
    {
        // Do nothing
    }
    @Override public boolean shouldDisplay(Map<String, Object> context)
    {
        return mailServerExistsCriteria.isMet();
    }
}
```

## Launch your plugin

After running atlas-run, Confluence should start with the new Admin Task. You're done!

<img src="/server/confluence/images/image2013-1-10-18:8:33.png" class="image-center" width="500" />

##### Related Content

Plugin Metadata Files used by UPM and Marketplace

[Web UI Modules](/server/confluence/web-ui-modules)<br>
[Conditions for WebItems](/server/confluence/web-item-plugin-module#conditionand-conditions-elements)
