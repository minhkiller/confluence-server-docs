---
aliases:
- /server/confluence/annotation-based-event-listener-example-2031698.html
- /server/confluence/annotation-based-event-listener-example-2031698.md
category: reference
confluence_id: 2031698
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031698
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031698
date: '2017-12-08'
legacy_title: Annotation Based Event Listener Example
platform: server
product: confluence
subcategory: modules
title: Annotation Based Event Listener example
---
# Annotation Based Event Listener example

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Available:</p></td>
<td><p>Confluence 3.3 and later</p></td>
</tr>
</tbody>
</table>

This page gives the example code for an annotation-based event listener, as described in the page about [event listener plugins](/server/confluence/event-listener-module).

The methods must be annotated with the `com.atlassian.event.api.EventListener` annotation and only take a single parameter of the type of event this method is to service.

The event listener must also register itself with the `EventPublisher` which can be injected into the constructor. The event listener will need to implement the DisposableBean interface to unregister itself when the plugin is disabled or uninstalled.

``` java
package com.atlassian.confluence.plugin.example.listener;

import com.atlassian.confluence.event.events.security.LoginEvent;
import com.atlassian.confluence.event.events.security.LogoutEvent;
import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;

public class AnnotatedListener implements DisposableBean{
    private static final Logger log = LoggerFactory.getLogger(AnnotatedListener.class);

    protected EventPublisher eventPublisher;

    public AnnotatedListener(EventPublisher eventPublisher) {
        this.eventPublisher = eventPublisher;
        eventPublisher.register(this);
    }

    @EventListener
    public void loginEvent(LoginEvent event) {
        log.error("Login Event: " + event);
    }

    @EventListener
    public void logoutEvent(LogoutEvent event) {
        log.error("Logout Event: " + event);
    }

    // Unregister the listener if the plugin is uninstalled or disabled.
    public void destroy() throws Exception
    {
        eventPublisher.unregister(this);
    }
}
```

INFO: An annotation-based event listener does not need to implement the `com.atlassian.event.EventListener` class.
