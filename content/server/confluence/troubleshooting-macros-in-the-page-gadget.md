---
aliases:
- /server/confluence/troubleshooting-macros-in-the-page-gadget-2031771.html
- /server/confluence/troubleshooting-macros-in-the-page-gadget-2031771.md
category: devguide
confluence_id: 2031771
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031771
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031771
date: '2017-12-08'
legacy_title: Troubleshooting Macros in the Page Gadget
platform: server
product: confluence
subcategory: faq
title: Troubleshooting macros in the page gadget
---
# Troubleshooting macros in the page gadget

Macros were originally designed to only be used in a Confluence instance. Rendering macros in the Confluence Page Gadget outside of a Confluence instance can result in minor quirks that require some workarounds to resolve. Please note, the workarounds described below are written for users who are confident developing in Confluence. Do not attempt any of the procedures below, if you do not have any experience in this area.

Please see the <a href="#confluence-page-gadget-documentation" class="unresolved">Confluence Page Gadget documentation</a> for the full list of working/non-working macros.

## My AJAX call isn't executing my callback

The page gadget uses a proxy to execute AJAX requests back to Confluence. This means that, in some cases, an AJAX call that previously worked in Confluence may not work in the page gadget.

If you include an error handler like this:

``` javascript
AJS.$.ajax({
url: /your/url,
type: "GET",
data: {
pageId: pageId
},
success: function(data) {
executeSuccess(data);
},
error: function(err, text) {
AJS.log('Error loading page ' + text);
}
});
```

You may see the following error:

``` bash
Failed to parse JSON
```

This generally occurs when your action returns raw html, while the page gadget expects JSON by default. To fix just add the following to the ajax call.

``` javascript
dataType : 'text',
```

###### Example Macros:

-   Page tree

## Some links are not being directed back to Confluence

When rendering the page gadget, Confluence attempts to fix all the urls so that they point directly to Confluence rather than using relative urls. However, this fix does not work for any links that are added dynamically (for example in the pagetree macro). Thus to fix this problem there is a javascript function available that will cycle through the links and fix any that have been added. So after any links are added just execute the following javascript:

``` javascript
if (AJS.PageGadget && AJS.PageGadget.contentsUpdated)
{AJS.PageGadget.contentsUpdated();
}
```

###### Examples:

-   Advanced Macros (recently updated)
-   Page tree

## The gadget isn't resizing

As in the previous section, the page gadget needs to be notified when the page has increased/decreased in size. Executing the above code will also ensure that the page content fits into the page gadget.

## I want the macro to render differently in the Page Gadget

Sometimes you would like to render a completely different view in the page gadget. To achieve this you can use the Page Gadget render type `com.atlassian.confluence.renderer.ConfluenceRenderContextOutputType#PAGE_GADGET`. This render type notifies the macro that it is being rendered in the context of a page gadget. This method is used when rendering the tasklist and gadget macros.

###### Examples:

-   Tasklist
-   Attachments Macro

### I would like to style my macro/theme differently in the Page Gadget

In the gadget iframe we have included:

``` xml
<body class="page-gadget">
... content ....
</body>
```

So if you would like to style your macro or theme specifically for the page gadget you can use the `body.page-gadget` selector.

Examples:

-   Easy Reader Theme
