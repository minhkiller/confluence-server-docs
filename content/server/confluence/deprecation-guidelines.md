---
aliases:
- /server/confluence/deprecation-guidelines-2031642.html
- /server/confluence/deprecation-guidelines-2031642.md
category: devguide
confluence_id: 2031642
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031642
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031642
date: '2017-12-08'
guides: guides
legacy_title: Deprecation Guidelines
platform: server
product: confluence
subcategory: learning
title: Deprecation guidelines
---
# Deprecation guidelines

{{% note %}}

These are guidelines related to the development of Confluence. The guidelines mainly apply to Atlassian employees, but reading them should provide insight for third-party plugin developers as well, so we decided to make them public.

{{% /note %}}

Because Confluence doesn't have an official API yet, you should assume that any change you make to manager interfaces, model objects, services, or really any commonly used code will in some way impact third-party plugin developers. As such, we should always be careful to deprecate, rather than remove old functionality.

{{% note %}}

Deprecation

All deprecated methods in Confluence MUST include, as the first thing after the `@deprecated` tag, the text "since n", where n is the version number at which the tag was added, followed by either a short explanation of why the method should not be used, or a direction to use a different method.

All deprecated methods MUST also include the `@Deprecated` annotation on the method.

{{% /note %}}

#### Rationale

Because we want to keep third-party developers happy, we should deprecate methods that may be used by plugins instead of just deleting them. **However**, deprecated methods pollute the namespace, and keeping them around indefinitely just encourages people to continue to use them.

Therefore, we should record *when* a method has been deprecated, and before each major release we should remove anything that has stayed deprecated for more than six months or two major versions (whichever is longer).

The use of the `@Deprecated` annotation also allows runtime tooling to discover and alert about deprecated method usage.

#### Examples

For a simple redirect, the deprecation tag is the only Javadoc the method should require. Developers should consult the doc for the linked alternative to find out more about what the method is likely to do:

``` java
/** @deprecated since 2.3 use {@link Space#isValidSpaceKey} */
@Deprecated
boolean isValidSpaceKey(String key);
```

For a "this is no longer the right way to do things" deprecation, a longer explanation may be required, and the old Javadoc may need to be retained for developers who are still stuck doing things the old way for some reason. A short explanation is put in the deprecated tag itself, and the detail is put in the main body of the Javadoc:

``` java
/**
* Return all the content a user has authored in this space.
*
* <b>Warning:</b> This method has been deprecated since Confluence 2.1 because it is
* insanely inefficient to do in the database. You should migrate your code to use the
* SmartListManager instead, which will get the results from Lucene.
*
* @deprecated since 2.1 use the {@link SmartListManager} for complex queries
*/
@Deprecated
List getContentInSpaceAuthoredBy(String spaceKey, String username);
```

Note that implementations of deprecated methods will result in compile warnings if they are not also marked as deprecated.

#### Fix Confluence Code to Avoid Deprecated Usage

When deprecating a class or method in Confluence, you must remove the majority - if not all - of the usages of that class or method throughout Confluence. If you don't have the time or inclination to do so, you should probably not deprecate the method.

This makes sure that our own code doesn't violate our own deprecation unnecessarily, and also provides a sanity check for whether you should deprecate something or not. If it is used too many times inside Confluence to begin changing, it could well be the same for external code and you should think hard about deprecating such a frequently used method.

#### When Not to Deprecate

In some situations, maintaining deprecated methods may be impossible. For example:

###### You should never deprecate when...

-   The underlying model has changed, rendering any client of the old code obselete. For example if you move from `Permission getPermission()` to `List getPermissions()`, the former method would return dangerously incorrect information if it were maintained, and thus should be deleted.
-   The old way of doing things is dangerous. For example, if `userManager.getAllUsers()` is being removed because it kills the server, we should not feel guilty forcing plugins to upgrade to the safe way of doing things.

###### You should make a judgement call when...

-   There would be significant effort required to maintain parallel, deprecated way of doing things for six months
-   You would be forced to write an ugly API because all the "right" method/class names are taken up with deprecated methods (assume the new way of doing things will stick around forever)
