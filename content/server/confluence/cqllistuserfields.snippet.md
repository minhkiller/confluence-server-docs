---
aliases:
- /server/confluence/-cqllistuserfields-29952562.html
- /server/confluence/-cqllistuserfields-29952562.md
category: devguide
confluence_id: 29952562
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=29952562
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=29952562
date: '2017-12-08'
legacy_title: _CQLListUserFields
platform: server
product: confluence
subcategory: other
title: _CQLListUserFields
---
# \_CQLListUserFields

-   [Creator](https://developer.atlassian.com/display/CONFDEV/Advanced+Searching+using+CQL#AdvancedSearchingusingCQL-creatorcreatorCreator)
-   [Contributor](https://developer.atlassian.com/display/CONFDEV/Advanced+Searching+using+CQL#AdvancedSearchingusingCQL-contributorcontributorContributor)
-   [Mention](https://developer.atlassian.com/display/CONFDEV/Advanced+Searching+using+CQL#AdvancedSearchingusingCQL-mentionMentionMention)
-   [Watcher](https://developer.atlassian.com/display/CONFDEV/Advanced+Searching+using+CQL#AdvancedSearchingusingCQL-Watcher)
-   [Favourite](https://developer.atlassian.com/display/CONFDEV/Advanced+Searching+using+CQL#AdvancedSearchingusingCQL-favouriteFavouriteFavourite,favorite)
