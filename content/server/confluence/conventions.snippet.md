---
aliases:
- /server/confluence/conventions-39368850.html
- /server/confluence/conventions-39368850.md
category: devguide
confluence_id: 39368850
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39368850
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39368850
date: '2017-12-08'
legacy_title: Conventions
platform: server
product: confluence
subcategory: other
title: Conventions
---
# Conventions

### Working Directory

For all of the tutorials, the working directory is called *atlas-development *and is located in the users home directory.

### Development Deployment Paradigm

The tutorials use QuickReload and expect to produce transformerless plugins.

### POM.xml

In the tutorials, there is a parent POM.xml file in the working directory.  The POM.xml file in the plugin directory has been configured to inherit from the POM.xml and all redundant entries have been removed.  Typically, only the artifact version, name, description and build instructions are included. 

### Development Environment

In order to keep things simple, the development has been done in a plain text editor, gvim.  Many find the [Eclipse IDE](/server/confluence/conventions.snippet) a more productive environment.
