---
aliases:
- /server/confluence/blueprint-module-24084732.html
- /server/confluence/blueprint-module-24084732.md
category: reference
confluence_id: 24084732
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=24084732
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=24084732
date: '2017-12-08'
legacy_title: Blueprint Module
platform: server
product: confluence
subcategory: modules
title: Blueprint module
---
# Blueprint module

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Applicable:</p></td>
<td><p>Confluence 5.1 and later</p></td>
</tr>
</tbody>
</table>

## Purpose of this module

As a plugin developer, you can use a Blueprint module to add to the Create dialog in Confluence to help users bootstrap the page creation process. Note that you need to define a `web-item` referring to a `blueprint` module for it to appear in the Create dialog. This page only documents the `blueprint` module. See [Confluence Blueprints](/server/confluence/confluence-blueprints) for concepts and tutorials.

## Configuration

The root element for the Blueprint module is `blueprint`. 

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th>Attributes</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>key</p></td>
<td><p>A unique identifier of the blueprint.</p>
<p><strong>Required.</strong></p></td>
</tr>
<tr class="even">
<td><p>index-key</p></td>
<td><p>Specifies the label added to all pages created from this blueprint. This users to locate all pages created with this template from an Index page.</p>
<p><strong>Required.</strong></p></td>
</tr>
<tr class="odd">
<td><p>create-result</p></td>
<td><p>Defines the screen to go to when creating this type of Blueprint. A value of <code>view</code> causes Confluence to bypass the page Editor and automatically create the page content. The user lands in the view of the created page. When <code>edit</code>, the user is sent to the editor which is pre-filed with the template content.</p>
<p><strong>Default:</strong> edit.</p></td>
</tr>
<tr class="even">
<td><p>index-template-key</p></td>
<td>Optionally define an index page template for your Blueprint. It must refer to a <a href="/server/confluence/content-template-module">content-template</a> defined your plugin.</td>
</tr>
<tr class="odd">
<td><p>i18n-index-title-key</p></td>
<td><p>The i18n key for the title of Index page.</p>
<p><strong>Default:</strong> Defaults to the <code>i18n-name-key</code> specified by the <strong>Create</strong> dialog <code>web-item</code> module.</p></td>
</tr>
</tbody>
</table>

The `blueprint` element can have the following child elements:

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th>Element</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="/server/confluence/content-template-module">content-template</a></p></td>
<td><p>A Blueprint must have at least one content template. It should define the content template resource for which this blueprint creates with.</p>
<p><strong>Required.</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="/server/confluence/dialog-wizard-module">dialog-wizard</a></p></td>
<td>This is an optional element which allows you to easily create a dialog wizard for your blueprint.</td>
</tr>
</tbody>
</table>

## Example

``` xml
<blueprint key="myplugin-blueprint" index-key="myplugin-index">
   <content-template ref="myplugin-template"/>
</blueprint>
```
