---
aliases:
- /server/confluence/confluence-internals-history-2031812.html
- /server/confluence/confluence-internals-history-2031812.md
category: devguide
confluence_id: 2031812
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031812
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031812
date: '2017-12-08'
guides: guides
legacy_title: Confluence Internals History
platform: server
product: confluence
subcategory: learning
title: Confluence internals history
---
# Confluence internals history

A brief history of Confluence noting when major features or internal changes were introduced. See the <a href="#release-notes" class="unresolved">release notes</a> for full details.

### <a href="#confluence-2-6" class="unresolved">Confluence 2.6</a>

Redesign of recent updates, children web UI. Introduced classic theme which maintains old look and feel.

Source structure changed to fit better with Maven 2.

### <a href="#confluence-2-5-5" class="unresolved">Confluence 2.5.5</a>

Server ID support.

### <a href="#confluence-2-5" class="unresolved">Confluence 2.5</a>

Pages can be restricted to multiple users and/or groups.

### <a href="#confluence-2-4" class="unresolved">Confluence 2.4</a>

Editable comments.

Bundled plugins shipped in atlassian-bundled-plugins.zip, including Plugin Repository Plugin.

First release built with Maven 2.

### <a href="#confluence-2-3" class="unresolved">Confluence 2.3</a>

Clustering possible with a clustered license.

Changed from EhCache caching layer to Tangosol Coherence. This was for both application caches and Hibernate second-level caches.

Moved Bandana (configuration framework) storage from home directory (`/config/`) to the BANDANA table in the database.

### <a href="#confluence-2-2" class="unresolved">Confluence 2.2</a>

Structure of attachments folder in home directory changed from `/attachments/<filename>/<version>` to `/attachments/<id>/<version>` to fix <a href="#doc-conf-4860-jira" class="unresolved">DOC:CONF-4860@JIRA</a>.

<a href="#personal-spaces" class="unresolved">Personal spaces</a>.

Simpler Atlassian-User configuration when `atlassian-user.xml` replaces `atlassianUserContext.xml`.

### <a href="#confluence-2-1" class="unresolved">Confluence 2.1</a>

Confluence starts using Atlassian-User.

### <a href="#confluence-2-0" class="unresolved">Confluence 2.0</a>

<a href="#export-word-documents" class="unresolved">Export Word documents</a>.
