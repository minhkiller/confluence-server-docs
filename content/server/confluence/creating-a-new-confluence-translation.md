---
aliases:
- /server/confluence/creating-a-new-confluence-translation-2031679.html
- /server/confluence/creating-a-new-confluence-translation-2031679.md
category: reference
confluence_id: 2031679
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031679
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031679
date: '2017-12-08'
legacy_title: Creating A New Confluence Translation
platform: server
product: confluence
subcategory: modules
title: Creating a new Confluence Translation
---
# Creating a new Confluence Translation

{{% note %}}

Caveat emptor!

This page is outdated and don't cover all the files to be translated.

{{% /note %}}

If you would like to translate Confluence into your local language, follow the instructions below on creating a language pack plugin from an example.

The Confluence community is sharing their in-progress and complete translations. You should check that a shared translation to your target language has not already been started <a href="#here" class="unresolved">here</a>.

## Preparation

Start by checking out the [technical overview of a Language Pack Plugin](/server/confluence/language-module). Once you are familiar with the structure and content of a plugin, you can move on to creating your own:

1.  Check that you have the latest version of Confluence <a href="http://www.atlassian.com/software/confluence/ConfluenceDownloadCenter.jspa" class="external-link">here</a>. If not, you are recommended to install the latest version for translation, though you can use any version newer than 2.2. Refer to the guide on <a href="#upgrading-confluence" class="unresolved">upgrading Confluence</a> for instructions.
2.  If you do not already have Apache Ant installed, download the <a href="http://ant.apache.org/bindownload.cgi" class="external-link">latest version</a> and setup your environmental variables according to the <a href="http://ant.apache.org/manual/install.html#installing" class="external-link">manual</a>
3.  If you are using Confluence 2.2.0 only, you will need to unzip the language plugin base files from [languages.zip](https://dac-lf.prod.atl-paas.net/server/confluence/attachments/2031679/2228568.zip) into a subdirectory of `<CONFLUENCE-INSTALL-DIRECTORY>` called `languages`

## Modifying The Example Language Pack Settings

This example will work from an example plugin [en\_AU.zip](https://dac-lf.prod.atl-paas.net/server/confluence/attachments/2031679/2228569.zip). (or better:  [UPDATED Confluence 3.2 ConfluenceActionSupport.properties](https://dac-lf.prod.atl-paas.net/server/confluence/attachments/2031679/2228230.properties) )

1.  Unzip the example en\_AU language pack [en\_AU.zip](https://dac-lf.prod.atl-paas.net/server/confluence/attachments/2031679/2228569.zip) into a subdirectory of `<CONFLUENCE-INSTALL-DIRECTORY>/languages` called `en_AU.` Note that is the file is just a renamed copy of default English properties file
2.  We will now update the properties file in the example to the latest version. Open your Confluence install directory and copy the `confluence\WEB-INF\classes\com\atlassian\confluence\core\ConfluenceActionSupport.properties` file to the example plugin directory `src\etc\com\atlassian\confluence\core`.
3.  Remove the old `ConfluenceActionSupport_en_AU.properties` file, and rename `ConfluenceActionSupport.properties` to `ConfluenceActionSupport_en_AU.properties`.
4.  Locate the plugin descriptor file, ConfluenceActionSupport properies file and flag image

    ``` xml
    <CONFLUENCE-INSTALL-DIRECTORY>/languages/en_AU/src/etc/atlassian-plugin.xml
    <CONFLUENCE-INSTALL-DIRECTORY>/languages/en_AU/src/etc/com/atlassian/confluence/core/ConfluenceActionSupport_en_AU.properties
    <CONFLUENCE-INSTALL-DIRECTORY>/languages/en_AU/src/etc/templates/languages/en_AU/en_AU.png
    ```

5.  Detemine your language plugin key &lt;KEY&gt; using your country and locale according to the [Language Module](/server/confluence/language-module) guide
6.  Atlassian has licensed a set of flags for use with translations. Delete en\_AU.png and download the appropriate flag from [Language Pack Flags](/server/confluence/language-pack-flags), renaming it to the correct key
7.  Update `atlassian-plugin.xml` to contain the relevant &lt;KEY&gt; and other references, including image type. Refer to the first section from the above [Language Module](/server/confluence/language-module) for help on deciding what to modify
8.  Rename the directory structure and filenames that contain en-AU to your own &lt;KEY&gt;. The directory should now appear as

    ``` xml
    <CONFLUENCE-INSTALL-DIRECTORY>/languages/<KEY>/src/etc/atlassian-plugin.xml
    <CONFLUENCE-INSTALL-DIRECTORY>/languages/<KEY>/src/etc/com/atlassian/confluence/core/ConfluenceActionSupport<KEY>.properties
    <CONFLUENCE-INSTALL-DIRECTORY>/languages/<KEY>/src/etc/templates/languages/<KEY>/<KEY>.png
    ```

    NOTE: You are now ready to build the plugin with the default English text to check that your setup is are correct. These next few steps deploy the default English version of the pack under your own language

9.  From the command line, go to `<CONFLUENCE-INSTALL-DIRECTORY>/languages` and execute

    ``` bash
    ant -Dlanguage=<KEY> build
    ```

10. Copy `<CONFLUENCE-INSTALL-DIRECTORY>/plugins/<KEY>/dist/languages-<KEY>.jar` to `<CONFLUENCE-INSTALL-DIRECTORY>/confluence/WEB-INF/lib`
11. Restart Confluence
12. From your browser, login as an Administrator, then go to Administration -&gt; Language and verify that you are able to select the translation

## Updating The Language Pack

To collaborate on the translation process, you may wish to upload your translation to the <a href="#community-translations-page" class="unresolved">Community Translations page</a>. Repeat these instructions to test each iteration of your translation attempt.

1.  Unzip [excelbundle0.9.zip](https://dac-lf.prod.atl-paas.net/server/confluence/attachments/2031627/2228582.zip) to your local drive.
2.  Browse to your Confluence install and go to the `\confluence\WEB-INF\classes\com\atlassian\confluence\core` directory. Copy the `ConfluenceActionSupport.properties` file there into the `translation_tool` directory and rename it to `ConfluenceActionSupport_en.properties`.
3.  If you want to start a fresh translation, skip this step. To work from an existing translation, copy it into the `translation_tool` directory and remove any country variant from the filename, eg `ConfluenceActionSupport_ru_RU.properties` becomes `ConfluenceActionSupport_ru.properties`.
4.  Call the translation tool to create the spreadsheet file. For example, to create a Russian translation, open a terminal window in the `translation_tool` directory and call

    ``` bash
    java -jar excelbundle.jar -export translation_ru.xls -l en,ru -r "%cd%"
    ```

5.  Edit the file content, referring to [Translating ConfluenceActionSupport Content](https://developer.atlassian.com/display/CONFDEV/Translating+ConfluenceActionSupport+Content) for more information on how to modify the string values.
6.  Call the translation tool to export the updates back into the localised properties file. For the example Russian translation, open a terminal window, go to the `translation_tool` directory and call

    ``` bash
    java -jar excelbundle.jar -import translation_ru.xls -l ru -r "%cd%"
    ```

7.  Once you have completed editing, you must copy and rename the localised translation back to the language plugin directory. For frequent updates, you may wish to create a script to do this.
8.  To view the updates after copying across the new properties file, select the language plugin for your translation, then restart Confluence and refresh your browser.

## Building The Language Pack Plugin

To build the new language pack plugin, execute Ant in the `confluence\src\etc\languages` directory:

``` bash
ant -Dlanguage=<LANGUAGE> build
```

A JAR will be created in the `languages/<LANGUAGE>/dist/` folder.

## Installation On A Confluence Server

To install the translation in another instance of Confluence.

1.  Copy `languages-<KEY>.jar` into the `<CONFLUENCE-INSTALL-DIRECTORY>/confluence/WEB-INF/lib` of your target installation
2.  Restart Confluence
3.  From your browser, login as an Administrator, then go to Administration -&gt; Language and select the translation

## Submitting A Translation (Optional)

If you would like to share your completed translation with other Confluence users, you can upload it <a href="#here" class="unresolved">here</a>.

By providing Atlassian permission to bundle complete translations with the Confluence install you will soon be able to select your local language from the Confluence translations list under System Administration, without needing to package it as a plugin.
