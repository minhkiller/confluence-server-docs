---
aliases:
- /server/confluence/centralized-class-for-osgi-component-imports-39368922.html
- /server/confluence/centralized-class-for-osgi-component-imports-39368922.md
category: devguide
confluence_id: 39368922
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39368922
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39368922
date: '2017-12-08'
legacy_title: Centralized Class for OSGi Component Imports
platform: server
product: confluence
subcategory: other
title: Centralized Class for OSGi Component Imports
---
# Centralized Class for OSGi Component Imports

Transformerless plugins are scanned for annotations at compile time for necessary OSGi imports.  One approach is to specify the annotation on every component import.  Another it to create a dummy class whose purpose is to be be scanned.  This centralizes the list of which imports a plugin is using.

An example of the java code for such a plugin is as follows:

**ComponentImports.java**

``` java
package com.example;

import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

/**
 * This class is used to replace <component-import /> declarations in the atlassian-plugin.xml.
 * This class will be scanned by the atlassian spring scanner at compile time.
 * There are no situations this class needs to be instantiated, it's sole purpose 
 * is to collect the component-imports in the one place.
 *
 * The list of components that may be imported can be found at: <your-confluence-url>/admin/pluginexports.action
 */
@SuppressWarnings("UnusedDeclaration")
@Scanned
public class ComponentImports
{
    @ComponentImport com.atlassian.confluence.plugin.services.VelocityHelperService velocityHelperService;
    @ComponentImport com.atlassian.confluence.setup.settings.SettingsManager settingsManager;
 
    private ComponentImports()
    {
        throw new Error("This class should not be instantiated");
    }
}
```

{{% note %}}

This example is leveraged from the <a href="https://bitbucket.org/atlassian/service-desk-automation-tutorial/src/7e7a8d4d1eabfc3fb3a3e2b40c6e1e02c297528b/src/main/java/com/atlassian/plugins/tutorial/servicedesk/osgi/GeneralOsgiImports.java?at=master&amp;fileviewer=file-view-default" class="external-link">Atlassian Service Desk Automation Tutorial</a>

{{% /note %}}
