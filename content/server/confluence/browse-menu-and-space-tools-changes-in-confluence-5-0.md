---
aliases:
- /server/confluence/browse-menu-and-space-tools-changes-in-confluence-5.0-16974586.html
- /server/confluence/browse-menu-and-space-tools-changes-in-confluence-5.0-16974586.md
category: devguide
confluence_id: 16974586
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=16974586
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=16974586
date: '2017-12-08'
legacy_title: Browse Menu and Space Tools changes in Confluence 5.0
platform: server
product: confluence
subcategory: updates
title: Browse menu and Space Tools changes in Confluence 5.0
---
# Browse menu and Space Tools changes in Confluence 5.0

Some plugin developers maybe impacted with some changes introduced in Confluence 5.0. These changes were introduced after the BETA2 release of Confluence 5. A brief overview:

-   You current version of plugin will still work with confluence 5.0 even if you don't update your plugin.
-   You might want to adjust your plugin to suit the new space admin layout.
-   The latest changes only affect the Space Admin/Tools area.

Bellow is the list of changes with the examples:

### No more Browse menu in the default theme

This change was in the Confluence 5 BETA and EAP releases. The Default theme no longer has the Browse menu. Note: the Documentation theme still has this.

<img src="/server/confluence/images/image2013-1-29-17:2:47.png" width="500" />

The items that used to be in the browse menu moved to either

-   the top level items ( section="system.header/left" )
-   the help menu ( section="system.help/pages")

Additionally, there is a new administration menu (section="system.admin/pages") which is only visible to system administrators and not visible to other users.

There is still a user menu ( section="system.user").

If your plugin used to add a link to the Browse menu under system.browse section it will appear under Help menu. We recommend changing the plugin for Confluence 5.0 and move the menu item into the appropriate place. You can use one of the sections described above but first, please, consider the newly added sections:

<img src="/server/confluence/images/image2013-1-29-17:4:12.png" class="confluence-thumbnail" width="300" />  

There is [a tutorial about the space main links section ](https://developer.atlassian.com/display/CONFDEV/Adding+Space+Shortcut+Links)and we will describe the Space Tools section.

### Space Tools Sections

We have re-organised space admin/operations screens. We simplified them and made them more intuitive for confluence's users.

<img src="/server/confluence/images/image2013-1-29-17:5:23.png" class="confluence-thumbnail" width="300" />

The list of sections:

-   system.space.tools/overview
-   system.space.tools/permissions
-   system.space.tools/contenttools
-   system.space.tools/lookandfeel
-   system.space.tools/integrations
-   system.space.tools/addons

Currently items defined by plugins will appear under "Add-ons" section unless they specify one of the section listed above. If plugin defines its own section with several items then the section will appear on the top.  

### Documentation theme still has that menu

<img src="/server/confluence/images/image2013-1-29-17:5:41.png" class="confluence-thumbnail" width="300" />

<img src="/server/confluence/images/image2013-1-29-17:5:56.png" class="confluence-thumbnail" width="300" />

One thing to keep in mind pace that is configured to use Documentation Theme will still have Browse menu and old layout of Space Details. <a href="https://confluence.atlassian.com/display/CONFKB/2012/12/17/Advance+warning+of+plans+to+merge+Documentation+theme+with+the+default+theme" class="external-link">Going forward we plan to remove documentation theme but for Confluence 5.0 the web items/sections should cater for both themes</a>.

In quite a few places we define the same item/section twice and display one of the items would have a condition that will only evaluate to true when inside the default theme. Bellow is the example of such item

``` xml
    <!-web item only shown for spaces with sidebar see the SpaceSidebarCondition ->
    <web-item key="watch-space" name="Watching" section="system.content.collector.pages" weight="1">
        <label key="add.space.notification" />
        <link linkId="space-watching">/spaces/addspacenotification.action?key=$generalUtil.urlEncode($helper.spaceKey)&amp;#url_xsrfToken()</link>
        <condition class="com.atlassian.confluence.plugin.descriptor.web.conditions.user.UserLoggedInCondition"/>
        <condition class="com.atlassian.confluence.plugin.descriptor.web.conditions.user.UserWatchingSpaceCondition" invert="true"/>
        <condition class="com.atlassian.confluence.plugin.descriptor.web.conditions.SpaceSidebarCondition"/> <!- Show the web fragment if the space sidebar is available. ->
        <styleClass>watch</styleClass>
    </web-item>

    <!-web item only shown for spaces with sidebar see the inverted SpaceSidebarCondition ->   
    <web-item key="startwatching" name="Watching" section="system.space.advanced/subscribesection" weight="20">
        <label key="space.watches.all.start" />
        <link linkId="space-watching">/spaces/addspacenotification.action?key=$generalUtil.urlEncode($helper.spaceKey)&amp;#url_xsrfToken()</link>
        <icon height="16" width="16">
            <link>/images/icons/watch_cancel_16.gif</link>
        </icon>
        <condition class="com.atlassian.confluence.plugin.descriptor.web.conditions.user.UserLoggedInCondition"/>
        <condition class="com.atlassian.confluence.plugin.descriptor.web.conditions.user.UserWatchingSpaceCondition" invert="true"/>
        <condition class="com.atlassian.confluence.plugin.descriptor.web.conditions.user.UserWatchingSpaceForContentTypeCondition" invert="true">
            <param name="type">blogpost</param>
        </condition>
        <condition class="com.atlassian.confluence.plugin.descriptor.web.conditions.SpaceSidebarCondition" invert="true"/>
    </web-item>
```

Any questions? Feel free to comment on the page to let us know.
