---
aliases:
- /server/confluence/confluence-blueprints-15335999.html
- /server/confluence/confluence-blueprints-15335999.md
category: devguide
confluence_id: 15335999
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=15335999
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=15335999
date: '2017-12-08'
guides: guides
legacy_title: Confluence Blueprints
platform: server
product: confluence
subcategory: learning
title: Confluence Blueprints
---
# Confluence Blueprints

## Introduction to Blueprints

A Confluence Blueprint gives users a way to create new pages based on pre-defined content.  Users view the blueprints available in an installation through the **Create** dialog.  As plugin developers you can hook into the **Create** dialog to add your own blueprints. With Confluence 5.3+ you can now also write [Space Blueprints](/server/confluence/space-blueprints) to provide users a way to create new spaces.

Basic blueprints can simply help create new pages with pre-defined content from a template, static or dynamic. Blueprints don't limit you to just pre defining content and is really up to your imagination. You can take user input in a dialog wizard and pre populate content, settings or just develop templates with placeholder text (only visible in the editor) to assist users creating certain types of documents.    

See the [Write a simple Confluence Blueprint plugin](/server/confluence/write-a-simple-confluence-blueprint-plugin) tutorial to build a simple blueprint plugin.

{{% note %}}

You can now easily start writing blueprints using the [atlas-create-confluence-plugin-module](https://developer.atlassian.com/display/DOCS/atlas-create-confluence-plugin-module) command part of the [Atlassian SDK](https://developer.atlassian.com/display/DOCS/Working+with+the+SDK) in 4.2.

{{% /note %}}

## Example source

If you want to see some example code, the **Hello Blueprint** now contains a space blueprint. The source code is available on Atlassian Bitbucket: https://bitbucket.org/atlassian/hello-blueprint. To clone the repository, issue the following command:

``` bash
git clone https://bitbucket.org/atlassian/hello-blueprint.git
```

**Hello Blueprint** was last tested with Confluence 5.10.1 and Atlassian SDK 6.2.6.

## Blueprint Concepts

This section introduces you to the concepts you need to develop a Confluence Blueprint. It contains the following topics:

### Plugin module types used in blueprints

Confluence Blueprints are built with the following plugin module types:

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><div class="tablesorter-header-inner">
Module
</div></th>
<th><div class="tablesorter-header-inner">
Purpose
</div></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="/server/confluence/blueprint-module">blueprint</a></p></td>
<td><p>Identifies the blueprint and binds it to a <strong>Create</strong> dialog option.</p></td>
</tr>
<tr class="even">
<td><p><a href="/server/confluence/content-template-module">content-template</a></p></td>
<td><p>Describes the templates available with your plugin. You must have one of these; you can have multiple if your plugin includes multiple templates.</p></td>
</tr>
<tr class="odd">
<td><p><a href="/server/confluence/web-item-plugin-module">web-item</a></p></td>
<td><p>Adds the option for your blueprint to the <strong>Create</strong> dialog.</p>
<p>You must specify a section of <code>system.create.dialog/content </code>and have a <code>param</code> element with name &quot;<code>blueprintKey</code>&quot; specified, where <code>value</code> is the blueprint module key.</p>
<p>The icon resource should be 200 x 150 pixel PNG icon resource representing the blueprint icon.</p></td>
</tr>
<tr class="even">
<td><p><a href="/server/confluence/dialog-wizard-module">dialog-wizard</a></p></td>
<td>Define a dialog wizard for users to input data during page creation</td>
</tr>
</tbody>
</table>

The following code illustrates the minimum required for a Confluence Blueprint to appear in the Create dialog:

**atlassian-plugin.xml**

``` xml
<resource type="i18n" name="i18n" location="i18n"/>
 
<content-template key="myplugin-template">
  <resource name="template" type="download" location="xml/basic.xml"/>
</content-template> 
 
<web-item key="blueprint-item" i18n-name-key="my.plugin.title" section="system.create.dialog/content">
    <description key="my.plugin.description"/>
    <resource name="icon" type="download" location="images/my-blueprint-icon.png"/>
    <param name="blueprintKey" value="myplugin-blueprint"/>
</web-item>
 
<blueprint key="myplugin-blueprint" index-key="myplugin-index">
   <content-template ref="myplugin-template"/>
</blueprint>
```

Note this assumes you have a `i18n.properties` file and `basic.xml` file in your plugin's resources directory.

### Content Templates

Each blueprint depends on a template with pre-defined content, which we refer to as content templates. Any markup that can go into a Confluence page can go in your template. The template content itself must be in an XML file defined in the `content-template `element. The file can contain any valid <a href="https://confluence.atlassian.com/display/DOC/Confluence+Storage+Format" class="external-link">Confluence storage format</a> markup.  The template file can live anywhere in your plugin project, by convention though, you should place it in a subdirectory of the `src/main/resources` folder.  For example, you might create a `src/main/resources/templates/basic.xml` file with the following content:

``` xml
<p>Hello, reader.</p>
```

{{% tip %}}

To design your content template, create and save the page in Confluence. Then go to Tools &gt; View Storage Format (you must be a space admin to see this option) and copy and paste the storage format into your xml file. This is particularly useful if you want to use macros or page layouts in your template.

{{% /tip %}}

### Placeholders/Instructional Text 

Content templates can provide instructional text to the user, which is a placeholder only visible in the editor. Once users easily click on the placeholder to start typing and the placeholder text will be replaced. The purpose of such text, is to guide the user in the information that is required for the template. You can also use instructional text to trigger an `@mention` prompt for user selection. In the following example we have two placeholders, one that is static text (replaced by internationalisation) and the other which will trigger an `@mention` prompt when clicked.

``` xml
<table>
  <tbody>
    <tr>
      <th>User</th>
      <th colspan="1">Phone</th>
    </tr>
    <tr>
      <td><ac:placeholder ac:type="mention">usermention</ac:placeholder></td>
      <td><ac:placeholder><at:i18n at:key="my.key"/></ac:placeholder></td>
    </tr>
  </tbody>
</table>
```

The user will see this in the editor:

<img src="/server/confluence/images/illustration.png" width="500" />

### Index Pages

Each Blueprint type will automatically have an index page created for each space. An index page is like a summary page which lists all the Blueprints of that type in that space. It essentially relies on Confluence labels to be applied to the pages created via Blueprints, which is specified by the `index-key` attribute on the `blueprint` module. Index pages are created when the first blueprint of that type is created for a space e.g. when the first *Meeting Note* blueprint page is created in a space, the *Meeting Notes *index page will also be created. A shortcut to the index page is also created by default in the Space sidebar.

By default, the index page for Blueprints includes a button to create the Blueprint and a table, listing out the blueprints of that type in the space. If you would like to customize and override this default, you can do this by specifying a `index-template-key` attribute on the `blueprint` module and referencing a content template in your plugin. For example:

``` xml
<blueprint key="myplugin-blueprint"
           index-key="myplugin-index"
           index-template-key="myplugin-index-template"/>
    <content-template ref="myplugin-template"/>
</blueprint>

<content-template key="myplugin-index-template" i18n-name-key="myplugin.templates.index.name">
    <resource name="template" type="download" location="xml/index.xml"/>
</content-template> 
```

Note that by default, index page titles are the same as the Blueprint name (that appears in the Create dialog). If you'd like to override the title, in the `blueprint` element specify an attribute "`i18n-index-title-key`".

### Internationalization, translation, and blueprints

You specify the internationalized plugin name and description with the i18n keys.  These keys appears in the component modules you use to define your blueprint.  You specify these keys in the  `src/main/resources/i18n.properties` file.  

``` xml
my.plugin.title=My Blueprint Plugin Title
my.plugin.description=This is a description of my plugin
myplugin.templates.content.name=My Blueprint Content Template
myplugin.templates.index.name=My Blueprint Index Template 
```

You must reference this file in your `atlassian-plugin.xml` descriptor using: 

``` xml
<resource type="i18n" name="i18n" location="i18n"/>
```

If you generated your plugin with an `atlas-create-product-plugin` command, the generation creates this file for you and the necessary references in the `atlassian-plugin.xml` file. 

In your blueprint templates, you can use at:i18n elements for translation strings. The key refers to a key in your plugin's i18n `.properties` file.

``` xml
<p><at:i18n at:key="my.blueprints.string" /></p>
```

Translations of Blueprint templates occur when the Blueprint is created by the user. However, if a user customizes the Blueprint template, it is translated in the globally configured language and then subsequent creations of that Blueprint will be bound to that language.

### Testing your Confluence Blueprint

Once you install a blueprint plugin into Confluence, you can test it using the following steps:

1.  Open Confluence in your browser.
2.  Press the Create button.  
    Confluence displays the **Create** dialog.  
    <img src="/server/confluence/images/createdialog.png" width="500" />
3.  Locate your new blueprint in the dialog and select it.  
    Selecting this item and pressing next should take you to the Editor screen, pre-loaded with your template.
4.  Saving the new page to view it.

An index page should be created for you, with a shortcut to it in the Space sidebar. Visit the index page and make sure your newly created page appears there. To customize your Blueprint as a space admin,  go to **Space Tools** (in the sidebar) And choose  **Content Tools &gt; Templates.**
