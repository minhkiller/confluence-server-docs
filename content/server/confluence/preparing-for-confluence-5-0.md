---
aliases:
- /server/confluence/preparing-for-confluence-5.0-13632907.html
- /server/confluence/preparing-for-confluence-5.0-13632907.md
category: devguide
confluence_id: 13632907
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=13632907
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=13632907
date: '2017-12-08'
legacy_title: Preparing for Confluence 5.0
platform: server
product: confluence
subcategory: development resources
title: Preparing for Confluence 5.0
---
# Preparing for Confluence 5.0

This documentation is intended for Confluence developers who want to ensure that their existing plugins and add-ons are compatible with Confluence 5.0, and to learn about the new features for developing add-ons in Confluence 5.0.

Confluence 5.0 introduces a modernised look and feel that is consistent with other Atlassian applications and delivers a number of improvements to the way users create and navigate content.

## Atlassian UI updates

Confluence 5.0 will include a major upgrade of [AUI](https://developer.atlassian.com/display/AUI) (the Atlassian User Interface) to AUI 5.0. This will also update the version of jQuery that Confluence supports.

There will be visual changes to the appearance of Atlassian UI. Your plugin will look different if it uses AUI. If it does not yet use AUI, your plugin will look out of place in Confluence 5.0. We recommend that you follow the [new Atlassian Design Guidelines](https://developer.atlassian.com/design/) (ADG). We also added [a tutorial](https://developer.atlassian.com/display/CONFDEV/Maintaining+web+resources+during+ADG+rollout) explaining how to maintain ADG resources in a plugin that has to stay backwards-compatible.

We will be incrementally upgrading AUI in each of our 5.0 <a href="http://www.atlassian.com/software/confluence/download-eap" class="external-link">EAP releases</a>, starting with Confluence 5.0-m3 available in early November 2012.

To see specific changes in this and future milestone releases, please view the [Atlassian UI Updates in Confluence 5.0](/server/confluence/atlassian-ui-updates-in-confluence-5-0) page.

## New header

Confluence 5.0 introduces a new header that is consistent with the headers of the other Atlassian products. The goal of the header is to give users quicker access to global objects and operations, to introduce a common navigation pattern across all Atlassian applications, and to modernise Confluence's look and feel.

The new header became available in <a href="https://confluence.atlassian.com/display/DOC/Confluence+5.0-m3+EAP+Release+Notes" class="external-link">Confluence 5.0-m3</a> and has been available to Confluence Cloud customers since October 2012. Confluence 5.0-m9 included a new 'Create' option and a new space sidebar, replacing the 'Browse' menu item. See the <a href="https://confluence.atlassian.com/display/DOC/Confluence+5.0-m9+EAP+Release+Notes" class="external-link">Confluence 5.0-m9 release notes</a> for information on enabling the EAP features in the release.

Notes:

-   **Global menu items (`system.browse`) have moved into the help menu.** It is important for you to reassess your global menu items, and if you believe they should be top-level menu items, switch this to `system.header/left`.  
    **Note:** Please only consider this for global menu items, and use with caution.

## New default space navigation

We have changed the design of the default theme to include a collapsible left-hand navigation panel, called the sidebar.

#### Sidebar links 

The sidebar will make it easier for your users to navigate your space hierarchy and access important links within the space and other related applications. Administrators will be able to add arbitrary links to the sidebar and modify space settings from the sidebar.

Plugin developers can check out the tutorial: [Adding Space Shortcut Links](/server/confluence/adding-space-shortcut-links).

#### Sidebar impact on themes

The default theme will include the sidebar, so your theme may be impacted if you base it on the default theme. In future releases of Confluence 5.x we plan to combine this with the Documentation theme. Confluence 5.0-m9 included the new sidebar, replacing the 'Browse' menu item. See the <a href="https://confluence.atlassian.com/display/DOC/Confluence+5.0-m9+EAP+Release+Notes" class="external-link">Confluence 5.0-m9 release notes</a> for information on enabling the EAP features in the release. We will have more information about the new sidebar in later EAP releases.

Theme developers can check out the tutorial: [Adapting a theme for Confluence 5.0](/server/confluence/adapting-a-theme-for-confluence-5-0) and add-on developers please read [Browse Menu and Space Tools changes in Confluence 5.0](/server/confluence/browse-menu-and-space-tools-changes-in-confluence-5-0).

*Example: Confluence Page with New Sidebar Navigation*

<img src="/server/confluence/images/pagewithsidebar.png" class="image-center" width="500" />

Note: If you use the Plugin SDK to start Confluence 5.0-m9 (EAP release), it will be loaded with a default database configured with the Documentation Theme, which doesn't have the new sidebar. To visualize the new Space IA sidebar, switch your space to use the default theme.

#### Sidebar top-level web-ui items 

For the sidebar we also have a new web-ui module: system.space.sidebar/main-links (<a href="https://developer.atlassian.com/display/CONFDEV/Web+UI+Modules" class="external-link">learn more</a>).

## A new 'Create' experience

Confluence 5.0 ships with a new Create experience. Add-on developers can make use of this create experience by creating add-ons which create different types of content.  We know that Confluence is awesome for building 'mini applications', like meeting notes, release notes, image galleries, even lists of documents and files. But customers don't always know how to create these things. We're working on a new way to package and deliver the functionality to customers so they can get more value out of Confluence.

In a future release of 5.0.x, we will bundle a number of content-centric applications in side Confluence. We are calling these **Blueprints**. A Blueprint is a collection of plugin-modules in Confluence, that when combined together:

-   Show users what types of content they can create
-   Guides them to be able to create the content
-   Helps them structure the content

A typical Blueprint would

-   extend the Confluence create experience to possibly include a "wizard-like" interface to creating a page
-   could come bundled with one or more templates or macros
-   can provide an "index" page: a page that lists all content for a particular label/type.   

Calling all add-on developers! Confluence **Blueprints** means that you can build these as well and exchange them through the <a href="https://marketplace.atlassian.com/" class="external-link">Atlassian Marketplace</a>, either as an enhancement to your existing plugin or as a standalone plugin.

The Confluence create experience is available from <a href="https://confluence.atlassian.com/display/DOC/Confluence+5.0-m9+EAP+Release+Notes" class="external-link">Confluence 5.0-m9</a> and higher. To help you get started we have a few tutorials available. See [Confluence Blueprints](/server/confluence/confluence-blueprints) for details. 

## Pluggable Admin Tasks

Plugins sometimes want to notify administrators about licenses, new features or alerts. [UPM has a plugin point for post-install and post-update screens](https://developer.atlassian.com/display/UPM/Plugin+Metadata+Files+used+by+UPM+and+Marketplace). When this is not enough, we've made Admin Tasks pluggable (<a href="https://jira.atlassian.com/browse/CONF-23261" class="external-link">CONF-23261</a>). See [Creating an Admin Task](/server/confluence/creating-an-admin-task).

<img src="/server/confluence/images/image2013-1-10-18:33:48.png" class="image-center" width="500" />

## New Render Mode for Mobile Macros

Have you written a Confluence macro as a plugin? In Confluence 4.3.2 and later, you can make your macros work in Confluence mobile. For many macros there is little effort involved. Confluence mobile will render a macro if it contains a device type specification of "mobile". For more complex macros, you may need to convert the CSS and JavaScript too. Details are in this document: <a href="https://developer.atlassian.com/display/CONFDEV/Making+your+macro+render+in+Confluence+mobile" class="external-link">Making your macro render in Confluence mobile</a>

## Note-worthy Modifications

-   **Modified Breadcrumb Interface**  
    We've modified the `Breadcrumb` interface. If you use Breadcrumbs and implement the `Breadcrumb` interface you will need to extend the `AbstractBreadcrumb` class or implement the new methods, otherwise you will run into compilation issues. 

## Milestone releases for developers

Please watch the <a href="http://confluence.atlassian.com/display/DOC/Confluence+Development+Releases" class="external-link">Development Releases</a> page for pre-release versions of Confluence 5.0 that you can use for testing purposes.

## Advanced Notices

Below are notices for future release consideration for plugin developers:

-   The [Creating a Template Bundle](/server/confluence/creating-a-template-bundle) plugin point will be removed for Confluence 5.1. This will be replaced the new pluggable create experience mentioned above.
-   The "Add" menu drop-down web item is hidden for the default theme in Confluence 5.0 and will probably be removed from other theme in future versions of Confluence. The new "Create" experience (see above) will replace the need for the Add button on the view page, consolidating all types of content in one place. There will be no migration path for the old Add drop-down.

## Have questions?

If you have any questions about preparing for Confluence 5.0, please register for an account on <a href="https://answers.atlassian.com/" class="external-link">Atlassian Answers</a>, our community Q&A resource, and <a href="https://answers.atlassian.com/questions/ask/?tags=confluence-development" class="external-link">ask a question</a>. We'll be keeping an eye on things to help out as soon as possible.
