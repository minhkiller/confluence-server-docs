---
aliases:
- /server/confluence/extending-autoconvert-8947120.html
- /server/confluence/extending-autoconvert-8947120.md
category: devguide
confluence_id: 8947120
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=8947120
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=8947120
date: '2017-12-08'
legacy_title: Extending Autoconvert
platform: server
product: confluence
subcategory: learning
title: Extending autoconvert
---
# Extending autoconvert

|                       |                 |
|-----------------------|-----------------|
| Level of experience   | BEGINNER        |
| Time estimate         | 1:00            |
| Atlassian application | CONFLUENCE 4.X+ |

## Tutorial overview

This tutorial shows you how to create a plugin that extends Confluence Autoconvert. Your plugin converts URL text into hyperlinked title text. The conversion appears dynamically as you edit in Confluence and add URLs from the http://developer.atlassian.com domain. In other words, your plugin converts URL text like the following from 

``` bash
https://developer.atlassian.com/display/CONFDEV/Plugin+Tutorial+-+Extending+Autoconvert
```

into a link that displays as [Plugin Tutorial - Extending Autoconvert](https://developer.atlassian.com/display/CONFDEV/Plugin+Tutorial+-+Extending+Autoconvert).  This particular plugin converts links from the **developer.atlassian.com** domain as an example, but could be modified for any domain.

**Concepts covered in this tutorial:**

-   Using a JavaScript file to provide an Autoconvert handler.
-   Using a `parseURI` JavaScript object to interpret URLs.

### Prerequisite knowledge

To complete this tutorial, you should already understand the basics of JavaScript development. 

### Plugin source

We encourage you to work through this tutorial. If you want to skip ahead or check your work when you have finished, you can find the plugin source code on Atlassian Bitbucket. Bitbucket serves a public Git repository containing the tutorial's code. To clone the repository, issue the following command:

``` bash
git clone git@bitbucket.org:atlassian_tutorial/extending-autoconvert-in-confluence.git
```

Alternatively, you can download the source using the **get source** option here: https://bitbucket.org/atlassian_tutorial/extending-autoconvert-in-confluence/.

{{% note %}}

About these Instructions

You can use any supported combination of OS and IDE to create this plugin. These instructions were written using Eclipse Indigo on Mac OS X. If you are using another OS or IDE combination, you should use the equivalent operations for your specific environment.

This tutorial was last tested with **Confluence 5.2.3**. 

{{% /note %}}

## Step 1. Create and prune the plugin skeleton

In this step, you'll create a plugin skeleton using `atlas-` commands. Since you won't need some of the files created in the skeleton, you'll also delete them in this step.

1.  Open a terminal and navigate to your Eclipse workspace directory.  
      
2.  Enter the following command to create a Confluence plugin skeleton:

    ``` bash
    $ atlas-create-confluence-plugin
    ```

3.  When prompted, enter the following information to identify your plugin:

    <table>
    <colgroup>
    <col style="width: 30%" />
    <col style="width: 70%" />
    </colgroup>
    <tbody>
    <tr class="odd">
    <td><p>group-id</p></td>
    <td><p><code>com.atlassian.tutorial.confluence</code></p></td>
    </tr>
    <tr class="even">
    <td><p>artifact-id</p></td>
    <td><p><code>confluence-autoconvert-dev-docs</code></p></td>
    </tr>
    <tr class="odd">
    <td><p>version</p></td>
    <td><p><code>1.0-SNAPSHOT</code></p></td>
    </tr>
    <tr class="even">
    <td><p>package</p></td>
    <td><p><code>com.atlassian.tutorial.confluence</code></p></td>
    </tr>
    </tbody>
    </table>

4.  Confirm your entries when prompted with `Y` or `y`.

    Your terminal notifies you of a successful build:

    ``` bash
    [INFO] ------------------------------------------------------------------------
    [INFO] BUILD SUCCESSFUL
    [INFO] ------------------------------------------------------------------------
    [INFO] Total time: 1 minute 11 seconds
    [INFO] Finished at: Thu Jul 18 11:30:23 PDT 2013
    [INFO] Final Memory: 82M/217M
    [INFO] ------------------------------------------------------------------------
    ```

5.  Change to the project directory created by the previous step.

    ``` bash
    $ cd confluence-autoconvert-dev-docs/
    ```

6.  Delete the test directories.

    Setting up testing for your plugin isn't part of this tutorial. Use the following commands to delete the generated test skeleton:

    ``` bash
    $ rm -rf ./src/test/java
    $ rm -rf ./src/test/resources/
    ```

7.  Delete the unneeded Java class files.

    ``` bash
    $ rm ./src/main/java/com/atlassian/tutorial/confluence/*.java
    ```

8.  Edit the `src/main/resources/atlassian-plugin.xml` file.  
      
9.  Remove the generated `myPluginComponent` component declaration.

    ``` xml
    <!-- publish our component -->
    <component key="myPluginComponent" class="com.example.plugins.tutorial.confluence.simplebp.MyPluginComponentImpl" public="true">
       <interface>com.example.plugins.tutorial.confluence.simplebp.MyPluginComponent</interface>
    </component>
    ```

10. Save and close `atlassian-plugin.xml`.

### Import your project into your IDE

1.   Make your project available to Eclipse.

    ``` bash
    atlas-mvn eclipse:eclipse
    ```

    You'll see a successful build message: 

    ``` bash
    [INFO] ------------------------------------------------------------------------
    [INFO] BUILD SUCCESSFUL
    [INFO] ------------------------------------------------------------------------
    [INFO] Total time: 54 seconds
    [INFO] Finished at: Tue Jul 16 11:03:59 PDT 2013
    [INFO] Final Memory: 82M/224M
    [INFO] ------------------------------------------------------------------------
    ```

2.  Start Eclipse.  
    You can open a new terminal window to perform this action.

    ``` bash
    $ cd ~/eclipse
    $ ./eclipse
    ```

3.  Click **File &gt; Import**.   
    Eclipse starts the **Import** wizard.      
4.  Expand the **General** folder tree to choose **Existing Projects into Workspace**.      
5.  Click **Next**.
6.  Click **Browse** and enter the root directory of your workspace.  
    Your Atlassian plugin folder should now appear under **Projects**.
7.  Ensure your plugin checkbox is ticked and click **Finish**.   
    Eclipse imports your project, and it's now visible in your Package Explorer view. 

## Step 2. Write the JavaScript for the `autoconvert` handler

Your plugin uses JavaScript to parse the URLs and return the title text. When you created your plugin project skeleton, it automatically included a directory for `js` and and empty file titled `confluence-autoconvert-dev-docs.js`. In this step, you'll add a `bind` method so your plugin executes during the `init.rte` event - when you edit a page in Confluence. You'll also add an alternation so your plugin executes only on http://developer.atlassian.com links.  

If URLs meet the criteria of the `if` conditions, you'll use `uri`, an object from `parseURI`. This enables your plugin to parse a URL, splitting each section after the initial `//` at single (`/)` forward slashes. You can try it out here: http://stevenlevithan.com/demo/parseuri/js/

Your plugin converts http://developer.atlassian.com URLs to display as page titles, so it needs to be able to parse 4 parts of the URL: 

``` bash
https://developer.atlassian.com/display/CONFDEV/Adding+a+Custom+Action+to+Confluence
```

If split at the forward slashes, the link contains `developer.atlassian.com`, `display`, `CONFDEV`, and the actual page title. You'll write JavaScript to use the fourth section as display text.

1.  Open `confluence-autoconvert-dev-docs.js` from `src/main/resources/js`.

2.  Add a `bind` method to listen for the `init.rte` event.

    The `init.rte` event is when the editor in Confluence is loaded.

    ``` javascript
    (function(){
    AJS.bind("init.rte", function() {

    });
    })();
    ```

3.  Create a handler that can later call the continuation `done()`.

    Add and assign the variable `pasteHandler` within the curly brackets of the `bind` method. Include the arguments `uri` to reference a `parseURI` object, the jQuery object `node`, and `done` to act as a continuation function for `pasteHandler` to call. The `parseURI` object interprets URLs to get the title of the page. You'll add additional code inside the closing curly brace }.

    ``` javascript
        var pasteHandler = function(uri, node, done){
        
        };
    ```

4.  Add and assign `directoryParts`.   
    Since the URL is separated by forward slashes, you'll include this in the assignment statement:

    ``` javascript
        var directoryParts = uri.directory.split('/'),
            pageName;
    ```

5.  Nest a condition to only apply the autoconvert plugin to http://developer.atlassian.com pages.

    Your condition verifies that the URL qualifies for the autoconvert action by checking for four parts in the URL and that the text matches the `uri.source`.

    ``` javascript
    if (uri.host == "developer.atlassian.com" &&
        directoryParts.length == 4 && directoryParts[0] == "" &&
        directoryParts[1] == "display" &&
        uri.anchor == "" &&
        node.text() == uri.source) {
    ```

6.  Define `pageName` as the fourth parameter ( `[3]`), and replace URL formatting with a space. 

    The `done(node)` method makes `node` available for additional URLs. 

    ``` javascript
        var pageName = decodeURIComponent(directoryParts[3]).replace(/\+/g, " ");

        node.text(pageName);
        done(node);
    ```

    {{% warning %}}

Call done() once in all code paths

In order for autoconvert to work, call `done()` exactly one time in all possible code paths. You can call `done()` with no arguments if you don't want to change the link, or you can pass the replacement or modified node to change the link.

    {{% /warning %}}

7.  Finish your alternation by directing ineligible URLs to the `done` method.

    Call `done()` again to finalize the code path. This completes the block, and uses the closing }; that you added in step 1.

    ``` javascript
            } else {
                done();
            }
    ```

8.  Register the handler.

    ``` javascript
    tinymce.plugins.Autoconvert.autoConvert.addHandler(pasteHandler);
    ```

9.  Save the changes and close the file.

Here's what your `confluence-autoconvert-dev-docs.js` file should look like at completion:

``` javascript
(function(){
    AJS.bind("init.rte", function() {
    var pasteHandler = function(uri, node, done){
        var directoryParts = uri.directory.split('/'),
            pageName;
        if (uri.host == "developer.atlassian.com" &&
                directoryParts.length == 4 && directoryParts[0] == "" &&
                directoryParts[1] == "display" &&
                uri.anchor == "" &&
                node.text() == uri.source) {

            pageName = decodeURIComponent(directoryParts[3]).replace(/\+/g, " ");

            node.text(pageName);
            done(node);
        } else {
            done();
        }
    };

    tinymce.plugins.Autoconvert.autoConvert.addHandler(pasteHandler);
});

})();
```

## Step 3. Add a `Web Resource` module to the `atlassian-plugin.xml` descriptor 

Now you'll add a `Web Resource` module to the `atlassian-plugin.xml` file to describe how Confluence should interact with your JavaScript file. The  `atlassian-plugin.xml`  file was also generated when you created your plugin skeleton. 

This module type defines the locations of other resources for your plugin to use, so you can use the `confluence-autoconvert-dev-docs.js` that you wrote earlier to handle the conversion of URLs. While it's possible to add this module via the `atlas-create-confluence-plugin-module` command, it's easier in this case to add it directly to your descriptor file.

1.  Open `atlassian-plugin.xml` from `src/main/resources`.  
      
2.  Locate the comment line `<!-- add our web resources-->`.  
    Resources were automatically generated when you created the plugin skeleton.  
      
3.  Add the following code to define the location of your JavaScript file.

    Here you'll add the JS file you just created.

    ``` xml
        <web-resource key="autoconvert-dev-docs" name="Autoconvert developer.atlassian.com example handler">
            <description>Changes link text for URLs pasted from https://developer.atlassian.com.</description>
            <resource type="download" name="confluence-autoconvert-dev-docs.js" location="js/confluence-autoconvert-dev-docs.js"/>
            <!-- This will ensure the resource is loaded after autoconvert, and only if autoconvert is enabled. -->
            <dependency>com.atlassian.confluence.plugins.confluence-paste:autoconvert-core</dependency>
            <!-- Assuming the dependency above is met, this context means that whenever the editor is loaded, so is your autoconvert handler. -->
            <context>editor</context>
        </web-resource>
    ```

4.  Save and close the file.  
      
5.  Update your project changes from the project root.  
    In this example, the command is for Eclipse:

    ``` bash
    $ atlas-mvn eclipse:eclipse
    ```

Your `atlassian-plugin.xml` should resemble the following:

``` xml
<atlassian-plugin key="${project.groupId}.${project.artifactId}" name="${project.name}" plugins-version="2">
    <plugin-info>
        <description>${project.description}</description>
        <version>${project.version}</version>
        <vendor name="${project.organization.name}" url="${project.organization.url}" />
        <param name="plugin-icon">images/pluginIcon.png</param>
        <param name="plugin-logo">images/pluginLogo.png</param>
    </plugin-info>
    <!-- add our i18n resource -->
    <resource type="i18n" name="i18n" location="confluence-autoconvert-dev-docs"/>
    
    <!-- add our web resources -->
    <web-resource key="confluence-autoconvert-dev-docs-resources" name="confluence-autoconvert-dev-docs Web Resources">
        <dependency>com.atlassian.auiplugin:ajs</dependency>
        
        <resource type="download" name="confluence-autoconvert-dev-docs.css" location="/css/confluence-autoconvert-dev-docs.css"/>
        <resource type="download" name="confluence-autoconvert-dev-docs.js" location="/js/confluence-autoconvert-dev-docs.js"/>
        <resource type="download" name="images/" location="/images"/>
        <context>confluence-autoconvert-dev-docs</context>
    </web-resource>
    
    <web-resource key="autoconvert-dev-docs" name="Autoconvert developer.atlassian.com example handler">
        <description>Changes link text for URLs pasted from https://developer.atlassian.com.</description>
        <resource type="download" name="confluence-autoconvert-dev-docs.js" location="js/confluence-autoconvert-dev-docs.js"/>
        <!-- This will ensure the resource is loaded after autoconvert, and only if autoconvert is enabled. -->
        <dependency>com.atlassian.confluence.plugins.confluence-paste:autoconvert-core</dependency>
        <!-- Assuming the dependency above is met, this context means that whenever the editor is loaded, so is your autoconvert handler. -->
        <context>editor</context>
    </web-resource>
    
    <!-- import from the product container -->
    <component-import key="applicationProperties" interface="com.atlassian.sal.api.ApplicationProperties" />
    
</atlassian-plugin>
```

## Step 4. Build, install, and run your plugin

In this step you'll install your plugin and run Confluence. Then, to test your autoconvert plugin, you'll add links to a Confluence page from developer.atlassian.com.

1.  Open a terminal window and navigate to the plugin root folder. 

    ``` bash
    $ cd confluence-autoconvert-dev-docs
    ```

2.  Start up Confluence from your project root using the `atlas-run` command.

    ``` bash
    atlas-run
    ```

    This command builds your plugin code, starts a Confluence instance, and installs your plugin. This may take a few minutes.

3.  Locate the URL for Confluence.  
    Your terminal outputs the location of your local Confluence instance, defaulted to http://localhost:1990/confluence.

    ``` bash
    [INFO] confluence started successfully in 71s at http://localhost:1990/confluence
    [INFO] Type CTRL-D to shutdown gracefully
    [INFO] Type CTRL-C to exit
    ```

4.  Open your browser and navigate to your local Confluence instance.        
5.  Login with `admin` / `admin`.
6.  Click **Create** to create a new Confluence page.      
7.  Choose **Blank page** and click **Create**.      
8.  Copy and paste various links into your page, and confirm that they convert to their page titles.  
    Here's a handful you can use. You'll want to paste them into your URL address bar, and then copy the link:

    ``` bash
    https://developer.atlassian.com/display/CONFDEV/Extending+Autoconvert
     
    https://developer.atlassian.com/display/CONFDEV/Extending+the+Confluence+Insert+Link+Dialog
     
    https://developer.atlassian.com/display/CONFDEV/Adding+a+Custom+Action+to+Confluence
    ```

9.  Verify the links transform into titles as you cut and paste them into the page.   
    Here's an example of the links as they should appear:
    
    <img src="/server/confluence/images/finalautoconvert.png" title="Verify the URLs autoconvert" alt="Verify the URLs autoconvert" width="500" />
