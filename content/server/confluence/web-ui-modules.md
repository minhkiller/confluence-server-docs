---
aliases:
- /server/confluence/web-ui-modules-2031617.html
- /server/confluence/web-ui-modules-2031617.md
category: reference
confluence_id: 2031617
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031617
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031617
date: '2017-12-08'
legacy_title: Web UI Modules
platform: server
product: confluence
subcategory: modules
title: Web UI modules
---
# Web UI modules

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Available:</p></td>
<td><p>Confluence 2.2 and later</p></td>
</tr>
</tbody>
</table>

Web UI plugin modules allow you to insert links, tabs and sections of links into the Confluence web interface.They're not much use on their own, but when combined with [XWork-WebWork Plugins](/server/confluence/xwork-webwork-module) they become a powerful way to add functionality to Confluence.

## Sections and Items

Web UI plugins can consist of two kinds of plugin modules:

-   [Web Item modules](/server/confluence/web-item-plugin-module) define links that are to be displayed in the UI at a particular location.
-   [Web Section modules](/server/confluence/web-section-plugin-module) define a collection of links to be displayed together, in a 'section'.

Web items and web sections (referred to collectively as 'web fragments') may be displayed in a number of different ways, depending on the location of the fragment and the theme under which it is being displayed.

## Locations

In a number of places in the Confluence UI, there are lists of links representing operations relevant to the content being viewed.

NOTE: Please be aware that the Descriptions below relate to the *default* Confluence theme. **Bold** text used in each description relates to a component on the product interface.

These are the *locations* that you can customise:

**Pages, blogs, comments**

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 70%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Location key</p></th>
<th><p>Description</p></th>
<th><p>Availability</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>system.content.action</p></td>
<td><p>The menu items on the <strong>Tools</strong> drop down menu available on pages and blogs. The sections of this menu include primary, marker, secondary and modify.</p>
<p><strong>Themeable.</strong></p>
<p><strong>Sectioned.</strong></p></td>
<td><p>2.8</p></td>
</tr>
<tr class="even">
<td><p>system.attachment</p></td>
<td><p>The links on the right of an <strong>Attachments</strong> list.</p>
<p><strong>Themeable.</strong></p></td>
<td><p>2.8</p></td>
</tr>
<tr class="odd">
<td><p>system.comment.action</p></td>
<td><p>The links within each comment listed at the end of pages and blogs. The sections include the primary section on the lower-left of a comment (i.e. the Edit, Remove and Reply links) and the secondary section on the lower-right (i.e. the Permanent link icon). Note you MUST select a section e.g. write &quot;system.comment.action/primary&quot; or &quot;system.comment.action/secondary&quot;.</p>
<p><strong>Themeable.</strong></p>
<p><strong>Sectioned.</strong></p></td>
<td><p>2.8</p></td>
</tr>
<tr class="even">
<td><p>system.content.metadata</p></td>
<td><p>The small icons to the left of the page metadata (&quot;Added by Mary, last edited by John&quot;) for attachments and permissions.</p>
<p><strong>Themeable.</strong></p></td>
<td><p>3.0</p></td>
</tr>
<tr class="odd">
<td>page.metadata.banner</td>
<td><p>The area to the right of the page breadcrumbs. Items can be added to this location to display information about a page (<a href="/server/confluence/adding-items-to-the-info-banner">tutorial</a>).</p>
<p><strong>Themeable.</strong></p>
<p><br />
</p></td>
<td>5.4</td>
</tr>
</tbody>
</table>

**Editor**

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 70%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Location key</p></th>
<th><p>Description</p></th>
<th><p>Availability</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>system.editor.action</p></td>
<td><p>Buttons in the wiki markup editor toolbar. The insert link, image and macro buttons are in the 'insert' section.</p>
<p><strong>Sectioned.</strong></p></td>
<td><p>3.1 - 3.5</p></td>
</tr>
<tr class="even">
<td>system.editor.link.browser.tabs</td>
<td>Tabs in the &quot;Insert Link&quot; dialog in the editor (<a href="/server/confluence/extending-the-confluence-insert-link-dialog">tutorial</a>).</td>
<td>4.3</td>
</tr>
</tbody>
</table>

**Users**

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 70%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Location key</p></th>
<th><p>Description</p></th>
<th><p>Availability</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>system.profile</p></td>
<td><p>The tabs above user profile views.</p>
<p><strong>Themeable.</strong></p></td>
<td><p>2.2</p></td>
</tr>
<tr class="even">
<td><p>system.profile.view</p></td>
<td><p>These links are only visible to Confluence administrators and appear either beneath views of user profiles without personal spaces or beneath their own profile view. For example, Administer User.</p>
<p><strong>Themeable.</strong></p></td>
<td><p>2.9</p></td>
</tr>
<tr class="odd">
<td><p>system.user</p></td>
<td><p>The menu items on the 'username' drop down menu available in the top bar of all pages. The sections of this menu include user-preferences, user-content and user-operations.</p>
<p><strong>Themeable.</strong></p>
<p><strong>Sectioned.</strong></p></td>
<td><p>2.8</p></td>
</tr>
</tbody>
</table>

**Labels**

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 70%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Location key</p></th>
<th><p>Description</p></th>
<th><p>Availability</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>system.labels</p></td>
<td><p>The <strong>View</strong> sub-categories of the global <strong>All Labels</strong> / <strong>Popular Labels</strong> area</p></td>
<td><p>2.2</p></td>
</tr>
<tr class="even">
<td><p>system.space.labels</p></td>
<td><p>The <strong>View</strong> sub-categories of the <strong>Labels</strong> tab area</p></td>
<td><p>2.2</p></td>
</tr>
</tbody>
</table>

**Spaces**

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 70%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Location key</p></th>
<th><p>Description</p></th>
<th><p>Availability</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>system.content.add</p></td>
<td><p>The menu items in the <strong>Add</strong> drop down menu available in pages, blogs and areas of the <strong>Space Admin</strong> and other Browse Space tabs. The sections of this menu include space and page.</p>
<p><strong>Themeable.</strong></p>
<p><strong>Sectioned.</strong></p></td>
<td><p>2.8</p></td>
</tr>
<tr class="even">
<td><p>system.space</p></td>
<td><p>The <strong>Space Admin</strong> and other Browse Space tabs.</p>
<p><strong>Themeable.</strong></p></td>
<td><p>2.2</p></td>
</tr>
<tr class="odd">
<td><p>system.space.actions</p></td>
<td><p>In versions of Confluence prior to and including version 2.9, these action icons appear in the top-right of most space-related views. However, from Confluence 2.10, this location key has been deprecated and has been superseded by 'system.content.add'.</p>
<p><strong>Themeable.</strong></p></td>
<td><p>2.2</p></td>
</tr>
<tr class="even">
<td><p>system.space.admin</p></td>
<td><p>The links in the left-hand menu of the <strong>Space Admin</strong> tab area.</p>
<p><strong>Sectioned.</strong></p></td>
<td><p>2.2 - 5.0</p></td>
</tr>
<tr class="odd">
<td><p>system.space.advanced</p></td>
<td><p>The links in the left-hand menu of the <strong>Advanced</strong> tab area.</p>
<p><strong>Sectioned.</strong></p></td>
<td><p>2.2 - 5.0</p></td>
</tr>
<tr class="even">
<td>system.space.tools</td>
<td><p>The tabs at the top of Space Tools. Existing sections are: /overview, /permissions, /contenttools, /lookandfeel, /integrations, /addons. Tutorial: <a href="/server/confluence/writing-a-space-admin-screen">Writing a Space Admin Screen</a>.</p>
<p><strong>Sectioned.</strong></p></td>
<td>5.0</td>
</tr>
<tr class="odd">
<td><p>system.space.pages</p></td>
<td><p>The <strong>View</strong> sub-categories of the <strong>Pages</strong> tab area.</p>
<p><strong>Themeable.</strong></p></td>
<td><p>2.2</p></td>
</tr>
<tr class="even">
<td>system.space.sidebar/main-links</td>
<td>The Main Links on the Confluence 5.0 Space Sidebar. Tutorial: <a href="/server/confluence/adding-space-shortcut-links-16973954.html#declaringa-main-link">Declaring a Main Link</a>.</td>
<td>5.0</td>
</tr>
</tbody>
</table>

**Global**

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 70%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Location key</p></th>
<th><p>Description</p></th>
<th><p>Availability</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>system.dashboard</p></td>
<td><p>Links on the lower-left of the default global dashboard, below the <strong>Spaces</strong> list.</p>
<p><strong>Themeable.</strong></p></td>
<td><p>2.10.2</p></td>
</tr>
<tr class="even">
<td><p>system.browse</p></td>
<td><p>The global section of the Browse menu. This section appears below the 'system.space.admin' options when inside a space. Deprecated in 5.0.</p>
<p><strong>Themeable.</strong></p></td>
<td><p>2.8 - 4.3</p></td>
</tr>
<tr class="odd">
<td>system.help</td>
<td>Global help menu.</td>
<td>5.0</td>
</tr>
</tbody>
</table>

**Administration Console**

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 70%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Location key</p></th>
<th><p>Description</p></th>
<th><p>Availability</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>system.admin</p></td>
<td><p>The links in the left-hand menu of the global <strong>Administration Console.</strong></p>
<p><strong>Sectioned.</strong></p></td>
<td><p>2.2</p></td>
</tr>
<tr class="even">
<td>system.admin.tasks/general</td>
<td>Admin Tasks displayed on the Admin Console. <a href="/server/confluence/creating-an-admin-task">Creating an Admin Task</a>.</td>
<td>5.0</td>
</tr>
</tbody>
</table>

-   Those locations marked as being 'themeable' can be moved around, reformatted or omitted by [Theme Module](/server/confluence/theme-module). The descriptions above refer to where they are located in the default theme.
-   Locations marked as being 'sectioned' require that web items be grouped under web sections. In sectioned locations, web items that are not placed under a section *will not be displayed*.
-   It is possible for themes to make any themeable locations sectioned, even when the default theme does not. We do *not* recommend this, as it would mean any plugin taking advantage of this would only be compatible with a particular theme.

{{% note %}}

Theme Compatibility

Themes based on Confluence versions prior to 2.2 will continue to function with Confluence 2.2, but will not be able to display any custom Web UI fragments until they are updated.

{{% /note %}}

## Web Section Definition

You may choose to create your own web sections or add to Confluence's predefined ones, if it makes logical sense to do that.

Here is a sample `atlassian-plugin.xml` fragment for a web section:

``` xml
<web-section key="mail" name="Mail" location="system.space.admin" weight="300">
    <label key="space-mail" />
    <condition class="com.atlassian.confluence.plugin.descriptor.web.conditions.NotPersonalSpaceCondition"/>
</web-section>
```

Here is another sample:

``` xml
<web-section key="page" name="Add Page Content" location="system.content.add" weight="200">
    <label key="page.word" />
</web-section>
```

The above example will create a new **section** on the 'Add' menu. You can then add a **web item** in the section. The location of this section depends on the relative weight compared to the other sections that have already been defined by Confluence or by other installed plugins.

Take a look at the full configuration of [Web Section plugin modules](/server/confluence/web-section-plugin-module).

###  Web sections in the Confluence 

The diagrams below illustrate the web sections available in the Confluence dropdown menus.

![](/server/confluence/images/toolssections.jpg)  
*Web sections for location system.content.action*

  
![](/server/confluence/images/addsections.jpg)  
*Web sections for location system.content.add*

## Web Item Definition

Here's a sample `atlassian-plugin.xml` fragment for a web item:

``` xml
<web-item key="spacelogo" name="Space Logo" section="system.space.admin/looknfeel" weight="40">
    <label key="configure.space.logo" />
    <link>/spaces/configurespacelogo.action?key=$space.key</link>
    <icon height="16" width="16">
        <link>/images/icons/logo_add_16.gif</link>
    </icon>
    <condition class="com.atlassian.confluence.plugin.descriptor.web.conditions.NotPersonalSpaceCondition"/>
</web-item>
```

Take a look at the full configuration of [Web Item plugin modules](/server/confluence/web-item-plugin-module).

## Q and A

#### How do I make use of sections or web items in my own themes?

Take a look at how they are used in the default themes, you should be able to get a good idea of the necessary code. For example, here is some sample code from `space.vmd`:

``` xml
#set ($webInterfaceContext = $action.webInterfaceContext)
#foreach ($item in $action.webInterfaceManager.getDisplayableItems("system.space", $webInterfaceContext))
    <li><a href="$item.link.getDisplayableUrl($req, $webInterfaceContext)" #if ($context == $item.key) class="current" #end>
        $item.label.getDisplayableLabel($req, $webInterfaceContext)
    </a></li>
#end
```

#### Can I create new locations for web UI plugins in my own themes?

Yes. Just pick a new key for the `location` or `section` parameters of your plugin modules. By convention, you should probably use the standard 'inverted domain name' prefix so as not to clash with anyone else's plugins. We reserve all `system.*` locations for Confluence's core use.

Once again, however, we don't recommend this as you end up with plugins that are only useful in your own themes. Try to at least provide an alternative set of UI modules for people who are using other themes and still want to access the same functionality. You could, for example, define alternative UI plugin modules that placed your functions in Confluence's standard locations, but have a &lt;condition&gt; that disabled them in favour of your custom locations if your theme was installed.

#### If I create a Web Item that links to my custom action, how do I make it appear in the same tabs/context as the other items in that location?

The best way is to look at the .vm file of one of the existing items in that location. You are most interested in the `#applyDecorator` directive being called from that file. For example `viewpage.vm`, which defines the "View" tab in the `system.page` location has the following `#applyDecorator` directive:

``` xml
#applyDecorator("root")
    #decoratorParam("helper" $action.helper)
    #decoratorParam("mode" "view")
    #decoratorParam("context" "page")

    <!-- some stuff... -->

#end
```

If you were writing a plugin that was destined to be added as another item in the page tabs, your Velocity file for that action would also have to have a similar decorator directive around it:

``` xml
#applyDecorator("root")
    #decoratorParam("helper" $action.helper)
    #decoratorParam("mode" "myPluginKey")
    #decoratorParam("context" "page")

    <!-- some stuff... -->

#end
```

Note that you should put your Web Item's plugin key as the 'mode'. This way, Confluence will make sure that the correct tab is highlighted as the active tab when people are viewing your action.  
NOTE: In some cases, such as the browse space tabs, you may have to use 'context' instead of 'mode'.

#### My web UI link does not appear when I use the Adaptavist Theme Builder plugin - why?

Theme Builder uses completely customisable navigation and as such can't automatically display web UI links because this would likely lead to duplication of many other, more common links.

You can, however use the {`menulink`} macro to insert any web UI link using the following notation:

``` bash
{menulink:webui|location=XXXX|key=YYYY}webui link{menulink}
```

Theme Builder 2.0.8 and above now supports a growing number of third party plugins as standard - for more information see the <a href="https://www.adaptavist.com/display/USERGUIDE/menulink+macro" class="external-link">online documentation</a>. If you have a publicly available plugin and want an in-built menulink location for it, please <a href="https://support.adaptavist.com" class="external-link">contact Adaptavist</a>.

#### The breadcrumb trail for my web UI administration/space administration/tab plugin is showing the class name - how do I fix it?

##### In the atlassian-plugin.xml:

``` xml
<!--Make sure each name is unique-->
       <resource type="i18n" name="i18n-viewreview"
location="resources/ViewReviewAction" />
```

##### In the java:

``` xml
//in an action
I18NBean i18NBean = getI18n();

//or in a macro or other sort of plugin
 ThemeHelper helper = this.getHelper();
               ConfluenceActionSupport action = (ConfluenceActionSupport) helper.getAction();
               Locale locale = action.getLocale();
               I18NBean i18nBean = i18NBeanFactory.getI18NBean(locale);

//and
   public void setI18NBeanFactory(I18NBeanFactory i18NBeanFactory)
   {
       this.i18NBeanFactory = i18NBeanFactory;
   }
```

##### Use a normal properties file and locate it as follows:

If we're talking about actions:  
The properties file with the same name as the relevant action can go in the same directory as the action. So, if you had `XYZAction.java`, then `XYZAction.properties` could live in the same directory. And you would not have to do anything in the `atlassian-plugin.xml` file.

If you don't want it to live there, or if you're not talking about an action:  
Define a resource in the `atlassian-plugin.xml` and tell it to live wherever you want. The standard is `resources`.

-   In the source: `etc/resources`
-   In the jar: `resources/`

The property that handles the breadcrumb has to be the fully qualified name of the class plus `.action.name`

So, for a SharePointAdmin property you might use: `com.atlassian.confluence.extra.sharepoint.SharePointAdmin.action.name=SharePoint Admin`

##### RELATED TOPICS

[Web Section Plugin Module](/server/confluence/web-section-plugin-module)  
[Web Item Plugin Module](/server/confluence/web-item-plugin-module)  
[Writing Confluence Plugins](/server/confluence/writing-confluence-plugins)
