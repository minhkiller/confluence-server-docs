---
aliases:
- /server/confluence/confluence-objects-accessible-from-velocity-2031768.html
- /server/confluence/confluence-objects-accessible-from-velocity-2031768.md
category: devguide
confluence_id: 2031768
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031768
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031768
date: '2017-12-08'
guides: guides
legacy_title: Confluence Objects Accessible From Velocity
platform: server
product: confluence
subcategory: learning
title: Confluence objects accessible from Velocity
---
# Confluence objects accessible from Velocity

Confluence has a few distinct Velocity contexts for different purposes in the application (user macros, email templates, exports), but the most commonly used context is called the "default context".

## Velocity usage guidelines for plugins

To allow deprecation and code change breakages to be detected at compile time, it is recommended that where possible you add functionality which calls Confluence code in your plugin Java code (i.e. actions or components) rather than in a Velocity template. You can call any method on your plugin action from Velocity with `$action.getComplicatedCustomObject()` instead of putting complicated logic in your Velocity template that is not checked by the compiler.

For example, if your plugin needs a calculate list of particular pages to display in the Velocity template, you should do the following:

-   inject a PageManager into your action class by adding a `setPageManager()` method and `pageManager` field ([more information on dependency injection](/server/confluence/spring-ioc-in-confluence#plugindependencyinjection))
-   in your action's `execute()` method, retrieve the desired pages using the `pageManager` object and store them in a field in your class called `calculatedPages`
-   add a method to your action, `getCalculatedPages()`, which returns the list of pages
-   in your Velocity template, use `$action.calculatedPages` to get the calculated pages from the action and display them.

Although it is supported at the moment, you should not be performing data updates directly from Velocity code and future versions of Confluence may prevent you doing this in your plugin.

## Default Velocity context

This list highlights the most important entries in the default Velocity context. The full list is defined in Confluence's source code in `velocityContext.xml`. The default Velocity context is used for templates rendered by:

-   [Confluence WebWork actions](/server/confluence/xwork-webwork-module)
-   [macros](/server/confluence/macro-module) which call <a href="http://docs.atlassian.com/atlassian-confluence/latest-server/index.html?com/atlassian/confluence/renderer/radeox/macros/MacroUtils.html" class="external-link">MacroUtils.defaultVelocityContext()</a>
-   mail notifications (with additions - see below)
-   <a href="#user-macros" class="unresolved">user macros</a> (with additions - see below).

<table>
<colgroup>
<col style="width: 30%" />
<col style="width: 70%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Variable</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><code>$action</code></p></td>
<td><p>The current WebWork action</p><p>Class Reference:</p><p>Your action class, normally a subclass of <a href="http://docs.atlassian.com/atlassian-confluence/latest-server/com/atlassian/confluence/core/ConfluenceActionSupport.html" class="external-link">ConfluenceActionSupport</a></p></td>
</tr>
<tr class="even">
<td><p><code>$i18n</code></p></td>
<td><p><code>$i18n.getText()</code> should be used for <a href="#plugin-internationalisation" class="unresolved">plugin internationalisation</a>.</p><p>Class Reference:</p><p><a href="http://docs.atlassian.com/atlassian-confluence/latest-server/index.html?com/atlassian/confluence/util/i18n/I18NBean.html" class="external-link">I18NBean</a></p></td>
</tr>
<tr class="odd">
<td><p><code>$dateFormatter</code></p></td>
<td><p>Provides a date and time formatter suitable for the exporting user's locale and environment.</p><p>Class Reference:</p><p><a href="http://docs.atlassian.com/atlassian-confluence/latest-server/com/atlassian/confluence/core/DateFormatter.html" class="external-link">DateFormatter</a></p></td>
</tr>
<tr class="even">
<td><p><code>$req</code></p></td>
<td><p>The current servlet request object (if available)</p><p>Class Reference:</p><p><a href="http://java.sun.com/j2ee/1.4/docs/api/javax/servlet/http/HttpServletRequest.html" class="external-link">HttpServletRequest</a></p></td>
</tr>
<tr class="odd">
<td><p><code>$req.contextPath</code></p></td>
<td><p>The current context path. Used for creating relative URLs:</p>
<pre><code>&lt;a
  href=&quot;$req.contextPath/dashboard.action&quot;&gt;
  Dashboard
&lt;/a&gt;</code></pre><p>Class Reference:</p><p>String</p></td>
</tr>
<tr class="even">
<td><p><code>$baseUrl</code></p></td>
<td><p>The base URL of the Confluence installation. Used for creating absolute URLs in email and RSS:</p>
<pre><code>&lt;a 
  href=&quot;$baseUrl/dashboard.action&quot;&gt;
  Back to Confluence
&lt;/a&gt;</code></pre><p>Class Reference:</p><p>String</p></td>
</tr>
<tr class="odd">
<td><p><code>$res</code></p></td>
<td><p>The current servlet response object (should not be accessed in Velocity)</p><p>Class Reference:</p><p><a href="http://java.sun.com/j2ee/1.4/docs/api/javax/servlet/http/HttpServletResponse.html" class="external-link">HttpServletResponse</a></p></td>
</tr>
<tr class="even">
<td><p><code>$settingsManager</code></p></td>
<td><p>Can retrieve the current global settings with <code>$settingsManager.getGlobalSettings()</code></p><p>Class Reference:</p><p><a href="http://docs.atlassian.com/atlassian-confluence/latest-server/index.html?com/atlassian/confluence/setup/settings/SettingsManager.html" class="external-link">SettingsManager</a></p></td>
</tr>
<tr class="odd">
<td><p><code>$generalUtil</code></p></td>
<td><p>A <code>GeneralUtil</code> object, with useful utility methods for URL encoding etc</p><p>Class Reference:</p><p><a href="http://docs.atlassian.com/atlassian-confluence/latest-server/index.html?com/atlassian/confluence/util/GeneralUtil.html" class="external-link">GeneralUtil</a></p></td>
</tr>
<tr class="even">
<td><p><code>$action.remoteUser</code>, <code>$remoteUser</code></p></td>
<td><p>The currently logged in user, or <code>null</code> if anonymous user.</p><p>Class Reference:</p><p><a href="http://docs.atlassian.com/atlassian-user/2.0/apidocs/com/atlassian/user/User.html" class="external-link">User</a></p></td>
</tr>
<tr class="odd">
<td><p><code>$userAccessor</code></p></td>
<td><p>For retrieving users, groups and checking membership</p><p>Class Reference:</p><p><a href="http://docs.atlassian.com/com/atlassian/confluence/atlassian-confluence/latest-server/index.html?com/atlassian/confluence/user/UserAccessor.html" class="external-link">UserAccessor</a></p></td>
</tr>
<tr class="even">
<td><p><code>$permissionHelper</code></p></td>
<td><p>Can be used to check permissions, but it is recommended that you check permission in your action</p><p>Class Reference:</p><p><a href="http://docs.atlassian.com/com/atlassian/confluence/atlassian-confluence/latest-server/index.html?com/atlassian/confluence/security/PermissionHelper.html" class="external-link">PermissionHelper</a></p></td>
</tr>
<tr class="odd">
<td><p><code>$attachmentManager</code></p></td>
<td><p>Retrieving attachments</p><p>Class Reference:</p><p><a href="http://docs.atlassian.com/atlassian-confluence/latest-server/index.html?com/atlassian/confluence/pages/AttachmentManager.html" class="external-link">AttachmentManager</a></p></td>
</tr>
<tr class="even">
<td><p><code>$spaceManager</code></p></td>
<td><p>Space operations</p><p>Class Reference:</p><p><a href="http://docs.atlassian.com/atlassian-confluence/latest-server/index.html?com/atlassian/confluence/spaces/SpaceManager.html" class="external-link">SpaceManager</a></p></td>
</tr>
</tbody>
</table>

## User macro Velocity context

User macros have a Velocity context which includes all the above items and some additional entries specific to user macros. See <a href="https://confluence.atlassian.com/display/DOC/User+Macro+Template+Syntax" class="external-link">Guide to User Macro Templates</a> for a list of the latter.

## Email notification Velocity context

{{% note %}}

From Confluence 5.2 onwards Soy templates are used instead of Velocity for email notifications.

{{% /note %}}

If customising the Velocity templates for Confluence's email notifications, the following items are available in addition to the default context above.

<table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Variable</p></th>
<th><p>Description</p></th>
<th><p>Class Reference</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><code>$stylesheet</code></p></td>
<td><p>Default stylesheet CSS contents</p></td>
<td><p>String</p></td>
</tr>
<tr class="even">
<td><p><code>$contextPath</code></p></td>
<td><p>Same as <code>$req.contextPath</code> in the default context</p></td>
<td><p>String</p></td>
</tr>
<tr class="odd">
<td><p><code>$subject</code></p></td>
<td><p>The email notification subject</p></td>
<td><p>String</p></td>
</tr>
<tr class="even">
<td><p><code>$wikiStyleRenderer</code></p></td>
<td><p>Wiki rendering support</p></td>
<td><p><a href="http://docs.atlassian.com/atlassian-renderer/5.0/apidocs/com/atlassian/renderer/WikiStyleRenderer.html" class="external-link">WikiStyleRenderer</a></p></td>
</tr>
<tr class="odd">
<td><p><code>$renderContext</code></p></td>
<td><p>Notification render context for use with <code>$wikiStyleRenderer</code></p></td>
<td><p><a href="http://docs.atlassian.com/atlassian-renderer/5.0/apidocs/com/atlassian/renderer/RenderContext.html" class="external-link">RenderContext</a></p></td>
</tr>
<tr class="even">
<td><p><code>$report</code></p></td>
<td><p>Daily report (only for digest notifications)</p></td>
<td><p><a href="https://docs.atlassian.com/atlassian-confluence/latest-server/index.html?com/atlassian/confluence/mail/reports/ChangeDigestReport.html" class="external-link">ChangeDigestReport</a></p></td>
</tr>
<tr class="odd">
<td><p><code>$showDiffs</code></p></td>
<td><p>Whether this notification should include diffs</p></td>
<td><p>boolean</p></td>
</tr>
<tr class="even">
<td><p><code>$showFullContent</code></p></td>
<td><p>Whether this notification should include full page content</p></td>
<td><p>boolean</p></td>
</tr>
<tr class="odd">
<td><p><code>$diffRenderer</code></p></td>
<td><p>Diff rendering support</p></td>
<td><p><a href="https://docs.atlassian.com/atlassian-confluence/latest-server/index.html?com/atlassian/confluence/diff/renderer/StaticHtmlChangeChunkRenderer.html" class="external-link">StaticHtmlChangeChunkRenderer</a></p></td>
</tr>
<tr class="even">
<td><p><code>$diff</code></p></td>
<td><p>Diff for the notification, if enabled</p></td>
<td><p><a href="https://docs.atlassian.com/atlassian-confluence/latest-server/index.html?com/atlassian/confluence/diff/ConfluenceDiff.html" class="external-link">ConfluenceDiff</a></p></td>
</tr>
</tbody>
</table>

## Export Velocity context

The export context does not include any of the values from the default context. See <a href="http://confluence.atlassian.com/display/DOC/Available+Velocity+Context+Objects+in+Exporters" class="external-link">Available Velocity Contexts in Exporters</a> for a complete list.

## Related Pages

-   [Accessing Confluence Components from Plugin Modules](/server/confluence/accessing-confluence-components-from-plugin-modules)
-   [XWork-WebWork Module](/server/confluence/xwork-webwork-module)
-   [Velocity Context Module](/server/confluence/velocity-context-module)
-   [XWork Plugin Complex Parameters and Security](/server/confluence/xwork-plugin-complex-parameters-and-security)
-   [Writing Confluence Plugins](/server/confluence/writing-confluence-plugins)
