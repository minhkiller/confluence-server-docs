---
aliases:
- /server/confluence/2031788.html
- /server/confluence/2031788.md
category: devguide
confluence_id: 2031788
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031788
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031788
date: '2017-12-08'
legacy_title: How do I get a reference to a component?
platform: server
product: confluence
subcategory: faq
title: How do I get a reference to a component?
---
# How do I get a reference to a component?

### <img src="http://confluence.atlassian.com/images/icons/favicon.png" class="confluence-external-resource" /> How do I get a reference to a component?

Confluence's component system is powered by Spring, but we've done a lot of nice things to make it easier for developers to get their hands on a component at any time.

##### Autowired Objects

If your object is being autowired (for example another plugin module or an XWork action), the easiest way to access a component is to add a basic Java setter method.

For example, if you need a <a href="http://www.atlassian.com/software/confluence/docs/api/latest/com/atlassian/confluence/spaces/SpaceManager.html" class="external-link">SpaceManager</a> simply add the following setter method. This setter will be called when the object is created.

``` java
public void setSpaceManager(SpaceManager spaceManager)
{
    this.spaceManager = spaceManager;
}
```

{{% note %}}

You can also write you own components which are automatically injected into your plugins in the same way. See <a href="/server/framework/atlassian-sdk/component-plugin-module/" class="createlink">Component Plugins</a> for more detail

{{% /note %}}

##### Non-autowired Objects

If your object is not being autowired, you may need to retrieve the component explicitly. This is done via the ContainerManager like so:

``` java
SpaceManager spaceManager = (SpaceManager) ContainerManager.getComponent("spaceManager");
```
