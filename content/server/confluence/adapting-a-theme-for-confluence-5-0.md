---
aliases:
- /server/confluence/adapting-a-theme-for-confluence-5.0-16973961.html
- /server/confluence/adapting-a-theme-for-confluence-5.0-16973961.md
category: devguide
confluence_id: 16973961
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=16973961
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=16973961
date: '2017-12-08'
legacy_title: Adapting a theme for Confluence 5.0
platform: server
product: confluence
subcategory: learning
title: Adapting a theme for Confluence 5.0
---
# Adapting a theme for Confluence 5.0

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Applicable:</p></td>
<td><p>This note applies to <strong>Confluence 5.0.</strong></p></td>
</tr>
</tbody>
</table>

As described in [Preparing for Confluence 5.0](/server/confluence/preparing-for-confluence-5-0), a new sidebar is introduced for spaces.

Themes can choose whether the sidebar is visible.

## Where are the items of the Browse menu displayed?

As explained in [Preparing for Confluence 5.0](/server/confluence/preparing-for-confluence-5-0), the Browse menu was replaced by the Advanced links in the space sidebar.

For legacy reasons, it is still displayed in some situations:

<table>
<colgroup>
<col style="width: 30%" />
<col style="width: 70%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p><strong>Location:</strong></p>
<p>system.browse</p>
<p><strong>Theme:</strong></p>
<p>All</p>
<p><strong>Displayed:</strong></p>
<p>In the Help menu.</p></td>
<td><img src="/server/confluence/images/image2012-12-12-15:41:30.png" /></td>
</tr>
<tr class="even">
<td><p><strong>Location:</strong></p>
<p>system.space</p>
<p><strong>Theme:</strong></p>
<p>With Space sidebar</p>
<p><strong>Displayed:</strong></p>
<p>In the contextual navigation of the Advanced section</p></td>
<td><img src="/server/confluence/images/image2012-12-12-15:40:34.png" /></td>
</tr>
<tr class="odd">
<td><p><strong>Location:</strong></p>
<p>system.space</p>
<p><strong>Theme:</strong></p>
<p>Without Space sidebar</p>
<p>(Example: Documentation Theme)</p>
<p><strong>Displayed:</strong></p>
<p>In the Browse menu</p>
<p>WARNING: Deprecated. Themes will need to handle it in future versions. Themes which do not use the sidebar can define their own way of displaying navigation elements.</p></td>
<td><p><img src="/server/confluence/images/image2012-12-12-15:33:38.png" /></p>
<p>(Deprecated)</p></td>
</tr>
</tbody>
</table>

## Themes which do not override decorators (page.vmd, blogpost.vmd, space.vmd)

If you want to display the sidebar in your theme, you only need to declare &lt;space-ia value="true"/&gt; in your theme definition:

``` xml
<theme key="simpletheme" name="Simple Theme" class="com.atlassian.confluence.themes.BasicTheme">
    <description>A simple custom theme</description>
    <param name="includeClassicStyles" value="false"/>
    <resource type="download" name="default-theme.css" location="/includes/css/default-theme.css">
        <param name="source" value="webContext"/>
    </resource>
    <space-ia value="true"/>
</theme>
```

## Themes which override decorators

If a theme overrides space.vmd, page.vmd or blogpost.vmd, it redefines the way a space is displayed. It is the theme's responsibility to display the appropriate navigation. Refer to the file space.vmd in the source of Confluence to update your template.

## Related topics

[Writing a Confluence Theme](/server/confluence/writing-a-confluence-theme)
