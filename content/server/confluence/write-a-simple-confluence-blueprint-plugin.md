---
aliases:
- /server/confluence/write-a-simple-confluence-blueprint-plugin-21464027.html
- /server/confluence/write-a-simple-confluence-blueprint-plugin-21464027.md
category: devguide
confluence_id: 21464027
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=21464027
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=21464027
date: '2017-12-08'
guides: guides
legacy_title: Write a simple Confluence Blueprint plugin
platform: server
product: confluence
subcategory: learning
title: Write a simple Confluence Blueprint plugin
---
# Write a simple Confluence Blueprint plugin

<table>
<colgroup>
<col style="width: 30%" />
<col style="width: 70%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Applicable:</p></td>
<td><p>This tutorial applies to Confluence Server 5.1 and higher.</p></td>
</tr>
<tr class="even">
<td><p>Level of experience:</p></td>
<td><p>Beginner.</p></td>
</tr>
<tr class="odd">
<td><p>Time estimate:</p></td>
<td><p>It should take you less than 1 hour to complete this tutorial.</p></td>
</tr>
</tbody>
</table>

{{% tip %}}

This tutorial will create a plugin that can only be installed on Confluence Server. To create a blueprint for Confluence Cloud head to [Multi-page Blueprints with Confluence Connect](https://developer.atlassian.com/display/CONFCLOUD/Multi-page+Blueprints+with+Confluence+Connect).

{{% /tip %}}

## Overview of the tutorial

This tutorial shows you how to create a very simple blueprint plugin.  This tutorial does not require writing any Java classes. To complete this tutorial, you should have installed the Atlassian SDK and worked through the [Atlassian Plugin SDK](https://developer.atlassian.com/display/DOCS/Set+up+the+Atlassian+Plugin+SDK+and+Build+a+Project) tutorial. 

### Plugin source

If you want to skip ahead or check your work when you finish, you can find the plugin source code on Atlassian Bitbucket. Bitbucket serves a public Git repository containing the tutorial's code. Alternatively, you can <a href="https://bitbucket.org/atlassian_tutorial/confluence-simple-blueprint/get/master.zip" class="external-link">download the source as a ZIP archive</a>.  To clone the repository, issue the following command:

``` bash
git clone https://bitbucket.org/atlassian_tutorial/confluence-simple-blueprint.git
```

{{% note %}}

About these Instructions

You can use any supported combination of OS and IDE to create this plugin. These instructions were written using Eclipse Classic Version 3.7.1 on Mac OS X. If you are using another OS or IDE combination, you should use the equivalent operations for your specific environment.

This tutorial was last tested with Confluence 5.10.1 using the Atlassian SDK 6.2.6.

{{% /note %}}

## Step 1. Create the plugin project and prune the skeleton

In this step, you'll generate skeleton code for your plugin.  Since you won't need many of the skeleton files, you also delete those unused files in this step. 

1.  Open a terminal on your machine and navigate to your Eclipse workspace directory.  
    If you aren't using Eclipse, you do this step in any directory where you normally keep your plugin code.
2.  Enter the following command to create a plugin skeleton:

    ``` bash
    atlas-create-confluence-plugin
    ```

    The `atlas-` commands are part of the Atlassian Plugin SDK, and automate some of the work of plugin development for you.  

3.  When prompted, enter the following information to identify your plugin:

    <table>
    <colgroup>
    <col style="width: 25%" />
    <col style="width: 75%" />
    </colgroup>
    <tbody>
    <tr class="odd">
    <td><p>group-id</p></td>
    <td><p><code>com.example.plugins.tutorial.confluence.simplebp</code></p></td>
    </tr>
    <tr class="even">
    <td><p>artifact-id</p></td>
    <td><p><code>simplebp</code></p></td>
    </tr>
    <tr class="odd">
    <td><p>version</p></td>
    <td><p><code>1.0-SNAPSHOT</code></p></td>
    </tr>
    <tr class="even">
    <td><p>package</p></td>
    <td><p><code>com.example.plugins.tutorial.confluence.simplebp</code></p>
    <div>
    <code></code>
    </div></td>
    </tr>
    </tbody>
    </table>

4.  Confirm your entries when prompted.  
    The SDK creates your project skeleton and puts it in a `simplebp` directory. 
5.  Change to the `simplebp` directory created by the previous step.

6.  Delete the test directories.

    Setting up blueprint testing is not part of this tutorial. Use the following command to delete the generated test skeleton:

    ``` bash
    rm -rf ./src/test/java
    rm -rf ./src/test/resources/
    ```

7.  Delete the unneeded Java class files.

    A basic blueprint doesn't require you to write any Java code. Use the following command to delete the generated Java class skeleton:

    ``` bash
    rm ./src/main/java/com/example/plugins/tutorial/confluence/simplebp/*.java
    ```

### Now is a good point to import the project into your IDE

1.  Change back to the root of the project
2.  Run the following command:

    ``` bash
    atlas-mvn eclipse:eclipse
    ```

3.  Start Eclipse.
4.  Select **File &gt; Import**.   
    Eclipse starts the **Import** wizard.
5.  Filter for **Existing Projects into Workspace** (or expand the **General** folder tree).
6.  Choose **Next** and browse to the root directory of your plugin (where the `pom.xml` file is located).   
    Your Atlassian plugin folder should appear under **Projects**.
7.  Select your plugin and choose **Finish**.   
    Eclipse imports your project.

## Step 2. Run your plugin 

At this point, you haven't actually done anything but create a skeleton plugin.  You can run that skeleton in Confluence anyway.  In this step, you do just that.  

1.  Use the **atlas-run** command to start a local Confluence instance.
2.  Run the following command:

    ``` bash
    atlas-run
    ```

    This command takes a minute or so to run. It builds your plugin code, starts a Confluence instance, and installs your plugin. When the process has finished, you will see many status lines on your screen concluding with something like the following:

    ``` bash
    [INFO] [talledLocalContainer] INFO: Starting Coyote HTTP/1.1 on http-1990
    [INFO] [talledLocalContainer] Tomcat 6.x started on port [1990]
    [INFO] confluence started successfully in 132s at http://localhost:1990/confluence
    [INFO] Type Ctrl-D to shutdown gracefully
    [INFO] Type Ctrl-C to exit
    ```

    You'll see the output includes a URL for the Confluence instance.

3.  Log into the instance as user `admin` using a password of `admin`.  
    The Confluence Dashboard appears.
4.  Leave Confluence running in your browser.  
     

## Step 3. Create some page content for your template

A template is an XML file that describes a page using Confluence source format. The simplest blueprints need only an XHTML template to do something cool.  In this step, you use Confluence to design a simple template and then use the Confluence Source Editor add-on to copy the source format.   From your browser, running Confluence, do the following.

1.  Choose **Spaces**  from the menu bar. 
2.  Select the **Demonstration Space**.  
    The system places you in the space home. 
3.  Press the **Create** button at the top of the page.  
    The system displays the **Create** dialog. It is this dialog you will customize by adding your own blueprint to it.   
    <img src="/server/confluence/images/createdialog.png" width="500" /> 
4.  Select the **Blank** **page** item.
5.  Press the **Create** button.  
    The system puts you in a new page. 
6.  Add a two column table to the page.

    | Name                  | Date                     |
    |-----------------------|--------------------------|
    | Enter your name here. | Enter today's date here. |

7.  Title your page **Template Content** and press **Save**.
8.  Go to<img src="/server/confluence/images/image2016-7-26-14:37:2.png" class="confluence-thumbnail" width="25" /> &gt; **View Storage Format**.   
9.  The storage format of the page will pop-up in a new browser window. Leave this open for now. 

## Step 4. Create a template for your project

Your plugin must include the template file in its directory structure. By convention, a template is a plugin resources so a good place to store your template is in the `resource` directory.  At this point, you have Confluence with the source content displayed. So, open a second terminal window on your local machine and do the following:

1.  Change directory to the root your `simplebb` project directory.
2.  Create a `templates` subdirectory in `resources`.

    ``` bash
    mkdir src/main/resources/templates
    ```

3.  Create a `mytemplate.xml` file in your newly created `templates` directory. 
4.  Open the `mytemplate.xml` file for editing.
5.  Bring up the browser with the source content.
6.  Copy the storage format into the `mytemplate.xml` file and close the browser window.

    ``` xml
    <table>
      <tbody>
        <tr>
          <th>Name</th>
          <th>Date</th>
        </tr>
        <tr>
          <td>Enter your name here</td>
          <td>Enter today's date here</td>
        </tr>
      </tbody>
    </table>
    ```

7.  Save and close the `mytemplate.xml` file.

## Step 5. Add additional resources to your project

Blueprints support internationalization. So, in this step, you'll modify the generated simplebp.properties file which is used to support internationalization.  You'll also add a image to your project to represent your blueprint in the **Create** dialog.  In your terminal window, do the following.

1.  Edit your project's `src/main/resources/simplebp.properties` file
2.  Add the following properties.

    ``` xml
    my.blueprint.title=Sample Template
    my.create-link.title=My Sample Template
    my.create-link.description=Create a new SimpleBP template
    ```

    You can change the title of the template or the description anyway you want.

    ``` xml
    my.blueprint.title=Sample Template
    my.create-link.title=My Sample Template
    my.create-link.description=Create a new SimpleBP template
    ```

3.  Save and close the `simplebp.properties` file.
4.  Save [myblueprint.png](/server/confluence/images/myblueprint.png) image to your project's `src/main/resources/images` directory.  
    ![myblueprint.png](/server/confluence/images/myblueprint.png "myblueprint.png")
5.  Check your work.  
    At this point, your project's src/main/resources directory should contain the following files:

    ``` text
    |____atlassian-plugin.xml
    |____css
    | |____simplebp.css
    |____images
    | |____myblueprint.png
    | |____pluginIcon.png
    | |____pluginLogo.png
    |____js
    | |____simplebp.js
    |____simplebp.properties
    |____templates
    | |____mytemplate.xml
    ```

## Step 6. Add the modules to the atlassian-plugin.xml file

Each blueprint relies on three component modules:

| Module             | Purpose                                                                                                                                           |
|--------------------|---------------------------------------------------------------------------------------------------------------------------------------------------|
| `content-template` | Describes the templates available with your plugin. You must have one of these; you can have multiple if your plugin includes multiple templates. |
| `blueprint`        | Identifies the blueprint.                                                                                                                         |
| `web-item`         | Adds the option for your blueprint to the **Create** dialog.                                                                                      |

Add the modules to the file, by going to your plugin project root and doing the following:

1.  Edit the `src/main/resources/atlassian-plugin.xml` file.
2.  Add a `content-template` module for your template.

    ``` xml
     <!-- Template for Blueprint -->
        <content-template key="simplebp-template" template-title-key="my.blueprint.title">
            <resource name="template" type="download" location="/templates/mytemplate.xml" />
        </content-template>
    ```

3.  Add a `blueprint` module for your template.  
     

    ``` xml
        <!-- Blueprint -->
        <blueprint key="my-blueprint" content-template-key="simplebp-template" index-key="my-index" />
    ```

4.  Add you a link for you template to the **Create** dialog.

    ``` xml
         <!-- Add to the Create Menu -->
        <web-item key="create-by-sample-template" i18n-name-key="my.create-link.title" section="system.create.dialog/content">
            <description key="my.create-link.description" />
            <resource name="icon" type="download" location="/images/myblueprint.png" />
            <param name="blueprintKey" value="my-blueprint" />
         </web-item>
    ```

5.  Check your work.

    ``` xml
    <atlassian-plugin key="${project.groupId}.${project.artifactId}" name="${project.name}" plugins-version="2">
        <plugin-info>
            <description>${project.description}</description>
            <version>${project.version}</version>
            <vendor name="${project.organization.name}" url="${project.organization.url}" />
            <param name="plugin-icon">images/pluginIcon.png</param>
            <param name="plugin-logo">images/pluginLogo.png</param>
        </plugin-info>
        <!-- add our i18n resource -->
        <resource type="i18n" name="i18n" location="simplebp"/>
        
        <!-- add our web resources -->
        <web-resource key="simplebp-resources" name="simplebp Web Resources">
            <dependency>com.atlassian.auiplugin:ajs</dependency>
            
            <resource type="download" name="simplebp.css" location="/css/simplebp.css"/>
            <resource type="download" name="simplebp.js" location="/js/simplebp.js"/>
            <resource type="download" name="images/" location="/images"/>
            <context>simplebp</context>
        </web-resource>
        
        <!-- Template for Blueprint -->
        <content-template key="simplebp-template" template-title-key="my.blueprint.title">
            <resource name="template" type="download" location="/templates/mytemplate.xml" />
        </content-template>
        
        <!-- Add to the Create Menu -->
        <web-item key="create-by-sample-template" i18n-name-key="my.create-link.title" section="system.create.dialog/content">
            <description key="my.create-link.description" />
            <resource name="icon" type="download" location="/images/myblueprint.png" />
            <param name="blueprintKey" value="my-blueprint" />
         </web-item>
         
        <!-- Blueprint -->
        <blueprint key="my-blueprint" content-template-key="simplebp-template" index-key="my-index" />
        
        <!-- import from the product container -->
        <component-import key="applicationProperties" interface="com.atlassian.sal.api.ApplicationProperties" />
        
        
    </atlassian-plugin>
    ```

6.  Save and close your `atlassian-plugin.xml` file.

## Step 7. Build your changes and see your plugin in action

Now, you are ready to build your plugin and see it in action.  You can build the plugin from within Confluence using the Developer Toolbar.  Go back to the Confluence browser window and do the following:

1.  Rebuild your plugin from the command line, [QuickReload](https://developer.atlassian.com/docs/developer-tools/automatic-plugin-reinstallation-with-quickreload) will automatically reload your plugin for you.
2.  Make sure you are still in the Demonstration Space.
3.  Press the **Create** button.  
    The **Create** dialog appears and now contains your blueprint entry.  
    <img src="/server/confluence/images/mysamplebp.png" width="500" /> 
4.  Choose the **My Sample Template** option.  
    The system creates a new page using your template.
