---
aliases:
- /server/confluence/encrypting-error-messages-in-sybase-2031804.html
- /server/confluence/encrypting-error-messages-in-sybase-2031804.md
category: devguide
confluence_id: 2031804
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031804
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031804
date: '2017-12-08'
legacy_title: Encrypting error messages in Sybase
platform: server
product: confluence
subcategory: faq
title: Encrypting error messages in Sybase
---
# Encrypting error messages in Sybase

<a href="http://infocenter.sybase.com/help/index.jsp?topic=/com.sybase.help.ase_12.5.svrtsg/html/svrtsg/svrtsg284.htm" class="external-link">Adaptive server messages</a>
