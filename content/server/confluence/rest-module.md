---
aliases:
- /server/confluence/rest-module-2031747.html
- /server/confluence/rest-module-2031747.md
category: reference
confluence_id: 2031747
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031747
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031747
date: '2017-12-08'
legacy_title: REST Module
platform: server
product: confluence
subcategory: modules
title: REST module
---
# REST module

{{% note %}}

The REST module is available only for OSGi-based plugins in Confluence 3.1 or later and requires version 2.2 or later of the [Atlassian Plugin Framework](https://developer.atlassian.com/display/PLUGINFRAMEWORK).

{{% /note %}}

The REST plugin module allows plugin developers to create their own REST API for Confluence.

This module type is shared with other Atlassian products. See the common [REST Plugin Module](https://developer.atlassian.com/display/DOCS/REST+Plugin+Module) documentation for details.

