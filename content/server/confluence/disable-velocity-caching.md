---
aliases:
- /server/confluence/disable-velocity-caching-2031808.html
- /server/confluence/disable-velocity-caching-2031808.md
category: devguide
confluence_id: 2031808
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031808
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031808
date: '2017-12-08'
legacy_title: Disable Velocity Caching
platform: server
product: confluence
subcategory: faq
title: Disable Velocity caching
---
# Disable Velocity caching

When you are developing plugins for Confluence, it is often useful to disable the caching of the Velocity templates so that you don't have to restart the server to see velocity changes.

Use the following steps to disable Velocity template caching in Confluence:

1.  Shut down your Confluence server
2.  Extract the velocity.properties file in your Confluence installation from confluence/WEB-INF/confluence-3.1.jar and copy it to another location for editing.
3.  Make the following changes to the velocity.properties file:
    -   On all the lines that end with `...resource.loader.cache`, set the values to `false`.
    -   Set the `class.resource.loader.cache` to `false`. (If this entry does not exist, you can skip this step.)
    -   Set `velocimacro.library.autoreload` to `true`. (Uncomment the line if necessary.)
4.  Put the updated `velocity.properties` in `confluence/WEB-INF/classes/`. This file takes precedence over the one found in the Confluence JAR file.
5.  Start your Confluence server again.

Note that the Velocity macro libraries (macros.vm, menu\_macros.vm) are only loaded once, when Velocity starts up. Therefore, any changes to these files in Confluence will require restarting the application to take effect regardless of the caching settings above.
