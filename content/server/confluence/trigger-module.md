---
aliases:
- /server/confluence/trigger-module-2031710.html
- /server/confluence/trigger-module-2031710.md
category: reference
confluence_id: 2031710
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031710
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031710
date: '2017-12-08'
legacy_title: Trigger Module
platform: server
product: confluence
subcategory: modules
title: Trigger module
---
# Trigger module

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Available:</p></td>
<td><p>Confluence 2.2 to Confluence 5.9</p></td>
</tr>
<tr class="even">
<td><p>Changed:</p></td>
<td><p>In Confluence 3.5 and later, the <code>trigger</code> element accepts an optional child element (<code>managed</code>), which defines the scheduled job's availability in Confluence's administration console.</p></td>
</tr>
<tr class="odd">
<td>Status:</td>
<td><strong>Deprecated</strong> in Confluence 5.10 - use <a href="https://docs.atlassian.com/atlassian-confluence/latest-server/com/atlassian/confluence/plugin/descriptor/JobConfigModuleDescriptor.html" class="external-link">Job Config</a> instead</td>
</tr>
</tbody>
</table>

Trigger plugin modules enable you to schedule when your [Job Module](/server/confluence/job-module) are scheduled to run Confluence.

-   For more information about plugins in general, read [Confluence Plugin Guide](/server/confluence/confluence-plugin-guide).
-   To learn how to install and configure plugins (including macros), read <a href="https://confluence.atlassian.com/x/x2Q" class="external-link">Installing a Plugin</a>.
-   For an introduction to writing your own plugins, read [Writing Confluence Plugins](/server/confluence/writing-confluence-plugins)

## Trigger Plugin Module

The Trigger plugin module schedules Jobs within a plugin. Triggers are one of two types:

-   cron - jobs are scheduled using cron syntax
-   simple - jobs are scheduled to repeat every X seconds

Here is an example `atlassian-plugin.xml` fragment containing a Job with it's corresponding Trigger module using a cron-style expression (for reference, this expression will execute the job with key 'myJob' every minute):

``` xml
<atlassian-plugin name="Sample Component" key="confluence.extra.component">
    ...
    <job key="myJob"
          name="My Job"
          class="com.example.myplugin.jobs.MyJob" />
    <trigger key="myTrigger" name="My Trigger">       
        <job key="myJob" />       
        <schedule cron-expression="0 * * * * ?" />     
        <managed editable="true" keepingHistory="true" canRunAdhoc="true" canDisable="true" />
    </trigger>
    ...
</atlassian-plugin>
```

The `trigger` element accepts the following attributes:

-   `name` -- represents how this component will be referred to in the Confluence interface.
-   `key` -- represents the internal, system name for your Trigger.

The `trigger` element also accepts the following elements:

-   `schedule` -- defines a cron expression in its `cron-expression` attribute. For more information on <a href="#cron-expressions" class="unresolved">cron expressions</a> can be found on the <a href="https://confluence.atlassian.com/x/5YDBBQ" class="external-link">Scheduled Jobs</a> page (or the <a href="http://www.quartz-scheduler.org/docs/tutorials/crontrigger.html" class="external-link">Cron Trigger tutorial on the Quartz website</a>).
-   `managed` *(Available in Confluence 3.5 and later only)* -- an optional element that defines the configuration of the job on the <a href="https://confluence.atlassian.com/x/5YDBBQ" class="external-link">Scheduled Jobs</a> administration page. If the managed element is omitted, then the job will not appear in the <a href="https://confluence.atlassian.com/x/5YDBBQ" class="external-link">Scheduled Jobs</a> administration page. This element takes the following attributes each of which take the values of either `true` or `false`:  
    NOTE: If any of these attributes are omitted, their values are assumed to be `false`.
    -   `editable` -- If `true`, the job's schedule can be edited.
    -   `keepingHistory` -- If `true`, the job's history persists and survives server restarts. If `false`, the job's history is only stored in memory and will be lost upon the next server restart.
    -   `canRunAdhoc` -- If `true`, the job can be executed manually on the *Scheduled Jobs* administration screen.
    -   `canDisable` -- If `true`, the job can be enabled/disabled on the *Scheduled Jobs* administration screen.

Here is another example, this time using a simple trigger that repeats every 3600000 milliseconds (1 hour) and will only repeat 5 times:

``` xml
...
    <trigger key="myTrigger" name="My Trigger">
        <job key="myJob" />
        <schedule repeat-interval="3600000" repeat-count="5" />
     </trigger>    
...
```
