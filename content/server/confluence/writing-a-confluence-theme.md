---
aliases:
- /server/confluence/writing-a-confluence-theme-2818703.html
- /server/confluence/writing-a-confluence-theme-2818703.md
category: devguide
confluence_id: 2818703
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2818703
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2818703
date: '2017-12-08'
legacy_title: Writing a Confluence Theme
platform: server
product: confluence
subcategory: learning
title: Writing a Confluence theme
---
# Writing a Confluence theme

|                       |                 |
|-----------------------|-----------------|
| Level of experience   | BEGINNER        |
| Time estimate         | 0:30            |
| Atlassian application | CONFLUENCE 4.X+ |

## Tutorial overview

This tutorial shows you how to create a Confluence custom theme as a plugin. You'll add `theme` and `colour-scheme` modules to your plugin to personalize look and feel. You can replace your organization's hex codes for the example colors used in this tutorial. 

### **Concepts covered in this tutorial:**

-   Adding `theme` and `colour-scheme` modules to your plugin descriptor
-   Using [QuickReload](https://developer.atlassian.com/docs/developer-tools/automatic-plugin-reinstallation-with-quickreload) to reload Confluence with source code changes

### Prerequisite knowledge

You should have a basic understanding of HTML and CSS. You can complete this tutorial successfully even if you've never created a plugin before.

### Plugin source

We encourage you to work through this tutorial. If you want to skip ahead or check your work when you have finished, you can find the plugin source code on Atlassian Bitbucket. Bitbucket serves a public Git repository containing the tutorial's code. To clone the repository, issue the following command:

``` bash
git clone git@bitbucket.org:atlassian_tutorial/creating-a-confluence-theme.git
```

{{% note %}}

About these Instructions

You can use any supported combination of OS and IDE to create this plugin. These instructions were written using Eclipse Indigo on Mac OS X. If you are using another OS or IDE combination, you should use the equivalent operations for your specific environment.

This tutorial was last tested with **Confluence 5.1.4**. 

{{% /note %}}

## Step 1. Create and prune the plugin skeleton

In this step, you'll create a plugin skeleton using `atlas-` commands. Since you won't need some of the files created in the skeleton, you'll also delete them in this step.

1.  Open a terminal and navigate to your Eclipse workspace directory.  
      
2.  Enter the following command to create a Confluence plugin skeleton:

    ``` bash
    $ atlas-create-confluence-plugin
    ```

3.  When prompted, enter the following information to identify your plugin:

    <table>
    <colgroup>
    <col style="width: 30%" />
    <col style="width: 70%" />
    </colgroup>
    <tbody>
    <tr class="odd">
    <td><p>group-id</p></td>
    <td><p><code>com.example.plugins.tutorial.confluence</code><br />
    </p></td>
    </tr>
    <tr class="even">
    <td><p>artifact-id</p></td>
    <td><p><code>theme-tutorial</code></p></td>
    </tr>
    <tr class="odd">
    <td><p>version</p></td>
    <td><p><code>1.0-SNAPSHOT</code></p></td>
    </tr>
    <tr class="even">
    <td><p>package</p></td>
    <td><p><code>com.atlassian.tutorial.confluence</code></p></td>
    </tr>
    </tbody>
    </table>

4.  Confirm your entries when prompted with `Y` or `y`.

    Your terminal notifies you of a successful build:

    ``` bash
    [INFO] ------------------------------------------------------------------------
    [INFO] BUILD SUCCESSFUL
    [INFO] ------------------------------------------------------------------------
    [INFO] Total time: 1 minute 11 seconds
    [INFO] Finished at: Thu Jul 18 11:30:23 PDT 2013
    [INFO] Final Memory: 82M/217M
    [INFO] ------------------------------------------------------------------------
    ```

5.  Change to the project directory created by the previous step.

    ``` bash
    $ cd theme-tutorial/
    ```

6.  Delete the test directories.

    Setting up testing for your plugin isn't part of this tutorial. Use the following commands to delete the generated test skeleton:

    ``` bash
    $ rm -rf ./src/test/java
    $ rm -rf ./src/test/resources/
    ```

7.  Delete the unneeded Java class files.

    ``` bash
    $ rm ./src/main/java/com/example/plugins/tutorial/confluence/*.java
    ```

8.  Edit the `src/main/resources/atlassian-plugin.xml` file.  
      
9.  Remove the generated `myPluginComponent` component declaration.

    ``` xml
    <!-- publish our component -->
    <component key="myPluginComponent" class="com.example.plugins.tutorial.confluence.simplebp.MyPluginComponentImpl" public="true">
       <interface>com.example.plugins.tutorial.confluence.simplebp.MyPluginComponent</interface>
    </component>
    ```

10. Save and close `atlassian-plugin.xml`.

### Import your project into your IDE

1.   Make your project available to Eclipse.

    ``` bash
    atlas-mvn eclipse:eclipse
    ```

    You'll see a successful build message: 

    ``` bash
    [INFO] ------------------------------------------------------------------------
    [INFO] BUILD SUCCESSFUL
    [INFO] ------------------------------------------------------------------------
    [INFO] Total time: 54 seconds
    [INFO] Finished at: Tue Jul 16 11:03:59 PDT 2013
    [INFO] Final Memory: 82M/224M
    [INFO] ------------------------------------------------------------------------
    ```

2.  Start Eclipse.  
    You can open a new terminal window to perform this action.

    ``` bash
    $ cd ~/eclipse
    $ ./eclipse
    ```

3.  Click **File &gt; Import**.   
    Eclipse starts the **Import** wizard.  
      
4.  Expand the **General** folder tree to choose **Existing Projects into Workspace**.  
      
5.  Click **Next**. 
6.  Click **Browse** and enter the root directory of your workspace.  
    Your Atlassian plugin folder should now appear under **Projects.** 
7.  Ensure your plugin checkbox is ticked and click **Finish**.   
    Eclipse imports your project, and it's now visible in your Package Explorer view. 

## Step 2. Create a simple theme & update the `atlassian-plugin.xml` descriptor

When you generated the plugin skeleton, a CSS directory was created automatically under `src/main/resources/css`. In this step, you'll add some rudimentary CSS to the file. You'll also declare this CSS resource in the `atlassian-plugin.xml` descriptor file. This file describes how your plugin should interact with Confluence. 

You'll add the `Theme` module to disable Confluence's default theme ( `<param name="includeClassicStyles" value="false"/>`), and instead include a resource to your custom CSS file. This module uses an existing Confluence class, `BasicTheme`, which instructs Confluence to load the theme from your plugin. This module also includes an optional `name` and `description` field, which are both visible from the Confluence Admin pages.

1.  In your IDE, navigate to `src/main/resources/css`.  
      
2.  Open the `theme-tutorial.css` file.  
      
3.  Add the following CSS to the file.

    This transforms the normally Atlassian-blue `#header` to black.

    ``` css
    #header .aui-header {
        background-color: black;
        border-color: black;
    }
    ```

4.  Save and close the file.  
      
5.  Open `atlassian-plugin.xml` from `src/main/resources`.  
      
6.  Add the following `<theme>` module before the closing tag of `<atlassian-plugin/>`:

    Configure `includeClassicStyles` to `false` so Confluence ignores backwards-compatible classic themes. 

    The paths to resources you add here are to Confluence directories, not your plugin: You'll add your resource in the next step.

    ``` xml
    <theme key="simpletheme" name="Simple Theme" class="com.atlassian.confluence.themes.BasicTheme">
        <description>A simple custom theme</description>
        <param name="includeClassicStyles" value="false"/>
        <resource type="download" name="default-theme.css" location="/includes/css/default-theme.css">
            <param name="source" value="webContext"/>
        </resource>
    </theme>
    ```

7.  Add your CSS file as a resource inside the `</theme>` tag.

    ``` xml
    <resource type="download" name="theme-tutorial.css" location="/css/theme-tutorial.css"/>
    ```

8.  Save the file.  
      
9.  Update your project changes.

    In this step we use Eclipse. You'll want to use the equivalent command for your IDE from the project root.

    ``` bash
    $ atlas-mvn eclipse:eclipse
    ```

    Here's what the descriptor file should look like up to this point:

    ``` xml
    <atlassian-plugin key="${project.groupId}.${project.artifactId}" name="${project.name}" plugins-version="2">
        <plugin-info>
            <description>${project.description}</description>
            <version>${project.version}</version>
            <vendor name="${project.organization.name}" url="${project.organization.url}" />
            <param name="plugin-icon">images/pluginIcon.png</param>
            <param name="plugin-logo">images/pluginLogo.png</param>
        </plugin-info>
        <!-- add our i18n resource -->
        <resource type="i18n" name="i18n" location="theme-tutorial"/>
        
        <!-- add our web resources -->
        <web-resource key="theme-tutorial-resources" name="theme-tutorial Web Resources">
            <dependency>com.atlassian.auiplugin:ajs</dependency>
            
            <resource type="download" name="theme-tutorial.css" location="/css/theme-tutorial.css"/>
            <resource type="download" name="theme-tutorial.js" location="/js/theme-tutorial.js"/>
            <resource type="download" name="images/" location="/images"/>
            <context>theme-tutorial</context>
        </web-resource>
        
    <theme key="simpletheme" name="Simple Theme" class="com.atlassian.confluence.themes.BasicTheme">
        <description>A simple custom theme</description>
        <param name="includeClassicStyles" value="false"/>
        <resource type="download" name="default-theme.css" location="/includes/css/default-theme.css">
            <param name="source" value="webContext"/>
        </resource>

            <resource type="download" name="theme-tutorial.css" location="/css/theme-tutorial.css"/>

    </theme>
        
        <!-- import from the product container -->
        <component-import key="applicationProperties" interface="com.atlassian.sal.api.ApplicationProperties" />
        
    </atlassian-plugin>
    ```

## Step 3. Build, install, and run your plugin

In this step you'll install your plugin and run Confluence. You'll activate your Simple Theme and verify your CSS changes.

1.  Open a terminal window and navigate to the plugin root folder. 

    ``` bash
    $ cd theme-tutorial/
    ```

2.  Start up Confluence from your project root using the `atlas-run` command.

    ``` bash
    atlas-run
    ```

    This command builds your plugin code, starts a Confluence instance, and installs your plugin. This may take a few minutes.

3.  Locate the URL for Confluence.  
    Your terminal outputs the location of your local Confluence instance, defaulted to <a href="http://localhost:1990/confluence" class="uri external-link">localhost:1990/confluence</a>.

    ``` bash
    [INFO] confluence started successfully in 71s at http://localhost:1990/confluence
    [INFO] Type CTRL-D to shutdown gracefully
    [INFO] Type CTRL-C to exit
    ```

4.  Open your browser and navigate to your local Confluence instance.  
      
5.  Login with `admin` / `admin`.  
      
6.  Click the **Cog icon** from the navigation bar, and choose **Confluence Admin**.  
    The administration console loads.  
      
7.  Click **Themes** from the left-hand sidebar, under **Look & Feel**.  
    The Site Theme page loads, with your custom-created "Simple Theme" listed as an option. Note the title and descriptive text match what you entered.  
    <img src="/server/confluence/images/themespageconfluence.png" title="The Themes page in Confluence" alt="The Themes page in Confluence" width="500" />
8.  Choose the radio button for **Simple Theme** and click **Confirm**.  
    The page reloads, with no changes apparent: the changes you made don't effect the admin page.   
      
9.  Click the **Confluence** logo from the upper left corner to return to the main dashboard.  
    Your CSS is installed! All spaces, save the admin console, will have this navigation color scheme now.   
    <img src="/server/confluence/images/confluencelookingsharpinblack.png" title="Confluence looking sharp in black" alt="Confluence looking sharp in black" width="500" />

## Step 4. Add a `colour-scheme` module to the descriptor file

Black is classic, but in this step you'll use a sophisticated `colour-scheme` module to your descriptor file. Right now, your plugin is pretty basic - it's a theme that just turns the header black. Here, you'll expand your theme to add multiple colors and layouts, and use [QuickReload](https://developer.atlassian.com/docs/developer-tools/automatic-plugin-reinstallation-with-quickreload) to reload the changes in Confluence. 

1.  Open `atlassian-plugin.xml`.   
      
2.  After the closing `</theme>` tag and before the closing `</atlassian-plugin`&gt; tag, add the following to your file.

    This creates an earthy theme with a white background, with different links, text colors, and other attributes.

    ``` xml
        <colour-scheme key="earth-colours" name="Brown and Red Earth Colours" class="com.atlassian.confluence.themes.BaseColourScheme">
            <colour key="property.style.topbarcolour" value="#440000"/>
            <colour key="property.style.spacenamecolour" value="#999999"/>
            <colour key="property.style.headingtextcolour" value="#663300"/>
            <colour key="property.style.linkcolour" value="#663300"/>
            <colour key="property.style.bordercolour" value="#440000"/>
            <colour key="property.style.navbgcolour" value="#663300"/>
            <colour key="property.style.navtextcolour" value="#ffffff"/>
            <colour key="property.style.navselectedbgcolour" value="#440000"/>
            <colour key="property.style.navselectedtextcolour" value="#ffffff"/>
        </colour-scheme>
    ```

3.  Now, tie your `colour-scheme` module to your `theme` module. 

    Add the following code before the closing `</theme>` tag to include it in the resources.

    ``` xml
     <colour-scheme key="${project.groupId}.${project.artifactId}:earth-colours"/>
    ```

4.  Save and close the file.   
      
5.  Update your project changes.   
    If using Eclipse, issue the following command from your project root:

    ``` bash
    $ atlas-mvn eclipse:eclipse
    ```

6.  Rebuild your plugin, and [QuickReload](https://developer.atlassian.com/docs/developer-tools/automatic-plugin-reinstallation-with-quickreload) will ensure you see the changes applied.

{{% note %}}

If you don't see the changes:

You may need to return to **Confluence Admin &gt; Themes** and toggle your theme on or off. Then, even from the admin page, you'll see the changes applied.

{{% /note %}}

<img src="/server/confluence/images/earthtonethemeapplied.png" title="Earth theme applied" alt="Earth theme applied" width="500" />

This is what your `atlassian-plugin.xml` descriptor file should resemble at this point:

``` xml
<atlassian-plugin key="${project.groupId}.${project.artifactId}" name="${project.name}" plugins-version="2">
    <plugin-info>
        <description>${project.description}</description>
        <version>${project.version}</version>
        <vendor name="${project.organization.name}" url="${project.organization.url}" />
        <param name="plugin-icon">images/pluginIcon.png</param>
        <param name="plugin-logo">images/pluginLogo.png</param>
    </plugin-info>
      <!-- add our i18n resource -->
    <resource type="i18n" name="i18n" location="theme-tutorial"/>
    
    <!-- add our web resources -->
    <web-resource key="theme-tutorial-resources" name="theme-tutorial Web Resources">
        <dependency>com.atlassian.auiplugin:ajs</dependency>
        
        <resource type="download" name="theme-tutorial.css" location="/css/theme-tutorial.css"/>
        <resource type="download" name="theme-tutorial.js" location="/js/theme-tutorial.js"/>
        <resource type="download" name="images/" location="/images"/>
        <context>theme-tutorial</context>
    </web-resource>  
    
    
    <theme key="simpletheme" name="Simple Theme" class="com.atlassian.confluence.themes.BasicTheme">
        <description>A simple custom theme</description>
        <param name="includeClassicStyles" value="false"/>
        <resource type="download" name="default-theme.css" location="/includes/css/default-theme.css">
            <param name="source" value="webContext"/>
        </resource>
        
        <resource type="download" name="theme-tutorial.css" location="/css/theme-tutorial.css"/>
        
        <colour-scheme key="${project.groupId}.${project.artifactId}:earth-colours"/>
    </theme>
    <colour-scheme key="earth-colours" name="Brown and Red Earth Colours" class="com.atlassian.confluence.themes.BaseColourScheme">
        <colour key="property.style.topbarcolour" value="#440000"/>
        <colour key="property.style.spacenamecolour" value="#999999"/>
        <colour key="property.style.headingtextcolour" value="#663300"/>
        <colour key="property.style.linkcolour" value="#663300"/>
        <colour key="property.style.bordercolour" value="#440000"/>
        <colour key="property.style.navbgcolour" value="#663300"/>
        <colour key="property.style.navtextcolour" value="#ffffff"/>
        <colour key="property.style.navselectedbgcolour" value="#440000"/>
        <colour key="property.style.navselectedtextcolour" value="#ffffff"/>
    </colour-scheme>
    
</atlassian-plugin>
```

## Next steps

You've changed the look and feel of your Confluence instance. For a more customized workspace in Confluence, you might consider <a href="https://confluence.atlassian.com/display/DOC/Customising+Site+and+Space+Layouts" class="external-link">customizing layouts of your Confluence spaces.</a>
