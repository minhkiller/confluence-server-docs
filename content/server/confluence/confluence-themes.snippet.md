---
aliases:
- /server/confluence/confluence-themes-33723229.html
- /server/confluence/confluence-themes-33723229.md
category: devguide
confluence_id: 33723229
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=33723229
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=33723229
date: '2017-12-08'
legacy_title: Confluence Themes
platform: server
product: confluence
subcategory: other
title: Confluence Themes
---
# Confluence Themes

## Introduction

Customizing the look and feel of confluence can be a very complex task, but it's really a question of how much customization is desired.  What follows are several tutorials on creating more and more complex themes.

-   [Writing a Confluence Theme - Draft](/server/confluence/writing-a-confluence-theme-draft.snippet)
