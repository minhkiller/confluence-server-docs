---
aliases:
- /server/confluence/confluence-server-developer-documentation-2031643.html
- /server/confluence/confluence-server-developer-documentation-2031643.md
category: devguide
confluence_id: 2031643
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031643
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031643
date: '2017-12-08'
legacy_title: Confluence Server Developer Documentation
platform: server
product: confluence
subcategory: index
title: Confluence Server Developer Documentation
---
# Confluence Server Developer Documentation

## Getting Started

There are two main ways to develop with Confluence Server -- using our remote API or developing a plugin. If you're integrating Confluence with another application, you'll most likely want to use the remote API. If you'd like to add capabilities to Confluence, a plugin may be the answer. To get started writing plugins, download the [plugin SDK](https://developer.atlassian.com/display/DOCS/Set+up+the+Atlassian+Plugin+SDK+and+Build+a+Project) and follow the instructions to set up a plugin development environment.

{{% tip %}}

Looking for info on developing add-ons for Confluence Cloud? Check out our new [Confluence Connect docs](https://developer.atlassian.com/display/CONFCLOUD).

{{% /tip %}}

## Main Topics

Get started by [setting up your Atlassian plugin development environment](https://developer.atlassian.com/display/DOCS/Set+up+the+Atlassian+Plugin+SDK+and+Build+a+Project).

**[Themes](/server/confluence/theme-module)**  
Want to customise the look and feel of Confluence? Learn how to provide your custom stylesheets, change layouts and include your own JavaScript elements into Confluence.

**[Custom Features](/server/confluence/xwork-webwork-module)**  
Add new functionality to Confluence by creating your own screens and actions.

**[Gadgets](https://developer.atlassian.com/display/GADGETS/Gadget+Development)**  
Learn how to write Gadgets to expose or consume content in Atlassian applications.

**[Remote API](/server/confluence/confluence-xml-rpc-and-soap-apis)**  
Confluence exposes its data via SOAP/XML-RPC and REST services. Learn how to use the remote APIs to integrate Confluence with your other applications.

## Atlassian Development Hubs

-   [Atlassian Connect](https://developer.atlassian.com/static/connect/docs/)
-   [Developer Quick Start](https://developer.atlassian.com/display/DOCS/Set+up+the+Atlassian+Plugin+SDK+and+Build+a+Project)
-   [Plugin Framework](https://developer.atlassian.com/display/DOCS/Plugin+Framework)
-   [Gadgets](https://developer.atlassian.com/display/GADGETS)
-   [REST APIs](https://developer.atlassian.com/display/DOCS/REST+API+Development)
-   [Confluence](https://developer.atlassian.com/display/CONFDEV)
-   [Jira](https://developer.atlassian.com/display/JIRADEV)
-   [Jira Software](https://developer.atlassian.com/display/JIRADEV/JIRA+Software)
-   [Bamboo](https://developer.atlassian.com/display/BAMBOODEV)
-   [Crowd](https://developer.atlassian.com/display/CROWDDEV)
-   [Fisheye/Crucible](https://developer.atlassian.com/display/FECRUDEV)
-   [Jira Mobile Connect](https://developer.atlassian.com/display/JMC)

{{% note %}}

## Plugin Modules

-   [About Page Panel Module](/server/confluence/about-page-panel-module)
-   [Blueprint Module](/server/confluence/blueprint-module)
-   [Code Formatting Module](/server/confluence/code-formatting-module)
-   [Component Import Module](/server/confluence/component-import-module)
-   [Component Module](/server/confluence/component-module)
-   [Component Module - Old Style](/server/confluence/component-module-old-style)
-   [Content Template Module](/server/confluence/content-template-module)
-   [CQL Field Module](/server/confluence/cql-field-module)
-   [CQL Function Module](/server/confluence/cql-function-module)
-   [Decorator Module](/server/confluence/decorator-module)
-   [Device Type Renderer Module](/server/confluence/device-type-renderer-module)
-   [Dialog Wizard Module](/server/confluence/dialog-wizard-module)
-   [Event Listener Module](/server/confluence/event-listener-module)
-   [Extractor Module](/server/confluence/extractor-module)
-   [Gadget Plugin Module](/server/confluence/gadget-plugin-module)
-   [Index Recoverer Module](/server/confluence/index-recoverer-module)
-   [Job Config Module](/server/confluence/job-config-module)
-   [Job Module](/server/confluence/job-module)
-   [Keyboard Shortcut Module](/server/confluence/keyboard-shortcut-module)
-   [Language Module](/server/confluence/language-module)
-   [Lifecycle Module](/server/confluence/lifecycle-module)
-   [Lucene Boosting Strategy Module](/server/confluence/lucene-boosting-strategy-module)
-   [Macro Module](/server/confluence/macro-module)
-   [Module Type Module](/server/confluence/module-type-module)
-   [Path Converter Module](/server/confluence/path-converter-module)
-   [Promoted Blueprints Module](/server/confluence/promoted-blueprints-module)
-   [Renderer Component Module](/server/confluence/renderer-component-module)
-   [REST Module](/server/confluence/rest-module)
-   [RPC Module](/server/confluence/rpc-module)
-   [Servlet Context Listener Module](/server/confluence/servlet-context-listener-module)
-   [Servlet Context Parameter Module](/server/confluence/servlet-context-parameter-module)
-   [Servlet Filter Module](/server/confluence/servlet-filter-module)
-   [Servlet Module](/server/confluence/servlet-module)
-   [Space Blueprint Module](/server/confluence/space-blueprint-module)
-   [Spring Component Module - Old Style](/server/confluence/spring-component-module-old-style)
-   [Theme Module](/server/confluence/theme-module)
-   [Trigger Module](/server/confluence/trigger-module)
-   [User Macro Module](/server/confluence/user-macro-module)
-   [Velocity Context Module](/server/confluence/velocity-context-module)
-   [WebDAV Resource Module](/server/confluence/webdav-resource-module)
-   [Web Resource Module](/server/confluence/web-resource-module)
-   [Web UI Modules](/server/confluence/web-ui-modules)
-   [Workflow Module](/server/confluence/workflow-module)
-   [XWork-WebWork Module](/server/confluence/xwork-webwork-module)

{{% /note %}}

## Resources

<a href="http://docs.atlassian.com/atlassian-confluence/latest-server" class="external-link">Javadoc</a>  
[REST API Prototype](/server/confluence/confluence-rest-apis-prototype-only)  
[SOAP/RPC Web Service API](/server/confluence/confluence-xml-rpc-and-soap-apis)  
[DTDs and Schemas Database](/server/confluence/dtds-and-schemas)  
[Confluence Architecture](/server/confluence/confluence-architecture)  
<a href="http://marketplace.atlassian.com" class="external-link">Atlassian Marketplace</a>  
[Confluence Developer Documentation Archives](https://developer.atlassian.com/display/ARCHIVES/Confluence+Documentation+Archives)

### Help

<a href="http://confluence.atlassian.com/display/DOC/Confluence+FAQ" class="external-link">Confluence FAQ</a>  
[Developer FAQ](/server/confluence/confluence-developer-faq)  
<a href="https://community.developer.atlassian.com/c/Confluence" class="external-link">The Atlassian Developer Community</a>  
<a href="http://blogs.atlassian.com/developer/" class="external-link">Atlassian Developer Blog</a>  
<a href="http://confluence.atlassian.com/display/APW/Partner+Directory+Home" class="external-link">Atlassian Partners</a>  






























































































































































































































































































































































