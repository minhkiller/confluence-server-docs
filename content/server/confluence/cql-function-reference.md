---
aliases:
- /server/confluence/cql-function-reference-29952571.html
- /server/confluence/cql-function-reference-29952571.md
category: reference
confluence_id: 29952571
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=29952571
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=29952571
date: '2017-12-08'
legacy_title: CQL Function Reference
platform: server
product: confluence
subcategory: api
title: CQL function reference
---
# CQL function reference

The instructions on this page describe how to use functions in CQL to define structured search queries to [search for content in Confluence](/server/confluence/advanced-searching-using-cql).  

## Functions Reference

A function in CQL appears as a word followed by parentheses which may contain one or more explicit values. In a clause, a function is preceded by an [operator](https://developer.atlassian.com/display/CONFDEV/Advanced+Searching+using+CQL#AdvancedSearchingusingCQL-operators), which in turn is preceded by a [field](https://developer.atlassian.com/display/CONFDEV/Advanced+Searching+using+CQL#AdvancedSearchingusingCQL-fields). A function performs a calculation on either specific Confluence data or the function's content in parentheses, such that only true results are retrieved by the function and then again by the clause in which the function is used.

This document also covers the reserved <a href="https://confluence.atlassian.com/display/JIRA/Advanced+Searching+Functions#AdvancedSearchingFunctions-characters" class="external-link">characters</a> and <a href="https://confluence.atlassian.com/display/JIRA/Advanced+Searching+Functions#AdvancedSearchingFunctions-words" class="external-link">words</a> that Confluence uses.

{{% note %}}

**Related topics:**

-   [Advanced Searching using CQL](/server/confluence/advanced-searching-using-cql)
-   [Performing text searches using CQL](/server/confluence/performing-text-searches-using-cql)
-   [CQL Field Reference](/server/confluence/cql-field-reference)

{{% /note %}}

## List of Functions

#### currentUser()

Perform searches based on the currently logged-in user.

Note that this function can only be used by logged-in users. Anonymous users cannot use this function.

###### Syntax

``` sql
currentUser()
```

###### Supported Fields

-   [Creator](https://developer.atlassian.com/display/CONFDEV/Advanced+Searching+using+CQL#AdvancedSearchingusingCQL-creatorcreatorCreator)
-   [Contributor](https://developer.atlassian.com/display/CONFDEV/Advanced+Searching+using+CQL#AdvancedSearchingusingCQL-contributorcontributorContributor)
-   [Mention](https://developer.atlassian.com/display/CONFDEV/Advanced+Searching+using+CQL#AdvancedSearchingusingCQL-mentionMentionMention)
-   [Watcher](https://developer.atlassian.com/display/CONFDEV/Advanced+Searching+using+CQL#AdvancedSearchingusingCQL-Watcher)
-   [Favourite](https://developer.atlassian.com/display/CONFDEV/Advanced+Searching+using+CQL#AdvancedSearchingusingCQL-favouriteFavouriteFavourite,favorite)

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
</tr>
</tbody>
</table>

###### Examples

-   Find content that was created by me

    ``` sql
    creator = currentUser()
    ```

-   Find content that mentions me but wasn't created by me

    ``` sql
    mention = currentUser() and creator != currentUser()
    ```

#### endOfDay()

Perform searches based on the end of the current day. See also [endOfWeek](#endofweek), [endOfMonth](#endofmonth) and [endOfYear](#endofyear); and [startOfDay](#startofday), [startOfWeek](#startofweek), [startOfMonth](#endofmonth) and [startOfYear](#startofyear).

###### Syntax

``` sql
endOfDay()
```

or

``` sql
endOfDay("inc")
```

where `inc` is an optional increment of `(+/-)nn(y|M|w|d|h|m)`

-   If the plus/minus `(+/-)` sign is omitted, plus is assumed.
-   nn: number; y: year, M: month; w: week; d: day; h: hour; m: minute.

###### Supported Fields

-   [Created](https://developer.atlassian.com/display/CONFDEV/Advanced+Searching+using+CQL#AdvancedSearchingusingCQL-createdCreatedCreated)
-   [Lastmodified](https://developer.atlassian.com/display/CONFDEV/Advanced+Searching+using+CQL#AdvancedSearchingusingCQL-lastmodifiedLastModifiedLastModified)

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
</tr>
</tbody>
</table>

###### Examples

-   Find content created since the end of yesterday

    ``` sql
    created > endOfDay("-1d")
    ```

#### endOfMonth()

Perform searches based on the end of the current month. See also [endOfDay](#endofday), [endOfWeek](#endofweek) and [endOfYear](#endofyear); and [startOfDay](#startofday), [startOfWeek](#startofweek), [startOfMonth](#endofmonth) and [startOfYear](#startofyear).

###### Syntax

``` sql
endOfMonth()
```

or

``` sql
endOfMonth("inc")
```

where `inc` is an optional increment of `(+/-)nn(y|M|w|d|h|m)`

-   If the plus/minus `(+/-)` sign is omitted, plus is assumed.
-   nn: number; y: year, M: month; w: week; d: day; h: hour; m: minute.

###### Supported Fields

-   [Created](https://developer.atlassian.com/display/CONFDEV/Advanced+Searching+using+CQL#AdvancedSearchingusingCQL-createdCreatedCreated)
-   [Lastmodified](https://developer.atlassian.com/display/CONFDEV/Advanced+Searching+using+CQL#AdvancedSearchingusingCQL-lastmodifiedLastModifiedLastModified)

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
</tr>
</tbody>
</table>

###### Examples

-   Find content modified before the end of the month

    ``` sql
    lastmodified < endOfMonth()
    ```

#### endOfWeek()

Perform searches based on the end of the current week. See also [endOfDay](#endofday), [endOfMonth](#endofmonth) and [endOfYear](#endofyear); and [startOfDay](#startofday), [startOfWeek](#startofweek), [startOfMonth](#endofmonth) and [startOfYear](#startofyear).

###### Syntax

``` sql
endOfWeek()
```

or

``` sql
endOfWeek("inc")
```

where `inc` is an optional increment of `(+/-)nn(y|M|w|d|h|m)`

-   If the plus/minus `(+/-)` sign is omitted, plus is assumed.
-   nn: number; y: year, M: month; w: week; d: day; h: hour; m: minute.

###### Supported Fields

-   [Created](https://developer.atlassian.com/display/CONFDEV/Advanced+Searching+using+CQL#AdvancedSearchingusingCQL-createdCreatedCreated)
-   [Lastmodified](https://developer.atlassian.com/display/CONFDEV/Advanced+Searching+using+CQL#AdvancedSearchingusingCQL-lastmodifiedLastModifiedLastModified)

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
</tr>
</tbody>
</table>

###### Examples

-   Find content created after the end of last week

    ``` sql
    created > endOfWeek("-1w")
    ```

#### endOfYear()

Perform searches based on the end of the current year. See also [startOfDay](#startofday), [startOfWeek](#startofweek) and [startOfMonth](#endofmonth); and [endOfDay](#endofday), [endOfWeek](#endofweek), [endOfMonth](#endofmonth) and [endOfYear](#endofyear).

``` sql
endOfYear()
```

or

``` sql
endOfYear("inc")
```

where `inc` is an optional increment of `(+/-)nn(y|M|w|d|h|m)`

-   If the plus/minus `(+/-)` sign is omitted, plus is assumed.
-   nn: number; y: year, M: month; w: week; d: day; h: hour; m: minute.

###### Supported Fields

-   [Created](https://developer.atlassian.com/display/CONFDEV/Advanced+Searching+using+CQL#AdvancedSearchingusingCQL-createdCreatedCreated)
-   [Lastmodified](https://developer.atlassian.com/display/CONFDEV/Advanced+Searching+using+CQL#AdvancedSearchingusingCQL-lastmodifiedLastModifiedLastModified)

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
</tr>
</tbody>
</table>

###### Examples

-   Find content created by the end of this year:

    ``` sql
    created < endOfYear()
    ```

#### startOfDay()

Perform searches based on the start of the current day. See also [endOfWeek](#endofweek), [endOfMonth](#endofmonth) and [endOfYear](#endofyear); and [startOfDay](#startofday), [startOfWeek](#startofweek), [startOfMonth](#endofmonth) and [startOfYear](#startofyear).

###### Syntax

``` sql
startOfDay()
```

or

``` sql
startOfDay("inc")
```

where `inc` is an optional increment of `(+/-)nn(y|M|w|d|h|m)`

-   If the plus/minus `(+/-)` sign is omitted, plus is assumed.
-   nn: number; y: year, M: month; w: week; d: day; h: hour; m: minute.

###### Supported Fields

-   [Created](https://developer.atlassian.com/display/CONFDEV/Advanced+Searching+using+CQL#AdvancedSearchingusingCQL-createdCreatedCreated)
-   [Lastmodified](https://developer.atlassian.com/display/CONFDEV/Advanced+Searching+using+CQL#AdvancedSearchingusingCQL-lastmodifiedLastModifiedLastModified)

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
</tr>
</tbody>
</table>

###### Examples

-   Find content created since the start of today

    ``` sql
    created > startOfDay()
    ```

-   Find content created in the last 7 days

    ``` sql
    created > startOfDay("-7d")
    ```

#### endOfMonth()

Perform searches based on the start of the current month. See also [endOfDay](#endofday), [endOfWeek](#endofweek) and [endOfYear](#endofyear); and [startOfDay](#startofday), [startOfWeek](#startofweek), [startOfMonth](#endofmonth) and [startOfYear](#startofyear).

###### Syntax

``` sql
startOfMonth()
```

or

``` sql
startOfMonth("inc")
```

where `inc` is an optional increment of `(+/-)nn(y|M|w|d|h|m)`

-   If the plus/minus `(+/-)` sign is omitted, plus is assumed.
-   nn: number; y: year, M: month; w: week; d: day; h: hour; m: minute.

###### Supported Fields

-   [Created](https://developer.atlassian.com/display/CONFDEV/Advanced+Searching+using+CQL#AdvancedSearchingusingCQL-createdCreatedCreated)
-   [Lastmodified](https://developer.atlassian.com/display/CONFDEV/Advanced+Searching+using+CQL#AdvancedSearchingusingCQL-lastmodifiedLastModifiedLastModified)

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
</tr>
</tbody>
</table>

###### Examples

-   Find content created since the start of the month

    ``` sql
    created >= startOfMonth()
    ```

#### startOfWeek()

Perform searches based on the start of the current week. See also [endOfDay](#endofday), [endOfMonth](#endofmonth) and [endOfYear](#endofyear); and [startOfDay](#startofday), [startOfWeek](#startofweek), [startOfMonth](#endofmonth) and [startOfYear](#startofyear).

###### Syntax

``` sql
startOfWeek()
```

or

``` sql
startOfWeek("inc")
```

where `inc` is an optional increment of `(+/-)nn(y|M|w|d|h|m)`

-   If the plus/minus `(+/-)` sign is omitted, plus is assumed.
-   nn: number; y: year, M: month; w: week; d: day; h: hour; m: minute.

###### Supported Fields

-   [Created](https://developer.atlassian.com/display/CONFDEV/Advanced+Searching+using+CQL#AdvancedSearchingusingCQL-createdCreatedCreated)
-   [Lastmodified](https://developer.atlassian.com/display/CONFDEV/Advanced+Searching+using+CQL#AdvancedSearchingusingCQL-lastmodifiedLastModifiedLastModified)

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
</tr>
</tbody>
</table>

###### Examples

-   Find content created since the start of the week

    ``` sql
    created >= startOfWeek()
    ```

#### startOfYear()

Perform searches based on the start of the current year. See also [startOfDay](#startofday), [startOfWeek](#startofweek) and [startOfMonth](#endofmonth); and [endOfDay](#endofday), [endOfWeek](#endofweek), [endOfMonth](#endofmonth) and [endOfYear](#endofyear).

``` sql
startOfYear()
```

or

``` sql
startOfYear("inc")
```

where `inc` is an optional increment of `(+/-)nn(y|M|w|d|h|m)`

-   If the plus/minus `(+/-)` sign is omitted, plus is assumed.
-   nn: number; y: year, M: month; w: week; d: day; h: hour; m: minute.

###### Supported Fields

-   [Created](https://developer.atlassian.com/display/CONFDEV/Advanced+Searching+using+CQL#AdvancedSearchingusingCQL-createdCreatedCreated)
-   [Lastmodified](https://developer.atlassian.com/display/CONFDEV/Advanced+Searching+using+CQL#AdvancedSearchingusingCQL-lastmodifiedLastModifiedLastModified)

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
</tr>
</tbody>
</table>

###### Examples

-   Find content created this year

    ``` sql
    created >= startOfYear()
    ```

#### favouriteSpaces()

Returns a list of space keys, corresponding to the favourite spaces of the logged in user. 

###### Syntax

``` sql
favouriteSpaces()
```

###### Supported Fields

-   [Space](https://developer.atlassian.com/display/CONFDEV/CQL+Field+Reference#CQLFieldReference-spaceSpaceSpace)

###### Supported Operators

###### 

<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
</tr>
</tbody>
</table>

###### Examples

-   Find content which exists in the favourite spaces of the logged in user 

``` sql
space IN favouriteSpaces()
```

-   Find content which exists in the favourite spaces of the logged in user as well as other listed spaces

``` sql
space IN (favouriteSpaces(), 'FS', 'TS')
```

###### Available from version

5.9

#### recentlyViewedContent()

Returns a list of IDs of recently viewed content for the logged in user.

###### Syntax

``` sql
recentlyViewedContent(limit, offset)
```

###### Supported Fields

-   ancestor
-   content
-   id
-   parent

Supported Operators

<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
</tr>
</tbody>
</table>

Examples

-   Find contents with limit to recent 10 history

``` sql
id in recentlyViewedContent(10)
```

-   Find contents with limit to recent 10 history, but skip first 20

``` sql
id in recentlyViewedContent(10, 20)
```

###### Available from version

5.9

#### recentlyViewedSpaces()

Returns a list of key of spaces recently viewed by the logged in user.

###### Syntax

``` sql
recentlyViewedSpaces(limit)
```

###### Supported Fields

-   space

Supported Operators

<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
</tr>
</tbody>
</table>

Examples

-   Find spaces with limit to recent 10 history

``` sql
 space in recentlyViewedSpaces(10)
```

###### Available from version

5.9

## Reserved Characters

CQL has a list of reserved characters :

-   **space** (`" "`)
-   `"+"`
-   `"."`
-   `","`
-   `";"`
-   `"?"`
-   `"|"`
-   `"*"`
-   `"/"`
-   `"%"`
-   `"^"`
-   `"$"`
-   `"#"`
-   `"@"`
-   `"["`
-   `"]"`

If you wish to use these characters in queries, you need to:

-   surround them with quote-marks (you can use either single quote-marks (`'`) or double quote-marks (`"`));  
    **and,** if you are searching a text field and the character is on the list of <a href="https://confluence.atlassian.com/display/JIRA/Performing+Text+Searches#PerformingTextSearches-escaping" class="external-link">reserved characters for Text Searches</a>,
-   precede them with two backslashes.

## Reserved Words

CQL has a list of reserved words. These words need to be surrounded by quote-marks if you wish to use them in queries:

"after", "and", "as", "avg", "before", "begin", "by","commit", "contains", "count", "distinct", "else", "empty", "end", "explain", "from", "having", "if", "in", "inner", "insert", "into", "is", "isnull", "left", "like", "limit", "max", "min", "not", "null", "or", "order", "outer", "right", "select", "sum", "then", "was", "where", "update"
