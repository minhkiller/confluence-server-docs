---
aliases:
- /server/confluence/plugin-resources-2031716.html
- /server/confluence/plugin-resources-2031716.md
category: devguide
confluence_id: 2031716
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031716
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031716
date: '2017-12-08'
guides: guides
legacy_title: Plugin Resources
platform: server
product: confluence
subcategory: learning
title: Plugin resources
---
# Plugin resources

If your plugin requires a lot of resources, you may wish to expose a directory of files as resources, rather than writing definitions for each individual file.

``` xml
<resource type="download" name="icons/" location="templates/extra/autofavourite/icons/"/>
```

-   The `name` and `location` must both have trailing slashes
-   Subdirectories are also exposed, so in the example above, `icons/small/icn_auto_fav.gif` will be mapped to the resource`templates/extra/autofavourite/icons/small/icn_auto_fav.gif`

After defining the downloadable resources directory, you can refer to it by URL. The URL for a downloadable resource is as follows:  
{`server root}/download/resources/`

{plugin key}:{module key}/{resource name}

NOTE:  *{*`module key`*} is optional.*

For example:  
http://bamboo.example.com/download/resources/com.atlassian.bamboo.plugin.autofavourite:autofavourite-resources/icn_auto_fav.gif  

For details:  
[Downloadable Plugin Resources](https://developer.atlassian.com/display/BAMBOODEV/Downloadable+Plugin+Resources)
