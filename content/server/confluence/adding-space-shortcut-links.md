---
aliases:
- /server/confluence/adding-space-shortcut-links-16973954.html
- /server/confluence/adding-space-shortcut-links-16973954.md
category: devguide
confluence_id: 16973954
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=16973954
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=16973954
date: '2017-12-08'
legacy_title: Adding Space Shortcut Links
platform: server
product: confluence
subcategory: learning
title: Adding space shortcut links
---
# Adding space shortcut links

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Applicable:</p></td>
<td><p>This tutorial applies to <strong>Confluence 5.0.</strong></p></td>
</tr>
<tr class="even">
<td><p>Level of experience:</p></td>
<td><p>This is an <strong>intermediate</strong> tutorial. You should have completed at least one beginner tutorial before working through this tutorial. See the<a href="https://developer.atlassian.com/display/DOCS/Tutorials"> list of developer tutorials</a>.</p></td>
</tr>
</tbody>
</table>

There are two kind of links in the space sidebar:

-   Main links
-   Space shortcut links (quick links)

![](/server/confluence/images/image2012-12-12-14:55:10.png)

This tutorial will explain how to create either of them. There is one additional kind of link:

-   Advanced screens. This part will not be treated in this tutorial, as it is not new to Confluence 5.0 - They are described on [Web UI Modules](/server/confluence/web-ui-modules) as the location "system.space.admin".

Here is a comparison of the sidebar link types:

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 40%" />
<col style="width: 40%" />
</colgroup>
<thead>
<tr class="header">
<th>Type</th>
<th>When are they created?</th>
<th>When are they removed?</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Main Links</td>
<td><ul>
<li>For all spaces, by default.</li>
</ul></td>
<td><ul>
<li>When the Space Administrator hides them.</li>
<li>When the plugin is uninstalled.</li>
</ul></td>
</tr>
<tr class="even">
<td>Quick Link / Space Shortcut Link</td>
<td><ul>
<li>When the Space Administrator creates one,</li>
<li>When the user uses the Create button,</li>
<li>When a plugin calls the API.</li>
</ul></td>
<td><ul>
<li>When the Space Administrator removes them.</li>
</ul></td>
</tr>
</tbody>
</table>

# Download the example

The example below is available on the following BitBucket repository:
https://bitbucket.org/atlassian_tutorial/space-plugin-example

# Preparation

You need to create a Confluence plugin and update the version of Confluence to 5.0-m9.

# Space shortcut link

If you have followed the [Create content plugin](/server/confluence/confluence-blueprints), your users can now open the Create dialog to insert a new page. Content of the same type is grouped in a collector. A link to the collector is created automatically in the space sidebar. This is called a "Space Shortcut Link" or "Quick Link".

Quick Links are the preferred way to link to collectors. Below is the example of the "Meeting Notes" collector. As you can see, there is a Quick Link for "Meeting Notes" in the side bar, next to "Requirements".

<img src="/server/confluence/images/image2012-12-12-11:24:59.png" width="500" />

Plugins using the confluence-create-content-plugin do not need to call the API, as the Quick Link is created when the user goes through the Create wizard.

### Programmatically creating a shortcut link

There are other situations when you would like to create Quick Links. If your plugin relies on the confluence-create-content-plugin, the shortcut link is created automatically; Otherwise you will need to call the API.

#### Declare a dependency the confluence-space-ia plugin

In your pom.xml's &lt;dependencies&gt; section, add the following element:

``` xml
<dependency>
    <groupId>com.atlassian.confluence.plugins</groupId>
    <artifactId>confluence-space-ia</artifactId>
    <version>${confluence.version}</version>
    <scope>provided</scope>
</dependency>
```

#### Import the component

In your atlassian-plugin.xml, import SidebarLinkService:

``` xml
<component-import key="sidebar-link-service" interface="com.atlassian.confluence.plugins.ia.service.SidebarLinkService" />
```

#### Call the service to create the shortcut link

``` java
// Get the ID of a page in the space
String id = Long.toString(space.getHomePage().getId());
// Note: Soon the API will use Long instead of String for the pageId.
if (sidebarLinkService.hasQuickLink(this.getSpaceKey(), id)) {
    sidebarLinkService.create(getSpaceKey(), id, null, null);
}
```

#### Parameters

For the method SidebarLinkService\#create():

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th>Name*</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>spaceKey</td>
<td><p>The key of the space where you want to insert your Quick Link.</p>
<p>Case-sensitive.</p></td>
</tr>
<tr class="even">
<td>pageId</td>
<td><p>The ID of the content you want to link to. It can either be a Page or a Blog post.</p>
<p>The EAP API takes a String, but it will be changed to a Long in Confluence 5.0.</p>
<p>If null, the url is mandatory.</p></td>
</tr>
<tr class="odd">
<td>customTitle</td>
<td><p>The title of the link.</p>
<p><strong>Default:</strong> Title of the target page or url.</p></td>
</tr>
<tr class="even">
<td>url</td>
<td>In case of a link to external content, url of the link.</td>
</tr>
<tr class="odd">
<td>iconClass</td>
<td><p>The CSS class of the icon.</p>
<p><strong>Default:</strong> The default icon for Quick Links.</p></td>
</tr>
</tbody>
</table>

**\*spaceKey parameter is required**

# Main link

This section is presented as a second part of this tutorial because it is not the preferable way to link to collectors: See the table in the introduction.

Pages and Blogs are examples of Main Links:

![](/server/confluence/images/image2012-12-12-13:52:22.png)

### Declaring a Main Link

Main Links are declared as Web-Items in atlassian-plugin.xml, under the location "system.space.sidebar/main-links". Here is the example for a custom link link:

``` xml
<web-item key="plugin-content-main-link" name="Plugin Content" section="system.space.sidebar/main-links" weight="30">
    <label key="plugin.content.title"/>
    <link id="plugin-content-main-link-id">/plugins/${project.artifactId}/view-custom-content.action?key=$generalUtil.urlEncode($helper.spaceKey)</link>
    <styleClass>plugin-content-main-link</styleClass>
</web-item>
```

This [Web Item](/server/confluence/web-item-plugin-module) conforms the the standard definition. The style class can be defined by a CSS as follows:

``` xml
<!-- CSS for the plugin-content-main-link icon -->
<web-resource key="space-links-web-resources">
    <resource type="download" name="space-links.css" location="css/space-links.css"/>
    <context>main</context>
</web-resource>
```

#### Calculating a URL in a Main Link

It sometimes happens that you don't know the URL of the link at the time of writing atlassian-plugin.xml. You can inject it using a Context Provider. Please refer to:

-   <a href="https://bitbucket.org/atlassian_tutorial/space-plugin-example" class="external-link">The sample plugin on BitBucket</a>,
-   [The Context Provider element on the Web Item page](/server/confluence/web-item-plugin-module#provider-element)

### Next Step

If you declare a main link for your custom content, you may want to create an index page for a custom content. See [Writing an space index page for a content type](/server/confluence/writing-an-space-index-page-for-a-content-type).
