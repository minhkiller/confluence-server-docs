---
aliases:
- /server/confluence/instruction-text-in-blueprints-18252354.html
- /server/confluence/instruction-text-in-blueprints-18252354.md
category: devguide
confluence_id: 18252354
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=18252354
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=18252354
date: '2017-12-08'
guides: guides
legacy_title: Instruction text in blueprints
platform: server
product: confluence
subcategory: learning
title: Instruction text in blueprints
---
# Instruction text in blueprints

## Including Instructional Text in Blueprints

Blueprints (and other add-ons) can make use of the new "Instructional Text" feature for templates. The purpose of instructional text is to:

-   Make it easy for a template creator to include information on how to fill out the template.
-   Make it easy for the template end-users to fill out a template.  This is achieved by:
    -   automatically clearing all "instructional text" as the user types in a specific text block, or
    -   automatically triggering a @mention prompt for user selection.
    -   automatically triggering the JIRA Issues macro dialog to enable the user to search for or create an issue.

<img src="/server/confluence/images/image2013-3-18-14:45:31.png" width="567" height="122" />

Instructional text can be inserted into a blueprint or template using the `<ac:placeholder>instruction text</ac:placeholder>` notation in the storage format.

There is also an optional `ac:type` attribute that can specify the type of instructional text that the placeholder represents. Confluence provides support for the following types: 

-   `mention`  - this type will activate the user search autocomplete to insert a user mention into the page. 
-   `jira` - this type will launch the JIRA Issues macro dialog and allow the user to search for or create an issue. 

Example storage format for instructional text.

``` xml
<ul>
    <li><ac:placeholder>This is an example of instruction text that will get replaced when a user selects the text and begins typing.</ac:placeholder></li>
    <li><ac:placeholder ac:type="jira">This placeholder will launch the JIRA Issues macro dialog when clicked</ac:placeholder></li>
</ul>
<ac:task-list>
    <ac:task>
        <ac:task-status>incomplete</ac:task-status>
        <ac:task-body><ac:placeholder ac:type="mention">@mention example. This placeholder will automatically search for a user to mention in the page when the user begins typing.</ac:placeholder></ac:task-body>
    </ac:task>
</ac:task-list>
```

## Adding Instruction Text Types

It is possible to add support for additional types of instructional text via a plugin. This can be achieved by adding a tiny\_mce plugin inside your plugin as follows.

### Add a new Javascript file to extend the editor

The following is a skeleton file to add support for another type of instructional text.

**editor\_plugin\_src.js**

``` javascript
(function($) {
    tinymce.create('tinymce.plugins.InstructionalTextExample', {
        init : function(ed) {
            // This adds support for this type of instructional text into the template editor
            if(AJS.Rte.Placeholder && AJS.Rte.Placeholder.addPlaceholderType) {
                AJS.Rte.Placeholder.addPlaceholderType({
                    type: 'example',
                    label: AJS.I18n.getText("property.panel.textplaceholder.display.example"),
                    tooltip: AJS.I18n.getText("property.panel.textplaceholder.display.example.tooltip"),
                    // The following defines how the placeholder can be activated. It is optional, and if omitted will have the following default values
                    activation: {
                        click: false,
                        keypress: true
                    }
                });
            }
            // This adds support to responding to this instruction text being replaced in the editor
            AJS.bind('editor.text-placeholder.activated', function(e, data) {
                if(data && data.placeholderType === "example") {
                    // do something special here for this type of instruction text
                }
            });
        },
        getInfo : function() {
            return {
                longname : 'Instructional Text Type Example',
                author : 'Atlassian',
                authorurl : 'http://www.atlassian.com',
                version : tinymce.majorVersion + "." + tinymce.minorVersion
            };
        }
    });
    tinymce.PluginManager.add('instructionaltextexample', tinymce.plugins.InstructionalTextExample);
})(AJS.$);


AJS.Rte.BootstrapManager.addTinyMcePluginInit(function(settings) {
    settings.plugins += ",instructionaltextexample";
});
```

Example `web-resource` for `atlassian-plugin.xml`:

``` xml
    <web-resource key="editor-example-instructional-text-resources" name="Example Instruction Text Resources">
        <description>Example Instruction Text Resources</description>
        <transformation extension="js">
            <transformer key="jsI18n"/>
        </transformation>
        <resource name="tinyMce-plugins-example-instructional-text.js" type="download" location="js/editor_plugin_src.js"/>
        <context>editor</context>
    </web-resource>
```
