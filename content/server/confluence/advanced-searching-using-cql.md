---
aliases:
- /server/confluence/advanced-searching-using-cql-29951914.html
- /server/confluence/advanced-searching-using-cql-29951914.md
category: reference
confluence_id: 29951914
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=29951914
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=29951914
date: '2017-12-08'
legacy_title: Advanced Searching using CQL
platform: server
product: confluence
subcategory: api
title: Advanced Searching using CQL
---
# Advanced Searching using CQL

The instructions on this page describe how to define and execute a search using the advanced search capabilities of the Confluence REST API. 

## What is an advanced search?

An advanced search allows you to use structured queries to search for content in Confluence. Your search results will take the same form as the Content model returned by the Content REST API.

When you perform an advanced search, you are using the Confluence Query Language (CQL).

A simple query in CQL (also known as a 'clause') consists of a *[field](#field)*, followed by an *[operator](#cql-operators)*, followed by one or more *values* or *[functions](/server/confluence/cql-function-reference)*. For example, the following simple query will find all content in the "TEST" space. It uses uses the Space *[field](#field)*, the [EQUALS](#equals) *operator*, and the *value* `"TEST"`.)

``` bash
space = "TEST"
```

It is not possible to compare two [fields](#fields).

NOTE: CQL gives you some SQL-like syntax, such as the [ORDER BY](#order-by) SQL keyword. However, CQL is not a database query language. For example, CQL does not have a `SELECT` statement.

-   [Function Reference](/server/confluence/cql-function-reference)

**Related topics:**

-   [Performing text searches using CQL](/server/confluence/performing-text-searches-using-cql)
-   [Adding a field to CQL](/server/confluence/adding-a-field-to-cql)
-   [CQL Field Module](/server/confluence/cql-field-module)
-   [CQL Function Module](/server/confluence/cql-function-module)

### How to perform an advanced search

The Content API REST Resource now supports CQL as a query parameter to filter the list of returned content.

``` xml
http://myhost:8080/rest/api/content/search?cql=space=TEST
```

To perform an advanced search:

1.  Add your query using the [fields](#fields), [operators](#cql-operators) and field values or [functions](#functions) as the value for the CQL query parameter.

2.  Execute a GET request on the resource, you can apply expansions and pagination as you would normally in the Confluence REST API.

### Performing text searches

You can use the [CONTAINS](#contains) operator to use Lucene's text-searching features when performing searches on these fields:

-   title
-   text
-   space.title

For details, please see the page on [Performing text searches](/server/confluence/performing-text-searches-using-cql).

### Setting precedence of operators

You can use parentheses in complex CQL statements to enforce the precedence of [operators](#cql-operators).

For example, if you want to find all pages in the Developer space as well as all blog posts created by the the system administrator (bobsmith), you can use parentheses to enforce the precedence of the boolean operators in your query. For example:

``` sql
(type=page and Space=DEV) OR (creator=bobsmith and type=blogpost)
```

Note: if you do not use parentheses, the statement will be evaluated left to right.

You can also use parentheses to group clauses, so that you can apply the [NOT](#not) operator to the group.

## Keyword reference

## A keyword in CQL is a word or phrase that

-   joins two or more clauses together to form a complex CQL query, or
-   alters the logic of one or more clauses, or
-   alters the logic of [operators](https://developer.atlassian.com/display/CONFDEV/Advanced+Searching+using+CQL#AdvancedSearchingusingCQL-operators), or
-   has an explicit definition in a CQL query, or
-   performs a specific function that alters the results of a CQL query.

**List of Keywords:**

#### AND

Used to combine multiple clauses, allowing you to refine your search.

Note: you can use [parentheses](https://developer.atlassian.com/display/CONFDEV/Advanced+Searching+using+CQL#AdvancedSearchingusingCQL-parentheses) to control the order in which clauses are executed.

###### Examples

-   Find all blogposts with the label "performance"

    ``` sql
    label = "performance" and type = "blogpost"
    ```

-   Find all pages created by jsmith in the DEV space

    ``` sql
    type = page and creator = jsmith and space = DEV
    ```

-   Find all content that mentions jsmith but was not created by jsmith

    ``` sql
    mention = jsmith and creator != jsmith
    ```

#### OR

Used to combine multiple clauses, allowing you to expand your search.

Note: you can use [parentheses](https://developer.atlassian.com/display/CONFDEV/Advanced+Searching+using+CQL#AdvancedSearchingusingCQL-parentheses) to control the order in which clauses are executed.

(Note: also see [IN](https://developer.atlassian.com/display/CONFDEV/CQL+Operators+Reference#CQLOperatorsReference-IN), which can be a more convenient way to search for multiple values of a field.)

###### Examples

-   Find all content in the IDEAS space or with the label idea

    ``` sql
    space = IDEAS or label = idea
    ```

-   Find all content last modified before the start of the year or with the label needs\_review

    ``` sql
    lastModified < startOfYear() or label = needs_review
    ```

#### NOT

Used to negate individual clauses or a complex CQL query (a query made up of more than one clause) using [parentheses](https://developer.atlassian.com/display/CONFDEV/Advanced+Searching+using+CQL#AdvancedSearchingusingCQL-parentheses), allowing you to refine your search.

(Note: also see [NOT EQUALS](https://developer.atlassian.com/display/CONFDEV/CQL+Operators+Reference#CQLOperatorsReference-NOT_EQUALS) ("!="), [DOES NOT CONTAIN](#does-not-contain) ("!~") and [NOT IN](https://developer.atlassian.com/display/CONFDEV/CQL+Operators+Reference#CQLOperatorsReference-NOT_IN).)

###### Examples

-   Find all pages with the "cql" label that aren't in the dev space

    ``` sql
    label = cql and not space = dev 
    ```

#### ORDER BY

Used to specify the fields by whose values the search results will be sorted.

By default, the field's own sorting order will be used. You can override this by specifying ascending order ("`asc`") or descending order ("`desc`").

Not all fields support Ordering. Generally, ordering is not supported where a piece of content can have multiple values for a field, for instance ordering is not supported on labels.

###### Examples

-   Find content in the DEV space ordered by creation date

    ``` sql
    space = DEV order by created
    ```

-   Find content in the DEV space ordered by creation date with the newest first, then title

    ``` sql
    space = DEV order by created desc, title
    ```

-   Find pages created by jsmith ordered by space, then title

    ``` sql
    creator = jsmith order by space, title asc
    ```

## Operator reference

## CQL Operators

An operator in CQL is one or more symbols or words which compares the value of a [field](https://developer.atlassian.com/display/CONFDEV/Advanced+Searching+using+CQL#AdvancedSearchingusingCQL-field) on its left with one or more values (or [functions](https://developer.atlassian.com/display/CONFDEV/Advanced+Searching+using+CQL#AdvancedSearchingusingCQL-functions)) on its right, such that only true results are retrieved by the clause. Some operators may use the [NOT](https://developer.atlassian.com/display/CONFDEV/CQL+Keywords+Reference#CQLKeywordsReference-NOT) keyword.

**List of Operators:**

-   [EQUALS: =](#equals)
-   [NOT EQUALS: !=](#not-equals)
-   [GREATER THAN: &gt;](#greater-than-gt)
-   [GREATER THAN EQUALS: &gt;=](#greater-than-equals-gt)
-   [LESS THAN: &lt;](#less-than-lt)
-   [LESS THAN EQUALS: &lt;=](#less-than-equals-lt)
-   [IN](#in)
-   [NOT IN](#not-in)
-   [CONTAINS: ~](#contains)
-   [DOES NOT CONTAIN: !~](#does-not-contain)

#### EQUALS: =

The "`=`" operator is used to search for content where the value of the specified field exactly matches the specified value. (Note: cannot be used with [text](https://developer.atlassian.com/display/CONFDEV/Advanced+Searching+using+CQL#AdvancedSearchingusingCQL-text) fields; see the [CONTAINS](#contains) operator instead.)

To find content where the value of a specified field exactly matches *multiple* values, use multiple "`=`" statements with the [AND](https://developer.atlassian.com/display/CONFDEV/CQL+Keywords+Reference#CQLKeywordsReference-AND) operator.

###### Examples

-   Find all content that were created by jsmith:

    ``` sql
    creator = jsmith
    ```

-   Find all content that has the title "Advanced Searching"

    ``` sql
    title = "Advanced Searching"
    ```

#### NOT EQUALS: !=

The "`!=`" operator is used to search for content where the value of the specified field does not match the specified value. (Note: cannot be used with [text](#performing-text-searches) fields; see the [DOES NOT MATCH](#does-not-match) ("`!~`") operator instead.)

Note: typing `field != value` is the same as typing `NOT field = value`.

{{% note %}}

Currently a negative expression cannot be the first clause in a CQL statement

{{% /note %}}

###### Examples

-   Find all content in the DEV space that was created by someone other than jsmith:

    ``` sql
    space = DEV and not creator = jsmith
    ```

    or:

    ``` sql
    space = DEV and creator != jsmith
    ```

-   Find all content that was created by me but doesn't mention me

    ``` sql
    creator = currentUser() and mention != currentUser()
    ```

#### GREATER THAN: &gt;

The "`>`" operator is used to search for content where the value of the specified field is greater than the specified value. Cannot be used with [text](#performing-text-searches) fields.

Note that the "`>`" operator can only be used with fields which support range operators (e.g. date fields and numeric fields). To see a field's supported operators, check the individual [field](https://developer.atlassian.com/display/CONFDEV/Advanced+Searching+using+CQL#AdvancedSearchingusingCQL-field) reference.

###### Examples

-   Find all content created in the last 4 weeks

    ``` sql
    created > now("-4w")
    ```

-   Find all attachments last modified since the start of the month

    ``` sql
    created > startOfMonth() and type = attachment
    ```

#### GREATER THAN EQUALS: &gt;=

The "`>=`" operator is used to search for content where the value of the specified field is greater than or equal to the specified value. Cannot be used with [text](#performing-text-searches) fields.

Note that the "`>=`" operator can only be used with fields which support range operators (e.g. date fields). To see a field's supported operators, check the individual [field](https://developer.atlassian.com/display/CONFDEV/Advanced+Searching+using+CQL#AdvancedSearchingusingCQL-field) reference.

###### Examples

-   Find all content created on or after 31/12/2008:

    ``` sql
    created >= "2008/12/31"
    ```

#### LESS THAN: &lt;

The "`<`" operator is used to search for content where the value of the specified field is less than the specified value. Cannot be used with [text](https://developer.atlassian.com/display/CONFDEV/Advanced+Searching+using+CQL#AdvancedSearchingusingCQL-text) fields.

Note that the "`<`" operator can only be used with fields which support range operators (e.g. date fields). To see a field's supported operators, check the individual [field](https://developer.atlassian.com/display/CONFDEV/Advanced+Searching+using+CQL#AdvancedSearchingusingCQL-field) reference.

###### Examples

-   Find all pages lastModified before the start of the year

    ``` sql
    lastModified < startOfYear() and type = page
    ```

#### LESS THAN EQUALS: &lt;=

The "`<=`" operator is used to search for content where the value of the specified field is less than or equal to than the specified value. Cannot be used with [text](#performing-text-searches) fields.

Note that the "`<=`" operator can only be used with fields which support range operators (e.g. date fields). To see a field's supported operators, check the individual [field](https://developer.atlassian.com/display/CONFDEV/Advanced+Searching+using+CQL#AdvancedSearchingusingCQL-field) reference.

###### Examples

-   Find blogposts created in the since the start of the fortnight

    ``` sql
    created >= startOfWeek("-1w") and type = blogpost
    ```

#### IN

The "`IN`" operator is used to search for content where the value of the specified field is one of multiple specified values. The values are specified as a comma-delimited list, surrounded by parentheses.

Using "`IN`" is equivalent to using multiple [EQUALS](#equals) (=) statements with the OR keyword, but is shorter and more convenient. That is, typing `creator IN (tom, jane, harry)` is the same as typing `creator = "tom" `[OR](https://developer.atlassian.com/display/CONFDEV/CQL+Keywords+Reference#CQLKeywordsReference-OR)` creator = "jane" `[OR](https://developer.atlassian.com/display/CONFDEV/CQL+Keywords+Reference#CQLKeywordsReference-OR) `creator = "harry"`.

###### Examples

-   Find all content that mentions either jsmith or jbrown or jjones:

    ``` sql
    mention in (jsmith,jbrown,jjones)
    ```

-   Find all content where the creator or contributor is either Jack or Jill:

    ``` sql
    creator in (Jack,Jill) or contributor in (Jack,Jill)
    ```

#### NOT IN

The "`NOT IN`" operator is used to search for content where the value of the specified field is not one of multiple specified values.

Using "`NOT IN`" is equivalent to using multiple

[NOT\_EQUALS](#not-equals) (`!=`) statements, but is shorter and more convenient. That is, typing `creator NOT IN (tom, jane, harry)` is the same as typing `creator != "tom"` [AND](https://developer.atlassian.com/display/CONFDEV/CQL+Keywords+Reference#CQLKeywordsReference-AND) `creator != "jane"` [AND](#and) `creator != "harry"`.

###### Examples

-   Find all content where the creator is someone other than Jack, Jill or John:

    ``` sql
    space = DEV and creator not in (Jack,Jill,John)
    ```

#### CONTAINS: ~

The "`~`" operator is used to search for content where the value of the specified field matches the specified value (either an exact match or a "fuzzy" match -- see examples below). The "~" operator can only be used with text fields, for example:

-   title
-   text

Note: when using the "`~`" operator, the value on the right-hand side of the operator can be specified using [Confluence text-search syntax](https://developer.atlassian.com/display/CONFDEV/Performing+text+searches+using+CQL).

###### Examples

-   Find all content where the title contains the word "win" (or simple derivatives of that word, such as "wins"):

    ``` sql
    title ~ win
    ```

-   Find all content where the title contains a [wild-card](/server/confluence/cql-operators-reference) match for the word "win":

    ``` sql
    title ~ "win*"
    ```

-   Find all content where the text contains the word "advanced" and the word "search":

    ``` sql
    text ~ "advanced search"
    ```

#### DOES NOT CONTAIN: !~

The "`!~`" operator is used to search for content where the value of the specified field is not a "fuzzy" match for the specified value. The "!~" operator can only be used with text fields, for example:

-   title
-   text

Note: when using the "`!~`" operator, the value on the right-hand side of the operator can be specified using [Confluence text-search syntax](https://developer.atlassian.com/display/CONFDEV/Performing+text+searches+using+CQL).

###### Examples

-   Find all content where the title does not contain the word "run" (or derivatives of that word, such as "running" or "ran"):

    ``` sql
    space = DEV and title !~ run
    ```

## Field reference

A field in CQL is a word that represents an indexed property of content in Confluence. In a clause, a field is followed by an [operator](https://developer.atlassian.com/display/CONFDEV/Advanced+Searching+using+CQL#AdvancedSearchingusingCQL-operator), which in turn is followed by one or more values (or [functions](https://developer.atlassian.com/display/CONFDEV/CQL+Function+Reference#CQLFunctionReference-function)). The operator compares the value of the field with one or more values or functions on the right, such that only true results are retrieved by the clause.

**List of Fields:**

-   [Ancestor](#ancestor)
-   [Container](#container)
-   [Content](#content)
-   [Created](#created)
-   [Creator](#creator)
-   [Contributor](#contributor)
-   [Favourite, favorite](#favourite-favorite)
-   [ID](#id)
-   [Label](#label)
-   [LastModified](#lastmodified)
-   [Macro](#macro)
-   [Mention](#mention)
-   [Parent](#parent)
-   [Space](#space)
-   [Text](#text)
-   [Title](#title)
-   [Type](#type)
-   [Watcher](#watcher)

#### Ancestor

Search for all pages that are descendants of a given ancestor page. This includes direct child pages and their descendents. It is more general than the [parent](#parent) field.

###### Syntax

``` sql
ancestor
```

###### Field Type

CONTENT

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
</tr>
</tbody>
</table>

###### Supported Functions

None

###### Examples

-   Find all descendent pages with a given anscestor page

    ``` sql
    ancestor = 123
    ```

-   Find descendants of a group of ancestor pages

    ``` sql
    ancestor in (123, 456, 789)
    ```

#### Container

Search for content that is contained in the content with the given ID

###### Syntax

``` sql
container
```

###### Field Type

CONTENT

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
</tr>
</tbody>
</table>

###### Supported Functions

None

###### Examples

-   Find attachments contained in a page with the given content id

    ``` sql
    container = 123 and type = attachment
    ```

-   Find content container in a set of pages with the given ids

    ``` sql
    container in (123, 223, 323)
    ```

#### Content

Search for content that have a given content ID. This is an alias of the [ID](https://developer.atlassian.com/display/CONFDEV/CQL+Field+Reference#ID) field.

###### Syntax

``` sql
content
```

###### Field Type

CONTENT

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
</tr>
</tbody>
</table>

###### Supported Functions

None  

###### Examples

-   Find content with a given content id

    ``` sql
    content = 123
    ```

-   Find content in a set of content ids

    ``` sql
    content in (123, 223, 323)
    ```

#### Created

Search for content that was created on, before or after a particular date (or date range).

Note: search results will be relative to your configured time zone (which is by default the Confluence server's time zone)

Use one of the following formats:

`"yyyy/MM/dd HH:mm"`  
`"yyyy-MM-dd HH:mm"`  
`"yyyy/MM/dd"`  
`"yyyy-MM-dd"`

###### Syntax

``` sql
created
```

###### Field Type

DATE

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
</tr>
</tbody>
</table>

###### Supported Functions

-   [endOfDay()](https://developer.atlassian.com/display/CONFDEV/CQL+Function+Reference#CQLFunctionReference-endOfDay)
-   [endOfMonth()](https://developer.atlassian.com/display/CONFDEV/CQL+Function+Reference#CQLFunctionReference-endOfMonth)
-   [endOfWeek()](https://developer.atlassian.com/display/CONFDEV/CQL+Function+Reference#CQLFunctionReference-endOfWeek)
-   [endOfYear()](https://developer.atlassian.com/display/CONFDEV/CQL+Function+Reference#CQLFunctionReference-endOfYear)
-   [startOfDay()](https://developer.atlassian.com/display/CONFDEV/CQL+Function+Reference#CQLFunctionReference-startOfDay)
-   [startOfMonth()](https://developer.atlassian.com/display/CONFDEV/CQL+Function+Reference#CQLFunctionReference-startOfMonth)
-   [startOfWeek()](https://developer.atlassian.com/display/CONFDEV/CQL+Function+Reference#CQLFunctionReference-startOfWeek)
-   [startOfYear()](https://developer.atlassian.com/display/CONFDEV/CQL+Function+Reference#CQLFunctionReference-startOfYear)

###### Examples

-   Find content created after the 1st September 2014

    ``` sql
    created > 2014/09/01
    ```

-   Find content created in the last 4 weeks

    ``` sql
    created >= now("-4w")
    ```

#### Creator

Search for content that was created by a particular user. You can search by the user's username.

###### Syntax

``` javascript
creator
```

###### Field Type

USER

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
</tr>
</tbody>
</table>

###### Supported Functions

-   [currentUser()](https://developer.atlassian.com/display/CONFDEV/CQL+Function+Reference#CQLFunctionReference-currentUser())

###### Examples

-   Find content created by jsmith

    ``` sql
    created = jsmith
    ```

-   Find content created by john smith or bob nguyen

    ``` sql
    created in (jsmith, bnguyen)
    ```

#### Contributor

Search for content that was created or edited by a particular user. You can search by the user's username.

###### Syntax

``` sql
contributor
```

###### Field Type

USER

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
</tr>
</tbody>
</table>

###### Supported Functions

-   [currentUser()](https://developer.atlassian.com/display/CONFDEV/CQL+Function+Reference#CQLFunctionReference-currentUser())

###### Examples

-   Find content created by jsmith

    ``` sql
    contributor = jsmith
    ```

-   Find content created by john smith or bob nguyen

    ``` sql
    contributor in (jsmith, bnguyen)
    ```

#### Favourite, favorite

Search for content that was favourited by a particular user. You can search by the user's username.

Due to security restrictions you are only allowed to filter on the logged in user's favourites. This field is available in both British and American spellings.

###### Syntax

``` sql
favourite
```

###### Field Type

USER

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
</tr>
</tbody>
</table>

###### Supported Functions

-   [currentUser()](https://developer.atlassian.com/display/CONFDEV/CQL+Function+Reference#CQLFunctionReference-currentUser())

###### Examples

-   Find content that is favourited by the current user

    ``` sql
    favourite = currentUser()
    ```

-   Find content favourited by jsmith, where jsmith is also the logged in user

    ``` sql
    favourite = jsmith
    ```

#### ID

Search for content that have a given content ID.

###### Syntax

``` sql
id
```

###### Field Type

CONTENT

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
</tr>
</tbody>
</table>

###### Supported Functions

None

###### Examples

-   Find content with the id 123

    ``` sql
    id = 123
    ```

-   Find content in a set of content ids

    ``` sql
    id in (123, 223, 323)
    ```

#### Label

Search for content that has a particular label

###### Syntax

``` sql
label
```

###### Field Type

STRING

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
</tr>
</tbody>
</table>

###### Supported Functions

None

###### Examples

-   Find content that has the label finished

    ``` sql
    label = finished
    ```

-   Find content that doesn't have the label draft or review

    ``` sql
    label not in (draft, review)
    ```

#### LastModified

Search for content that was last modified on, before, or after a particular date (or date range).

The search results will be relative to your configured time zone (which is by default the Confluence server's time zone)

Use one of the following formats:

`"yyyy/MM/dd HH:mm"`  
`"yyyy-MM-dd HH:mm"`  
`"yyyy/MM/dd"`  
`"yyyy-MM-dd"`

###### Syntax

``` sql
lastmodified
```

###### Field Type

DATE

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
</tr>
</tbody>
</table>

###### Supported Functions

-   [endOfDay()](https://developer.atlassian.com/display/CONFDEV/CQL+Function+Reference#CQLFunctionReference-endOfDay)
-   [endOfMonth()](https://developer.atlassian.com/display/CONFDEV/CQL+Function+Reference#CQLFunctionReference-endOfMonth)
-   [endOfWeek()](https://developer.atlassian.com/display/CONFDEV/CQL+Function+Reference#CQLFunctionReference-endOfWeek)
-   [endOfYear()](https://developer.atlassian.com/display/CONFDEV/CQL+Function+Reference#CQLFunctionReference-endOfYear)
-   [startOfDay()](https://developer.atlassian.com/display/CONFDEV/CQL+Function+Reference#CQLFunctionReference-startOfDay)
-   [startOfMonth()](https://developer.atlassian.com/display/CONFDEV/CQL+Function+Reference#CQLFunctionReference-startOfMonth)
-   [startOfWeek()](https://developer.atlassian.com/display/CONFDEV/CQL+Function+Reference#CQLFunctionReference-startOfWeek)
-   [startOfYear()](https://developer.atlassian.com/display/CONFDEV/CQL+Function+Reference#CQLFunctionReference-startOfYear)

###### Examples

-   Find content that was last modified on 1st September 2014

    ``` sql
    lastmodified = 2014-09-01
    ```

-   Find content that was last modified before the start of the year

    ``` sql
    lastmodified < startOfYear()
    ```

-   Find content that was last modified on or after 1st September but before 9am on 3rd September 2014

    ``` sql
    lastmodified >= 2014-09-01 and lastmodified < "2014-09-03 09:00"
    ```

#### Macro

Search for content that has an instance of the macro with the given name in the body of the content

###### Syntax

``` sql
macro
```

###### Field Type

STRING

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
</tr>
</tbody>
</table>

###### Supported Functions

none

###### Examples

-   Find content that has the JIRA issue macro

    ``` sql
    macro = jira
    ```

-   Find content that has Table of content macro or the widget macro

    ``` sql
    macro in (toc, widget)
    ```

#### Mention

Search for content that mentions a particular user. You can search by the user's username.

###### Syntax

``` sql
mention
```

###### Field Type

USER

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
</tr>
</tbody>
</table>

###### Supported Functions

-   [currentUser()](https://developer.atlassian.com/display/CONFDEV/CQL+Function+Reference#CQLFunctionReference-currentUser())

###### Examples

-   Find content that mentions jsmith or kjones

    ``` sql
    mention in (jsmith, kjones)
    ```

-   Find content that mentions jsmith

    ``` sql
    mention = jsmith
    ```

#### Parent

Search for child content of a particular parent page

###### Syntax

``` sql
parent
```

###### Field Type

CONTENT

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
</tr>
</tbody>
</table>

###### Supported Functions

###### Examples

-   Find child pages of a parent page with ID 123

    ``` sql
    parent = 123
    ```

#### Space

Search for content that is in a particular Space. You can search by the space's key.

###### Syntax

``` sql
space
```

###### Field Type

SPACE

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
</tr>
</tbody>
</table>

###### Supported Functions

none

###### Examples

-   Find content in the development space or the QA space

    ``` sql
    space in (DEV, QA)
    ```

-   Find content in the development space

    ``` sql
    space = DEV
    ```

#### Text

This is a "master-field" that allows you to search for text across a number of other text fields. These are the same fields used by Confluence's search user interface.

-   <a href="http://developer.atlassian.com#Title" class="external-link">Title</a>
-   Content body
-   <a href="http://developer.atlassian.com#Labels" class="external-link">Labels</a>

Note: [Confluence text-search syntax](https://developer.atlassian.com/display/CONFDEV/Performing+text+searches+using+CQL) can be used with this field.

###### Syntax

``` sql
text
```

###### Field Type

TEXT

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
</tr>
</tbody>
</table>

###### Supported Functions

none

###### Examples

-   Find content that contains the word Confluence

    ``` sql
    text ~ Confluence
    ```

-   Find content in the development space

    ``` sql
    space = DEV
    ```

#### Title

Search for content by title, or with a title that contains particular text.

Note: [Confluence text-search syntax](https://developer.atlassian.com/display/CONFDEV/Performing+text+searches+using+CQL) can be used with this fields when used with the [CONTAINS](https://developer.atlassian.com/display/CONFDEV/Advanced+Searching+using+CQL#AdvancedSearchingusingCQL-CONTAINS:~) operator ("~", "!~")

###### Syntax

``` sql
title
```

###### Field Type

TEXT

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
</tr>
</tbody>
</table>

###### Supported Functions

none

###### Examples

-   Find content with the title "Advanced Searching using CQL"

    ``` sql
    title = "Advanced Searching using CQL"
    ```

-   Find content that matches Searching CQL (i.e. a "fuzzy" match):

    ``` sql
    title ~ "Searching CQL"
    ```

#### Type

Search for content of a particular type. Supported content types are:

-   page
-   blogpost
-   comment
-   attachment

###### Syntax

``` sql
type
```

###### Field Type

TYPE

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
</tr>
</tbody>
</table>

###### Supported Functions

none

###### Examples

-   Find blogposts or pages

    ``` sql
    type IN (blogpost, page)
    ```

-   Find attachments

    ``` sql
    type = attachment
    ```

#### Watcher

Search for content that a particular user is watching. You can search by the user's username.

###### Syntax

``` sql
watcher
```

###### Field Type

USER

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
</tr>
</tbody>
</table>

###### Supported Functions

-   [currentUser()](https://developer.atlassian.com/display/CONFDEV/CQL+Function+Reference#CQLFunctionReference-currentUser())

###### Examples

-   Search for content that you are watching:

    ``` sql
    watcher = currentUser()
    ```

-   Search for content that the user "jsmith" is watching:

    ``` sql
    watcher = "jsmith"
    ```
    